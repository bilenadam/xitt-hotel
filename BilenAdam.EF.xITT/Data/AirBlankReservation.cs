//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BilenAdam.EF.xITT.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class AirBlankReservation
    {
        public int AirReservationId { get; set; }
        public string AGNR { get; set; }
        public string PASSWORT { get; set; }
        public string GESAMTPREIS { get; set; }
    
        public virtual AirReservation AirReservation { get; set; }
    }
}
