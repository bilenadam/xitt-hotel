//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BilenAdam.EF.xITT.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class BankPayment
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BankPayment()
        {
            this.FormOfBankPayments = new HashSet<FormOfBankPayment>();
        }
    
        public int Id { get; set; }
        public int BankAccountId { get; set; }
        public Nullable<int> ContactId { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public System.DateTime CreatedTimeUTC { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string ResonOfPayment { get; set; }
        public int DocumentNo { get; set; }
    
        public virtual Contact Contact { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormOfBankPayment> FormOfBankPayments { get; set; }
    }
}
