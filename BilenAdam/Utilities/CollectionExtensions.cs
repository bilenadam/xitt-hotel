﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class CollectionExtensions
{
    public static bool HasAny<T>(this IEnumerable<T> coll)
    {
        return coll != null && coll.Any();
    }

    public static bool HasAny<T>(this IEnumerable<T> coll, Func<T, bool> predicate)
    {
        return coll != null  && coll.Any(predicate);
    }


    public static bool HasAny<T>(this T[] coll)
    {
        return coll != null && coll.Length > 0;
    }
    public static bool HasAny<T>(this T[] coll, Func<T, bool> predicate)
    {
        return coll.HasAny()&&coll.Any(predicate);
    }

    public static T[] WhereToArray<T>(this T[] coll, Func<T, bool> predicate )
    {
        if (!coll.HasAny(predicate))
            return null;
        else
            return coll.Where(predicate).ToArray();

    }

   


}

