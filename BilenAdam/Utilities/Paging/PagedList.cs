﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//TODO başka yere taşınabilir
namespace BilenAdam.Utilities.Paging
{
    public class PagedList<T> : List<T>, IEnumerable<T>
    {
        public PagedList()
        {

        }
        public PagedList(IQueryable<T> source, int selectedPageNo, int pageSize = 10, int pageLinkCount = 9)
        {
            //if (source.Any())
            //{ 

            this.TotalCount = source.Count();
            this.PageSize = pageSize;
            this.SelectedPageNo = selectedPageNo;
            this.PageCount = TotalCount > 0 ? (int)Math.Ceiling(TotalCount / (double)PageSize) : 0;
            this.PageLinkCount = pageLinkCount;
            this.AddRange(source.Skip((selectedPageNo - 1) * pageSize).Take(pageSize).ToList());
            //}
        }

      

        public int TotalCount { get; set; }

        public int PageLinkCount { get; set; }

        public int PageCount { get; set; }

        public int SelectedPageNo { get; set; }

        public int PageSize { get; set; }

        public bool HasPreviousPage
        {
            get
            {
                return (SelectedPageNo > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (SelectedPageNo < PageCount);
            }
        }

        public bool IsFirstPage
        {
            get
            {
                return (SelectedPageNo == 1);
            }
        }

        public bool IsLastPage
        {
            get
            {
                return (SelectedPageNo >= PageCount);
            }
        }

        public int StartPage
        {
            get
            {
                int start = 1;

                if (PageCount <= PageLinkCount)
                    return start;

                if (SelectedPageNo > (PageCount - (int)Math.Floor(PageLinkCount / (double)2)))
                    start = (PageCount - PageLinkCount);
                else
                    start = (SelectedPageNo <= (int)Math.Floor(PageLinkCount / (double)2)) ? 1 : SelectedPageNo - (int)Math.Floor(PageLinkCount / (double)2);

                return start;
                //return (PageNo - 1) * PageSize + 1;
            }
        }

        public int EndPage
        {
            get
            {
                if (PageCount <= PageLinkCount)
                    return PageCount;

                //var numberOfLastItemOnPage = FirstItemNo + PageSize - 1;

                //return (numberOfLastItemOnPage > TotalCount) ? TotalCount : numberOfLastItemOnPage;
                return StartPage + PageLinkCount;
            }
        }
    }
}