﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Utilities.Paging
{
    public static class PagedListExtensions
    {   
        //TODO page sizelar configden alınabilir
        public static PagedList<T> ToPagedList<T>(this IQueryable<T> items,int PageNo = 1 , int PageSize = 10 ) //where T : BilenAdam.Persistance.ClassInterface
        {
            return new PagedList<T>(items, PageNo, PageSize);
        }

      
       

        public static PagedList<T> ToPagedList<T, S>(this IQueryable<S> source, int selectedPageNo, Func<S, T> injector, int pageSize = 10, int pageLinkCount = 9)
        {
            PagedList<T> list = new PagedList<T>();

            list.TotalCount = source.Count();
            list.PageSize = pageSize;
            list.SelectedPageNo = selectedPageNo;
            list.PageCount = list.TotalCount > 0 ? (int)Math.Ceiling(list.TotalCount / (double)pageSize) : 0;
            list.PageLinkCount = pageLinkCount;
            var sourceData = source.Skip((selectedPageNo - 1) * pageSize).Take(pageSize).ToArray();
            var pageData = sourceData.Select(s => injector(s));
            list.AddRange(pageData);
            return list;

        }

       

        

    }
}
