﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace BilenAdam.Utilities.Security
{
    public class Crypto
    {

        /// 

        /// Default Constructor
        /// 

        public Crypto()
        {

        }

        /// 

        /// Generates a cryptographic Hash Key for the provided text data.
        /// Basically a digital fingerprint
        /// 

        /// text to hash
        /// Unique hash representing string
        public static String GetMD5Hash(String dataToHash)
        {
            String hexResult = "";
            string[] tabStringHex = new string[16];
            byte[] result = GetMD5HashBytes(dataToHash);
            for (int i = 0; i < result.Length; i++)
            {
                tabStringHex[i] = (result[i]).ToString("x");
                hexResult += tabStringHex[i];
            }
            return hexResult;
        }

        public static byte[] GetMD5HashBytes(String dataToHash)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = Encoding.ASCII.GetBytes(dataToHash);
            return md5.ComputeHash(data);
          
        }

        /// 

        /// Encrypts text with Triple DES encryption using the supplied key
        /// 

        /// The text to encrypt
        /// Key to use for encryption
        /// The encrypted string represented as base 64 text
        public static string EncryptTripleDES(string plaintext, string key)
        {
            TripleDESCryptoServiceProvider DES =
                         new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
            DES.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            DES.Mode = CipherMode.ECB;
            ICryptoTransform DESEncrypt = DES.CreateEncryptor();
            byte[] Buffer = ASCIIEncoding.ASCII.GetBytes(plaintext);
            return Convert.ToBase64String(
                     DESEncrypt.TransformFinalBlock(Buffer, 0, Buffer.Length)).TrimEnd('=').Replace('+', '-').Replace('/', '_');
        }

        /// 

        /// Decrypts supplied Triple DES encrypted text using the supplied key
        /// 

        /// Triple DES encrypted base64 text
        /// Decryption Key
        /// The decrypted string
        public static string DecryptTripleDES(string base64Text, string key)
        {

            string safe = base64Text.Replace('_', '/').Replace('-', '+');

            safe = safe.PadRight(safe.Length + (4 - safe.Length % 4) % 4, '=');


            TripleDESCryptoServiceProvider DES =
                                    new TripleDESCryptoServiceProvider();
            MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
            DES.Key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key));
            DES.Mode = CipherMode.ECB;
            ICryptoTransform DESDecrypt = DES.CreateDecryptor();
            byte[] Buffer = Convert.FromBase64String(safe);
            return ASCIIEncoding.ASCII.GetString(
                    DESDecrypt.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }

    } // class
}
