﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class StringCultureExtensions
{
    public static string ClearGermanAccent(this string text)
    {
        return text.Replace("Ä", "AE").Replace("Ü", "UE").Replace("Ö", "OE").Replace("ß", "SS").Replace("ä", "ae").Replace("ü", "ue").Replace("ö", "oe");
    }
    public static string ClearAccent(this string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            return text;

        return new BilenAdam.Utilities.Text.Accent.ASCIIFolding().Fold(text.ClearGermanAccent());
    }
    public static string ToUpper(this string text, string culture)
    {
        return getTextInfo(culture).ToUpper(text);
    }
    public static string ToLower(this string text, string culture)
    {
        return getTextInfo(culture).ToLower(text);
    }
    public static string ToTileCase(this string text, string culture)
    {
        return getTextInfo(culture).ToTitleCase(text);
    }

    static HashSet<string> cultures = new HashSet<string>(CultureInfo.GetCultures(CultureTypes.AllCultures).Select(c => c.Name), StringComparer.InvariantCultureIgnoreCase);
    public static bool IsValidCultureInfoName(string name)
    {
        return !string.IsNullOrWhiteSpace(name) && cultures.Contains(name);
    }

    private static System.Globalization.TextInfo getTextInfo(string culture)
    {
        System.Globalization.TextInfo cultText = null;

        if (IsValidCultureInfoName(culture))
            cultText = new System.Globalization.CultureInfo(culture).TextInfo;
        else
            cultText = System.Globalization.CultureInfo.InvariantCulture.TextInfo;

        return cultText;
    }







}



