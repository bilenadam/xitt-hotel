﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

public static class StringExtensions
    {

    public static string DefaultIfNullOrWhiteSpace(this string str,string defaultValue)
    {
        if (string.IsNullOrWhiteSpace(str))
            return defaultValue;
        else
            return str;
    }
    public static string ToUrlSafe(this string text)
    {
        if (string.IsNullOrWhiteSpace(text))
            return String.Empty;
        else
            return Regex.Replace(Regex.Replace(text.ClearAccent(),@"\W", "_"),"_+","_").Trim('_');
    }
        public static string ReplaceFirstOccurrence(this string Source, string Find, string Replace)
        {
            int Place = Source.IndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static string ReplaceLastOccurrence(this string Source, string Find, string Replace)
        {
            int Place = Source.LastIndexOf(Find);
            string result = Source.Remove(Place, Find.Length).Insert(Place, Replace);
            return result;
        }

        public static string[] SplitToLines(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return null;

            return Regex.Split(text, "\r\n|\r|\n");
        }

        public static string[] SplitWithWhitespaces(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return null;

            return Regex.Split(text.Trim(), @"\s+");
        }

        public static string RemoveWhitespaces(this string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return null;

            return Regex.Replace(text, @"\s+","");
        }

    public static string GetFirstLine(this string text)
    {
        var lines = text.SplitToLines();
        if (lines == null)
            return null;

        return lines.FirstOrDefault(s => !string.IsNullOrWhiteSpace(s));
    }
    public static string GetLastLine(this string text)
        {
            var lines = text.SplitToLines();
            if (lines == null)
                return null;

            return lines.LastOrDefault(s=>!string.IsNullOrWhiteSpace(s));
        }

        public static string AggregateBy(this IEnumerable<string> collection,string seperator)
        {
            return collection.Aggregate((s1, s2) => s1 + seperator + s2);

        }

        public static string AggregateByComma(this IEnumerable<string> collection)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in collection)
            {
                sb.Append(" ");
                sb.Append(s);
                sb.Append(",");
            }

            return sb.ToString().TrimEnd(',').Trim();

        }

        public static string AggregateByLines(this IEnumerable<string> collection)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in collection)
            {
                sb.AppendLine(s);

            }

            return sb.ToString().Trim();

        }
        public static string AggregateByWhiteSpace(this IEnumerable<string> collection)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in collection)
            {
                sb.Append(" ");
                sb.Append(s);

            }

            return sb.ToString().Trim();

        }

    public static string ToTitleCase(string text)
    {
        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
        TextInfo textInfo = cultureInfo.TextInfo;
        return textInfo.ToTitleCase(text);
    }


    static string uppercaseSplitMatchPatter = @"(\P{Lu}+)|(\p{Lu}+\P{Lu}*)";
    public static string[] SplitAtUpperCase(string text)
    {
        MatchCollection mc = Regex.Matches(text, uppercaseSplitMatchPatter);

        string[] parts = new string[mc.Count];

        for (int i = 0; i < mc.Count; i++)
        {
            parts[i] = mc[i].ToString();
        }
        return parts;
    }

    public static string AddWhitespaceAtUpperCase(string text)
    {
        MatchCollection mc = Regex.Matches(text, uppercaseSplitMatchPatter);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < mc.Count; i++)
        {
            sb.Append(mc[i].ToString());
            if (i != mc.Count - 1) sb.Append(" ");

        }
        return sb.ToString();
    }

}

