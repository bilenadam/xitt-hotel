﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam
{
    public static class TryParseUtil
    {
        public static int? TryParseInt(string s)
        {
            int i = 0;
            if (int.TryParse(s, out i))
                return i;
            else
                return null;
        }

        public static short? TryParseShort(string s)
        {
            short i = 0;
            if (short.TryParse(s, out i))
                return i;
            else
                return null;
        }

        public static byte? TryParseByte(string s)
        {
            byte i = 0;
            if (byte.TryParse(s, out i))
                return i;
            else
                return null;
        }

        public static decimal? TryParseDecimal(string s, IFormatProvider fp)
        {
            decimal val;
            if (decimal.TryParse(s, System.Globalization.NumberStyles.AllowDecimalPoint,fp, out  val))
                return val;
            else
                return null;
        }
    }
}
