﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Utilities.Extensions
{
    public class DayNameSelectionList
    {
        
       
        public static List<System.DayOfWeek> Parse(string arrayText)
        {
            if (arrayText != null)
            {
                List<System.DayOfWeek> list = new List<DayOfWeek>();

                for (int i = 0; i < 7; i++)
                {
                    if (arrayText.Contains(i.ToString()))
                    {
                        list.Add((System.DayOfWeek)i);
                    }
                   
                }
                return list;
            }
            else
            {
                return null;

            }
        }
        /// <summary>
        /// checks if the day selected in array
        /// </summary>
        /// <param name="arrayText">--23-5-</param>
        /// <param name="day">Monday</param>
        /// <returns></returns>
        public static bool IsDaySelected(string arrayText, System.DayOfWeek day)
        {
            var d = (int)day;
            return arrayText.Contains(d.ToString()); 
        }
        /// <summary>
        /// Creates text representation for selectedDays
        /// </summary>
        /// <example>
        /// Monday,Tuesday,Thursday = -12-4--
        /// </example>
        /// <param name="selectedDays"></param>
        /// <returns></returns>
        public static string CreateDayArray(IEnumerable<System.DayOfWeek> selectedDays, char replaceChar = '-')
        {
            var all = Enum.GetValues(typeof(System.DayOfWeek)).Cast<System.DayOfWeek>();
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < 7; i++)
            {
                if (selectedDays.Contains((System.DayOfWeek)i))
                {
                    sb.Append(i.ToString());
                }
                else
                {
                    sb.Append(replaceChar);
                }
            }

            return sb.ToString();
        }
        /// <summary>
        /// Creates text representation for selectedDays
        /// </summary>
        /// <example>
        /// Monday,Tuesday,Thursday = -12-4--
        /// </example>
        /// <param name="selectedDays"></param>
        /// <returns></returns>
        public static string CreateDayArray(params System.DayOfWeek[] selectedDays)
        {
            return CreateDayArray(selectedDays);
        }
 
    }
}
