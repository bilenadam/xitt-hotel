﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class DateTimeExtensions
{
    public static DateTime StartOfWeek(this DateTime dt)
    {
        return dt.StartOfWeek(System.Globalization.CultureInfo.CurrentCulture);
    }
    public static DateTime StartOfWeek(this DateTime dt, System.Globalization.CultureInfo culture)
    {
        return dt.StartOfWeek(culture.DateTimeFormat);
    }

    public static DateTime StartOfWeek(this DateTime dt, System.Globalization.DateTimeFormatInfo dateFormats)
    {
        return dt.StartOfWeek(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);

    }
    public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
    {
        int diff = dt.DayOfWeek - startOfWeek;
        if (diff < 0)
        {
            diff += 7;
        }

        return dt.AddDays(-1 * diff).Date;
    }
    public static DateTime EndOfWeek(this DateTime dt)
    {
        return dt.EndOfWeek(System.Globalization.CultureInfo.CurrentCulture);
    }
    public static DateTime EndOfWeek(this DateTime dt, System.Globalization.CultureInfo culture)
    {
        return dt.EndOfWeek(culture.DateTimeFormat);
    }

    public static DateTime EndOfWeek(this DateTime dt, System.Globalization.DateTimeFormatInfo dateFormats)
    {
        return dt.EndOfWeek(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek);

    }
    public static DateTime EndOfWeek(this DateTime dt, DayOfWeek startOfWeek)
    {
        return dt.StartOfWeek(startOfWeek).AddDays(6);
    }

    public static DateTime StartOfMonth(this DateTime dt)
    {
        return new DateTime(dt.Year, dt.Month, 1);
    }
   
    public static DateTime EndOfMonth(this DateTime dt)
    {
        return dt.StartOfMonth().AddMonths(1).AddDays(-1);
    }

    public static DateTime StartOfYear(this DateTime dt)
    {
        return new DateTime(dt.Year, 1, 1);
    }
    public static DateTime EndofYear(this DateTime dt)
    {
        return dt.StartOfYear().AddYears(1).AddDays(-1);
    }
    public static DateTime EndOfDay(this DateTime dt)
    {
        return dt.Date.AddDays(1).AddMilliseconds(-1);
    }

    public static string GetDayName(this DateTime dt, System.Globalization.CultureInfo culture)
    {
        return culture.DateTimeFormat.GetDayName(dt.DayOfWeek);
    }

    public static string GetDayName(this DateTime dt)
    {
        return dt.GetDayName(System.Globalization.CultureInfo.CurrentCulture);
    }

    public static string ToShortTimeString(this TimeSpan ts)
    {
        return ts.ToString("hhmm");
    }

    public static string ToShortDateString(this DateTime? dt)
    {
        if (dt.HasValue)
            return dt.Value.ToShortDateString();
        else
            return string.Empty;
    }

}

