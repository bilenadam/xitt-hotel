﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam
{
    public static class CurrencyCodeParser
    {
        public static CurrencyCode Parse(string code)
        {
            try
            {
                return (CurrencyCode)Enum.Parse(typeof(CurrencyCode), code.ToUpperInvariant().Trim());
            }
            catch
            {
                throw new FormatException("Invalit currency code");
            }
        }
    }
}
