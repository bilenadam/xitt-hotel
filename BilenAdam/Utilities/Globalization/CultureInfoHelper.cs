﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


    public class CultureInfoHelper
    {
        public static CultureInfo DE = CultureInfo.GetCultureInfo("de");
        public static CultureInfo TR = CultureInfo.GetCultureInfo("tr");
        public static CultureInfo EN = CultureInfo.GetCultureInfo("en");

    public static CultureInfo CurrentCultureInfo => System.Globalization.CultureInfo.CurrentCulture;
    public static CultureInfo InvariantCultureInfo => System.Globalization.CultureInfo.InvariantCulture;
       
    }

