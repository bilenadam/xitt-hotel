﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Utilities.IoC
{
    public interface IContainer
    {
       

        void Register(Type serviceType, Func<IContainer, object> serviceFactory);
        object GetService(Type serviceType);
        IEnumerable<object> GetServices(Type serviceType);
    }

    
}
