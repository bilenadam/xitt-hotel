﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Utilities.IoC
{
    public static class ContainerExtensions
    {
        public static void Register<T>(this IContainer container, Func<IContainer, T> serviceFactory)
        {
            container.Register(typeof(T), i=>serviceFactory(i));
        }

        public static IEnumerable<T> GetServices<T>(this IContainer container) where T : class
        {
            var services = container.GetServices(typeof(T));
            if(services == null)
                return null;
            return services.OfType<T>();
        }
        public static T GetService<T>(this IContainer container) where T : class
        {
            return container.GetService(typeof(T)) as T;
        }
    }
}
