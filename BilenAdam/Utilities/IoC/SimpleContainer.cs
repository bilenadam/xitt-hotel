﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Utilities.IoC
{
    public class SimpleContainer : IContainer
    {

      
        Dictionary<Type, List<Func<IContainer, object>>> factories = new Dictionary<Type, List<Func<IContainer, object>>>(100);

        public object GetService(Type serviceType)
        {
            if (!factories.ContainsKey(serviceType))
            {
                return null;
            }
            return (factories[serviceType].First()(this));
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (!factories.ContainsKey(serviceType))
            {
                return  null;
            }

            return (factories[serviceType].Select(i => i(this))).ToArray();
        }

        public void Register(Type serviceType, Func<IContainer, object> serviceFactory)
        {
            if (!factories.ContainsKey(serviceType))
            {
                List<Func<IContainer, object>> fact = new List<Func<IContainer, object>>(5);

                factories.Add(serviceType, fact);
            }
            factories[serviceType].Add(l => serviceFactory(l));
        }
    }
}
