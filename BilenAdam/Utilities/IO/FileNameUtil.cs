﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BilenAdam.Utilities.IO
{
    public class FileNameUtil
    {
        StringBuilder fileNameBuilder = new StringBuilder();
        public string ExtWithoutDot { get; }

        public string ExtWithDot => "."+ ExtWithoutDot;
        public FileNameUtil(string ext)
        {
            ExtWithoutDot = ext.TrimStart('.');
        }

        public bool HasName => fileNameBuilder.Length > 0;

        public byte Version { get; set; } = 0;

        public override string ToString() => fileNameBuilder.ToString() + (Version > 0 ? "_" +Version.ToString()  : "") + ExtWithDot;

        public FileNameUtil AppendSeperator(char seperator)
        {
            if (fileNameBuilder.Length > 0 && !char.IsWhiteSpace(seperator))
                fileNameBuilder.Append(seperator);

            return this;
        }

        public FileNameUtil Append(string part, char seperator = '_')
        {
            AppendSeperator(seperator);
            fileNameBuilder.Append(part);
            return this;
        }

   


        public FileNameUtil AppendUnsafe(string name, char seperator = '_') => Append(PathUtils.SafeFileName(name),seperator);
        



        public FileNameUtil AppendDateTime(bool utc = false, string format = "yyMMddHHmmss", char seperator = '_') => AppendUnsafe(utc ? DateTime.UtcNow.ToString(format) : DateTime.Now.ToString(format),seperator);
        public FileNameUtil AppendDate(bool utc = false, string format = "yyMMdd", char seperator = '_') => AppendDateTime(utc, format, seperator);

        public FileNameUtil AppendTime(bool utc = false, string format = "HHmmss", char seperator = '_') => AppendDateTime(utc, format, seperator);



    }
}
