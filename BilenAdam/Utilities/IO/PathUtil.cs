﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BilenAdam.Utilities.IO
{
    public class PathUtil
    {
        StringBuilder directory = new StringBuilder();
    
        public PathUtil(string root)
        {
           
            directory.Append(root);
            if (root.Last() != Path.DirectorySeparatorChar)
                directory.Append(Path.DirectorySeparatorChar);

            
        }

        public PathUtil AddFolder(string folder)
        {
            string safeFolder = PathUtils.SafeFolderName(folder);
            directory.Append(safeFolder);
            if (safeFolder.Last() != Path.DirectorySeparatorChar)
                directory.Append(Path.DirectorySeparatorChar);

            return this;
        }

        public PathUtil AddDateTimeFolder(DateTime dateTime,string format = "yyMMddHHmmss") => AddFolder(dateTime.ToString(format));
        public PathUtil AddTodayFolder(bool utc = false, string format = "yyMMdd") => AddDateTimeFolder(utc ? DateTime.UtcNow:DateTime.Now, format);

        public PathUtil AddNowFolder(bool utc = false, string format = "HHmmss") => AddDateTimeFolder(utc ? DateTime.UtcNow : DateTime.Now, format);


        

        public override string ToString()
        {
                return directory.ToString();
        }

    }
}
