﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace BilenAdam.Utilities.IO
{
    public static class PathUtils
    {
        private static string replaceText(string text, char[] invalid, char replace)
        {
            char[] result = text.ToCharArray();
            for (int i = 0; i < result.Length; i++)
            {
                foreach (var c in invalid)
                {
                    if (result[i] == c)
                    {
                        result[i] = replace;
                        break;
                    }
                }
            }

            return new String(result);
        }


        public static string SafeFileName(string fileName, char replace = '_') => replaceText(fileName, System.IO.Path.GetInvalidFileNameChars(), replace);
        public static string SafeFolderName(string folderName, char replace = '_') => replaceText(folderName, Path.GetInvalidPathChars(), replace);


        
        public static string GenerateRandomTextFileName() => GenerateRandomFileName("txt");

        public static string GenerateRandomFileName(string ext)
        {
            return GenerateRandomFolderName() + "." + ext.TrimStart('.');
        }

        public static string GenerateRandomFolderName()
        {
            var name = Path.GetRandomFileName();
            if (Path.HasExtension(name))
                return Path.GetFileNameWithoutExtension(name);
            else
                return name;
        }

        
       


    }
}
