﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Utilities
{
    public static class AgeCalculator
    {
        public static int CalculateAge(DateTime dob, DateTime date)
        {   
            int age = date.Year - dob.Year;
            if (dob > date.AddYears(-age)) age--;

            return age;
        }

        public static DateTime GetBirthDateMin(DateTime date, int age)
        {
            return date.AddYears(-1 * (age +1)).AddDays(1);
        }
    }
}
