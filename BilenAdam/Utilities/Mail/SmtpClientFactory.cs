﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Utilities.Mail
{
    public class SmtpClientFactory
    {
        public static readonly string XITTInfoAddress = "no-reply@xitt.de";
        public static readonly string XITTDispoAddress = "no-reply@xitt.de";

        public static SmtpClient CreateInfoXitt()
        {
         
            return Create("smtp.ionos.de", XITTInfoAddress, "!TT!2020!No", port: 587, enableSSL: true);
        }
        public static SmtpClient CreateDispoXitt()
        {

            return Create("smtp.ionos.de", XITTDispoAddress, "!TT!2020!No", port: 587, enableSSL: true);
        }
        public static SmtpClient Create(string smtpServer, string username, string password, int port = 25, bool enableSSL = false)
        {
            SmtpClient client = new SmtpClient();
            client.Port = port;
            client.EnableSsl = enableSSL;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(username, password);
            client.Host = smtpServer;

            return client;
        }

     
    }
}
