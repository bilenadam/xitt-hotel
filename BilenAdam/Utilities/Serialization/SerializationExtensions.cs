﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;
using System.Xml;

namespace BilenAdam.Utilities.Serialization
{
    public static class SerializationExtensions
    {



        public static bool CanDeserializeFromXml<T>(this XElement xml)
        {
            if (xml == null)
                return false;
            try
            {
                if (xml.DeserializeFromXml<T>().Equals(default(T)))
                    return false;
                else
                    return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool CanDeserializeFromString<T>(this string xml)
        {
            return XElement.Parse(xml).CanDeserializeFromXml<T>();
        }

        public static T DeserializeFromXml<T>(this XElement xml)
        {
            XDocument doc = new XDocument();
            
            doc.Add(new XElement(xml));


            using (var reader = doc.CreateReader())
            {
                return (T)(new XmlSerializer(typeof(T)).Deserialize(reader));
            }
        }

        public static T DeserializeFromString<T>(this string xml)
        {
            return XElement.Parse(xml).DeserializeFromXml<T>();
        }

        public static XElement SerializeToXml<T>(this T obj)
        {
            if (obj == null)
                return null;
            XDocument doc = new XDocument();
            using (var writer = doc.CreateWriter())
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, string.Empty);
                new XmlSerializer(typeof(T), "").Serialize(writer, obj, namespaces);
            }


            return new XElement(doc.Root);
        }

        public static string SerializeToString<T>(this T obj)
        {
            return obj.SerializeToXml().ToString(SaveOptions.DisableFormatting);
        }


        public static void SetElementObj<T>(this XElement e, T o, string elementName )
        {

            var ename = elementName;
            var newEl = o.SerializeToXml();
            newEl.Name = ename;

            if (e.Element(ename) != null)
            {
                e.Element(ename).ReplaceWith(newEl);
            }
            else
            {
                e.Add(newEl);
            }
        }

        public static T GetElementObj<T>(this XElement e, string elementName )
        {
            if (e == null)
                return default(T);
            try
            {
                var elm = new XElement(e.Element(elementName));
                elm.Name = typeof(T).Name;
                return elm.DeserializeFromXml<T>();
            }
            catch
            {
                return default(T);
            }
        }
    }
}
