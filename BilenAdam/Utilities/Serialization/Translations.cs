﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BilenAdam.Utilities.Serialization
{
    [XmlRoot("trans")]
    public class Translations : System.Collections.Specialized.NameValueCollection, IXmlSerializable
    {
        public Translations()
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
        }

        public Translations(IEnumerable<KeyValuePair<string, string>> items)
            : this()
        {
            foreach (var item in items)
            {
                Add(item.Key, item.Value);
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {

                var key = reader.GetAttribute("l");
                reader.ReadStartElement("t");


                var value = reader.ReadString();

                if (!string.IsNullOrWhiteSpace(key) && !string.IsNullOrWhiteSpace(value))
                {
                    this.Add(key, value);
                    reader.ReadEndElement();
                }
                //else
                //{
                //    reader.Skip();
                //}

            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            var items = AllKeys.SelectMany(GetValues, (k, v) => new { key = k, value = v });
            foreach (var item in items)
            {
                if (string.IsNullOrWhiteSpace(item.key) || string.IsNullOrWhiteSpace(item.value))
                    continue;
                writer.WriteStartElement("t");
                writer.WriteAttributeString("l", item.key);
                writer.WriteString(item.value);
                writer.WriteEndElement();
            }
        }



        public override void Add(string name, string value)
        {


            if (base.GetValues(name) != null && base.GetValues(name).Contains(value))
                return;


            base.Add(name, value);
        }

        public IEnumerable<KeyValuePair<string, string>> AllItems()
        {
            return AllKeys.SelectMany(GetValues, (k, v) => new KeyValuePair<string, string>(k, v));
        }
    }

    public static class TranslationCollectionExtensions
    {

        public static XElement ToXElement(this Translations collection)
        {
            XmlSerializer ser = new XmlSerializer(typeof(Translations));

            StringWriter writer = new StringWriter();
            ser.Serialize(writer, collection);
            return XElement.Parse(writer.ToString());

        }




        public static Translations ToTranslations(this XElement element)
        {

            XmlSerializer ser = new XmlSerializer(typeof(Translations));
            return (Translations)ser.Deserialize(element.CreateReader());
        }


    }
}
