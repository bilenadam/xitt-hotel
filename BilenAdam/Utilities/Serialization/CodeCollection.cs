﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace BilenAdam.Utilities.Serialization
{
    [XmlRoot("codes")]
    public class CodeCollection : Dictionary<string, string>, IXmlSerializable
    {
        public CodeCollection()
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
        }

        public CodeCollection(IDictionary<string,string> codes)
            : base(StringComparer.InvariantCultureIgnoreCase)
        {
            if (codes != null)
            {
                foreach (KeyValuePair<string, string> entry in codes)
                {
                    Add(entry.Key, entry.Value);
                }
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;
            reader.Read();

            if (wasEmpty)
                return;

            while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
            {

                var key = reader.GetAttribute("code");
                reader.ReadStartElement("code");
                var value = reader.ReadString();
                reader.ReadEndElement();
                this.Add(key, value);


            }
            reader.ReadEndElement();
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {


            foreach (string key in this.Keys)
            {
                writer.WriteStartElement("code");
                writer.WriteAttributeString("code", key);
                writer.WriteString(this[key]);
                writer.WriteEndElement();
            }
        }
    }

    public static class CodeCollectionExtensions
    {


        public static XElement ToXElement(this CodeCollection collection)
        {
            if (collection == null)
                return null;

            XmlSerializer ser = new XmlSerializer(typeof(CodeCollection));

            StringWriter writer = new StringWriter();
            ser.Serialize(writer, collection);
            return XElement.Parse(writer.ToString());

        }

        public static CodeCollection ToCodeCollection(this XElement element)
        {
            if (element == null)
                return null;

            XmlSerializer ser = new XmlSerializer(typeof(CodeCollection));
            return (CodeCollection)ser.Deserialize(element.CreateReader());
        }
    }
}
