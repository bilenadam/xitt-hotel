﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Utilities.Base32;

namespace BilenAdam
{
    public class PnrHelper
    {
       const ulong interval = (ulong)Int16.MaxValue *  (ulong)Int16.MaxValue;
       const ulong tripPlanStart = interval;
       const ulong hotelCharterStart = 2 * interval;
       const ulong airCharterStart = 3 * interval;
        
        public static ulong Parse(string pnr)
        {
            ulong id = 0;
            if (SimpleCrockfordBase32Encoding.TryDecode(pnr, out id))
                return id;
            else
                return 0;
        }

        public static string ToPnr(ulong id)
        {
            return SimpleCrockfordBase32Encoding.Encode(id);
        }

        public static bool TryParsePnr(string pnr, out int id)
        {
            ulong i = 0;
            if (SimpleCrockfordBase32Encoding.TryDecode(pnr, out i))
            {
                id = (int)(i - tripPlanStart);

                return true;
            }
            id = -1;
            return false;
        }

        public static int ParseTripPlanPnr(string pnr)
        {
            var id = Parse(pnr);
            if (id > tripPlanStart && id < tripPlanStart+ interval)
                return (int)(id - tripPlanStart);
            else
                return 0;
        }

        public static string ToTripPlanPnr(int id)
        {
            if (id < 0)
                throw new ArgumentOutOfRangeException();

            return ToPnr(tripPlanStart + (ulong)id);
        }

        public static int ParseHotelCharterPnr(string pnr)
        {
            var id = Parse(pnr);
            if (id > hotelCharterStart && id < hotelCharterStart + interval)
                return (int)(id - hotelCharterStart);
            else
                return 0;
        }

        public static string ToHotelCharterPnr(int id)
        {
            if (id < 0)
                throw new ArgumentOutOfRangeException();

            return ToPnr(hotelCharterStart + (ulong)id);
        }

        public static int ParseAirCharterPnr(string pnr)
        {
            var id = Parse(pnr);
            if (id > airCharterStart && id < airCharterStart + interval)
                return (int)(id - airCharterStart);
            else
                return 0;
        }

        public static string ToAirCharterPnr(int id)
        {
            if (id < 0)
                throw new ArgumentOutOfRangeException();

            return ToPnr(airCharterStart + (ulong)id);
        }


    }
}
