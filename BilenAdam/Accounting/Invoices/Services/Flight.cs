﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices.Services
{
    public class Flight
    {
        public string Vendor { get; set; }
        public string FlightNo { get; set; }
        
        public string Origin { get; set; }
        public string Destination { get; set; }
        
        public DateTime FlightDate { get; set; }
        public TimeSpan Departure { get; set; }

        public TimeSpan? Arrival { get; set; }

        public string BookingClass { get; set; }

        public string Bagg { get; set; }

    }
}
