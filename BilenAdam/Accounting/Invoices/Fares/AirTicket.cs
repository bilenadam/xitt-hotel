﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices.Fares
{
    public class AirTicket : Fare
    {
        public string PassengerName { get; set; }

        public string TicketNumber { get; set; }

        public decimal Fare { get; set; }

        public decimal Taxes { get; set; }

        public decimal ServiceFee { get; set; }


    }
}
