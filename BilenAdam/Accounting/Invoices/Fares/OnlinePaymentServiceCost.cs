﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices.Fares
{
    public class OnlinePaymentServiceCost : Fare
    {
        public string PaymentType { get; set; }

        public string OrderID { get; set; }

        public decimal Cost { get; set; }
    }
}
