﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices.Fares
{
    public class Fare
    {
        public Fare()
        {
            VAT = new ValueAddedTax();
        }
        public ValueAddedTax VAT { get; set; }
        public decimal TotalFare { get; set; }
    }
}
