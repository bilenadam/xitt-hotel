﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices
{
    public class CompanyProfile
    {
       
        public string Name { get; set; }

        public string Address { get; set; }

        public string ZipCode { get; set; }
        public string City { get; set; }

        public string Country { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }

        public string LogoUrl { get; set; }



    }
}
