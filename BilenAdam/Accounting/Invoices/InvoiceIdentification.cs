﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices
{
    public class InvoiceIdentification
    {
        public string InvoiceNumber { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string InvoiceLocation { get; set; }

        public string ProcessNumber { get; set; }

        

    }
}
