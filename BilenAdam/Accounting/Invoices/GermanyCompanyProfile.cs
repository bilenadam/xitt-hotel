﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices
{
    public class GermanyCompanyProfile : CompanyProfile
    {
        public string Geschaeftsfuerer { get; set; }
        public string HRB { get; set; }
        public string USTID { get; set; }

        public string BankName { get; set; }

        public string KontoNr { get; set; }
        public string BLZ { get; set; }
        public string IBAN { get; set; }
        public string BIC { get; set; }

    }
}
