﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices
{
    public class Invoice
    {
        public InvoiceIdentification ID { get; set; }
        public Customer Customer { get; set; }

        public GermanyCompanyProfile CompanyProfile { get; set; }
        public List<Services.Flight> Flights { get; set; } 

        public List<Fares.AirTicket> AirTickets { get; set; }

        public List<Services.CustomService> CustomServices { get; set; }
        public List<Fares.CustomFare> CustomFares { get; set; }

        public List<Fares.OnlinePaymentServiceCost> OnlinePaymentServiceCosts { get; set; }

        public string Currency { get; set; }
        public decimal TotalFare { get; set; }

        public decimal TotalVat { get; set; }

        public string Notes { get; set; }



    }
}
