﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Invoices
{
    public class ValueAddedTax
    {
        public decimal Ratio { get; set; }
        public decimal TaxAmount { get; set; }
    }
}
