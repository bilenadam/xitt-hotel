﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Accounts
{
    public enum AccountType : int
    {
        Cash = 1000,
        Bank = 1020,
        Pos = 1081,
        OnlinePos = 1082,
        SofortUberweisung = 1083,
        Customers = 1200,
        Suppliers = 3200,
        Buying = 7400,
        FinanceExpenses = 7800,
        Sales = 6000
    }
}
