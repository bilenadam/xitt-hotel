﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Accounts
{
    public class AccountCodeFactory
    {
        public string Create(AccountType type, params int[] codeExtras)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0:0000}", (int)type);
           

            if(codeExtras != null && codeExtras.Length > 0)
            {
                foreach (var c in codeExtras)
                {
                    sb.AppendFormat("{0:0000}", c);
                }
            }
            return sb.ToString();
        }


      
       

       
    }
}
