﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Accounting.Transactions
{
    public enum TransactionType : int
    {
        AirTicketSale = 1,
        AirTicketBuy = 2,
        AirTicketSaleRefund = 3,
        AirTicketBuyRefund = 4,
        CashIn = 5,
        CashOut = 6,
        BankTransferIn = 7,
        BankTransferOut = 8,
        CreditCardPayment = 9,
        CreditCardRefund = 10,
        ConcardisPayment = 11,
        ConcardisRefund = 12,
        SofortTransfer = 13,
        SofortTransferRefund = 14
    }
}


public static class TransactionTypeResourceExtensions
{
    public static string GetLocalName(this BilenAdam.Accounting.Transactions.TransactionType type)
    {
        return BilenAdam.Resources.Accounting.TransactionResources.ResourceManager.GetString(type.ToString());
    }
}
