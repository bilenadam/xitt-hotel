﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Hotel
{
    public interface IHotelMetaDataProvider
    {
        IHotelDestination GetHotelDestination(int Id);

        IHotelMetaData GetHotel(int Id);
    }
}
