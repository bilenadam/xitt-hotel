﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Hotel
{
    public interface IHotelContactInfo
    {
        string Tel { get; }
        string Fax { get; }
        string EMail { get; }
        string Web { get; }
    }
}
