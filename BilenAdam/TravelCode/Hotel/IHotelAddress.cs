﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Hotel
{
    public interface IHotelAddress
    {
        string Address { get; }
        string ZipCode { get; }
        string City { get; }

        string CountryCode { get; }
    }
}
