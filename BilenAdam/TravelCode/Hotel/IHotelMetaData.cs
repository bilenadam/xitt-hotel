﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Hotel
{
    public interface IHotelMetaData
    {
        int Id { get; }
        string GIATA { get; }

        string Name { get; }

        int HotelDestinationId { get; }

        byte Stars { get; }

        IHotelContactInfo HotelContactInfo { get; }
        ILatLong LatLong { get; }
        IHotelAddress Address { get; }

    }
}
