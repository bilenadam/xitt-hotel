﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Hotel
{
    public interface IHotelDestination 
    {
        int Id { get; }
        int? ParentId { get; }
        string CountryCode { get; }
    }
}
