﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Air
{
    public interface IIataAirport : IIataCode
    {
        string CityCode { get; }
    }
}
