﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.TravelCode.Air;

namespace BilenAdam.TravelCode.Air
{
    public interface IIataCode
    {
        string Code { get; }
        string CountryCode { get; }

        bool IsCity { get; }
    }
}


