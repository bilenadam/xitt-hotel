﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Air
{
    public interface IAirMetaDataProvider
    {
        IAirline GetAirline(string code);
        IIataAirport GetAirport(string code);

        IIataCode GetCity(string code);
    }
}
