﻿namespace BilenAdam.TravelCode.Air
{
    public interface IAirline
    {
        string IATACode { get;  }
        string ICAOCode { get;  }
    }
}