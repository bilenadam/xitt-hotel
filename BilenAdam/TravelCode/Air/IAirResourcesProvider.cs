﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Air
{
    public interface IAirResourcesProvider
    {
        string GetAirlineName(string code, CultureInfo culture);
        string GetCityName(string code, CultureInfo culture);

        string GetAirportName(string code, CultureInfo culture);

    }
}
