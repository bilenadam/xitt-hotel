﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BilenAdam.TravelCode.Countries
{
    public class CountryTranslations
    {
        public static XElement GetTranslationsXml(string lang)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = string.Format("BilenAdam.TravelCode.Countries.Resources.countries_{0}.xml", lang);
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                XElement translations = XElement.Load(stream);
                return translations;
            }
        }
    }
}
