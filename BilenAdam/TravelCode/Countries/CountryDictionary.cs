﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Countries
{
    public class CountryDictionary
    {
        static Dictionary<string, Country> countryDictionary = new Dictionary<string, Country>(StringComparer.Ordinal);
        static Country[] _all;
       
        static CountryDictionary()
        {
            countryDictionary.Add("AD", new Country("AD", "Andorra", "EU"));
            countryDictionary.Add("AE", new Country("AE", "United Arab Emirates", "AS"));
            countryDictionary.Add("AF", new Country("AF", "Afghanistan", "AS"));
            countryDictionary.Add("AG", new Country("AG", "Antigua and Barbuda", "NA"));
            countryDictionary.Add("AI", new Country("AI", "Anguilla", "NA"));
            countryDictionary.Add("AL", new Country("AL", "Albania", "EU"));
            countryDictionary.Add("AM", new Country("AM", "Armenia", "AS"));
            countryDictionary.Add("AO", new Country("AO", "Angola", "AF"));
            countryDictionary.Add("AQ", new Country("AQ", "Antarctica", "AN"));
            countryDictionary.Add("AR", new Country("AR", "Argentina", "SA"));
            countryDictionary.Add("AS", new Country("AS", "American Samoa", "OC"));
            countryDictionary.Add("AT", new Country("AT", "Austria", "EU"));
            countryDictionary.Add("AU", new Country("AU", "Australia", "OC"));
            countryDictionary.Add("AW", new Country("AW", "Aruba", "NA"));
            countryDictionary.Add("AX", new Country("AX", "Åland", "EU"));
            countryDictionary.Add("AZ", new Country("AZ", "Azerbaijan", "AS"));
            countryDictionary.Add("BA", new Country("BA", "Bosnia and Herzegovina", "EU"));
            countryDictionary.Add("BB", new Country("BB", "Barbados", "NA"));
            countryDictionary.Add("BD", new Country("BD", "Bangladesh", "AS"));
            countryDictionary.Add("BE", new Country("BE", "Belgium", "EU"));
            countryDictionary.Add("BF", new Country("BF", "Burkina Faso", "AF"));
            countryDictionary.Add("BG", new Country("BG", "Bulgaria", "EU"));
            countryDictionary.Add("BH", new Country("BH", "Bahrain", "AS"));
            countryDictionary.Add("BI", new Country("BI", "Burundi", "AF"));
            countryDictionary.Add("BJ", new Country("BJ", "Benin", "AF"));
            countryDictionary.Add("BL", new Country("BL", "Saint Barthélemy", "NA"));
            countryDictionary.Add("BM", new Country("BM", "Bermuda", "NA"));
            countryDictionary.Add("BN", new Country("BN", "Brunei", "AS"));
            countryDictionary.Add("BO", new Country("BO", "Bolivia", "SA"));
            countryDictionary.Add("BQ", new Country("BQ", "Bonaire", "NA"));
            countryDictionary.Add("BR", new Country("BR", "Brazil", "SA"));
            countryDictionary.Add("BS", new Country("BS", "Bahamas", "NA"));
            countryDictionary.Add("BT", new Country("BT", "Bhutan", "AS"));
            countryDictionary.Add("BV", new Country("BV", "Bouvet Island", "AN"));
            countryDictionary.Add("BW", new Country("BW", "Botswana", "AF"));
            countryDictionary.Add("BY", new Country("BY", "Belarus", "EU"));
            countryDictionary.Add("BZ", new Country("BZ", "Belize", "NA"));
            countryDictionary.Add("CA", new Country("CA", "Canada", "NA"));
            countryDictionary.Add("CC", new Country("CC", "Cocos [Keeling] Islands", "AS"));
            countryDictionary.Add("CD", new Country("CD", "Congo", "AF"));
            countryDictionary.Add("CF", new Country("CF", "Central African Republic", "AF"));
            countryDictionary.Add("CG", new Country("CG", "Republic of the Congo", "AF"));
            countryDictionary.Add("CH", new Country("CH", "Switzerland", "EU"));
            countryDictionary.Add("CI", new Country("CI", "Ivory Coast", "AF"));
            countryDictionary.Add("CK", new Country("CK", "Cook Islands", "OC"));
            countryDictionary.Add("CL", new Country("CL", "Chile", "SA"));
            countryDictionary.Add("CM", new Country("CM", "Cameroon", "AF"));
            countryDictionary.Add("CN", new Country("CN", "China", "AS"));
            countryDictionary.Add("CO", new Country("CO", "Colombia", "SA"));
            countryDictionary.Add("CR", new Country("CR", "Costa Rica", "NA"));
            countryDictionary.Add("CU", new Country("CU", "Cuba", "NA"));
            countryDictionary.Add("CV", new Country("CV", "Cape Verde", "AF"));
            countryDictionary.Add("CW", new Country("CW", "Curacao", "NA"));
            countryDictionary.Add("CX", new Country("CX", "Christmas Island", "AS"));
            countryDictionary.Add("CY", new Country("CY", "Cyprus", "EU"));
            countryDictionary.Add("CZ", new Country("CZ", "Czechia", "EU"));
            countryDictionary.Add("DE", new Country("DE", "Germany", "EU"));
            countryDictionary.Add("DJ", new Country("DJ", "Djibouti", "AF"));
            countryDictionary.Add("DK", new Country("DK", "Denmark", "EU"));
            countryDictionary.Add("DM", new Country("DM", "Dominica", "NA"));
            countryDictionary.Add("DO", new Country("DO", "Dominican Republic", "NA"));
            countryDictionary.Add("DZ", new Country("DZ", "Algeria", "AF"));
            countryDictionary.Add("EC", new Country("EC", "Ecuador", "SA"));
            countryDictionary.Add("EE", new Country("EE", "Estonia", "EU"));
            countryDictionary.Add("EG", new Country("EG", "Egypt", "AF"));
            countryDictionary.Add("EH", new Country("EH", "Western Sahara", "AF"));
            countryDictionary.Add("ER", new Country("ER", "Eritrea", "AF"));
            countryDictionary.Add("ES", new Country("ES", "Spain", "EU"));
            countryDictionary.Add("ET", new Country("ET", "Ethiopia", "AF"));
            countryDictionary.Add("FI", new Country("FI", "Finland", "EU"));
            countryDictionary.Add("FJ", new Country("FJ", "Fiji", "OC"));
            countryDictionary.Add("FK", new Country("FK", "Falkland Islands", "SA"));
            countryDictionary.Add("FM", new Country("FM", "Micronesia", "OC"));
            countryDictionary.Add("FO", new Country("FO", "Faroe Islands", "EU"));
            countryDictionary.Add("FR", new Country("FR", "France", "EU"));
            countryDictionary.Add("GA", new Country("GA", "Gabon", "AF"));
            countryDictionary.Add("GB", new Country("GB", "United Kingdom", "EU"));
            countryDictionary.Add("GD", new Country("GD", "Grenada", "NA"));
            countryDictionary.Add("GE", new Country("GE", "Georgia", "AS"));
            countryDictionary.Add("GF", new Country("GF", "French Guiana", "SA"));
            countryDictionary.Add("GG", new Country("GG", "Guernsey", "EU"));
            countryDictionary.Add("GH", new Country("GH", "Ghana", "AF"));
            countryDictionary.Add("GI", new Country("GI", "Gibraltar", "EU"));
            countryDictionary.Add("GL", new Country("GL", "Greenland", "NA"));
            countryDictionary.Add("GM", new Country("GM", "Gambia", "AF"));
            countryDictionary.Add("GN", new Country("GN", "Guinea", "AF"));
            countryDictionary.Add("GP", new Country("GP", "Guadeloupe", "NA"));
            countryDictionary.Add("GQ", new Country("GQ", "Equatorial Guinea", "AF"));
            countryDictionary.Add("GR", new Country("GR", "Greece", "EU"));
            countryDictionary.Add("GS", new Country("GS", "South Georgia and the South Sandwich Islands", "AN"));
            countryDictionary.Add("GT", new Country("GT", "Guatemala", "NA"));
            countryDictionary.Add("GU", new Country("GU", "Guam", "OC"));
            countryDictionary.Add("GW", new Country("GW", "Guinea-Bissau", "AF"));
            countryDictionary.Add("GY", new Country("GY", "Guyana", "SA"));
            countryDictionary.Add("HK", new Country("HK", "Hong Kong", "AS"));
            countryDictionary.Add("HM", new Country("HM", "Heard Island and McDonald Islands", "AN"));
            countryDictionary.Add("HN", new Country("HN", "Honduras", "NA"));
            countryDictionary.Add("HR", new Country("HR", "Croatia", "EU"));
            countryDictionary.Add("HT", new Country("HT", "Haiti", "NA"));
            countryDictionary.Add("HU", new Country("HU", "Hungary", "EU"));
            countryDictionary.Add("ID", new Country("ID", "Indonesia", "AS"));
            countryDictionary.Add("IE", new Country("IE", "Ireland", "EU"));
            countryDictionary.Add("IL", new Country("IL", "Israel", "AS"));
            countryDictionary.Add("IM", new Country("IM", "Isle of Man", "EU"));
            countryDictionary.Add("IN", new Country("IN", "India", "AS"));
            countryDictionary.Add("IO", new Country("IO", "British Indian Ocean Territory", "AS"));
            countryDictionary.Add("IQ", new Country("IQ", "Iraq", "AS"));
            countryDictionary.Add("IR", new Country("IR", "Iran", "AS"));
            countryDictionary.Add("IS", new Country("IS", "Iceland", "EU"));
            countryDictionary.Add("IT", new Country("IT", "Italy", "EU"));
            countryDictionary.Add("JE", new Country("JE", "Jersey", "EU"));
            countryDictionary.Add("JM", new Country("JM", "Jamaica", "NA"));
            countryDictionary.Add("JO", new Country("JO", "Jordan", "AS"));
            countryDictionary.Add("JP", new Country("JP", "Japan", "AS"));
            countryDictionary.Add("KE", new Country("KE", "Kenya", "AF"));
            countryDictionary.Add("KG", new Country("KG", "Kyrgyzstan", "AS"));
            countryDictionary.Add("KH", new Country("KH", "Cambodia", "AS"));
            countryDictionary.Add("KI", new Country("KI", "Kiribati", "OC"));
            countryDictionary.Add("KM", new Country("KM", "Comoros", "AF"));
            countryDictionary.Add("KN", new Country("KN", "Saint Kitts and Nevis", "NA"));
            countryDictionary.Add("KP", new Country("KP", "North Korea", "AS"));
            countryDictionary.Add("KR", new Country("KR", "South Korea", "AS"));
            countryDictionary.Add("KW", new Country("KW", "Kuwait", "AS"));
            countryDictionary.Add("KY", new Country("KY", "Cayman Islands", "NA"));
            countryDictionary.Add("KZ", new Country("KZ", "Kazakhstan", "AS"));
            countryDictionary.Add("LA", new Country("LA", "Laos", "AS"));
            countryDictionary.Add("LB", new Country("LB", "Lebanon", "AS"));
            countryDictionary.Add("LC", new Country("LC", "Saint Lucia", "NA"));
            countryDictionary.Add("LI", new Country("LI", "Liechtenstein", "EU"));
            countryDictionary.Add("LK", new Country("LK", "Sri Lanka", "AS"));
            countryDictionary.Add("LR", new Country("LR", "Liberia", "AF"));
            countryDictionary.Add("LS", new Country("LS", "Lesotho", "AF"));
            countryDictionary.Add("LT", new Country("LT", "Lithuania", "EU"));
            countryDictionary.Add("LU", new Country("LU", "Luxembourg", "EU"));
            countryDictionary.Add("LV", new Country("LV", "Latvia", "EU"));
            countryDictionary.Add("LY", new Country("LY", "Libya", "AF"));
            countryDictionary.Add("MA", new Country("MA", "Morocco", "AF"));
            countryDictionary.Add("MC", new Country("MC", "Monaco", "EU"));
            countryDictionary.Add("MD", new Country("MD", "Moldova", "EU"));
            countryDictionary.Add("ME", new Country("ME", "Montenegro", "EU"));
            countryDictionary.Add("MF", new Country("MF", "Saint Martin", "NA"));
            countryDictionary.Add("MG", new Country("MG", "Madagascar", "AF"));
            countryDictionary.Add("MH", new Country("MH", "Marshall Islands", "OC"));
            countryDictionary.Add("MK", new Country("MK", "Macedonia", "EU"));
            countryDictionary.Add("ML", new Country("ML", "Mali", "AF"));
            countryDictionary.Add("MM", new Country("MM", "Myanmar [Burma]", "AS"));
            countryDictionary.Add("MN", new Country("MN", "Mongolia", "AS"));
            countryDictionary.Add("MO", new Country("MO", "Macao", "AS"));
            countryDictionary.Add("MP", new Country("MP", "Northern Mariana Islands", "OC"));
            countryDictionary.Add("MQ", new Country("MQ", "Martinique", "NA"));
            countryDictionary.Add("MR", new Country("MR", "Mauritania", "AF"));
            countryDictionary.Add("MS", new Country("MS", "Montserrat", "NA"));
            countryDictionary.Add("MT", new Country("MT", "Malta", "EU"));
            countryDictionary.Add("MU", new Country("MU", "Mauritius", "AF"));
            countryDictionary.Add("MV", new Country("MV", "Maldives", "AS"));
            countryDictionary.Add("MW", new Country("MW", "Malawi", "AF"));
            countryDictionary.Add("MX", new Country("MX", "Mexico", "NA"));
            countryDictionary.Add("MY", new Country("MY", "Malaysia", "AS"));
            countryDictionary.Add("MZ", new Country("MZ", "Mozambique", "AF"));
            countryDictionary.Add("NA", new Country("NA", "Namibia", "AF"));
            countryDictionary.Add("NC", new Country("NC", "New Caledonia", "OC"));
            countryDictionary.Add("NE", new Country("NE", "Niger", "AF"));
            countryDictionary.Add("NF", new Country("NF", "Norfolk Island", "OC"));
            countryDictionary.Add("NG", new Country("NG", "Nigeria", "AF"));
            countryDictionary.Add("NI", new Country("NI", "Nicaragua", "NA"));
            countryDictionary.Add("NL", new Country("NL", "Netherlands", "EU"));
            countryDictionary.Add("NO", new Country("NO", "Norway", "EU"));
            countryDictionary.Add("NP", new Country("NP", "Nepal", "AS"));
            countryDictionary.Add("NR", new Country("NR", "Nauru", "OC"));
            countryDictionary.Add("NU", new Country("NU", "Niue", "OC"));
            countryDictionary.Add("NZ", new Country("NZ", "New Zealand", "OC"));
            countryDictionary.Add("OM", new Country("OM", "Oman", "AS"));
            countryDictionary.Add("PA", new Country("PA", "Panama", "NA"));
            countryDictionary.Add("PE", new Country("PE", "Peru", "SA"));
            countryDictionary.Add("PF", new Country("PF", "French Polynesia", "OC"));
            countryDictionary.Add("PG", new Country("PG", "Papua New Guinea", "OC"));
            countryDictionary.Add("PH", new Country("PH", "Philippines", "AS"));
            countryDictionary.Add("PK", new Country("PK", "Pakistan", "AS"));
            countryDictionary.Add("PL", new Country("PL", "Poland", "EU"));
            countryDictionary.Add("PM", new Country("PM", "Saint Pierre and Miquelon", "NA"));
            countryDictionary.Add("PN", new Country("PN", "Pitcairn Islands", "OC"));
            countryDictionary.Add("PR", new Country("PR", "Puerto Rico", "NA"));
            countryDictionary.Add("PS", new Country("PS", "Palestine", "AS"));
            countryDictionary.Add("PT", new Country("PT", "Portugal", "EU"));
            countryDictionary.Add("PW", new Country("PW", "Palau", "OC"));
            countryDictionary.Add("PY", new Country("PY", "Paraguay", "SA"));
            countryDictionary.Add("QA", new Country("QA", "Qatar", "AS"));
            countryDictionary.Add("RE", new Country("RE", "Réunion", "AF"));
            countryDictionary.Add("RO", new Country("RO", "Romania", "EU"));
            countryDictionary.Add("RS", new Country("RS", "Serbia", "EU"));
            countryDictionary.Add("RU", new Country("RU", "Russia", "EU"));
            countryDictionary.Add("RW", new Country("RW", "Rwanda", "AF"));
            countryDictionary.Add("SA", new Country("SA", "Saudi Arabia", "AS"));
            countryDictionary.Add("SB", new Country("SB", "Solomon Islands", "OC"));
            countryDictionary.Add("SC", new Country("SC", "Seychelles", "AF"));
            countryDictionary.Add("SD", new Country("SD", "Sudan", "AF"));
            countryDictionary.Add("SE", new Country("SE", "Sweden", "EU"));
            countryDictionary.Add("SG", new Country("SG", "Singapore", "AS"));
            countryDictionary.Add("SH", new Country("SH", "Saint Helena", "AF"));
            countryDictionary.Add("SI", new Country("SI", "Slovenia", "EU"));
            countryDictionary.Add("SJ", new Country("SJ", "Svalbard and Jan Mayen", "EU"));
            countryDictionary.Add("SK", new Country("SK", "Slovakia", "EU"));
            countryDictionary.Add("SL", new Country("SL", "Sierra Leone", "AF"));
            countryDictionary.Add("SM", new Country("SM", "San Marino", "EU"));
            countryDictionary.Add("SN", new Country("SN", "Senegal", "AF"));
            countryDictionary.Add("SO", new Country("SO", "Somalia", "AF"));
            countryDictionary.Add("SR", new Country("SR", "Suriname", "SA"));
            countryDictionary.Add("SS", new Country("SS", "South Sudan", "AF"));
            countryDictionary.Add("ST", new Country("ST", "São Tomé and Príncipe", "AF"));
            countryDictionary.Add("SV", new Country("SV", "El Salvador", "NA"));
            countryDictionary.Add("SX", new Country("SX", "Sint Maarten", "NA"));
            countryDictionary.Add("SY", new Country("SY", "Syria", "AS"));
            countryDictionary.Add("SZ", new Country("SZ", "Swaziland", "AF"));
            countryDictionary.Add("TC", new Country("TC", "Turks and Caicos Islands", "NA"));
            countryDictionary.Add("TD", new Country("TD", "Chad", "AF"));
            countryDictionary.Add("TF", new Country("TF", "French Southern Territories", "AN"));
            countryDictionary.Add("TG", new Country("TG", "Togo", "AF"));
            countryDictionary.Add("TH", new Country("TH", "Thailand", "AS"));
            countryDictionary.Add("TJ", new Country("TJ", "Tajikistan", "AS"));
            countryDictionary.Add("TK", new Country("TK", "Tokelau", "OC"));
            countryDictionary.Add("TL", new Country("TL", "East Timor", "OC"));
            countryDictionary.Add("TM", new Country("TM", "Turkmenistan", "AS"));
            countryDictionary.Add("TN", new Country("TN", "Tunisia", "AF"));
            countryDictionary.Add("TO", new Country("TO", "Tonga", "OC"));
            countryDictionary.Add("TR", new Country("TR", "Turkey", "AS"));
            countryDictionary.Add("TT", new Country("TT", "Trinidad and Tobago", "NA"));
            countryDictionary.Add("TV", new Country("TV", "Tuvalu", "OC"));
            countryDictionary.Add("TW", new Country("TW", "Taiwan", "AS"));
            countryDictionary.Add("TZ", new Country("TZ", "Tanzania", "AF"));
            countryDictionary.Add("UA", new Country("UA", "Ukraine", "EU"));
            countryDictionary.Add("UG", new Country("UG", "Uganda", "AF"));
            countryDictionary.Add("UM", new Country("UM", "U.S. Minor Outlying Islands", "OC"));
            countryDictionary.Add("US", new Country("US", "United States", "NA"));
            countryDictionary.Add("UY", new Country("UY", "Uruguay", "SA"));
            countryDictionary.Add("UZ", new Country("UZ", "Uzbekistan", "AS"));
            countryDictionary.Add("VA", new Country("VA", "Vatican City", "EU"));
            countryDictionary.Add("VC", new Country("VC", "Saint Vincent and the Grenadines", "NA"));
            countryDictionary.Add("VE", new Country("VE", "Venezuela", "SA"));
            countryDictionary.Add("VG", new Country("VG", "British Virgin Islands", "NA"));
            countryDictionary.Add("VI", new Country("VI", "U.S. Virgin Islands", "NA"));
            countryDictionary.Add("VN", new Country("VN", "Vietnam", "AS"));
            countryDictionary.Add("VU", new Country("VU", "Vanuatu", "OC"));
            countryDictionary.Add("WF", new Country("WF", "Wallis and Futuna", "OC"));
            countryDictionary.Add("WS", new Country("WS", "Samoa", "OC"));
            countryDictionary.Add("XK", new Country("XK", "Kosovo", "EU"));
            countryDictionary.Add("YE", new Country("YE", "Yemen", "AS"));
            countryDictionary.Add("YT", new Country("YT", "Mayotte", "AF"));
            countryDictionary.Add("ZA", new Country("ZA", "South Africa", "AF"));
            countryDictionary.Add("ZM", new Country("ZM", "Zambia", "AF"));
            countryDictionary.Add("ZW", new Country("ZW", "Zimbabwe", "AF"));


            _all = countryDictionary.Values.OrderBy(l => l.Name).ToArray();


            foreach (var item in CountryTranslations.GetTranslationsXml("tr").Elements("item"))
            {
                var code = item.Element("id").Value;
                var name = (item.FirstNode.NextNode as System.Xml.Linq.XText).Value;

                if(countryDictionary.ContainsKey(code))
                {
                    countryDictionary[code].Translations.Add("tr", name);
                }
            }

            foreach (var item in CountryTranslations.GetTranslationsXml("de").Elements("item"))
            {
                var code = item.Element("id").Value;
                var name = (item.FirstNode.NextNode as System.Xml.Linq.XText).Value;

                if (countryDictionary.ContainsKey(code))
                {
                    countryDictionary[code].Translations.Add("de", name);
                }
            }
        }

        public static Country GetCountry(string key)
        {
            var k = key.ToUpperInvariant();
            if (countryDictionary.ContainsKey(k))
                return countryDictionary[k];
            else
                return null;
        }

        public static IEnumerable<Country> GetAll()
        {
            return _all;
        }
    }
}
