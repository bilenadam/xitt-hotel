﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode.Countries
{
    public class Country : ICountry
    {
        public Country(string code, string name, string continent)
        {
            this.Code = code;
            this.Name = name;
            this.Continent = continent;

            this.Translations = new Dictionary<string, string>(3);
            this.Translations.Add("en", name);
        }

        public Country(string code, string name, string continent, string turkishName, string germanName) : this(code,name,continent)
        {
            this.Translations.Add("de", germanName);
            this.Translations.Add("tr", turkishName);
        }
        public string Code
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Continent
        {
            get;
            set;
        }

        public Dictionary<string, string> Translations
        {
            get;
            set;
        }

        public string GetLocalizedName(System.Globalization.CultureInfo culture)
        {
            return GetLocalizedName(culture.TwoLetterISOLanguageName);
        }

        public string GetLocalizedName(string lang)
        {
            if (Translations != null && Translations.ContainsKey(lang))
            {
                return Translations[lang];
            }
            else
                return Name;
        }
    }
}
