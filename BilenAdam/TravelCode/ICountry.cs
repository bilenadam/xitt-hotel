﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.TravelCode
{
    public interface ICountry
    {
        string Code { get;  }
        string Name { get; }

        string Continent { get; }
    }
}
