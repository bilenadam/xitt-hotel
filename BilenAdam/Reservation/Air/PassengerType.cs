﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Air
{
    /// <summary>
    /// enumeration of types of passengers for air reservations. mainly used for fare discounts, and rules
    /// </summary>
    public enum PassengerType
    {
        /// <summary>
        /// Adult Passenger, age : 18 - 64
        /// </summary>
        Adult,
        /// <summary>
        /// Child Passenger, age : 2 - 11
        /// </summary>
        Child,
        /// <summary>
        /// Infant without a seat, age : 0 - 2
        /// </summary>
        Infant
       

    }
}
public static class PassengerTypeCounts
{
    public static int SafeGet(this IDictionary<PassengerType, int> counts, PassengerType key)
    {
        if (counts.ContainsKey(key))
            return counts[key];
        else
            return 0;
    }
    public static int Seated(this IReadOnlyDictionary<PassengerType, int> counts)
    {
        return counts.Where(c => c.Key != PassengerType.Infant).Sum(c => c.Value);
    }

    public static int AllPaxCount(this IReadOnlyDictionary<PassengerType, int> counts)
    {
        return counts.Sum(c => c.Value);
    }

    public static IDictionary<PassengerType, int> TrimDictionary(this IDictionary<PassengerType, int> counts)
    {
        return counts.Where(k => k.Value > 0).ToDictionary(k => k.Key, v => v.Value);
    }

    public static int GetAdultTypesCount(this IDictionary<PassengerType, int> counts)
    {
        int adt = 0;
        foreach (var item in counts)
        {
            switch (item.Key)
            {
                case PassengerType.Adult:
                    adt += item.Value;
                    break;
                default:
                    break;
            }


        }

        return adt;
    }

    public static int GetChildTypesCount(this IDictionary<PassengerType, int> counts)
    {
        int chd = 0;

        foreach (var item in counts)
        {
            switch (item.Key)
            {
                case PassengerType.Child:
                    chd += item.Value;
                    break;

                default:
                    break;
            }
        }

        return chd;
    }
}

