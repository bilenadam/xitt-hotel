﻿using System;
namespace BilenAdam.Reservation.Air
{
    public interface IAirSegment 
    {
        TimeSpan Arrival { get; }
        string Baggage { get; }
        string BookingClass { get; }
        TimeSpan Departure { get; }
        string Destination { get; }
        string DestinationTerminal { get; }
        DateTime FlightDate { get; }
        string FlightNo { get; }
        string Origin { get; }
        string OriginTerminal { get; }
        string Vendor { get; }

        string Via { get; }
    }
}
