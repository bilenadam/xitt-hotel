﻿using BilenAdam.TravelCode.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air
{
    public interface IFlight
    {
        string Carrier { get; }
      
        short FlightNo { get; }

        string Origin { get; }

        string Destination { get; }
    
        string Via { get; }
        DateTime Departure { get; }
        DateTime Arrival { get; }



    }
}
