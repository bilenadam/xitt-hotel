﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air
{
    public enum SaleType : byte
    {
        OneWay = 1,
        RoundTrip = 2
    }
}
