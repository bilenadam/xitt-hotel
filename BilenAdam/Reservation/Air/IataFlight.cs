﻿using BilenAdam.TravelCode.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air
{
    public interface IataFlight : IFlight
    {

        IAirline GetAirline();
        IIataAirport GetOriginAirport();
        IIataAirport GetDestinationAirport();

    }
}
