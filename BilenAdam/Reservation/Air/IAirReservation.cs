﻿using System;
using System.Collections.Generic;
namespace BilenAdam.Reservation.Air
{
    public interface IAirReservation
    {
        string Pnr { get;  }

        IEnumerable<IAirSegment> GetSegments();

        IEnumerable<IAirTicket> GetTickets();

        DateTime? TimeOption { get;  }
    }
}
