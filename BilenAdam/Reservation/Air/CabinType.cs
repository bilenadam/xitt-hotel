﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Air
{
    public enum CabinType
    {
        Any,
        Economy,
        Business,
        First
    }
}
