﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.TravelCode.Air;

namespace BilenAdam.Reservation.Air
{
    public   class Flight : IFlight
    {
        public string Carrier { get; set; }

        public short FlightNo { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }

        public DateTime Departure { get; set; }

        public DateTime Arrival { get; set; }

        public string Via { get; set; }
    }
}
