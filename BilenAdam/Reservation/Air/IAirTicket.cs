﻿using BilenAdam.Reservation.Air.Fare;
using System;
using System.Collections.Generic;

namespace BilenAdam.Reservation.Air
{
    public interface IAirTicket
    {
       
        string FirstName { get;  }
        string LastName { get;  }
        PassengerType PassengerType { get;  }
      
        string TicketNumber { get;  }
        string Title { get;  }

        IEnumerable<IFareAmount> GetFareAmounts();
    }
}
