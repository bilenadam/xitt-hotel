﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air.Fare
{
    public static  class KnownFareCodes
    {
        public static string Fare = "Fare";
        public static string Tax = "Tax";
        public static string SF = "SF";
        public static string Markup = "Markup";
        public static string AgencyServiceFee = "AgencySF";
        
    }
}
