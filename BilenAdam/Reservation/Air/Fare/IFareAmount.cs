﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air.Fare
{
    public interface IFareAmount
    {
        FareAmountType Type { get; }

        string Code { get; }

        decimal Amount { get; }
    }
}
