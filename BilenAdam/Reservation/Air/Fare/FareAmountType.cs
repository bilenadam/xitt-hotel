﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air.Fare
{
    public enum FareAmountType
    {
        Fare = 0,
        Tax = 1,
        SF = 2
    }
}
