﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air.Fare
{
    public class FareAmount : IFareAmount
    {
        public FareAmount(FareAmountType type, string code,decimal amount)
        {
            Type = type;
            Code = code;
            Amount = amount;
        }
        public FareAmount(decimal amount) : this( FareAmountType.Fare, KnownFareCodes.Fare, amount)
        {
            
        }

        public virtual FareAmountType Type { get; }

        public virtual string Code { get; } 

        public virtual decimal Amount { get;  }

      
    }
}
