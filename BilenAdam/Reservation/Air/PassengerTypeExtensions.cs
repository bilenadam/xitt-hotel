﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Elements;
using BilenAdam.Reservation.Air;


    /// <summary>
    /// common extensions for 
    /// </summary>
    public static class PassengerTypeExtensions
    {
        /// <summary>
        /// gets passenger type for traveller for specific service date range
        /// </summary>
        /// <param name="traveller"></param>
        /// <param name="minServiceDate"></param>
        /// <param name="maxServiceDate"></param>
        /// <returns></returns>
        public static PassengerType CalculatePassengetType(this ITraveller traveller, DateTime minServiceDate, DateTime maxServiceDate)
        {
            if (traveller.Dob == null)
                return PassengerType.Adult;

            var minage = traveller.CalculateAge(minServiceDate);
            var maxage = traveller.CalculateAge(maxServiceDate);


            if (maxage <= 2)
                return PassengerType.Infant;
            else if (maxage <= 12)
                return PassengerType.Child;

            return PassengerType.Adult;
        }

        /// <summary>
        /// gets passenger type for 
        /// </summary>
        /// <param name="traveller"></param>
        /// <param name="flights"></param>
        /// <returns></returns>
        public static PassengerType CalculatePassengetType(this ITraveller traveller, IEnumerable<IFlight> flights)
        {
            if (traveller.Dob == null)
                return PassengerType.Adult;

            var minServiceDate = flights.Min(f => f.Departure);
            var maxServiceDate = flights.Max(f => f.Departure);

            return traveller.CalculatePassengetType(minServiceDate, maxServiceDate);
        }

       
       
    }

