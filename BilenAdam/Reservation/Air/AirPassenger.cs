﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Elements;

namespace BilenAdam.Reservation.Air
{
    public class AirPassenger : IAirPassenger
    {
        public PassengerType PassengerType { get; set; }

        public int PaxRef { get; set; }

        public bool isMale { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public DateTime? Dob { get; set; }

        public IPassport Passport { get; set; }
    }
}
