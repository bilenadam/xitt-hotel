﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Tours
{
    public enum TourPassengerType
    {
        Adult,
        SingleAdult,
        AdditionalAdult,
        Infant,
        Child
    }
}
