﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.Reservation;

namespace BilenAdam.Reservation
{
    public enum ReservationStatus
    {
       
        OnRequest,
        RequestRejected,
        Confirmed,
        Ticket,
        Canceled,
        TimeOption


    }
}


public static class ReservationStatusExtensions
{
    public static string ToLocalized(this ReservationStatus status)
    {
        switch (status)
        {
            case ReservationStatus.OnRequest:
                return BilenAdam.Resources.Travel.ReservationStatus.OnRequest;
            case ReservationStatus.RequestRejected:
                return BilenAdam.Resources.Travel.ReservationStatus.RequestRejected;
            case ReservationStatus.Confirmed:
                return BilenAdam.Resources.Travel.ReservationStatus.Confirmed;
            case ReservationStatus.Ticket:
                return BilenAdam.Resources.Travel.ReservationStatus.Ticket;
            case ReservationStatus.Canceled:
                return BilenAdam.Resources.Travel.ReservationStatus.Canceled;
            case ReservationStatus.TimeOption:
                return BilenAdam.Resources.Travel.ReservationStatus.TimeOption;
            default:
                return status.ToString(); ;
        }
    }
}
