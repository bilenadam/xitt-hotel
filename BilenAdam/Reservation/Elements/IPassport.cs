﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Elements
{
    public interface IPassport 
    {
        string Country { get; }
        string Number { get; }
        DateTime ExpireDate { get; }
    }
}
