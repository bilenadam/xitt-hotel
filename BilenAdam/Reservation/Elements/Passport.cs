﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements
{
    public class Passport : IPassport
    {
        public string Country { get; set; }

        public string Number { get; set; }

        public DateTime ExpireDate { get; set; }
    }
}
