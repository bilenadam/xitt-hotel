﻿using BilenAdam.Reservation.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam
{
    public static class TravellerExtensions
    {
        public static string FirstNSecondName(this ITraveller traveller)
        {
            if (!string.IsNullOrWhiteSpace(traveller.MiddleName))
                return traveller.FirstName + " " + traveller.MiddleName;
            else
                return traveller.FirstName;
        }

        public static string FullName(this ITraveller traveller)
        {
            return traveller.FirstNSecondName() + " " + traveller.LastName;
        }

        public static bool SameName(this ITraveller traveller, ITraveller traveller2)
        {
            return traveller.FullName() == traveller2.FullName();
        }



       
    }
}
