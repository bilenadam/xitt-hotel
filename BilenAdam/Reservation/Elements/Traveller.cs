﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements.Simple
{
    public class Traveller : ITraveller
    {
        public int PaxRef{get;set;}

        public bool isMale{get;set;}

        public string FirstName{get;set;}

        public string MiddleName{get;set;}

        public string LastName{get;set;}

        public DateTime? Dob{get;set;}

        public IPassport Passport { get; set; }
    }
}
