﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements.NameMatching
{
    public class NameMatchUtil
    {
        public List<Name> LeftSideUnmatched { get; }
        public List<Name> RightSideUnmatched { get; }
        public List<NameMatch> Matches { get; }
        public NameMatchUtil(List<Name> leftSide,List<Name> rightSide)
        {
            LeftSideUnmatched = new List<Name>(leftSide);
            RightSideUnmatched = new List<Name>(rightSide);
            Matches = new List<NameMatch>();

            var compList = new NameCompareUtil().Compare(leftSide, rightSide);
            while (compList.Count > 0 && LeftSideUnmatched.Count > 0 && RightSideUnmatched.Count > 0)
            {
                var diff = compList.First();

                if (LeftSideUnmatched.Contains(diff.LeftSide) && RightSideUnmatched.Contains(diff.RightSide))
                {
                    LeftSideUnmatched.Remove(diff.LeftSide);
                    RightSideUnmatched.Remove(diff.RightSide);
                    Matches.Add(diff);
                }
                compList.Remove(diff);

            }
        }
    }
}
