﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements.NameMatching
{
    public class Name
    {
        public int PaxRef { get;  }
     

        public string FullName { get;  }

        public string CleanName { get;  }


        public DateTime? Dob { get; }

        public PassengerType PassengerType { get; }

        public int Ptype { get; }
    

        public Name(int paxRef, string fullname, DateTime? dob, PassengerType passengerType)
        {
            PaxRef = paxRef;
            FullName = fullname;
            CleanName = Clean(fullname);
            Dob = dob;
            PassengerType = passengerType;
            Ptype = (int)Ptype;
        }

        public static string Clean(string fullname)
        {
            return fullname.ClearGermanAccent().ToUpperInvariant().Replace(" ", "").Replace("-", "");
        }
    }
}
