﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Utilities.Text;

namespace BilenAdam.Reservation.Elements.NameMatching
{
    public class NameMatch
    {
        public Name LeftSide { get;  }
        public Name RightSide { get;  }
     
        public int NameDifference { get; }

        public int DobDifference { get; }

        public int PTypeDifference { get; }


        public NameMatch(Name leftSide, Name rightSide)
        {
            NameDifference = LevenshteinDistance.Compute(leftSide.CleanName, rightSide.CleanName);
            LeftSide = leftSide;
            RightSide = rightSide;
            PTypeDifference = Math.Abs(leftSide.Ptype - rightSide.Ptype);

            if(leftSide.Dob != null && rightSide.Dob != null)
            {
                if (leftSide.Dob == rightSide.Dob)
                    DobDifference = 0;
                else
                    DobDifference = 4;
            }
            else if(leftSide.Dob == null && rightSide.Dob == null)
            {
                DobDifference = 1;
            }
            else
            {
                DobDifference = 2;
            }
        }


    }
}
