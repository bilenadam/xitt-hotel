﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements.NameMatching
{
    public class NameCompareUtil
    {
      

        public List<NameMatch>  Compare(IEnumerable<Name> leftSide, IEnumerable<Name> rightSide)
        {
            List<NameMatch> compareList = new List<NameMatch>();
            foreach (var left in leftSide)
            {
                foreach (var right in rightSide)
                {
                    var diff = new NameMatch(left, right);

                    bool last = true;
                    for (int i = 0; i < compareList.Count; i++)
                    {
                        if (compareList[i].NameDifference < diff.NameDifference)
                            continue;
                        else if (compareList[i].NameDifference == diff.NameDifference && compareList[i].PTypeDifference < diff.PTypeDifference)
                            continue;
                        else if (compareList[i].NameDifference == diff.NameDifference && compareList[i].PTypeDifference == diff.PTypeDifference && compareList[i].DobDifference <= diff.DobDifference)
                            continue;
                        else
                        {
                            last = false;
                            compareList.Insert(i, diff);
                            break;
                        }
                    }
                    if(last)
                        compareList.Add(diff);
                }
            }

            return compareList;
        }
    }
}
