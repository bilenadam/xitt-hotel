﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements
{
    public interface IBillingInformation
    {
        bool IsCompany { get; }
        string Name { get; }
        string Address { get; }
        string City { get; }
        string Country { get; }
        string ZipCode { get; }
    }
}
