﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Elements
{
    public class BillingInformation : IBillingInformation
    {
        public bool IsCompany {get;set;}

        public string Name {get;set;}

        public string Address {get;set;}

        public string City {get;set;}

        public string Country {get;set;}

        public string ZipCode {get;set;}
    }
}
