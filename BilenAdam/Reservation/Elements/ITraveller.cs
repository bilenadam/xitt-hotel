﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace BilenAdam.Reservation.Elements
{
    public interface ITraveller 
    {
        
        int PaxRef { get; }
        bool isMale { get; }
        string FirstName { get; }
        string MiddleName { get; }
        string LastName { get; }
        DateTime? Dob { get; }

        IPassport Passport { get; }
        
    }

    public static class TravellerExtensions
    {
        public static int? CalculateAge(this ITraveller traveller, DateTime date)
        {
            if (traveller.Dob == null)
                return null;

            int age = date.Year - traveller.Dob.Value.Year;
            if (traveller.Dob > date.AddYears(-age))
                age--;
            return age;
        
        }

       
    }
}
