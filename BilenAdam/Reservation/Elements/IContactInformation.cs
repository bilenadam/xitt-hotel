﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Elements
{
    public interface IContactInformation 
    {
        string Email { get;  }
        string Name { get;  }
        string Phone { get;  }
    }
}
