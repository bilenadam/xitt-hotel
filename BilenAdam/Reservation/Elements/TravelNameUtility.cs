﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Air.Utilities
{
    public class TravellerNameUtility
    {
        public static string ClearTravellerName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return name;

            var n = name.ToUpperInvariant().ClearAccent();
            n = System.Text.RegularExpressions.Regex.Replace(n, "[^A-Z]", "");
            return n;
        }
    }
}
