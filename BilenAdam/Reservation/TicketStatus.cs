﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation;

namespace BilenAdam.Reservation
{
    /// <summary>
    /// enumeration for ticket/stock statuses
    /// </summary>
    public enum TicketStatus
    {
        
        CanceledTimeOption,
        ExpiredTimeOption,
        ValidTimeOption,
        Ticket,
        Void,
        Refund
        
    }
}

public static class TicketStatusExtensions
{
    public static string ToLocalized(this TicketStatus status)
    {
        switch (status)
        {
            case TicketStatus.CanceledTimeOption:
                return BilenAdam.Resources.Travel.TicketStatus.CanceledTimeOption;
            case TicketStatus.ExpiredTimeOption:
                return BilenAdam.Resources.Travel.TicketStatus.ExpiredTimeOption;
            case TicketStatus.ValidTimeOption:
                return BilenAdam.Resources.Travel.TicketStatus.ValidTimeOption;
            case TicketStatus.Ticket:
                return BilenAdam.Resources.Travel.TicketStatus.Ticket;
            case TicketStatus.Void:
                return BilenAdam.Resources.Travel.TicketStatus.Void;
            case TicketStatus.Refund:
                return BilenAdam.Resources.Travel.TicketStatus.Refund;
            default:
                return status.ToString();
        }
    }
}
