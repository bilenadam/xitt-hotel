﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation
{
    /// <summary>
    /// availability status of fare/itinerary product.
    /// <remarks>
    /// Intended to use within availability and fare searches 
    /// </remarks>
    /// </summary>
    public enum AvailabilityStatus
    {
        InstantConfirmation = 1,
        OnRequest = 2,
        SoldOut = 3,
        StopSale = 4,
        NotAvailable = 5
    }
}
