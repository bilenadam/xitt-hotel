﻿using BilenAdam.Utilities.Base32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation
{
    public class PriceEncoding
    {
        public  int Flaten(decimal price) => (int)(Math.Ceiling(price));


        public string FlatenAndEncode(decimal price) => Encode(Flaten(price));

        public string Encode(int price) => SimpleCrockfordBase32Encoding.EncodeInt(price);
        

        public int Decode(string cls)
        {
            if (SimpleCrockfordBase32Encoding.TryDecodeAsInt(cls, out int price))
                return price;
            else
                return 0;
        }


    }
}
