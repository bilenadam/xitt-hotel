﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Hotels
{
    public enum ServiceFeeTypes
    {
        TransferFee = 1,
        HandlingFee = 2,
        Comission = 3,
        Refund = 4,
        Markup = 5
    }
}
