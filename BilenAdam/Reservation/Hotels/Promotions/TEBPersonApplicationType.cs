﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Hotels.Promotions
{
    public enum TEBPersonApplicationType
    {
        Person = 1,
        PersonWeek = 2,
        PersonDay = 3
    }
}
