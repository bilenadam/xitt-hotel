﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Hotels.Promotions
{
    public enum PromotionType
    {
        EarlyBooking = 1,
        LongStay = 2,
        Age = 3,
        Honeymoon = 4,
        TurboEarlyBooking = 5,
        Day = 6,
        RollingEarlyBooking = 7
    }
}
