﻿using BilenAdam.Reservation.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Hotels
{
    public interface IRoomGuest : ITraveller
    {
        RoomGuestType GuestType { get; }
    }
}
