﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Hotels
{
    public interface IRoom
    {
        int RoomRef { get; }
        IEnumerable<IRoomGuest> GetGuests();
    }
}
