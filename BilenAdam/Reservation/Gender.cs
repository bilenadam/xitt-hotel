﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation
{
    /// <summary>
    /// Gender enumeration
    /// </summary>
    public enum Gender
    {
        Male,
        Female
    }
}
