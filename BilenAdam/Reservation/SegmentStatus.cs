﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation
{
    /// <summary>
    /// enumeration for statuses of segments of reservations
    /// </summary>
    public enum SegmentStatus
    {
        Confirmed,
        Canceled,
        Waitlist,
        ScheduleChange,
        Void,
        Reissued
    }
}
