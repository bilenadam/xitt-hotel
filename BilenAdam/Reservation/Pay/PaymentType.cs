﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Pay
{
    public enum PaymentType
    {
        OnlinePos,
        Pos,
        Cash,
        Credit
    }
}
