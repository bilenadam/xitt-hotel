﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Pay
{
    public interface IFormOfPayment
    {
        decimal Amount { get; }
        PaymentType PaymentType { get; }
        int AccountId { get; }
    }
}
