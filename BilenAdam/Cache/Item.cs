﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Cache
{
    public class Item<V>
    {
        public DateTime Expires { get; set; }
        public V Value { get; set; }
    }
}
