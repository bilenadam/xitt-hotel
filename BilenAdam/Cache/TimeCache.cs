﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BilenAdam.Cache
{
    public class TimeCache<K,V>
    {
        private Dictionary<K, Item<V>> items = new Dictionary<K, Item<V>>();
        private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();

        public TimeCache()
        {

        }

        public IEnumerable<V> GetAll()
        {
            cacheLock.EnterUpgradeableReadLock();
            try
            {
                if (items.Count == 0)
                    return null;

                return items.Values.Select(i => i.Value);
            }
            finally
            {
                cacheLock.ExitUpgradeableReadLock();
            }
        }

        public TimeCache(IEnumerable<KeyValuePair<K,V>> preload, DateTime expires)
        {
                foreach (var item in preload)
                {
                    items.Add(item.Key, new Item<V> { Expires = expires, Value = item.Value });
                }
           
        }

        public bool ContainsKey(K key)
        {
            return items.ContainsKey(key);
        }
        public V Get(K key, Func<K,V> valueFactory, DateTime expires)
        {
            cacheLock.EnterUpgradeableReadLock();
            try
            {
                Item<V> item = null;
                if(items.TryGetValue(key,out item))
                {
                    if(item.Expires < DateTime.Now)
                    {
                        cacheLock.EnterWriteLock();
                        try
                        {
                            item.Value = valueFactory(key);
                            item.Expires = expires;
                        }
                        finally
                        {
                            cacheLock.ExitWriteLock();
                        }
                    }

                    
                }
                else
                {
                    cacheLock.EnterWriteLock();
                    try
                    {
                        item = new Item<V> { Expires = expires };
                        item.Value = valueFactory(key);
                        items.Add(key, item);
                       
                    }
                    finally
                    {
                        cacheLock.ExitWriteLock();
                    }
                    
                }
                if (item != null)
                    return item.Value;
                else
                    return default(V);
            }
            finally
            {
                cacheLock.ExitUpgradeableReadLock();
            }
        }
    }
}
