﻿using BilenAdam.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.IoC
{
    public static class DependencyResolver
    {

        static object _lock = new object();
        public static IDependencyResolver Current
        {
            get
            {
                lock (_lock)
                {
                    return new ConcurrentDependencyResolver();
                }
            }
        }
    }
}
