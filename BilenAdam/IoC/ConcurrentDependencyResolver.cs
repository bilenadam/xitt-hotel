﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;

namespace BilenAdam.IoC
{
    public class ConcurrentDependencyResolver : IDependencyResolver
    {

      
        static ConcurrentDictionary<Type, ConcurrentBag<Func<IDependencyResolver, object>>> factories = new ConcurrentDictionary<Type, ConcurrentBag<Func<IDependencyResolver, object>>>();

        public T GetService<T>()
        {
            var services = GetServices<T>();
            if (services.HasAny())
                return services.First();
            else
                return default(T);

        }

        public IEnumerable<T> GetServices<T>()
        {
            ConcurrentBag<Func<IDependencyResolver, object>> funcs = null;

            if(factories.TryGetValue(typeof(T),out funcs))
            {
                return funcs.Select(f => (T)f.Invoke(this));
            }
            else
            {
                return null;
            }
        }

        public void Register<T>(Func<IDependencyResolver, T> serviceFactory)
        {
            ConcurrentBag<Func<IDependencyResolver, object>> funcs = factories.GetOrAdd(typeof(T), new ConcurrentBag<Func<IDependencyResolver, object>>());
            funcs.Add(c=>serviceFactory(c));

        }
    }
}
