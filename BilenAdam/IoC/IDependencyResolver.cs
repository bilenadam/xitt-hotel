﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.IoC
{
    public interface IDependencyResolver
    {
       

        void Register<T>(Func<IDependencyResolver, T> serviceFactory);
        T GetService<T>();
        IEnumerable<T> GetServices<T>();
    }

    
}
