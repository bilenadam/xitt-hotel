﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Log
{
    public class LogContext
    {
        [ThreadStatic]
        static LogContext current;

        public static LogContext CurrentContext
        {
            get
            {
                if (current == null)
                    current = new LogContext();

                return current;
            }
        }

        private LogContext()
        {
            
        }

        List<ILog> logs = new List<ILog>();


        public static void LogFile(string message)
        {
            if (current != null && current.logs.Count > 0)
            {
                foreach (var log in current.logs)
                {
                    log.LogFile(message);
                }

            }
        }


        public static void LogMessage(string message)
        {
            if(current != null && current.logs.Count > 0)
            {
                foreach (var log in current.logs)
                {
                    log.LogMessage(message);
                }
               
            }
        }

        public static void LogExecption(Exception exc)
        {
            if (current != null && current.logs.Count > 0)
            {
                foreach (var log in current.logs)
                {
                    log.LogException(exc);
                }

            }
        }

        public static void AddLog(ILog log)
        {
            if(!CurrentContext.logs.Contains(log))
                CurrentContext.logs.Add(log);
        }

        public static void RemoveLog(ILog log)
        {
            if (CurrentContext.logs.Contains(log))
                CurrentContext.logs.Remove(log);
        }
      
         
        public static IEnumerable<ILog> GetLogs()
        {
            if(current != null && current.logs.Count > 0)
            {
                return current.logs.ToArray();
            }


            return null;
        }


        public static void AddLogs(IEnumerable<ILog> logs)
        {
            if (logs == null)
                return;

            foreach (var log in logs)
            {
                AddLog(log);
            }
        }

        public static void RemoveLogs(IEnumerable<ILog> logs)
        {
            if (logs == null)
                return;

            foreach (var log in logs)
            {
                RemoveLog(log);
            }
        }




       

    }
    
        
}
