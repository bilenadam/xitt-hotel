﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log
{
    public interface IObjectLogSerializer
    {
        string Serialize(object obj);
    }
}
