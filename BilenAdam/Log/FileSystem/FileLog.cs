﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using BilenAdam.Utilities.IO;

namespace BilenAdam.Log.FileSystem
{
    public class FileLog
    {
       
       
        public void SaveText(string path, string text)
        {
            try
            {
                File.WriteAllText(path, text);
                LogContext.LogFile(path);
            }
            catch
            {
            }
        }

        public async Task SaveTextAsync(string path, string text)
        {
            try
            {
                using (var writer = File.CreateText(path))
                {
                    await writer.WriteAsync(text);
                }
                LogContext.LogFile(path);
            }
            catch
            {
            }
        }



        public void SaveXml(string path, string text)
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = "  ";
                settings.NewLineChars = "\r\n";
                settings.NewLineHandling = NewLineHandling.Replace;
                using (XmlWriter writer = XmlWriter.Create(path, settings))
                {

                    XmlDocument document = new XmlDocument();

                    document.LoadXml(text);
                    document.Save(writer);
                }
                LogContext.LogFile(path);
            }
            catch
            {
            }
        }

        public void SaveText(string path, System.IO.Stream data)
        {
            try
            {
                using (var logfile = System.IO.File.Create(path))
                {
                    data.CopyTo(logfile);
                    logfile.Flush();
                    //logfile.Close();
                }
                LogContext.LogFile(path);
            }
            catch
            {
            }
        }

        public void SaveXml(string path, System.IO.Stream data)
        {
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.IndentChars = "  ";
                settings.NewLineChars = "\r\n";
                settings.NewLineHandling = NewLineHandling.Replace;
                using (XmlWriter writer = XmlWriter.Create(path, settings))
                {

                    XmlDocument document = new XmlDocument();

                    document.Load(data);
                    document.Save(writer);
                }

                LogContext.LogFile(path);
            }
            catch
            {
            }
        }
    }
}
