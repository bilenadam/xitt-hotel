﻿using BilenAdam.Utilities.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log
{
    public class LogPathUtil
    {
        PathUtil path;
        FileNameUtil fileName;
        public LogPathUtil(string ext)
        {
            path = new PathUtil(new AppLogSettings().GetAppLogFolder());
            fileName = new FileNameUtil(ext);
        }



        public LogPathUtil AppendName(string name, char seperator = '_')
        {
            fileName.AppendUnsafe(name, seperator);
            return this;
        }


        public LogPathUtil AppendDateTimeName(DateTime dateTime, string format = "yyMMddHHmmss", char seperator = '_') => AppendName(dateTime.ToString(format), seperator);


        public LogPathUtil AppendDateTimeName(bool utc = false, string format = "yyMMddHHmmss", char seperator = '_') => AppendName(utc ? DateTime.UtcNow.ToString(format) : DateTime.Now.ToString(format), seperator);
        public LogPathUtil AppendTodayName(bool utc = false, string format = "yyMMdd", char seperator = '_') => AppendDateTimeName(utc, format, seperator);

        public LogPathUtil AppendNowName(bool utc = false, string format = "HHmmss", char seperator = '_') => AppendDateTimeName(utc, format, seperator);


        public LogPathUtil AddFolder(string folder)
        {
            path.AddFolder(folder);
            return this;
        }


        public LogPathUtil AddDateTimeFolder(DateTime dateTime, string format = "yyMMddHHmmss") => AddFolder(dateTime.ToString(format));
         

        public LogPathUtil AddTodayFolder(bool utc = false, string format = "yyMMdd") => AddDateTimeFolder(utc ? DateTime.UtcNow : DateTime.Now, format);

        public LogPathUtil AddNowFolder(bool utc = false, string format = "HHmmss") => AddDateTimeFolder(utc ? DateTime.UtcNow : DateTime.Now, format);

        public LogPathUtil AddYearFolder(bool utc = false) => AddDateTimeFolder(utc ? DateTime.UtcNow : DateTime.Now, "yy");
        public LogPathUtil AddMonthFolder(bool utc = false) => AddDateTimeFolder(utc ? DateTime.UtcNow : DateTime.Now, "MM");
        public LogPathUtil AddDayFolder(bool utc = false) => AddDateTimeFolder(utc ? DateTime.UtcNow : DateTime.Now, "dd");

        public string Create(bool addversionifexists = true)
        {
            string folder = path.ToString();
            System.IO.Directory.CreateDirectory(folder);

            string fullPath = System.IO.Path.Combine(folder, fileName.HasName ? fileName.ToString() : PathUtils.GenerateRandomFileName(fileName.ExtWithDot));

            if (addversionifexists)
            {
                while (System.IO.File.Exists(fullPath))
                {
                    if (fileName.HasName)
                    {
                        fileName.Version++;
                        fullPath = System.IO.Path.Combine(folder, fileName.ToString());
                    }
                    else
                    {
                        fullPath = System.IO.Path.Combine(folder, PathUtils.GenerateRandomFileName(fileName.ExtWithDot));
                    }
                }
            }
            return fullPath;
        }

    

    }
}
