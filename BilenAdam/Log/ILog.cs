﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log
{
    public interface ILog
    {
        void LogMessage(string message);

        void LogException(Exception exc);

        void LogFile(string filepath);

        void LogObject(object obj);
    }
}
