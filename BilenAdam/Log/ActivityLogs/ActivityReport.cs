﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log.ActivityLogs
{
    public class ActivityReport : IActivityReport
    {
        public string ActivityCode { get; set; }
        public DateTime ActivityTime
        {
            get;set;
        }

        public string ActivityType
        {
            get; set;
        }

        public string ApplicationName
        {
            get; set;
        }

        public string IpAddress
        {
            get; set;
        }

        public string LogReport
        {
            get; set;
        }

        public string Parameters
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }
    }
}
