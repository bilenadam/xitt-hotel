﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log.ActivityLogs
{
    public interface IActivityReport
    {
        string ActivityCode { get; }
        string ApplicationName { get; }
        DateTime ActivityTime { get; }

        string UserName { get; }

        string IpAddress { get; }

        string ActivityType { get; }

        string Parameters { get; }

        string LogReport { get; }
    }
}
