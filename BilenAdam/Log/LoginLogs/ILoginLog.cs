﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log.LoginLogs
{
    public interface ILoginLog
    {
        string ApplicationName { get; }
        DateTime LoginTime { get; }

        string Username { get; }

        string IpAddress { get; }

        bool IsSuccess { get; }
    }
}
