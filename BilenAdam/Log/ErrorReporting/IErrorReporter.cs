﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log.ErrorReporting
{
    public interface IErrorReporter
    {
        void Report(IErrorReport error);
    }
}
