﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log.ErrorReporting
{
    public interface IErrorReport
    {
        string ApplicationName { get; }
        DateTime ErrorTime { get; }

        string UserName { get; }

        string IpAddress { get; }
       
        string ErrorType { get; }

        string UserReport { get; }

        string ControllerName { get; }

        string ActionName { get; }
    }
}
