﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Log
{
    public class AppLogSettings : ApplicationSettings
    {
        public const string AppLogFolderKey = "app.logFolder";

        public string GetAppLogFolder()
        {
            return Get(AppLogFolderKey, @"C:\BilenAdam\Log");
        }



    }
}
