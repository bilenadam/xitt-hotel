﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using System.IO;
using System.Xml;

namespace BilenAdam.Log.Soap
{
    public class SoapLogExtension : SoapExtension, IDisposable
    {

        string session;
        public SoapLogExtension()
        {
           session = System.IO.Path.GetRandomFileName().Replace(".", "");
          
        }


        private Stream originalStream;
        private Stream internalStream;

        /// <summary>
        ///  initialize with config
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public override object GetInitializer(System.Type serviceType)
        {
            return null;
        }

        /// <summary>
        /// initialize with attribute
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return null;
        }


        public override void Initialize(object initializer)
        {
           
        }


        public override System.IO.Stream ChainStream(System.IO.Stream stream)
        {
            originalStream = stream;
            internalStream = new MemoryStream();
            return internalStream;
        }


        public override void ProcessMessage(System.Web.Services.Protocols.SoapMessage message)
        {
            LogMessage(message);
        }

        private void LogMessage(System.Web.Services.Protocols.SoapMessage message)
        {
          
            var log = new Log.Soap.SoapLogService().GetLog(message);

            switch (message.Stage)
            {
                case SoapMessageStage.BeforeSerialize:
                    break;

                case SoapMessageStage.AfterSerialize:
                    internalStream.Position = 0;
                    try
                    {
                        log.Log(session,message, internalStream);
                    }
                    catch
                    {
                    }
                    internalStream.Position = 0;
                    CopyStream(internalStream, originalStream);
                    internalStream.Position = 0;
                    break;

                case SoapMessageStage.BeforeDeserialize:
                    CopyStream(originalStream, internalStream);
                    internalStream.Position = 0;
                    try
                    {
                        log.Log(session,message, internalStream);
                    }
                    catch
                    {
                    }
                    internalStream.Position = 0;
                    break;

                case SoapMessageStage.AfterDeserialize:
                    break;
            }

        }




        private void CopyStream(Stream fromStream,
        Stream toStream)
        {
            try
            {
                StreamReader sr =
                new StreamReader(fromStream);
                StreamWriter sw =
                new StreamWriter(toStream);
                sw.WriteLine(sr.ReadToEnd());
                sw.Flush();
            }
            catch
            {

            }
        }



        #region IDisposable implementation
        bool _disposed;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SoapLogExtension()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                // free other managed objects that implement
                // IDisposable only
                this.internalStream.Dispose();
                this.internalStream = null;
            }

            // release any unmanaged objects
            // set the object references to null
            /* example
           if (nativeResource != IntPtr.Zero) 
           {
               Marshal.FreeHGlobal(nativeResource);
               nativeResource = IntPtr.Zero;
           }
            * 
            */
            _disposed = true;
        }
        #endregion
    }
}
