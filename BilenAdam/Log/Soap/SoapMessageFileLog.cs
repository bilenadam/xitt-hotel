﻿using BilenAdam.Log.FileSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace BilenAdam.Log.Soap
{
    public class SoapMessageFileLog : ISoapMessageLog
    {
        
       
        public void Log(string session,System.Web.Services.Protocols.SoapMessage message, System.IO.Stream messageStream)
        {
            var path = buildFileName(message, session);


            new FileSystem.FileLog().SaveXml(path, messageStream);
        }

        string buildFileName(System.Web.Services.Protocols.SoapMessage message, string session)
        {
            string address = message.Url.Split(new string[] { "//" }, StringSplitOptions.None).Skip(1).First().Split('/').First();
            string stage = message.Stage == System.Web.Services.Protocols.SoapMessageStage.AfterSerialize ? "request" : "response";
            string methodname = message.MethodInfo.Name;

            return new LogPathUtil(".xml").AddFolder(address).AddTodayFolder().AppendName(methodname).AppendName(session).AppendName(stage).Create();


        }

       
    }
}
