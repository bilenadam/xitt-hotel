﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Log.Soap
{
    public interface ISoapMessageLog
    {
        void Log(string session,System.Web.Services.Protocols.SoapMessage message, System.IO.Stream messageStream);
    }
}
