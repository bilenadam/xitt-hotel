﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using BilenAdam.Crm;
namespace BilenAdam.Security
{
    public interface IContactPrincipal : IPrincipal
    {
       IContact Contact { get; }
    }
}
