﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Crm;
namespace BilenAdam.Security
{
    public class ContactPrincipal : IContactPrincipal
    {
        public ContactPrincipal(IIdentity identity,IContact contact, string[] roles)
        {
            Identity = identity;
            Contact = contact;
            Roles = roles;
        }
       
        public IContact Contact {
            get;
            private set;
        }

        public IIdentity Identity
        {
            get; private set;
        }

        public string[] Roles { get; private set; }

        public bool IsInRole(string role)
        {
            return Roles != null && Roles.Any(r => r == role);
        }
        
    }
}
