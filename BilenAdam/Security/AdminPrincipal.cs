﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;

namespace BilenAdam.Security
{
    public class AdminPrincipal : IPrincipal
    {
        public AdminPrincipal(IIdentity identity, string[] roles)
        {
            Identity = identity;
            Roles = roles;
        }

        public IIdentity Identity
        {
            get; private set;
        }

        public string[] Roles { get; private set; }

        public bool IsInRole(string role)
        {
            return Roles != null && Roles.Any(r => r == role);
        }
    }
}
