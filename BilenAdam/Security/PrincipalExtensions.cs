﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Security;
using System.Security.Principal;

public static class PrincipalExtensions
{
    public static bool IsContactPrincipal(this IPrincipal principal)
    {
        return principal is IContactPrincipal;
    }

    public static IContactPrincipal AsContactPrincipal(this IPrincipal principal)
    {
        return principal as IContactPrincipal;
    }

    public static bool IsAdminPrincipal(this IPrincipal principal)
    {
        return principal is AdminPrincipal;
    }

    public static AdminPrincipal AsAdminPrincipal(this IPrincipal principal)
    {
        return principal as AdminPrincipal;
    }
}

