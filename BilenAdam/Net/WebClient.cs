﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Net
{
    public class WebClient
    {
        public WebClient()
        {

        }
       
        CookieContainer session;
        public WebClient(System.Net.CookieContainer session)
        {
            this.session = session;
        }



        HttpWebRequestBuilder create(string url)
        {
            if (session == null)
                return new HttpWebRequestBuilder(url).Date(DateTime.Now);
            else
                return new HttpWebRequestBuilder(url).Date(DateTime.Now).CookieContainer(session);
        }




        public bool Exists(string url)
        {
            try
            {
                HttpWebRequest request = create(url).Head().Request;

                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

        public string DoGet(string url, Func<HttpWebRequest, HttpWebRequest> preGetFilter = null)
        {
            var Request = create(url).Get().Request;

            if (preGetFilter != null)
                Request = preGetFilter(Request);

            var Response = Request.GetResponse();
            using (var streamReader = new StreamReader(Response.GetStreamForResponse()))
            {
                return streamReader.ReadToEnd();
            }

        }



      


        public string DoPost(string url, string body, string contentType = "application/x-www-form-urlencoded", Func<HttpWebRequest, HttpWebRequest> prePostFilter = null)
        {
            var Request = create(url).Post().ContentType(contentType).Request;

          

            if (prePostFilter != null)
                Request = prePostFilter(Request);

            if (!string.IsNullOrWhiteSpace(body))
            {
                using (var streamWriter = new StreamWriter(Request.GetRequestStream()))
                {

                    streamWriter.Write(body);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }


            var Response = Request.GetResponse();
            using (var streamReader = new StreamReader(Response.GetStreamForResponse()))
            {
                var response = streamReader.ReadToEnd();
                return response;
            }

        }


        public byte[] Download(string url)
        {
            var Request = create(url).Get().Request;
            var Response = Request.GetResponse();

            using (var ms = new MemoryStream())
            {
                Response.GetResponseStream().CopyTo(ms);
                Response.Close();
                return ms.ToArray();
            }



        }
    }
}
