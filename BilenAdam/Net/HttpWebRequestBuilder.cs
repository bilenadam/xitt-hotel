﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Net
{
    public class HttpWebRequestBuilder
    {
        public HttpWebRequest Request { get; }
        
        public HttpWebRequestBuilder(string url)
        {
            Request = (HttpWebRequest)WebRequest.Create(url);
        }

        public HttpWebRequestBuilder Build(Action<HttpWebRequest> action)
        {
            action(Request);
            return this;
        }

        public HttpWebRequestBuilder CookieContainer(CookieContainer cookieContainer) => Build(r => r.CookieContainer = cookieContainer);
        public HttpWebRequestBuilder Method(string method) => Build(r => r.Method = method);
        public HttpWebRequestBuilder Post() => Method(WebRequestMethods.Http.Post);
        public HttpWebRequestBuilder Get() => Method(WebRequestMethods.Http.Get);
        public HttpWebRequestBuilder Head() => Method(WebRequestMethods.Http.Head);
        public HttpWebRequestBuilder ContentType(string contentType) => Build(r => r.ContentType = contentType);
        public HttpWebRequestBuilder ApplicationContent() => ContentType("application/x-www-form-urlencoded");
        public HttpWebRequestBuilder Json() => ContentType("application/json");

        public HttpWebRequestBuilder Xml() => ContentType("text/xml");

    
        public HttpWebRequestBuilder Date(DateTime dateTime) => Build(r => r.Date = dateTime);

        public HttpWebRequestBuilder AddHeader(string name, string value) => Build(r => r.Headers.Add(name, value));

        public HttpWebRequestBuilder AcceptEncoding(string value) => AddHeader("Accept-Encoding", value);

        public HttpWebRequestBuilder AcceptZip() => AcceptEncoding("gzip, deflate");

        public HttpWebRequestBuilder BasicAuthorization(string username, string password)
            =>AddHeader("Authorization", "Basic " + System.Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password)));
         

    }
}
