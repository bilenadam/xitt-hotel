﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Net
{
    public class QueryStringBuilder
    {
        public NameValueCollection parameters = new NameValueCollection();

        public QueryStringBuilder(string name, string value)
        {
            parameters.Add(name, value);
        }

        public QueryStringBuilder Add(string name, string value)
        {
            parameters.Add(name, value);
            return this;
        }

        public override string ToString()
        {
            if (parameters.Count == 0)
                return String.Empty;

            StringBuilder sb = new StringBuilder();
           // sb.Append("?");
            bool first = true;
            foreach (var key in parameters.AllKeys)
            {
                foreach (var val in parameters.GetValues(key))
                {
                    if (!first)
                        sb.Append("&");

                    sb.AppendFormat("{0}={1}", WebUtility.UrlEncode(key), WebUtility.UrlEncode(val));

                    first = false;
                }
            }
            return sb.ToString();
        }
    }
}
