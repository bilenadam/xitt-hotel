﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Net
{
    public class HttpWebResponseUtil
    {
        public Stream GetStreamForResponse(HttpWebResponse webResponse)
        {
            switch (webResponse.ContentEncoding.ToUpperInvariant())
            {
                case "GZIP":
                    return new GZipStream(webResponse.GetResponseStream(), CompressionMode.Decompress);
                case "DEFLATE":
                    return new DeflateStream(webResponse.GetResponseStream(), CompressionMode.Decompress);

                default:
                    return  webResponse.GetResponseStream();
            }
        }

       
    }
}

public static class HttpWebResponseExtensions
{
    public static Stream GetStreamForResponse(this HttpWebResponse webResponse)
    {
        return new BilenAdam.Net.HttpWebResponseUtil().GetStreamForResponse(webResponse);
    }

    public static Stream GetStreamForResponse(this WebResponse webResponse)
    {
        if (webResponse is HttpWebResponse)
            return new BilenAdam.Net.HttpWebResponseUtil().GetStreamForResponse(webResponse as HttpWebResponse);
        else
            return webResponse.GetResponseStream();
    }
}
