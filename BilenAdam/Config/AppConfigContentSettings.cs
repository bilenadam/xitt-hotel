﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Config
{
    public class AppConfigContentSettings : ApplicationContentSettings
    {
        public const string AppConfigFolderKey = "app.content.configFolder";
        public string GetFolderPath()
        {
            return getOrAddCached(AppConfigFolderKey, k => Path.Combine(GetAppContentFolder(), "Config"));
        }

       
    }
}
