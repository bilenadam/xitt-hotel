﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Config
{
    public class AppHotelContentSettings : ApplicationContentSettings
    {
        public const string AppHotelFolderKey = "app.content.hotelFolder";

        public string GetFolderPath()
        {
            return getOrAddCached(AppHotelFolderKey, k => Path.Combine(GetAppContentFolder(), "Hotel"));
        }
    }
}
