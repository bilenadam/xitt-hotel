﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam
{
    public class ApplicationSettings
    {
        static System.Collections.Concurrent.ConcurrentDictionary<string, string> cache = new System.Collections.Concurrent.ConcurrentDictionary<string, string>();

      
        public const string NaturalLanguageKey = "app.naturalLanguage";
       

        protected string getOrAddCached(string key, Func<string,string> addFactory)
        {
            var value = cache.GetOrAdd(key, k =>
            {
                string v = addFactory(key);
                if (string.IsNullOrWhiteSpace(v))
                    return string.Empty;
                else
                    return v;
            });

            if (string.IsNullOrWhiteSpace(value))
                return null;
            else
                return value;
        }

        public string Get(string key, string defaultValue = null)
        {
           return getOrAddCached(key, k =>
           {
               if (System.Configuration.ConfigurationManager.AppSettings.HasKeys() && System.Configuration.ConfigurationManager.AppSettings.AllKeys.Contains(key))
                   return System.Configuration.ConfigurationManager.AppSettings.Get(key);
               else
                   return null;
           }) ?? defaultValue;

            
        }

        public T Get<T>(string key, T defaultValue, Func<string,T> parser)
        {
            try
            {
                return parser(Get(key));
            }
            catch
            {
                return defaultValue;
            }


        }



        public string GetNaturalLanguage(string defaultValue = null)
        {
            return Get(NaturalLanguageKey, defaultValue);
        }

        public System.Configuration.ConnectionStringSettings GetConnectionStringSettings(string name)
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings[name];
           
        }
        public string GetConnectionString(string name)
        {
            return getOrAddCached(name, k =>
            {
                var cs = System.Configuration.ConfigurationManager.ConnectionStrings[k];
                if (cs != null)
                    return cs.ConnectionString;
                else
                    return null;
            });

        }
    }
}
