﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Config
{
    public class AppDataContentSettings : ApplicationContentSettings
    {
        public const string AppFilesFolderKey = "app.content.dataFolder";

        public string GetFolderPath()
        {
            return getOrAddCached(AppFilesFolderKey, k => Path.Combine(GetAppContentFolder(), "Data"));
        }
    }
}
