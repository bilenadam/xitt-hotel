﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Config
{
    public class AppAirContentSettings : ApplicationContentSettings
    {
        public string GetAirContentFolder()
        {
            return getOrAddCached("app.content.airFolder", k => System.IO.Path.Combine(GetAppContentFolder(), "Air"));

        }

        public string GetAirLogoFolder()
        {
            return getOrAddCached("app.content.air.logoFolder", k => System.IO.Path.Combine(GetAirContentFolder(), "Logo"));

        }

        public string GetAirFullTextFolder()
        {
            return getOrAddCached("app.content.air.fulltextFolder", k => System.IO.Path.Combine(GetAirContentFolder(), "FullText"));

        }
    }
}
