﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Config
{
    public class ApplicationContentSettings : ApplicationSettings
    {
        public const string AppContentFolderKey = "app.contentFolder";
      
       
       
        public string GetAppContentFolder()
        {
            return Get(AppContentFolderKey, @"C:\BilenAdam\ApplicationContent");
        }


       


    }
}
