﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BilenAdam.Resources.Travel {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ReservationStatus {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ReservationStatus() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BilenAdam.Resources.Travel.ReservationStatus", typeof(ReservationStatus).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Canceled.
        /// </summary>
        public static string Canceled {
            get {
                return ResourceManager.GetString("Canceled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirmed.
        /// </summary>
        public static string Confirmed {
            get {
                return ResourceManager.GetString("Confirmed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Requested.
        /// </summary>
        public static string OnRequest {
            get {
                return ResourceManager.GetString("OnRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejected.
        /// </summary>
        public static string RequestRejected {
            get {
                return ResourceManager.GetString("RequestRejected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tickets Issued.
        /// </summary>
        public static string Ticket {
            get {
                return ResourceManager.GetString("Ticket", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Option.
        /// </summary>
        public static string TimeOption {
            get {
                return ResourceManager.GetString("TimeOption", resourceCulture);
            }
        }
    }
}
