﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BilenAdam.Resources.Travel {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Passenger {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Passenger() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BilenAdam.Resources.Travel.Passenger", typeof(Passenger).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passenger.
        /// </summary>
        public static string _Passenger {
            get {
                return ResourceManager.GetString("_Passenger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adult.
        /// </summary>
        public static string Adult {
            get {
                return ResourceManager.GetString("Adult", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Age.
        /// </summary>
        public static string Age {
            get {
                return ResourceManager.GetString("Age", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date Of Birth.
        /// </summary>
        public static string BirthDate {
            get {
                return ResourceManager.GetString("BirthDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Child.
        /// </summary>
        public static string Child {
            get {
                return ResourceManager.GetString("Child", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}. Pax Invalid Date of Birth. Child passengers must be older then 2 and younger then 12 at the time of flight.
        /// </summary>
        public static string ChildBirthDateError {
            get {
                return ResourceManager.GetString("ChildBirthDateError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Firstname.
        /// </summary>
        public static string FirstName {
            get {
                return ResourceManager.GetString("FirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}. Guest must be age of {1}.
        /// </summary>
        public static string HotelGuestAgeError {
            get {
                return ResourceManager.GetString("HotelGuestAgeError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Infant.
        /// </summary>
        public static string Infant {
            get {
                return ResourceManager.GetString("Infant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}. Pax Invalid Date of Birth. Infant passengers must be younger then 2  at the time of flight.
        /// </summary>
        public static string InfantBirthDateError {
            get {
                return ResourceManager.GetString("InfantBirthDateError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bay.
        /// </summary>
        public static string IsMale {
            get {
                return ResourceManager.GetString("IsMale", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lastname.
        /// </summary>
        public static string LastName {
            get {
                return ResourceManager.GetString("LastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Middle name.
        /// </summary>
        public static string MiddleName {
            get {
                return ResourceManager.GetString("MiddleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Military.
        /// </summary>
        public static string Military {
            get {
                return ResourceManager.GetString("Military", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mr.
        /// </summary>
        public static string Mr {
            get {
                return ResourceManager.GetString("Mr", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ms.
        /// </summary>
        public static string Ms {
            get {
                return ResourceManager.GetString("Ms", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passenger Informations.
        /// </summary>
        public static string PassengerInfo {
            get {
                return ResourceManager.GetString("PassengerInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passenger Type.
        /// </summary>
        public static string PassengerType {
            get {
                return ResourceManager.GetString("PassengerType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Passaport Number.
        /// </summary>
        public static string PassNo {
            get {
                return ResourceManager.GetString("PassNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Personal Information.
        /// </summary>
        public static string PersonalInfo {
            get {
                return ResourceManager.GetString("PersonalInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Seated Infant.
        /// </summary>
        public static string SeatedInfant {
            get {
                return ResourceManager.GetString("SeatedInfant", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Senior.
        /// </summary>
        public static string Senior {
            get {
                return ResourceManager.GetString("Senior", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}. Pax Invalid Date of Birth. Senior passengers must be older then 65 at the time of flight.
        /// </summary>
        public static string SeniorBirthDateError {
            get {
                return ResourceManager.GetString("SeniorBirthDateError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Student.
        /// </summary>
        public static string Student {
            get {
                return ResourceManager.GetString("Student", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TC No.
        /// </summary>
        public static string TCKNo {
            get {
                return ResourceManager.GetString("TCKNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Youth.
        /// </summary>
        public static string Youth {
            get {
                return ResourceManager.GetString("Youth", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0}. Pax Invalid Date of Birth. Young passengers must be younger than 24 at the time of flight..
        /// </summary>
        public static string YouthBirthDateError {
            get {
                return ResourceManager.GetString("YouthBirthDateError", resourceCulture);
            }
        }
    }
}
