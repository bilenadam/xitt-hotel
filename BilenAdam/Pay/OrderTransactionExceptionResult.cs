﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay
{
    public class OrderTransactionExceptionResult : IOrderTransactionResult
    {
        public OrderTransactionExceptionResult(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public OrderTransactionExceptionResult(Exception exc) : this(exc.Message)
        {

        }

        public bool Approved
        {
            get { return false; }
        }

        public string ErrorMessage
        {
            get;
            set;
        }
    }
}
