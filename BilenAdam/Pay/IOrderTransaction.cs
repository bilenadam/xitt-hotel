﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay
{
    public interface IOrderTransaction
    {
        IOrder Order { get; }
        IOrderTransactionResult Result { get; }
    }
}
