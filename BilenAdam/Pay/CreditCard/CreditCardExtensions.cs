﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public static class CreditCardExtensions
    {
        public static string MaskedCreditCardNumber(this BilenAdam.Pay.CreditCard.ICreditCard creditCard)
        {
            if (creditCard.CardNumber.Length > 12)
            {
                return string.Format("{0} * * {1}", creditCard.CardNumber.Substring(0, 4), creditCard.CardNumber.Substring(12));
            }
            else
            {
                return " * * ";
            }
        }
    }

