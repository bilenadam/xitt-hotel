﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay.CreditCard
{
    public interface ICreditCard 
    {
        string CardHolder { get; }
        string CardNumber { get; }
        string Cvv { get; }
        int ExpireMonth { get; }
        int ExpireYear { get; }
    }
}
