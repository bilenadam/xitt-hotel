﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay.CreditCard
{
    public interface ICreditCardInfo
    {
        string CardHolder { get;  }
        string MaskedCardNumber { get;  }
        string ExpireMonth { get;  }
        string ExpireYear { get;  }
    }
}
