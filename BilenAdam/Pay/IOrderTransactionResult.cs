﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay
{
    public interface IOrderTransactionResult
    {
        bool Approved { get; }
        string ErrorMessage { get; }
    }
}
