﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay
{
    public interface IOrderTransactionHandler<T> where T : IOrderTransaction
    {
        bool CanHandle(T transaction);
        void Handle(T transaction);
    }
}
