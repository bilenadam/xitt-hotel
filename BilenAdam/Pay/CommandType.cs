﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Pay
{
    public enum CommandType
    {
        PreAuth,
        PostAuth,
        VoidAuth,
        Cancel
    }
}
