﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Pay
{
    public interface ICommandResult
    {
         bool Approved { get;}
         string Message { get; }
    }
}
