﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Pay
{
    public interface IPostAuthOrderAgent
    {
        bool CanHandle(IOrder order);
        IOrderTransaction PostAuth(IOrder order);
        IOrderTransaction VoidAuth(IOrder order);
    }
}
