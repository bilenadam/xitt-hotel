﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Pay
{
    public enum OrderState
    {
        None,
        PreAuth,
        Auth,
        Void,
        Cancel
    }
}
