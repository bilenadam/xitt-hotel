﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Crm.Profile
{
    public abstract class ContactProfile : IContact
    {
        public int Id { get; set; }
        public abstract ContactType ContactType { get;  }
        public abstract string Name { get; }
        public bool IsActive { get; set; }

       

        public string Country { get; set; }

        public string City { get; set; }
    }
}
