﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Crm.Profile
{
    public class PersonProfile : ContactProfile, Reservation.Elements.IContactInformation, Reservation.Elements.IBillingInformation
    {

        
        public string Salutation
        {
            get;
            set;
        }


        
        public string Title
        {
            get;
            set;
        }

      
        public string Firstname
        {
            get;
            set;
        }

       
        public string Lastname
        {
            get;
            set;
        }

        public string Street
        {
            get;
            set;
        }

        public string StreetNo
        {
            get;
            set;
        }
        public string ZipCode
        {
            get;
            set;
        }
        
      
        public string Email
        {
            get;
            set;
        }
        public string Phone
        {
            get;
            set;
        }

        public bool IsCompany
        {
            get
            {
                return false;
            }
        }

        public string Address
        {
            get
            {
                return Street + " " + StreetNo;
            }
        }

       
        public override string Name
        {
            get
            {
                return Firstname + " " + Lastname;
            }
        }

        public override ContactType ContactType
        {
            get
            {
                return ContactType.Person;
            }
        }
    }
}
