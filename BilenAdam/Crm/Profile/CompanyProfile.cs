﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Crm.Profile
{
    public class CompanyProfile : ContactProfile, Reservation.Elements.IContactInformation, Reservation.Elements.IBillingInformation
    {

        public string Email
        {
            get;
            set;
        }

        public string CompanyName { get; set; }
        public ContactType CompanyType { get; set; }
       

        public string Phone
        {
            get;
            set;
        }
        public string Street
        {
            get;
            set;
        }

        public string StreetNo
        {
            get;
            set;
        }
        public string ZipCode
        {
            get;
            set;
        }

        public string Fax { get; set; }

        public string WebSite { get; set; }

        public string Ceo { get; set; }
        public string CeoTel { get; set; }

        public string Manager { get; set; }
        public string ManagerTel { get; set; }
        public DateTime StartedtoWork { get; set; }
        public string Notes { get; set; }

        public bool IsCompany
        {
            get
            {
                return true;
            }
        }

        public string Address
        {
            get
            {
                return Street + " " + StreetNo;
            }
        }

       

        public override string Name
        {
            get
            {
                return CompanyName;
            }
        }

        public override ContactType ContactType
        {
            get
            {
                return CompanyType;
            }
        }
    }
}
