﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Membership
{
    public interface IMembership
    {

        int? GetReferrerId();
        int? GetContactId();

        BilenAdam.Crm.ContactType GetContactType();

    }
}
