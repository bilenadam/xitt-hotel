﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Crm;

namespace BilenAdam.Membership
{
    public class NoMembership : IMembership
    {
        public int? GetUserId()
        {
            return null;
        }

        public int? GetContactId()
        {
            return null;
        }

        public ContactType GetContactType()
        {
            return BilenAdam.Crm.ContactType.Person;
        }

        public int? GetReferrerId()
        {
            return null;
        }
    }
}
