﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Crm
{
    public enum ContactType : byte
    {
        Person = 1,
        Company = 2,
        TravelAgency = 3,
        Broker = 4,
        Airline = 5,
        HandlingPartner = 6,
        Incoming = 7,
        TourOperator = 8,
        Hotel=9,
        SearchEngine = 10
    }
}
