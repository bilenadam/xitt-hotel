﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Crm
{
    
    public interface IContact
    {
        int Id { get; }
        string Name { get; }

        ContactType ContactType { get; }
    }
}
