﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BilenAdam.Data.Tracking
{
    public class EntityChange
    {
        public ChangeType ChangeType { get; set; }

        public string TableName { get; set; }
        public int Id { get; set; }
       
        public XElement Data { get; set; }
        
    }
}
