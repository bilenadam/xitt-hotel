﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.Tracking
{
    public class ChangeSetTracker
    {
        public IEnumerable<EntityChange> Track(ChangeSet cs)
        {
            List<EntityChange> changes = new List<EntityChange>();

            foreach (var item in cs.Deletes.OfType<ITrackable>())
            {
                var c = item.GetChange();
                c.ChangeType = ChangeType.Delete;
                changes.Add(c);
            }

            foreach (var item in cs.Inserts.OfType<ITrackable>())
            {
                var c = item.GetChange();
                c.ChangeType = ChangeType.Insert;
                changes.Add(c);
            }

            foreach (var item in cs.Updates.OfType<ITrackable>())
            {
                var c = item.GetChange();
                c.ChangeType = ChangeType.Update;
                changes.Add(c);
            }
            return changes;
        }
    }
}
