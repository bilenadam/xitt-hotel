﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.Tracking
{
    public enum ChangeType
    {
        Insert = 1,
        Update = 2,
        Delete = 3
    }
}
