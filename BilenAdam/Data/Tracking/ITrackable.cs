﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.Tracking
{
    public interface ITrackable
    {
        EntityChange GetChange();
    }
}
