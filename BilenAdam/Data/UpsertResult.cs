﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam
{
    public class UpsertResult<T>
    {


        public UpsertStatus Status { get; set; }
        public T Result { get; set; }
    }

    public enum UpsertStatus
    {
        // None,
        NoChange,
        Updated,
        Inserted,
        
    }
}
