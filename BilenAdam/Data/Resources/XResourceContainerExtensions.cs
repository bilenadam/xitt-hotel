﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.Resources;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq.Expressions;


public static class XResourceContainerExtensions
{
    public static string GetResource(this IXResourceContainer container, string key, string language)
    {
        return new XResourceUtil(container).GetResource(key, language);

    }

    public static IDictionary<string, string> GetTranslations(this IXResourceContainer container, string key)
    {
        return new XResourceUtil(container).GetTranslations(key);
    }



    public static void SetResource(this IXResourceContainer container, string key, string language, string value)
    {
        new XResourceUtil(container).SetResource(key, language, value);
    }

    public static string GetPropertyResouce<T>(this T container, Expression<Func<T, string>> propertyLambda, string language = null) where T : IXResourceContainer
    {
        return new GenericXResourceUtil<T>(container).GetPropertyResouce(propertyLambda, language ?? CultureInfoHelper.CurrentCultureInfo.TwoLetterISOLanguageName);
    }



    public static void SetPropertyResource<T>(this T container, Expression<Func<T, string>> propertyLambda, string language, string value) where T : IXResourceContainer
    {
        new GenericXResourceUtil<T>(container).SetPropertyResource(propertyLambda, language, value);
    }

    public static IDictionary<string, string> GetPropertyTranslations<T>(this T container, Expression<Func<T, string>> propertyLambda) where T : IXResourceContainer
    {
        return new GenericXResourceUtil<T>(container).GetPropertyTranslations(propertyLambda);
    }
}

