﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.Resources
{
    public class GenericXResourceUtil<T> : XResourceUtil where T : IXResourceContainer
    {
        public GenericXResourceUtil(T container) : base(container)
        {

        }

        public string GetPropertyResouce(Expression<Func<T, string>> propertyLambda, string language)
        {
            var expression = (MemberExpression)propertyLambda.Body;
            string name = expression.Member.Name;

            return container.GetResource(name, language) ?? propertyLambda.Compile().Invoke((T)container);
        }

        public void SetPropertyResource(Expression<Func<T, string>> propertyLambda, string language, string value) 
        {
            var expression = (MemberExpression)propertyLambda.Body;
            string name = expression.Member.Name;

            SetResource(name, language, value);
        }

        public IDictionary<string, string> GetPropertyTranslations(Expression<Func<T, string>> propertyLambda) 
        {
            var expression = (MemberExpression)propertyLambda.Body;
            string name = expression.Member.Name;

            return GetTranslations(name);
        }
    }
}
