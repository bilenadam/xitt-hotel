﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.Resources
{
    public interface INameResourceContainer : IXResourceContainer
    {
        string Name { get; }
    }
}
