﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.Resources
{
    public class NameResourceUtil : XResourceUtil
    {
        const string namePropertyName = "Name";

        public NameResourceUtil(INameResourceContainer container) : base(container)
        {

        }
        public string GetNameResouce( string language)
        {
            var l = language ?? System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName;

            return GetResource(namePropertyName, l) ?? ((INameResourceContainer)container).Name;
        }



        public void SetNameResource(string language, string value)
        {
            SetResource(namePropertyName, language, value);
        }

    }
}
