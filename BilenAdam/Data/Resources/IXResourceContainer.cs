﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BilenAdam.Data.Resources
{
    public interface IXResourceContainer
    {
        XElement xRes { get; set; }
    }
}
