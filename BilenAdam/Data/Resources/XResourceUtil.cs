﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Linq.Expressions;

namespace BilenAdam.Data.Resources
{
    public class XResourceUtil
    {
        public const string RESOURCES_ELEMENTNAME = "resources";
        public const string RESOURCE_ELEMENTNAME = "res";
        public const string KEY_ATTRIBUTE = "key";
        public const string LANGUAGE_ATTRIBUTE = "lan";

        protected readonly IXResourceContainer container;
        public XResourceUtil(IXResourceContainer resourceContainer)
        {
            container = resourceContainer;
        }

        private string getXpath(string l, string k)
        {
            return string.Format("/{0}[@{1}='{2}' and @{3}='{4}']", RESOURCE_ELEMENTNAME, KEY_ATTRIBUTE, k, LANGUAGE_ATTRIBUTE, l);
        }

        public  string GetResource(string key, string language)
        {

            var k = key.ToLowerInvariant();
            var l = language.ToLowerInvariant();

            if (container.xRes != null)
            {
                var e = container.xRes.XPathSelectElement(getXpath(l, k));

                if (e != null)
                    return e.Value;

            }

            return null;

        }

        public void SetResource( string key, string language, string value)
        {
            var k = key.ToLowerInvariant();
            var l = language.ToLowerInvariant();

            XElement xs = null;

            if (container.xRes == null)
                xs = new XElement(RESOURCES_ELEMENTNAME);
            else
                xs = new XElement(container.xRes);



            var xk = xs.XPathSelectElement(getXpath(l, k));
            if (xk == null)
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    xk = new XElement(RESOURCE_ELEMENTNAME, new XAttribute(KEY_ATTRIBUTE, k), new XAttribute(LANGUAGE_ATTRIBUTE, l), value);

                    xs.Add(xk);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    xk.SetValue(value);
                }
                else
                {
                    xk.Remove();
                }
            }



            container.xRes = xs;
        }

        public IDictionary<string, string> GetTranslations(string key)
        {

            var k = key.ToLowerInvariant();

            if (container.xRes != null)
            {
                var elements = container.xRes.XPathSelectElements(string.Format("/{0}[@{1}='{2}']", RESOURCE_ELEMENTNAME, KEY_ATTRIBUTE, k));

                if (elements != null && elements.Count() > 0)
                {
                    Dictionary<string, string> translations = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
                    foreach (var e in elements)
                    {
                        translations.Add(e.Attribute(LANGUAGE_ATTRIBUTE).Value, e.Value);
                    }
                    return translations;
                }

            }

            return null;
        }

    }
}
