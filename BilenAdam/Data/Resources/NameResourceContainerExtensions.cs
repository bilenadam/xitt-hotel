﻿using BilenAdam.Data.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class NameResourceContainerExtensions
    {
    public static string GetNamePropertyResouce<T>(this T container,  string language = null) where T : INameResourceContainer
    {
        return new NameResourceUtil(container).GetNameResouce(language ?? System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName);

    }



    public static void SetNamePropertyResource<T>(this T container,  string language, string value) where T : INameResourceContainer
    {
        new NameResourceUtil(container).SetNameResource(language, value);
    }
}

