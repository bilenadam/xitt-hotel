﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data
{
    public interface IXDataTable
    {
        System.Xml.Linq.XElement XData
        {
            get;
            set;
        }
    }
}
