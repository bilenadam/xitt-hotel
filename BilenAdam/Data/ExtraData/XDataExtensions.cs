﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Utilities.Serialization;
using System.Xml.Linq;


public static class XDataExtensions
{
    public static string GetXData(this BilenAdam.Data.IXDataView e, string propertyName)
    {
        if (e.XData != null && e.XData.Element(propertyName) != null)
            return e.XData.Element(propertyName).Value;
        else
            return null;
    }

    public static string GetXData(this BilenAdam.Data.IXDataTable e, string propertyName)
    {
        if (e.XData != null && e.XData.Element(propertyName) != null)
            return e.XData.Element(propertyName).Value;
        else
            return null;
    }

    public static void SetXData(this BilenAdam.Data.IXDataTable e, string propertyName, string data)
    {
        if (e.XData == null)
            e.XData = new System.Xml.Linq.XElement("XData");


        var clone = new XElement(e.XData);
        if (clone.Element(propertyName) != null)
        {
            clone.Element(propertyName).ReplaceWith(new XElement(propertyName, data));
        }
        else
        {
            clone.Add(new XElement(propertyName, data));
        }

        e.XData = new XElement(clone);
    }
}

