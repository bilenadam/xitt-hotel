﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.SqlClient
{
    public static class SqlCommandFactoryHelpers
    {
        public static SqlCommand AddTypedParameter(this SqlCommand cmd, string name, System.Data.SqlDbType sqlDbType, object value)
        {
            SqlParameter parameter = new SqlParameter(name, sqlDbType);
            parameter.Value = value;
            cmd.Parameters.Add(parameter);
            return cmd;
        }

        public static SqlCommand AddTypedParameter(this SqlCommand cmd, string name, System.Data.SqlDbType sqlDbType,int size, object value)
        {
            SqlParameter parameter = new SqlParameter(name, sqlDbType,size);
            parameter.Value = value;
            cmd.Parameters.Add(parameter);
            return cmd;
        }
        public static SqlCommand AddDBNullableParameter(this SqlCommand cmd, string name, System.Data.SqlDbType sqlDbType, string value)
        {
            SqlParameter parameter = new SqlParameter(name, sqlDbType);
            parameter.IsNullable = true;
            if (!string.IsNullOrWhiteSpace(value))
                parameter.Value = value;
            else
                parameter.Value = DBNull.Value;

            cmd.Parameters.Add(parameter);
            return cmd;
        }

        public static SqlCommand AddDBNullableParameter(this SqlCommand cmd, string name, System.Data.SqlDbType sqlDbType,int size, string value)
        {
            SqlParameter parameter = new SqlParameter(name, sqlDbType,size);
            parameter.IsNullable = true;
            if (!string.IsNullOrWhiteSpace(value))
                parameter.Value = value;
            else
                parameter.Value = DBNull.Value;

            cmd.Parameters.Add(parameter);
            return cmd;
        }

        public static SqlCommand AddDBNullableParameter<T>(this SqlCommand cmd, string name, System.Data.SqlDbType sqlDbType, Nullable<T> value) where T : struct
        {
            SqlParameter parameter = new SqlParameter(name, sqlDbType);
            parameter.IsNullable = true;
            if (value != null)
                parameter.Value = value.Value;
            else
                parameter.Value = DBNull.Value;

            cmd.Parameters.Add(parameter);
            return cmd;
        }
    }
}
