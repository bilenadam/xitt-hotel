﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace System.Linq
{
    public static class RepositoryExtensions
    {
        public static T GetorInsert<T>(this Table<T> table, T item, Func<T, bool> match) where T : class
        {
            var ext = table.FirstOrDefault(match);
            if (ext == null)
            {
                table.InsertOnSubmit(item);
                return item;
            }
            else
            {
                return ext;
            }
        }

        public static T GetorInsert<T>(this EntitySet<T> es, Func<T, bool> match, Func<T> objectFactory) where T : class
        {
            var ext = es.FirstOrDefault(match);
            if (ext == null)
            {
                ext = objectFactory();

                if (!es.Contains(ext))
                    es.Add(ext);

            }
            return ext;

        }
        public static T GetById<T>(this Table<T> table,  int id) where T:class
        {
            var itemParameter = Expression.Parameter(typeof(T), "item");

            Expression<Func<T, bool>> whereExpression = null;

       
                whereExpression = Expression.Lambda<Func<T, bool>>
                (
                Expression.Equal(Expression.Property(itemParameter, "Id"), Expression.Constant(id)
                ),
                new[] { itemParameter }
                );
            

            return table.Where(whereExpression).SingleOrDefault();
        }

        public static T GetByCode<T>(this Table<T> table, string code) where T : class
        {
            var itemParameter = Expression.Parameter(typeof(T), "item");

            Expression<Func<T, bool>> whereExpression = null;


            whereExpression = Expression.Lambda<Func<T, bool>>
            (
            Expression.Equal(Expression.Property(itemParameter, "Code"), Expression.Constant(code)
            ),
            new[] { itemParameter }
            );


            return table.Where(whereExpression).SingleOrDefault();
        }

        public static IQueryable<T> GetAllActive<T>(this Table<T> table) where T:class
        {
            if (typeof(T).GetProperty("IsActive")!= null)
            {

                var itemParameter = Expression.Parameter(typeof(T), "item");
                var whereExpression = Expression.Lambda<Func<T, bool>>
                    (
                    Expression.Equal(
                        Expression.Property(
                            itemParameter,
                            "IsActive"
                            ),
                        Expression.Constant(true)
                        ),
                    new[] { itemParameter }
                    );

                return table.Where(whereExpression);
            }
            else
            {
                return table;
            }
        }

     

      
    
       

        public static T DeleteByIdOnSubmit<T>(this Table<T> table, int ID) where T : class
        {
            T existingEntity = table.GetById(ID);
            table.DeleteOnSubmit(existingEntity);
            return existingEntity;
        }

        public static IEnumerable<T> DeleteAllOnSubmit<T>(this Table<T> table, Expression<Func<T, bool>> predicate) where T : class
        {
            var existingEntities = table.Where(predicate);
            table.DeleteAllOnSubmit(existingEntities);
            return existingEntities;
        }

        public static T DeleteFirstOnSubmit<T>(this Table<T> table, Expression<Func<T, bool>> predicate) where T : class
        {
            var existingEntity = table.FirstOrDefault(predicate);
            if (existingEntity == null)
                return null;
            table.DeleteOnSubmit(existingEntity);
            return existingEntity;
        }
        /// <summary>
        /// seki repository clasını kullanan kodları bulup değiştirmek çok yorucu
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataContext"></param>
        /// <returns></returns>
        public static Table<T> GetRepository<T>(this DataContext dataContext) where T : class
        {
            return dataContext.GetTable<T>();
        }


    }
}
