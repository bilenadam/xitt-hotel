﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterAddGuestsController : Controller
    {
        [HttpPost]
        public ActionResult Start(int hotelRoomId, int hotelCharterReservationId)
        {

            this.AddWizard(new AddGuestsWizard() { HotelCharterReservationId = hotelCharterReservationId, HotelCharterRoomId = hotelRoomId });
            return RedirectToAction("Index", new { hotelCharterReservationId = hotelCharterReservationId });
        }
        public ActionResult Index(int hotelCharterReservationId)
        {
            var w = this.GetWizard<AddGuestsWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);


            var room = db.HotelCharterRooms.FirstOrDefault(r => r.Id == w.HotelCharterRoomId);

            Models.AddGuests.AddGuestsModel model = new Models.AddGuests.AddGuestsModel
            {
                HotelCharterReservationId = w.HotelCharterReservationId,
                HotelCharterRoomId = w.HotelCharterRoomId,
                NewGuests = new List<Models.RoomGuestModel>(),
                ExistingGuests = room.HotelCharterGuests.ToArray(),
                NewGuestsCount = 1
            };

            if(w.NewGuests != null && w.NewGuests.Length > 0)
            {
                model.NewGuests.AddRange(w.NewGuests);
                model.NewGuestsCount = w.NewGuests.Length;
            }

            var total = 7 - model.ExistingGuests.Length;
            int travellerRef = room.HotelCharterReservation.HotelCharterRooms.Sum(r => r.HotelCharterGuests.Count())+1;
            while(model.NewGuests.Count < total)
            {
                model.NewGuests.Add(new Models.RoomGuestModel() { PaxRef = ++travellerRef });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(Models.AddGuests.AddGuestsModel model)
        {
            var w = this.GetWizard<AddGuestsWizard>(model.HotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            using (var db = this.CreateHotelDataContext())
            {

                var newGuests = model.NewGuests.Take(model.NewGuestsCount).ToArray();


                w.SetGuests(db, newGuests);

            }
            w.BeginSearch();
            return Redirect(this.Url.Nav<HotelCharterAmmendController>().Action(c => c.Index(w.HotelCharterReservationId), new { Id = w.HotelCharterReservationId }));
        }

        public ActionResult Cancel(int hotelCharterReservationId)
        {
            var w = this.GetWizard<AddGuestsWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            return Redirect(w.RedirectToReservation(this));
        }
    }
}
