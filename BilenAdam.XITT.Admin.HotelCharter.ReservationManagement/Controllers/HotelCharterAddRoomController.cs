﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterAddRoomController : Controller
    {
        [HttpPost]
        public ActionResult Start(int Id)
        {

            this.AddWizard(new AddRoomWizard() { HotelCharterReservationId = Id });
            return RedirectToAction("Index", new { hotelCharterReservationId = Id });
        }
        public ActionResult Index(int hotelCharterReservationId)
        {
            var w = this.GetWizard<AddRoomWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            

            Models.AddRoom.AddRoomModel model = new Models.AddRoom.AddRoomModel
            {
                HotelCharterReservationId = w.HotelCharterReservationId,
                NewGuests = new List<Models.RoomGuestModel>(),
                NewGuestsCount = 1
            };

            if (w.NewGuests != null && w.NewGuests.Length > 0)
            {
                model.NewGuests.AddRange(w.NewGuests);
                model.NewGuestsCount = w.NewGuests.Length;
            }

            var total = 5;
            int travellerRef = db.HotelCharterReservations.First(r=>r.Id ==  hotelCharterReservationId).HotelCharterRooms.Sum(r => r.HotelCharterGuests.Count())+1;
            while (model.NewGuests.Count < total)
            {
                model.NewGuests.Add(new Models.RoomGuestModel() { PaxRef = ++travellerRef });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(Models.AddRoom.AddRoomModel model)
        {
            var w = this.GetWizard<AddRoomWizard>(model.HotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            using (var db = this.CreateHotelDataContext())
            {

                var newGuests = model.NewGuests.Take(model.NewGuestsCount).ToArray();


                w.SetGuests(db, newGuests);

            }
            w.BeginSearch();
            return Redirect(this.Url.Nav<HotelCharterAmmendController>().Action(c => c.Index(w.HotelCharterReservationId), new {  Id = w.HotelCharterReservationId  }));
        }

        public ActionResult Cancel(int hotelCharterReservationId)
        {
            var w = this.GetWizard<AddRoomWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();

            this.RemoveWizard(w);
            return this.Redirect2Action<HomeController>(c => c.Index(hotelCharterReservationId), new { Id = hotelCharterReservationId });
        }
    }
}
