﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterMergeRoomsController : Controller
    {
        [HttpPost]
        public ActionResult Start(int Id)
        {
            
            this.AddWizard(new MergeRoomsWizard() { HotelCharterReservationId = Id });
            return RedirectToAction("Index", new { hotelCharterReservationId = Id });
        }
        [HttpGet]
        public ActionResult Index(int hotelCharterReservationId)
        {
            var w = this.GetWizard<MergeRoomsWizard>(hotelCharterReservationId);

            if (w == null)
                return RedirectToAction("Start", new { Id = hotelCharterReservationId });


            var db = this.CreateHotelDataContext();
         


            Models.MergeRooms.RoomSelectionList model = new Models.MergeRooms.RoomSelectionList
            {
                 HotelCharterReservationId = w.HotelCharterReservationId ,
                Items = new List<Models.RoomSelectionItem>()
            };



            var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == w.HotelCharterReservationId);

            if (rez.HotelCharterRooms.Count(r => r.CancelTimeUTC == null) > 2)
            {

                foreach (var room in rez.HotelCharterRooms)
                {
                    model.Items.Add(new Models.RoomSelectionItem { Room = room, IsSelected = false });
                }


                ViewData.SetDB(db);
                return View(model);
            }
            else
            {
                var roomIds = rez.HotelCharterRooms.Where(r => r.CancelTimeUTC == null).Select(r => r.Id).ToArray();

                w.SelectRooms(db, roomIds);
                db.Dispose();
                w.BeginSearch();
                return Redirect(this.Url.Nav<HotelCharterAmmendController>().Action(c => c.Index(w.HotelCharterReservationId), new {  Id = w.HotelCharterReservationId  }));
            }
        }

        [HttpPost]
        public ActionResult Index(Models.MergeRooms.RoomSelectionList model)
        {
            var w = this.GetWizard<MergeRoomsWizard>(model.HotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();


            w.SelectRooms(db, model.Items.Where(i => i.IsSelected).Select(i => i.Room.Id).ToArray());
            var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == w.HotelCharterReservationId);
            if (rez.HotelCharterRooms.Where(r=>w.SelectedRoomIds.Contains(r.Id)).SelectMany(r=>r.HotelCharterGuests).Count(g=>g.IsAdult) > 3)
            {
              
                foreach (var item in model.Items)
                {
                    item.Room = rez.HotelCharterRooms.FirstOrDefault(g => g.Id == item.Room.Id);
                }
                ViewData.SetDB(db);
                return View();
            }
            else
            {
                db.Dispose();
                w.BeginSearch();
                return Redirect(w.RedirectToAmmendReservation(this));
            }

        }

        public ActionResult Cancel(int hotelCharterReservationId)
        {
            var w = this.GetWizard<MergeRoomsWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();

            return Redirect(w.RedirectToReservation(this));
        }

    }
}
