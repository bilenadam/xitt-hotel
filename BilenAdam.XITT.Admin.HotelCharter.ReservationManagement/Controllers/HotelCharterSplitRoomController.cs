﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;


namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterSplitRoomController : Controller
    {
        [HttpPost]
        public ActionResult Start(int hotelRoomId, int hotelCharterReservationId)
        {
            
            this.AddWizard(new SplitRoomWizard() { HotelCharterReservationId = hotelCharterReservationId, HotelCharterRoomId = hotelRoomId });
            return RedirectToAction("Index", new { hotelCharterReservationId = hotelCharterReservationId });
        }
        public ActionResult Index(int hotelCharterReservationId)
        {
            var w = this.GetWizard<SplitRoomWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);


            Models.SplitRoom.GuestSelectionList model = new Models.SplitRoom.GuestSelectionList
            {
                 HotelCharterReservationId = w.HotelCharterReservationId ,
                HotelCharterRoomId = w.HotelCharterRoomId,
                Items = new List<Models.GuestSelectionItem>()
            };



            var room = db.HotelCharterRooms.FirstOrDefault(r => r.Id == w.HotelCharterRoomId);

            foreach (var g in room.HotelCharterGuests)
            {
                model.Items.Add(new Models.GuestSelectionItem { Guest = g, IsSelected = false });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(Models.SplitRoom.GuestSelectionList model)
        {

            var w = this.GetWizard<SplitRoomWizard>(model.HotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();


            w.SelectGuests(db, model.Items.Where(i => i.IsSelected).Select(i => i.Guest.Id).ToArray());

            if (w.SearchQuery.Rooms.Any(r => r.AdultCount == 0))
            {
                ModelState.AddModelError("", Resources.HotelCharterWorkflowResources.EveryRoomMustHaveAnAdult);

                foreach (var item in model.Items)
                {
                    item.Guest = db.HotelCharterGuests.FirstOrDefault(g => g.Id == item.Guest.Id);
                }
                ViewData.SetDB(db);
                return View();
            }
            else
            {
                db.Dispose();
                w.BeginSearch();
                return Redirect(this.Url.Nav<HotelCharterAmmendController>().Action(c => c.Index(w.HotelCharterReservationId), new {  Id = w.HotelCharterReservationId  }));
            }

        }

        public ActionResult Cancel(int hotelCharterReservationId)
        {
            var w = this.GetWizard<SplitRoomWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();

            this.RemoveWizard<BaseWizard>(w);
            return this.Redirect2Action<HomeController>(c => c.Index(hotelCharterReservationId), new { Id = hotelCharterReservationId });
        }
    }
}
