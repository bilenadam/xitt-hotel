﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterChangeHotelController : Controller
    {
        [HttpPost]
        public ActionResult Start( int Id)
        {

            this.AddWizard(new ChangeHotelWizard() { HotelCharterReservationId = Id });
            return RedirectToAction("Index", new { Id = Id });
        }

        public ActionResult Index(int Id)
        {
            var w = this.GetWizard<ChangeHotelWizard>(Id);

            if (w == null)
            {
                w = new ChangeHotelWizard() { HotelCharterReservationId = Id };
                this.AddWizard(w);
            }


           
                w.CreateSearchQuery();
                w.BeginSearch();
                return Redirect(w.RedirectToAmmendReservation(this));
            
        }

    }
}
