﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterRemoveGuestsController : Controller
    {
        [HttpPost]
        public ActionResult Start(int hotelRoomId, int hotelCharterReservationId)
        {

            this.AddWizard(new RemoveGuestsWizard() { HotelCharterReservationId = hotelCharterReservationId, HotelCharterRoomId = hotelRoomId });
            return RedirectToAction("Index", new { hotelCharterReservationId = hotelCharterReservationId });
        }

        public ActionResult Index(int hotelCharterReservationId)
        {
            var w = this.GetWizard<RemoveGuestsWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);


            Models.RemoveGuests.GuestSelectionList model = new Models.RemoveGuests.GuestSelectionList
            {
                HotelCharterReservationId = w.HotelCharterReservationId,
                HotelCharterRoomId = w.HotelCharterRoomId,
                Items = new List<Models.GuestSelectionItem>()
            };



            var room = db.HotelCharterRooms.FirstOrDefault(r => r.Id == w.HotelCharterRoomId);

            foreach (var g in room.HotelCharterGuests)
            {
                model.Items.Add(new Models.GuestSelectionItem { Guest = g, IsSelected = false });
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(Models.RemoveGuests.GuestSelectionList model)
        {

            var w = this.GetWizard<RemoveGuestsWizard>(model.HotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


            var db = this.CreateHotelDataContext();


            w.SelectGuests(db, model.Items.Where(i => i.IsSelected).Select(i => i.Guest.Id).ToArray());

            if (w.SearchQuery.Rooms.Any(r => r.AdultCount == 0))
            {
                ModelState.AddModelError("", Resources.HotelCharterWorkflowResources.EveryRoomMustHaveAnAdult);

                foreach (var item in model.Items)
                {
                    item.Guest = db.HotelCharterGuests.FirstOrDefault(g => g.Id == item.Guest.Id);
                }
                ViewData.SetDB(db);
                return View();
            }
            else
            {
                db.Dispose();
                w.BeginSearch();
                return Redirect(w.RedirectToAmmendReservation(this));
            }

        }

        public ActionResult Cancel(int hotelCharterReservationId)
        {
            var w = this.GetWizard<RemoveGuestsWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();

            return Redirect(w.RedirectToReservation(this));
        }
    }
}
