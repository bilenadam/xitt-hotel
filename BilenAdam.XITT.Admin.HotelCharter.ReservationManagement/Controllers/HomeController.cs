﻿using BilenAdam.Data.HotelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Sync(string pnr, string returnUrl)
        {
            //var hotelCharterReservationId = PnrHelper.ParseHotelCharterPnr(pnr);
            //using (var hdb = BilenAdam.Data.HotelData.HotelDataContextFactory.Create())
            //using (var tdb = BilenAdam.Data.TravelData.TravelDataContextFactory.Create())
            //{
            //    var charterRez = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == hotelCharterReservationId);
            //    var hotelRez = tdb.HotelReservations.FirstOrDefault(r => r.Pnr == pnr);

            //    new Hotels.Charter.Reservation.Sync.ReservationSync().Transfer(charterRez, hotelRez);
            //    tdb.SubmitChanges();

            //}

            return Redirect(returnUrl);
        }
        public ActionResult Show(string pnr)
        {
            try
            {
                return RedirectToAction("Index", new { Id = PnrHelper.ParseHotelCharterPnr(pnr) });
            }
            catch
            {
                return HttpNotFound();
            }
        }
        public ActionResult Index( int Id)
        {
            var db = HotelDataContextFactory.Create();

           

            var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == Id);
            if (rez == null)
                return HttpNotFound();

            ViewData.SetDB(db);
            return View(rez);
        }

      
        public ActionResult AddTransfer(int Id)
        {
            new Hotels.Charter.Reservation.Workflow.AddTransferActivity().AddTransfer(Id);
            return RedirectToAction("Index", new { Id = Id });
        }

        public ActionResult UpdateDates(int Id, DateTime checkIn, DateTime checkOut)
        {

            new Hotels.Charter.Reservation.Workflow.ChangeDatesActivity().ChangeDates(Id, checkIn, checkOut);
            return RedirectToAction("Index", new { Id = Id });
        }

        public ActionResult UpdateGuest(int guestId, string name, string lastname)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var guest = db.HotelCharterGuests.FirstOrDefault(g => g.Id == guestId && g.HotelCharterRoom.CancelTimeUTC == null);

                if (guest == null)
                    return HttpNotFound();

                guest.FirstName = name;
                guest.LastName = lastname;

                db.SubmitChanges();

                return RedirectToAction("Index", new { Id = guest.HotelCharterRoom.HotelCharterRezervationId });
            }
        }

        public ActionResult AddMarkup(int roomId, decimal markup)
        {
            var rezId = new Hotels.Charter.Reservation.Workflow.AddMarkupActivity().AddMarkup(roomId, markup);
            return RedirectToAction("Index", new { Id = rezId });
        }

        [HttpPost]
        public ActionResult SetSupplier(int reservationid, int suppliercontactid)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == reservationid);
                rez.SupplierContactId = suppliercontactid;
                db.SubmitChanges();
                return RedirectToAction("Index", new { Id = reservationid });
            }
        }
    }
}