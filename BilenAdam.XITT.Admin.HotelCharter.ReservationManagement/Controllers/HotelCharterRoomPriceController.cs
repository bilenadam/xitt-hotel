﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterRoomPriceController : Controller
    {
        [HttpPost]
        public ActionResult SetTotalDayRate(int roomId, decimal totalDayRate)
        {

            var id = new BilenAdam.Hotels.Charter.Reservation.Workflow.SetRoomTotalDayRatesActivity().SetTotalDayRates(roomId, totalDayRate);
            return this.Redirect2Action<HomeController>(c => c.Index(id), new { Id = id });
        }

        [HttpPost]
        public ActionResult SetServiceFeeAmount(int itemid, decimal amount)
        {

            var rezid = new BilenAdam.Hotels.Charter.Reservation.Workflow.SetRoomServiceFeeActivity().SetServiceFee(itemid, amount);
            return this.Redirect2Action<HomeController>(c => c.Index(rezid), new { Id = rezid });
        }
    }
}