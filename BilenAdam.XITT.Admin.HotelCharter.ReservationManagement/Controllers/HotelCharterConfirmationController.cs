﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Confirmation;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterConfirmationController : Controller
    {
        [HttpPost]
        public ActionResult Confirm(int Id)
        {
           new Wizard( Id).Confirm(User.Identity.Name);
            
            

            return this.Redirect2Action<HomeController>(c => c.Index(Id),new { Id = Id});

        }
    }
}
