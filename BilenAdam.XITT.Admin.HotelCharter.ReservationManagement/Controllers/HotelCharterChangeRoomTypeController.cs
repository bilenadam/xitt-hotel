﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterChangeRoomTypeController : Controller
    {
        [HttpPost]
        public ActionResult Start(int hotelRoomId, int hotelCharterReservationId)
        {

            this.AddWizard(new ChangeRoomTypeWizard() { HotelCharterReservationId = hotelCharterReservationId, HotelCharterRoomId = hotelRoomId });
            return RedirectToAction("Index", new { hotelCharterReservationId = hotelCharterReservationId });
        }

        public ActionResult Index(int hotelCharterReservationId)
        {
            var w = this.GetWizard<ChangeRoomTypeWizard>(hotelCharterReservationId);

            if (w == null)
                return HttpNotFound();


          
                w.CreateSearchQuery();
                w.BeginSearch();
                return Redirect(this.Url.Nav<HotelCharterAmmendController>().Action(c => c.Index(w.HotelCharterReservationId), new { Id = w.HotelCharterReservationId }));
            
        }
    }
}
