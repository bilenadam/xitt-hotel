﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement
{
    public static class ControllerExtensions
    {



        public static BilenAdam.Data.HotelData.HotelDataContext CreateHotelDataContext(this Controller controller, DataLoadOptions loadOptions = null)
        {
            return Data.HotelData.HotelDataContextFactory.Create(loadOptions);
        }

     

        public static T GetWizard<T>(this Controller controller, int CharterReservationId) where T : IHotelCharterActivityWizard
        {
            return new HotelCharterActivityWizardStorage<T>(controller).GetWizard(CharterReservationId);
        }
        public static bool AddWizard<T>(this Controller controller, T wizard) where T : IHotelCharterActivityWizard
        {
            return new HotelCharterActivityWizardStorage<T>(controller).AddWizard(wizard);
        }

        public static void RemoveWizard<T>(this Controller controller, T wizard) where T : IHotelCharterActivityWizard
        {
            new HotelCharterActivityWizardStorage<T>(controller).RemoveWizard(wizard);
        }
    }
}
