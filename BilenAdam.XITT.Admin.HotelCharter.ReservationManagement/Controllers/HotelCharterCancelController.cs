﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterCancelController : Controller
    {
        [HttpPost]
        public ActionResult CancelReservation(int Id, decimal refundRatio)
        {
            if(refundRatio >= 0 && refundRatio <= 1)
                new BilenAdam.Hotels.Charter.Reservation.Workflow.CancelBookingActivity().CancelBooking(Id, User.Identity.Name,1-refundRatio);
            return this.Redirect2Action<HomeController>(c => c.Index(Id), new { Id = Id });
        }

        [HttpPost]
        public ActionResult CancelRoom(int Id, decimal refundRatio)
        {
            if (refundRatio >= 0 && refundRatio <= 1)
                new BilenAdam.Hotels.Charter.Reservation.Workflow.CancelBookingActivity().CancelRoom(Id, User.Identity.Name,1-refundRatio);
            using (var db = this.CreateHotelDataContext())
            {
                var room = db.HotelCharterRooms.FirstOrDefault(r => r.Id == Id);

                return this.Redirect2Action<HomeController>(c => c.Index(Id), new { Id = room.HotelCharterRezervationId });

            }


        }
    }
}