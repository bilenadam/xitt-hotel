﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers
{
    public class HotelCharterAmmendController : Controller
    {
        [HttpGet]
        public ActionResult Index(int Id)
        {
            var w = this.GetWizard<BaseWizard>(Id);
            if (w == null)
                return HttpNotFound();


            w.searchCompleted.Wait();

          

            return View(w);
        }
        [HttpPost]
        public ActionResult Index(int Id, string propertyProductId)
        {
            var w = this.GetWizard<BaseWizard>(Id);
            if (w == null)
                return HttpNotFound();

           return Redirect(w.SelectProduct(this,propertyProductId));
        }

        [HttpGet]
        public ActionResult Cancel(int Id)
        {
            var w = this.GetWizard<BaseWizard>(Id);
            if (w == null)
                return HttpNotFound();

            this.RemoveWizard<BaseWizard>(w);
            return this.Redirect2Action<HomeController>(c => c.Index(Id), new { Id = Id });
        }
    }
}
