﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Models.AddRoom
{
    public class AddRoomModel
    {
        public int HotelCharterReservationId { get; set; }
        public int NewGuestsCount { get; set; }
        public List<RoomGuestModel> NewGuests { get; set; }
    }
}
