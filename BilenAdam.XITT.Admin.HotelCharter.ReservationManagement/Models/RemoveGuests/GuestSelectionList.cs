﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Models.RemoveGuests
{
    public class GuestSelectionList
    {
        public int HotelCharterReservationId { get; set; }
        public int HotelCharterRoomId { get; set; }
        public List<GuestSelectionItem> Items { get; set; }
    }
}
