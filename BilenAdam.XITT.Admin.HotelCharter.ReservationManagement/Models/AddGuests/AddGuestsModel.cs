﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Models.AddGuests
{
    public class AddGuestsModel
    {
        public int HotelCharterReservationId { get; set; }
        public int HotelCharterRoomId { get; set; }
        public int NewGuestsCount { get; set; }
        public BilenAdam.Data.HotelData.HotelCharterGuest[] ExistingGuests { get; set; }
        public List<RoomGuestModel> NewGuests { get; set; }
    }
}
