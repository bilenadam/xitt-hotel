﻿using BilenAdam.Reservation.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Elements;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Models
{
    public class RoomGuestModel : IRoomGuest, ITraveller
    {
        public int? AssociatedTraveller
        {
            get
            {
                return null;
            }
        }

        public DateTime? Dob
        {
            get;set;
        }

        public string FirstName
        {
            get; set;
        }

        public RoomGuestType GuestType
        {
            get;set;
        }

        public bool isMale
        {
            get; set;
        }

        public string LastName
        {
            get; set;
        }

        public string MiddleName
        {
            get; set;
        }

        public IPassport Passport
        {
            get
            {
                return null;
            }
        }

        public int PaxRef
        {
            get; set;
        }

      

        public ITraveller GetTraveller()
        {
            return this;
        }


    }
}
