﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Models.MergeRooms
{
    public class RoomSelectionList
    {
        public int HotelCharterReservationId { get; set; }
        public List<RoomSelectionItem> Items { get; set; }
    }
}
