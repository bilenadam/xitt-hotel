﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Models
{
    public class RoomSelectionItem
    {
        public bool IsSelected { get; set; }

        public BilenAdam.Data.HotelData.HotelCharterRoom Room { get; set; }
    }
}
