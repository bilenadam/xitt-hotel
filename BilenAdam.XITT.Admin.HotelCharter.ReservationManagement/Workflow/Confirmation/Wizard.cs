﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using System.Data.Linq;
using System.Web.Mvc;
using BilenAdam.Hotels.Charter.Reservation.Workflow;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Confirmation
{
    public class Wizard : IHotelCharterActivityWizard
    {

        public int HotelCharterReservationId
        {
            get;
            private set;
        }


        public Wizard(int hotelCharterReservationId)
        {
            this.HotelCharterReservationId = hotelCharterReservationId;
        }




        public void Confirm(string username)
        {
            new ConfirmBookingActivity().ConfirmBooking(HotelCharterReservationId, username);
        }



    }
}
