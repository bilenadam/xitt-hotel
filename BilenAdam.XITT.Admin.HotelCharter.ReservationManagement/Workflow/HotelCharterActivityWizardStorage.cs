﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow
{
    public class HotelCharterActivityWizardStorage<T> : BilenAdam.Web.Worklfow.SessionStorage.WizardSessionStorage<IHotelCharterActivityWizard> where T : IHotelCharterActivityWizard
    {
        public HotelCharterActivityWizardStorage(Controller controller) :base(controller.Session)
        {

        }

        public virtual T GetWizard(int HotelCharterReservationId)
        {
            return  (T)base.GetWizard(HotelCharterReservationId.ToString());
        }

        public virtual bool AddWizard(T item)
        {
            return base.AddWizard(item.HotelCharterReservationId.ToString(), item);
        }

        public virtual void RemoveWizard(T item)
        {
            base.RemoveWizard(item.HotelCharterReservationId.ToString());
        }
    }
}
