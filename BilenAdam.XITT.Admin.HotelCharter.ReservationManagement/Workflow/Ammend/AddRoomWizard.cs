﻿using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using BilenAdam.Hotels.Charter.Reservation.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend
{
    public class AddRoomWizard : BaseWizard
    {
        public Models.RoomGuestModel[] NewGuests { get; set; }

        public PropertyProduct SelectedProduct;

        public void SetGuests(BilenAdam.Data.HotelData.HotelDataContext db, IEnumerable<Models.RoomGuestModel> newGuests)
        {
            NewGuests = newGuests.ToArray();

            var room = db.HotelCharterRooms.FirstOrDefault(ro => ro.HotelCharterRezervationId == HotelCharterReservationId && ro.CancelTimeUTC == null);

            SearchQuery = new Query.SearchQuery
            {
                HotelId = room.HotelRoom.HotelId,
                CheckIn = room.CheckIn,
                CheckOut = room.CheckOut,
                DestinationId = null,
                MarketId = room.HotelCharterReservation.MarketId,
                ReservationDate = room.HotelCharterReservation.CreateTimeUTC.Date,
                Rooms = new Query.RoomPlan[] {
                    new Query.RoomPlan { AdultCount = 0, RoomRef = 1 }
                },
                IncludeHotelTransfer = room.HotelCharterRoomServiceFees.Any(s => s.Code == "TransferFee"),
                IsPackageReservation = false
            };

            var r = SearchQuery.Rooms[0];


            foreach (var guest in newGuests)
            {



                if (r.AdultCount == 0)
                    guest.GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.SinglePax;
                else if (r.AdultCount == 1)
                {
                    NewGuests[0].GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.DoublePerPax;
                    guest.GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.DoublePerPax;
                }
                else
                    guest.GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.AddPax;


                if (guest.Dob == null || guest.Dob.Value < SearchQuery.CheckIn.AddYears(-15))
                {
                    r.AdultCount++;
                }
                else
                {
                    guest.GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.Child;
                    var age = room.CheckOut.Year - guest.Dob.Value.Year;

                    if (r.FirstChildAge == null || r.FirstChildAge > age)
                    {
                        r.ThirdChildAge = r.SecondChildAge;
                        r.SecondChildAge = r.FirstChildAge;
                        r.FirstChildAge = age;
                    }
                    else if (r.SecondChildAge > age || r.SecondChildAge == null)
                    {
                        r.ThirdChildAge = r.SecondChildAge;
                        r.SecondChildAge = age;
                    }
                    else if (r.ThirdChildAge > age || r.ThirdChildAge == null)
                    {
                        r.ThirdChildAge = age;
                    }
                    else
                    {
                        r.AdultCount++;

                        if (r.AdultCount > 2)
                            guest.GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.AddPax;
                        else
                            guest.GuestType = BilenAdam.Reservation.Hotels.RoomGuestType.DoublePerPax;
                    }
                }
            }
        }



        public override string SelectProduct(Controller controller, string productId)
        {
            SelectedProduct = SearchResults.FirstOrDefault(r => r.ProductId == productId);

            new AddRoomActivity().AddGuests(HotelCharterReservationId, SelectedProduct, NewGuests);
            base.SyncToTravelData();
            return base.RedirectToReservation(controller);

        }
    }
}
