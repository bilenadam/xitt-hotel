﻿using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using BilenAdam.Hotels.Charter.Reservation.Workflow;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend
{
    public class ChangeHotelWizard : BaseWizard
    {
        public PropertyProduct SelectedProduct;

        public void CreateSearchQuery()
        {
            using (var db = BilenAdam.Data.HotelData.HotelDataContextFactory.Create())
            {


                var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == HotelCharterReservationId);

                SearchQuery = new Query.SearchQuery
                {
                    CheckIn = rez.GetCheckIn(),
                    CheckOut = rez.GetCheckOut(),
                    DestinationId = rez.Hotel.HotelDestionationId,
                    MarketId = rez.MarketId,
                    ReservationDate = rez.CreateTimeUTC.Date,
                    Rooms = new Query.RoomPlan[] {
                    new Query.RoomPlan { AdultCount = 0, RoomRef = 1 }
                },
                    IncludeHotelTransfer = rez.HotelCharterRooms.Any(r => r.HasTransferFee() && r.CancelTimeUTC == null),
                    IsPackageReservation = false
                };
                var checkout = rez.GetCheckOut();
                foreach (var guest in rez.HotelCharterRooms.Where(r => r.CancelTimeUTC == null).SelectMany(r => r.HotelCharterGuests))
                {
                    Query.RoomPlan r = SearchQuery.Rooms[0];
                    if (guest.IsAdult)
                    {
                        r.AdultCount++;
                    }
                    else
                    {

                        if (r.FirstChildAge == null)
                            r.FirstChildAge = checkout.Year - guest.Dob.Year;
                        else if (r.SecondChildAge == null)
                            r.SecondChildAge = checkout.Year - guest.Dob.Year;
                        else
                            r.ThirdChildAge = checkout.Year - guest.Dob.Year;
                    }
                }
            }

        }
        public override string SelectProduct(Controller controller, string productId)
        {

            SelectedProduct = SearchResults.FirstOrDefault(r => r.ProductId == productId);

            new ChangeHotelActivity().ChangeHotel(HotelCharterReservationId, SelectedProduct);
            base.SyncToTravelData();
            return base.RedirectToReservation(controller);
        }




    }
}
