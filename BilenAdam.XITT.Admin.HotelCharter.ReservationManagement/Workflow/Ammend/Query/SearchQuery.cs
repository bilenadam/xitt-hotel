﻿using BilenAdam.Reservation.Search.Hotels.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Hotels.Query.Filters;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend.Query
{
    public class SearchQuery : IHotelQuery
    {
        public int MarketId { get; set; }
       
        public int? DestinationId { get; set; }

        public int? HotelId { get; set; }

        public DateTime ReservationDate { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }

        public RoomPlan[] Rooms { get; set; }

        public bool IncludeHotelTransfer { get; set; }

        public bool IsPackageReservation { get; set; }

        string IHotelQuery.Destination
        {
            get
            {
                return DestinationId?.ToString();
            }
        }

        DateTime IHotelQuery.CheckIn
        {
            get
            {
                return CheckIn;
            }
        }

        DateTime IHotelQuery.CheckOut
        {
            get
            {
                return CheckOut;
            }
        }

        bool IHotelQuery.IncludeHotelTransfer
        {
            get
            {
                return IncludeHotelTransfer;
            }
        }

        bool IHotelQuery.IsPackageSearch
        {
            get
            {
                return IsPackageReservation;
            }
        }

        CurrencyCode IHotelQuery.Currency
        {
            get
            {
                return CurrencyCode.EUR;
            }
        }

        IEnumerable<IRoomPlan> IHotelQuery.GetRooms()
        {
            return Rooms;
        }

        IEnumerable<IHotelFilter> IHotelQuery.GetHotelFilters()
        {
            if (HotelId != null)
                yield return new HotelNameFilter { HotelId = HotelId.Value.ToString() };
        }
    }
}
