﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend.Query
{
    public class RoomPlan : BilenAdam.Reservation.Search.Hotels.Query.IRoomPlan
    {
        public int AdultCount { get; set; }

        public int RoomRef
        {
            get; set;
        }

        public IEnumerable<int> ChildAges
        {
            get
            {
                if (ThirdChildAge != null && SecondChildAge != null && FirstChildAge != null)
                    return new int[] { FirstChildAge.Value, SecondChildAge.Value, ThirdChildAge.Value };
                else if (SecondChildAge != null && FirstChildAge != null)
                    return new int[] { FirstChildAge.Value, SecondChildAge.Value };
                else if (FirstChildAge != null)
                    return new int[] { FirstChildAge.Value };
                else
                    return new int[] { };
            }
        }

        public int ChildCount
        {
            get
            {
                if (ThirdChildAge != null)
                    return 3;
                else if (SecondChildAge != null)
                    return 2;
                else if (FirstChildAge != null)
                    return 1;
                else
                    return 0;
            }
           
        }

      
        public int? FirstChildAge { get; set; }
        public int? SecondChildAge { get; set; }
        
        public int? ThirdChildAge { get; set; }
    }
}
