﻿using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using BilenAdam.Hotels.Charter.Reservation.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend
{
    public class MergeRoomsWizard : BaseWizard
    {
        public PropertyProduct SelectedProduct;
        public int[] SelectedRoomIds { get; set; }

        public void SelectRooms(BilenAdam.Data.HotelData.HotelDataContext db, int[] rooms)
        {
            SelectedRoomIds = rooms;
            var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == HotelCharterReservationId);
            SearchQuery = new Query.SearchQuery
            {
                HotelId = rez.HotelId,
                CheckIn = rez.GetCheckIn(),
                CheckOut = rez.GetCheckOut(),
                DestinationId = null,
                MarketId = rez.MarketId,
                ReservationDate = rez.CreateTimeUTC.Date,
                Rooms = new Query.RoomPlan[] {
                    new Query.RoomPlan { AdultCount = 0, RoomRef = 1 }
                },
                IncludeHotelTransfer = rez.HotelCharterRooms.Any(r => rooms.Contains(r.Id) && r.HasTransferFee()),
                IsPackageReservation = false
            };

            foreach (var guest in rez.HotelCharterRooms.Where(r => rooms.Contains(r.Id)).SelectMany(r => r.HotelCharterGuests))
            {
                Query.RoomPlan r = SearchQuery.Rooms[0];


                if (guest.IsAdult)
                {
                    r.AdultCount++;
                }
                else
                {
                    if (r.FirstChildAge == null)
                        r.FirstChildAge = SearchQuery.CheckOut.Year - guest.Dob.Year;
                    else if (r.SecondChildAge == null)
                        r.SecondChildAge = SearchQuery.CheckOut.Year - guest.Dob.Year;
                    else
                        r.ThirdChildAge = SearchQuery.CheckOut.Year - guest.Dob.Year;
                }
            }
        }

        public override string SelectProduct(Controller controller, string productId)
        {
            SelectedProduct = SearchResults.FirstOrDefault(r => r.ProductId == productId);
            new MergeRoomsActivity().MergeRooms(HotelCharterReservationId, SelectedProduct, SelectedRoomIds);
            base.SyncToTravelData();
            return base.RedirectToReservation(controller);
        }

    }
}
