﻿using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using BilenAdam.Hotels.Charter.Reservation.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend
{
    public class SplitRoomWizard : BaseWizard
    {
        public int[] SelectedGuestIds { get; set; }
        public int HotelCharterRoomId { get; set; }

        public PropertyProduct SelectedProduct;

       

        public void SelectGuests(BilenAdam.Data.HotelData.HotelDataContext db ,int[] guests)
        {
            var room = db.HotelCharterRooms.FirstOrDefault(r => r.Id == HotelCharterRoomId);

            SearchQuery = new Query.SearchQuery
            {
                HotelId = room.HotelRoom.HotelId,
                CheckIn = room.CheckIn,
                CheckOut = room.CheckOut,
                DestinationId = null,
                MarketId = room.HotelCharterReservation.MarketId,
                ReservationDate = room.HotelCharterReservation.CreateTimeUTC.Date,
                Rooms = new Query.RoomPlan[] {
                    new Query.RoomPlan { AdultCount = 0, RoomRef = 1 },
                    new Query.RoomPlan { AdultCount = 0, RoomRef = 2 }
                },
                IncludeHotelTransfer = room.HasTransferFee(),
                IsPackageReservation = false
            };

            foreach (var guest in room.HotelCharterGuests)
            {
                Query.RoomPlan r = SearchQuery.Rooms[0];
                if (guests.Contains(guest.Id))
                    r = SearchQuery.Rooms[1];

                if(guest.IsAdult)
                {
                    r.AdultCount++;
                }
                else
                {
                    if (r.FirstChildAge == null)
                        r.FirstChildAge = guest.CalculateAge(room.CheckOut);
                    else if(r.SecondChildAge == null)
                        r.SecondChildAge = guest.CalculateAge(room.CheckOut);
                    else
                        r.ThirdChildAge = guest.CalculateAge(room.CheckOut);
                }
            }

            SelectedGuestIds = guests;

        }

       


        public override string SelectProduct(Controller controller,string productId)
        {
            SelectedProduct  = SearchResults.FirstOrDefault(r => r.ProductId == productId);
             new SplitRoomActivity().SplitRoom(HotelCharterRoomId, SelectedProduct, SelectedGuestIds);
            base.SyncToTravelData();
            return base.RedirectToReservation(controller);

            

        }

      
    }
}
