﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Controllers;
using System.Linq;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;

namespace BilenAdam.XITT.Admin.HotelCharter.ReservationManagement.Workflow.Ammend
{
    public abstract class BaseWizard : IHotelCharterActivityWizard
    {
        
      

        public int HotelCharterReservationId
        {
            get;
            set;
        }

        

        public Query.SearchQuery SearchQuery { get; set; }

        public IEnumerable<PropertyProduct> SearchResults { get; set; }

        public System.Threading.ManualResetEventSlim searchCompleted = new System.Threading.ManualResetEventSlim(true);
        public void BeginSearch(bool useSingleAvailability = true)
        {
            if (searchCompleted.IsSet)
                searchCompleted.Reset();

            System.Threading.ThreadPool.QueueUserWorkItem(o => Search(useSingleAvailability));
        }

        protected void Search(bool useSingleAvailability = true)
        {

            try
            {
                HotelMultiAvailabilityProvider search = new HotelMultiAvailabilityProvider() { DefaultMarketId = 1 };
                
                 SearchResults = search.Search(SearchQuery, SearchQuery.ReservationDate);
                

               

            }
            catch
            {

            }
            finally
            {
                searchCompleted.Set();
            }
        }

        

    
        

        public abstract string SelectProduct(Controller controller,string productId);

        public string RedirectToReservation(Controller controller)
        {
            controller.RemoveWizard(this);
            return controller.Url.Nav<HomeController>().Action(c => c.Index(HotelCharterReservationId), new { Id = HotelCharterReservationId });
        }

        public string RedirectToAmmendReservation(Controller controller)
        {
            return controller.Url.Nav<HotelCharterAmmendController>().Action(c => c.Index(HotelCharterReservationId), new { Id = HotelCharterReservationId });
    
        }

        public void SyncToTravelData()
        {
            //string pnr = PnrHelper.ToHotelCharterPnr(HotelCharterReservationId);
            //using (var hdb = BilenAdam.Data.HotelData.HotelDataContextFactory.Create())
            //using (var tdb = BilenAdam.Data.TravelData.TravelDataContextFactory.Create())
            //{
            //    var charterRez = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == HotelCharterReservationId);
            //    var hotelRez = tdb.HotelReservations.FirstOrDefault(r => r.Pnr == pnr);

            //  //  new ReservationSync().Transfer(charterRez, hotelRez);
            // //   tdb.SubmitChanges();

            //}
        }
    }
}
