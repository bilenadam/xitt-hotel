﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace BilenAdam.XITT.Admin.Web
{
    public class AdminMvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
            BilenAdam.Web.ModelBinding.ModelBindingInitializer.RegisterBinders();
        }

        protected void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
             "Localization", // Route arg
             "{" + BilenAdam.Web.Resources.LanguageRouteParameter.LANGUAGE_ROUTE_PARAMETER + "}/{controller}/{action}", // URL with parameters
             new
             {
                 controller = "Home",
                 action = "Index",
                 language = UrlParameter.Optional
             } // Parameter defaults
             ,
             new { language = BilenAdam.Web.Resources.LanguageRouteParameter.ValidLanguagesPattern }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }

        public void Application_OnBeginRequest(object sender, EventArgs e)
        {
            BilenAdam.Web.Resources.LanguageRouteParameter.SetCulture();


        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            if (Request.IsLocal)
            {
                BilenAdam.Web.Membership.FormAuthenticationExtensions.SetAdminPrincipal("Developer", new[] { "Developer" });
            }
            else
            {
                BilenAdam.Web.Membership.FormAuthenticationExtensions.SetAdminPrincipal();
            }


        }

    }
}
