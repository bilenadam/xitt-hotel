﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc.Html
{
    public static class SearchFormExtensions
    {
        public static MvcHtmlString SearchFormTextInput(this HtmlHelper htmlHelper, string name, object htmlAttributes = null)
        {
            return htmlHelper.FormControlTextInput(name, htmlHelper.ViewContext.RequestContext.HttpContext.Request[name], htmlAttributes);
        }

        public static MvcHtmlString SeachFormControlActiveSelect(this HtmlHelper htmlHelper, string name,  object htmlAttributes = null)
        {
            var selection = htmlHelper.ViewContext.RequestContext.HttpContext.Request[name];

            IEnumerable<SelectListItem> items = new[] {
                new SelectListItem { Value = "", Text = "All" },
                new SelectListItem { Value = true.ToString(), Text = "Aktif", Selected = selection== true.ToString() },
                new SelectListItem { Value = false.ToString(), Text = "Pasif", Selected = selection == false.ToString() }
            };
            return htmlHelper.FormControlSelect(name, items, htmlAttributes);
        }

        public static MvcHtmlString SearchFormDateInput(this HtmlHelper htmlHelper, string name, object htmlAttributes = null)
        {
            if(DateTime.TryParse(htmlHelper.ViewContext.RequestContext.HttpContext.Request[name], out DateTime date))
                return htmlHelper.FormControlDateInput(name, date , htmlAttributes);
            else
                return htmlHelper.FormControlDateInput(name, null, htmlAttributes);
        }
    }
}
