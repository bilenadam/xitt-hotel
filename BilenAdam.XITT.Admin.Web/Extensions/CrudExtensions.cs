﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Resources.App;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{
    public static class CrudButtonExtensions
    {

        public static MvcHtmlString SubmitButton(this HtmlHelper helper, string text, object htmlAttributes = null)
        {
            TagBuilder buttonBuilder = new TagBuilder("button");
            buttonBuilder.Attributes["type"] = "submit";
            buttonBuilder.SetInnerText(text);

            if (htmlAttributes != null)
                buttonBuilder.MergeAttributes(htmlAttributes is RouteValueDictionary ? (RouteValueDictionary)htmlAttributes : HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), false);

            buttonBuilder.AddCssClass("btn");

            return new MvcHtmlString(buttonBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString SaveButton(this HtmlHelper helper, object htmlAttributes = null)
        {
            return SubmitButton(helper, Crud.Save, HtmlAttributeExtensions.ExtendHtmlAttr(htmlAttributes, new { @class = "btn-primary" }));
        }

        public static MvcHtmlString LinkButton(this UrlHelper urlHelper, string text, string actionName, string controllerName = null, object routeValues = null, object htmlAttributes = null)
        {

            var url = urlHelper.Action(actionName);

            if (!string.IsNullOrWhiteSpace(controllerName) && routeValues != null)
                url = urlHelper.Action(actionName, controllerName, routeValues);
            else if (!string.IsNullOrWhiteSpace(controllerName) && routeValues == null)
                url = urlHelper.Action(actionName, controllerName);
            else if (string.IsNullOrWhiteSpace(controllerName) && routeValues != null)
                url = urlHelper.Action(actionName, routeValues);

            TagBuilder buttonBuilder = new TagBuilder("a");
            buttonBuilder.Attributes["href"] = url;
            buttonBuilder.SetInnerText(text);

            if (htmlAttributes != null)
                buttonBuilder.MergeAttributes(htmlAttributes is RouteValueDictionary ? (RouteValueDictionary)htmlAttributes : HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), false);

            buttonBuilder.AddCssClass("btn");
           

            return new MvcHtmlString(buttonBuilder.ToString(TagRenderMode.Normal));

        }

        public static MvcHtmlString NewLinkButton(this UrlHelper urlHelper, string actionName = "Create", string controllerName = null, object routeValues = null, object htmlAttributes = null)
        {
            return urlHelper.LinkButton(Crud.New, actionName, controllerName, routeValues,  HtmlAttributeExtensions.ExtendHtmlAttr(htmlAttributes, new { @class = "btn-success" }));
        }

        public static MvcHtmlString EditLinkButton(this UrlHelper urlHelper, string actionName = "Edit", string controllerName = null, object routeValues = null, object htmlAttributes = null)
        {
            return urlHelper.LinkButton(Crud.New, actionName, controllerName, routeValues, HtmlAttributeExtensions.ExtendHtmlAttr(htmlAttributes, new { @class = "btn-success" }));
        }

        public static MvcHtmlString DeleteLinkButton(this UrlHelper urlHelper, string actionName = "Delete", string controllerName = null, object routeValues = null, object htmlAttributes = null)
        {
            return urlHelper.LinkButton(Crud.Delete, actionName, controllerName, routeValues, HtmlAttributeExtensions.ExtendHtmlAttr(htmlAttributes, new { @class = "btn-danger" }));
        }


    }
}
