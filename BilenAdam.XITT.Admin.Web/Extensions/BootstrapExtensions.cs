﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc.Html
{
    public static class BootstrapExtensions
    {
        public static MvcHtmlString FormControlLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            return htmlHelper.LabelFor(expression, htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "control-label" }));
        }

        public static MvcHtmlString FormControlTextInput(this HtmlHelper htmlHelper, string name, string value, object htmlAttributes = null)
        {
            return htmlHelper.TextBox(name, value, htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormControlTextInputFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            return htmlHelper.TextBoxFor(expression, htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control", @placeholder = htmlHelper.DisplayNameFor(expression) }));
        }



        public static MvcHtmlString FormControlDateInput(this HtmlHelper htmlHelper, string name, DateTime? value, object htmlAttributes = null)
        {
            return htmlHelper.DateBox(name, value, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormControlDateInputFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime>> expression, object htmlAttributes = null)
        {
            return htmlHelper.DateBoxFor(expression, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }
        public static MvcHtmlString FormControlDateInputFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, object htmlAttributes = null)
        {
            return htmlHelper.DateBoxFor(expression, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormControlDateTimeInputFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime>> expression, object htmlAttributes = null)
        {
            return htmlHelper.DateTimeBoxFor(expression, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }
        public static MvcHtmlString FormControlDateTimeInputFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, object htmlAttributes = null)
        {
            return htmlHelper.DateTimeBoxFor(expression, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormControlSelect(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> items, object htmlAttributes = null)
        {
            return htmlHelper.Select(name, items, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormControlSelectFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> items, object htmlAttributes = null)
        {
            return htmlHelper.SelectFor(expression, items, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }
      
        public static MvcHtmlString FormControlNumberSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, byte?>> expression, byte start, byte count, bool addEmpty, object htmlattributes = null)
        {
            return htmlHelper.NumberSelectFor(expression, start, count, addEmpty, htmlHelper.ExtendHtmlAttr(htmlattributes, new { @class = "form-control" }));
        }
        public static MvcHtmlString FormControlEnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, byte?>> expression, object htmlAttributes = null) where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            return htmlHelper.EnumSelectFor<TModel, TEnum>(expression, htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormCurrencySelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlAttributes = null)
        {
            return htmlHelper.CurrencySelectFor(expression, htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control", @placeholder = htmlHelper.DisplayNameFor(expression) }));
        }

        public static MvcHtmlString FormControlMoneyInput(this HtmlHelper htmlHelper, string name, decimal? value, object htmlAttributes = null)
        {
            return htmlHelper.MoneyBox(name, value, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }

        public static MvcHtmlString FormControlMoneyInputFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, decimal>> expression, object htmlAttributes = null)
        {
            return htmlHelper.MoneyBoxFor(expression, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }
        public static MvcHtmlString FormControlMoneyInputFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, decimal?>> expression, object htmlAttributes = null)
        {
            return htmlHelper.MoneyBoxFor(expression, htmlattributes: htmlHelper.ExtendHtmlAttr(htmlAttributes, new { @class = "form-control" }));
        }
    }
}
