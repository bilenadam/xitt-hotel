﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;

namespace System.Web.Mvc.Html
{
    public static class TravelDataExtensions
    {
        public static HotelDataContext GetOrSetTravelDB(this ViewDataDictionary viewData)
        {
            var db = viewData.GetDB<HotelDataContext>();
            if(db == null)
            {
                db = HotelDataContextFactory.Create();
                viewData.SetDB(db);
            }
            return db;

        }

        static List<SelectListItem> createSelectList(IQueryable<Contact> contacts, int? selectedValue = null)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Value = "", Text = "..." });
            foreach (var c in contacts.OrderBy(i=>i.Name))
            {
                items.Add(new SelectListItem { Value = c.Id.ToString(), Text = c.Name , Selected = c.Id == selectedValue });
            }
            return items;
        }
        public static MvcHtmlString FormReferrerSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int?>> expression, object htmlAttributes = null)
        {
            var db = htmlHelper.ViewData.GetOrSetTravelDB();
            return htmlHelper.FormControlSelectFor(expression, createSelectList(db.Contacts.Where(c => c.Type == (int)BilenAdam.Crm.ContactType.SearchEngine)), htmlAttributes);
        }
        static int[] supplierTypes = new int[]
        {
            (int)BilenAdam.Crm.ContactType.Incoming,
            (int)BilenAdam.Crm.ContactType.TourOperator,
            (int)BilenAdam.Crm.ContactType.Broker,
            (int)BilenAdam.Crm.ContactType.Hotel,
            (int)BilenAdam.Crm.ContactType.HandlingPartner
        };
        public static MvcHtmlString FormSupplierSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int?>> expression, object htmlAttributes = null)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return htmlHelper.FormControlSelectFor(expression, createSelectList(db.Contacts.Where(c => supplierTypes.Contains(c.Type))), htmlAttributes);
            }
        }

        public static MvcHtmlString FormSupplierSelect(this HtmlHelper htmlHelper,string name, int? selected, object htmlAttributes = null)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return htmlHelper.FormControlSelect(name, createSelectList(db.Contacts.Where(c => supplierTypes.Contains(c.Type))), htmlAttributes);
            }
        }

        public static MvcHtmlString FormSupplierSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int>> expression, object htmlAttributes = null)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return htmlHelper.FormControlSelectFor(expression, createSelectList(db.Contacts.Where(c => supplierTypes.Contains(c.Type))), htmlAttributes);
            }
        }


        public static MvcHtmlString FormContactSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int?>> expression, object htmlAttributes = null)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                var itemList = createSelectList(db.Contacts.Where(c => c.Type != (int)BilenAdam.Crm.ContactType.Person));
                return htmlHelper.FormControlSelectFor(expression, itemList, htmlAttributes);
            }
        }
        public static MvcHtmlString FormContactSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int>> expression, object htmlAttributes = null)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                var itemList = createSelectList(db.Contacts.Where(c => c.Type != (int)BilenAdam.Crm.ContactType.Person));
                return htmlHelper.FormControlSelectFor(expression, itemList, htmlAttributes);
            }
        }

        public static string DisplaySupplierName(this HtmlHelper htmlHelper, int contactId)
        {
            return htmlHelper.DisplayContactName(contactId);
        }

        public static string DisplayContactName(this HtmlHelper htmlHelper, int? contactId)
        {
            if (contactId == null)
                return string.Empty;
            var db = htmlHelper.ViewData.GetOrSetTravelDB();
            return db.Contacts.First(c => c.Id == contactId).Name;
        }
    }
}
