﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;
using System.Data.Linq;
using BilenAdam.Web;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelQuotaController : Controller
    {
        public ActionResult HotelSearch(string name = null, string countryCode = null, int? cityId = null, int? districtId = null, int pageIndex = 1, int pageSize = 10)
        {



            using (var db = this.CreateHotelDataContext())
            {
                var data = db.Hotels.AsQueryable();

                if (!string.IsNullOrEmpty(name))
                    data = data.Where(o => o.Name.Contains(name));

                if (!string.IsNullOrEmpty(countryCode))
                    data = data.Where(o => o.HotelDestination.CountryCode == countryCode);


                if (districtId != null && cityId != null)
                    data = data.Where(o => o.HotelDestination.Id == districtId && o.HotelDestination.ParentId == cityId);
                else if (cityId != null)
                    data = data.Where(o => o.HotelDestionationId == cityId);

                var model = data.ToPagedList<Hotel>(pageIndex, pageSize);
                return View(model);
            }
        }

        #region Hotel CRUD & List



        public ActionResult ChangingQuota(int hotelId)
        {
            int tempId = 0;
            int[] roomIds = string.IsNullOrWhiteSpace(Request["RoomId"]) ? null : Request["RoomId"].Split(',').Where(s => int.TryParse(s, out tempId)).Select(s => int.Parse(s)).ToArray();
            int[] marketIds = string.IsNullOrWhiteSpace(Request["MarketId"]) ? null : Request["MarketId"].Split(',').Where(s => int.TryParse(s, out tempId)).Select(s => int.Parse(s)).ToArray();
            int deadLine = 0;
            DateTime beginDate = DateTime.Now;
            DateTime endDate = DateTime.Now;

            Int32.TryParse(Request["DeadLine"], out deadLine);


            if (Request["AddQuota"] != null || Request["ReplaceQuota"] != null)
            {



                if (!DateTime.TryParse(Request["BeginDate"], out beginDate))
                {
                    beginDate = DateTime.Now.AddDays(-15);
                }
                if (!DateTime.TryParse(Request["EndDate"], out endDate))
                {
                    endDate = DateTime.Now.AddDays(15);
                }

                Int32.TryParse(Request["hotelId"], out hotelId);

                byte amountQuota = 0;
                byte.TryParse(Request["Amount"], out amountQuota);

                //listeyi bind ediyor

                using (var db = this.CreateHotelDataContext())
                {
                    for (DateTime i = beginDate; i <= endDate; i = i.AddDays(1))
                    {
                        foreach (var roomId in roomIds)
                        {
                            foreach (var marketId in marketIds)
                            {
                                var itm = db.HotelQuotas.FirstOrDefault(o => o.QuotaDate == i && o.RoomId == roomId && o.MarketId == marketId);
                                if (itm == null)
                                {
                                    itm = new HotelQuota()
                                    {
                                        Amount = amountQuota,
                                        QuotaDate = i,
                                        RoomId = roomId,
                                        MarketId = marketId

                                    };
                                    db.HotelQuotas.InsertOnSubmit(itm);

                                }
                                else
                                {
                                    if (Request["AddQuota"] != null)
                                        itm.Amount += amountQuota;
                                    else
                                        itm.Amount = amountQuota;
                                }
                            }
                        }



                    }

                    db.SaveChanges(User.Identity.Name);
                }
            }




            if (!DateTime.TryParse(Request["BeginDate"], out beginDate))
            {
                beginDate = DateTime.Now.AddDays(-15);
            }
            if (!DateTime.TryParse(Request["EndDate"], out endDate))
            {
                endDate = DateTime.Now.AddDays(15);
            }

            ViewBag.BeginData = beginDate;
            ViewBag.EndDate = endDate;

            BindingList(hotelId, roomIds, marketIds);
            using (var db = this.CreateHotelDataContext())
            {

                var data = db.vHotelQuotas.AsQueryable().Where(o => o.HotelId == hotelId && o.QuotaDate >= beginDate && o.QuotaDate <= endDate);
                if (roomIds != null)
                    data = data.Where(o => roomIds.Contains(o.RoomId));
                if (marketIds != null)
                    data = data.Where(o => marketIds.Contains(o.MarketId));

                var model = data.ToList();

                //string roomName = ((List<SelectListItem>)ViewBag.RoomList).FirstOrDefault(o => o.Selected).Text;
                //string marketName = ((List<SelectListItem>)ViewBag.MarketList).FirstOrDefault(o => o.Selected).Text;

                if (marketIds != null && roomIds != null)
                    for (DateTime i = beginDate; i <= endDate; i = i.AddDays(1))
                    {
                        var itm = model.FirstOrDefault(o => o.QuotaDate == i);
                        if (itm == null)
                        {
                            foreach (var roomId in roomIds)
                            {
                                foreach (var marketId in marketIds)
                                {



                                    model.Add(new vHotelQuota()
                                    {
                                        Amount = 0,
                                        QuotaDate = i,
                                        RoomId = roomId,
                                       // RoomName = roomName,
                                        MarketId = marketId,
                                     //   MarketName = marketName,
                                        HotelId = hotelId

                                    });
                                }

                            }
                        }

                    }


                return View(model.OrderBy(o => o.QuotaDate).ToList());

            }


        }






        public void BindingList(int hotelId, int[] roomIds, int[] marketIds)
        {
            using (var db = this.CreateHotelDataContext())
            {

                var roomSelectList = db.HotelRooms.Where(o => o.HotelId == hotelId).Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.Id.ToString()
                }).ToList();

                if (roomSelectList.Count > 0)
                {
                    if (roomIds != null && roomIds.Length > 0)
                    {
                        foreach (var item in roomSelectList)
                        {
                            var id = int.Parse(item.Value);
                            item.Selected = roomIds.Contains(id);
                        }
                    }
                 
                }

                ViewBag.RoomList = roomSelectList;

                var marketSelectList = db.HotelMarkets.Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.Id.ToString()
                }).ToList();

                if (marketSelectList.Count > 0)
                {
                    if (marketIds != null && marketIds.Length > 0)
                    {
                        foreach (var item in marketSelectList)
                        {
                            var id = int.Parse(item.Value);
                            item.Selected = marketIds.Contains(id);
                        }
                    }
                  
                }
                ViewBag.MarketList = marketSelectList;
            }
        }
        #endregion



    }
}
