﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelSPOController : Controller
    {
        public ActionResult Index(int hotelSeasonId)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);
            ViewData["HotelSeason"] = db.HotelSeasons.FirstOrDefault(s => s.Id == hotelSeasonId);
            var model = db.HotelFareHs.Where(f => f.HotelSeasonId == hotelSeasonId && f.IsSPOEnabled != null).ToArray();
            return View(model);
        }

        [HttpGet]
        public ActionResult Create(int hotelSeasonId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.HotelSeasons.FirstOrDefault(s => s.Id == hotelSeasonId);

                var model = new HotelFareH { HotelId = data.HotelId, IsSPOEnabled = true, MarketId = data.HotelMarketId, HotelSeasonId = data.Id, SeasonCode = "SPO", Currency = "EUR" };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Create(HotelFareH model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                model.SeasonCode = "SPO";
                db.HotelFareHs.InsertOnSubmit(model);
                db.SaveChanges(User.Identity.Name);

                return RedirectToAction("Fares", new { Id = model.Id });
            }
        }


        [HttpGet]
        public ActionResult Delete(int Id, int hotelSeasonId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                db.HotelFareHs.DeleteByIdOnSubmit(Id);
                db.SaveChanges(User.Identity.Name);
            }

            return RedirectToAction("Index", new { hotelSeasonId = hotelSeasonId });
        }


        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);
            var model = db.HotelFareHs.FirstOrDefault(f => f.Id == Id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(HotelFareH model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelFareHs.FirstOrDefault(f => f.Id == model.Id);
                item.UpdateFrom(model, "HotelId", "MarketId", "HotelSeasonId", "SeasonCode");
                db.SaveChanges(User.Identity.Name);
            }

            return RedirectToAction("Index", new { hotelSeasonId = model.HotelSeasonId });
        }


        [HttpGet]
        public ActionResult Fares(int Id, int? pensionId = null)
        {
            var db = this.CreateHotelDataContext();
          
            var spoHeader = db.HotelFareHs.FirstOrDefault(h => h.Id == Id);

            if(spoHeader == null)
            {
                db.Dispose();
                return HttpNotFound();
            }

            Models.HotelSPOFareDetails model = new Models.HotelSPOFareDetails
            {
                 Header = spoHeader
            };

            if (pensionId != null)
            {

                model.PensionData = db.HotelPensions.FirstOrDefault(p => p.Id == pensionId);

                model.RoomGroups = new List<Models.HotelSPOFareRoomGroup>();

                foreach (var room in spoHeader.Hotel.HotelRooms.OrderBy(r => r.Name))
                {
                   // Models.HotelSPOFareRoomGroup roomGroup = new Models.HotelSPOFareRoomGroup { RoomData = room, Details = spoHeader.HotelFareDs.Where(d => d.PensionId == pensionId).ToList() };
                    
                    model.RoomGroups.Add(new Models.HotelSPOFareRoomGroup { RoomData = room, Details = spoHeader.HotelFareDs.Where(d => d.PensionId == pensionId && d.RoomId == room.Id).ToList() });
                }
            }
           


            ViewData.SetDB(db);
            return View(model);

        }

        [HttpPost]
        public ActionResult AddDetail(int fareHeaderId, int roomId, int pensionid, decimal fare, byte adt, byte chd, byte chd1min, byte chd1max, byte chd2min, byte chd2max, byte chd3min, byte chd3max, byte chd4min, byte chd4max)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var newDetail = new BilenAdam.Data.HotelData.HotelFareD
                {
                     FareHId = fareHeaderId,
                     RoomId = roomId,
                     PensionId = pensionid,
                     Fare = fare,
                     Adults = adt,
                     Childs = chd
                };
                StringBuilder fareText = new StringBuilder();
                fareText.AppendFormat("{0} ADT {1} CHD ",adt,chd);
                if (chd >= 1)
                {
                    newDetail.FirstChildMinAge = chd1min;
                    newDetail.FirstChildMaxAge = chd1max;
                    fareText.AppendFormat("({0} - {1})", chd1min, chd1max);
                }
                if (chd >= 2)
                {
                    newDetail.SecondChildMinAge = chd2min;
                    newDetail.SecondChildMaxAge = chd2max;
                    fareText.AppendFormat("({0} - {1})", chd2min, chd2max);
                }
                if (chd >= 3)
                {
                    newDetail.ThirdChildMinAge = chd3min;
                    newDetail.ThirdChildMaxAge = chd3max;
                    fareText.AppendFormat("({0} - {1})", chd3min, chd3max);
                }
                if (chd >= 4)
                {
                    newDetail.FourthChildMinAge = chd4min;
                    newDetail.FourthChildMaxAge = chd4max;
                    fareText.AppendFormat("({0} - {1})", chd4min, chd4max);
                }
                db.HotelFareDs.InsertOnSubmit(newDetail);

                
                db.SaveChanges(User.Identity.Name);

               

                return Json(new { id = newDetail.Id, fare = newDetail.Fare.ToMoneyString(), text = fareText.ToString() });
            }
        }

        [HttpPost]
        public ActionResult DeleteDetail(int Id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                db.HotelFareDs.DeleteByIdOnSubmit(Id);
                db.SaveChanges(User.Identity.Name);
            }

            return Json(true);
        }

        [HttpPost]
        public ActionResult UpdateDetail(int Id,decimal fare)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelFareDs.FirstOrDefault(d => d.Id == Id);
                item.Fare = fare;
                db.SaveChanges(User.Identity.Name);
            }


            return Json(true);
        }

    }
}
