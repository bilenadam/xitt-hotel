﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using System.Data.Linq;
using BilenAdam.Utilities.Paging;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelPromotionController : Controller
    {
        public ActionResult HotelSearch(string name = null, string countryCode = null, int? cityId = null, int? districtId = null, int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.Hotels.AsQueryable();

                if (!string.IsNullOrEmpty(name))
                    data = data.Where(o => o.Name.Contains(name));

                if (!string.IsNullOrEmpty(countryCode))
                    data = data.Where(o => o.HotelDestination.CountryCode == countryCode);


                if (districtId != null && cityId != null)
                    data = data.Where(o => o.HotelDestination.Id == districtId && o.HotelDestination.ParentId == cityId);
                else if (cityId != null)
                    data = data.Where(o => o.HotelDestionationId == cityId);

                var model = data.ToPagedList<Hotel>(pageIndex, pageSize);
                return View(model);
            }
        }
        public ActionResult Index(int hotelSeasonId)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var hotelSeason = db.HotelSeasons.FirstOrDefault(s => s.Id == hotelSeasonId);
            ViewData["HotelSeason"] = hotelSeason;
            

            var data = db.HotelPromotions.Where(o => o.HotelSeasonId == hotelSeasonId).ToArray();


            return View(data);

        }

        [HttpPost]
        public ActionResult Delete(int itemId, int hotelSeasonId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                db.HotelPromotionCombinations.DeleteAllOnSubmit(p => p.HotelPromotionId1 == itemId || p.HotelPromotionId2 == itemId);
                db.HotelPromotions.DeleteFirstOnSubmit(p => p.Id == itemId);
       
                db.SaveChanges(User.Identity.Name);
            }
            return this.Redirect2Action(c => c.Index(1), new { hotelSeasonId = hotelSeasonId });
        }
        [HttpPost]
        public ActionResult QuickUpdate(int itemId, string propertyName, string propertyValue)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelPromotions.FirstOrDefault(p => p.Id == itemId);
                if (item != null)
                {
                    var property = item.GetType().GetProperty(propertyName);



                    try
                    {
                        object value = Convert.ChangeType(propertyValue, Nullable.GetUnderlyingType(property.PropertyType) ?? property.PropertyType);
                        property.SetValue(item, value);
                    }
                    catch
                    {
                        property.SetValue(item, null);
                    }



                }

                db.SaveChanges(User.Identity.Name);
            }

            return Content("OK");
        }

        [HttpPost]
        public ActionResult QuickRoomType(int itemId, int[] roomTypes)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelPromotions.FirstOrDefault(p => p.Id == itemId);
                if (item != null)
                {
                   

                    if(roomTypes == null || (roomTypes.Length == 1 && roomTypes[0] == 0))
                    {
                        item.RoomTypeSpecific = false;
                        db.HotelPromotionRoomTypes.DeleteAllOnSubmit(r => r.HotelPromotionId == item.Id );
                    }
                    else
                    {
                        item.RoomTypeSpecific = true;

                        if (roomTypes.Length > item.HotelPromotionRoomTypes.Count)
                        {
                            var newItem = roomTypes.First(i => item.HotelPromotionRoomTypes.Count == 0 || item.HotelPromotionRoomTypes.All(r => r.HotelRoomId != i));
                            item.HotelPromotionRoomTypes.Add(new HotelPromotionRoomType { HotelRoomId = newItem });
                        }
                        else
                        {
                            db.HotelPromotionRoomTypes.DeleteAllOnSubmit(r => r.HotelPromotionId == item.Id &&  !roomTypes.Contains(r.HotelRoomId));
                        }
                    }

                  

                  
                }

                db.SaveChanges(User.Identity.Name);
            }

            return Content("OK");
        }



        [HttpPost]
        public ActionResult ChangeCombination(int p1, int p2, string cmd)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    if (cmd == "add")
                    {
                        db.HotelPromotionCombinations.InsertOnSubmit(new HotelPromotionCombination { HotelPromotionId1 = p1, HotelPromotionId2 = p2 });
                    }
                    else
                    {
                        db.HotelPromotionCombinations.DeleteFirstOnSubmit(p => (p.HotelPromotionId1 == p1 && p.HotelPromotionId2 == p2) || (p.HotelPromotionId1 == p2 && p.HotelPromotionId2 == p1));
                    }

                    db.SaveChanges(User.Identity.Name);
                }
            }
            catch
            {

            }

            return Content("OK");
        }

        [HttpPost]
        public ActionResult CreateEarlyBookingPromotion(HotelPromotion model)
        {
            using (var db = this.CreateHotelDataContext())
            {
               
                try
                {
                    var season = db.HotelSeasons.FirstOrDefault(s => s.Id == model.HotelSeasonId);
                    model.MarketId = season.HotelMarketId;
                    model.HotelId = season.HotelId;
                    model.DiscountType = (int)BilenAdam.Reservation.AmountType.Percent;
                    model.HotelPromotionType = (int)BilenAdam.Reservation.Hotels.Promotions.PromotionType.EarlyBooking;
                   
                    db.HotelPromotions.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }
                catch
                {

                }
            }
            return this.Redirect2Action(c => c.Index(1), new { hotelSeasonId = model.HotelSeasonId });
        }

        [HttpPost]
        public ActionResult CreateRollingEarlyBookingPromotion(HotelPromotion model)
        {
            using (var db = this.CreateHotelDataContext())
            {

                try
                {
                    var season = db.HotelSeasons.FirstOrDefault(s => s.Id == model.HotelSeasonId);
                    model.MarketId = season.HotelMarketId;
                    model.HotelId = season.HotelId;
                    model.DiscountType = (int)BilenAdam.Reservation.AmountType.Percent;
                    model.HotelPromotionType = (int)BilenAdam.Reservation.Hotels.Promotions.PromotionType.RollingEarlyBooking;

                    db.HotelPromotions.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }
                catch
                {

                }
            }
            return this.Redirect2Action(c => c.Index(1), new { hotelSeasonId = model.HotelSeasonId });
        }

        [HttpPost]
        public ActionResult CreateLongStayPromotion(HotelPromotion model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                try
                {
                    var season = db.HotelSeasons.FirstOrDefault(s => s.Id == model.HotelSeasonId);
                    model.MarketId = season.HotelMarketId;
                    model.HotelId = season.HotelId;
                    model.DiscountType = (int)BilenAdam.Reservation.AmountType.Percent;
                    model.HotelPromotionType = (int)BilenAdam.Reservation.Hotels.Promotions.PromotionType.LongStay;
              
                    db.HotelPromotions.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }
                catch
                {

                }
            }
            return this.Redirect2Action(c => c.Index(1), new { hotelSeasonId = model.HotelSeasonId });
        }

        [HttpPost]
        public ActionResult CreateTurboEarlyBookingPromotion(HotelPromotion model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                try
                {
                    var season = db.HotelSeasons.FirstOrDefault(s => s.Id == model.HotelSeasonId);
                    model.MarketId = season.HotelMarketId;
                    model.HotelId = season.HotelId;
                    model.DiscountType = (int)BilenAdam.Reservation.AmountType.Fix;
                    model.HotelPromotionType = (int)BilenAdam.Reservation.Hotels.Promotions.PromotionType.TurboEarlyBooking;
                   
                    db.HotelPromotions.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }
                catch
                {

                }
            }
            return this.Redirect2Action(c => c.Index(1), new { hotelSeasonId = model.HotelSeasonId });
        }

        [HttpPost]
        public ActionResult CreateDayPromotion(HotelPromotion model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                try
                {
                    var season = db.HotelSeasons.FirstOrDefault(s => s.Id == model.HotelSeasonId);
                    model.MarketId = season.HotelMarketId;
                    model.HotelId = season.HotelId;
                    model.DiscountType = (int)BilenAdam.Reservation.AmountType.Percent;
                    model.DiscountRate = 1;
                    model.HotelPromotionType = (int)BilenAdam.Reservation.Hotels.Promotions.PromotionType.Day;
                

                    db.HotelPromotions.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }
                catch
                {

                }
            }
            return this.Redirect2Action(c => c.Index(1), new { hotelSeasonId = model.HotelSeasonId });
        }
    }
}
