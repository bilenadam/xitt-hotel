﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

using System.Data.Linq;
using System.IO;
using System.Web.Routing;
using BilenAdam.XITT.Admin.HotelCharter.Models;
using BilenAdam.Web;
using BilenAdam.Hotels.Charter.Images;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelImageController : Controller
    {
        #region Hotel Action CRUD & List

        public ActionResult Index(int hotelId, int pageIndex = 1, int pageSize = 10)
        {


            using (var db = this.CreateHotelDataContext())
            {
                var data = db.HotelImages.AsQueryable().Where(f => f.HotelId == hotelId).OrderBy(f => f.OrderBy).ToList();

                return View(data);
            }
        }


        [HttpPost]
        public ActionResult UploadImages(int hotelId)
        {
            try
            {
                var imgHelper = new HotelImageHelper();

                foreach (string f in Request.Files)
                {
                    System.Web.HttpPostedFileBase file = Request.Files[f] as System.Web.HttpPostedFileBase;
                    using (var db = this.CreateHotelDataContext())
                    {
                        var item = new HotelImage { HotelId = hotelId, OrderBy = 0 };



                        item.OrderBy = (byte)db.HotelImages.Count(i => i.HotelId == hotelId);

                        item.OrderBy++;
                        db.HotelImages.InsertOnSubmit(item);



                        item.FileName = imgHelper.SaveImage(file, item.HotelId);


                        db.SaveChanges(User.Identity.Name);

                    }
                }

            }
            catch
            {

            }

            return Content("OK");
        }

        [HttpGet]
        public ActionResult MoveUp(int id)
        {
            // PreparingData(0);
            var model = new HotelImage();

            if (id != 0)
            {
                using (var db = this.CreateHotelDataContext())
                {
                    model = db.HotelImages.FirstOrDefault(f => f.Id == id);
                    var imgLst = db.HotelImages.Where(f => f.HotelId == model.HotelId);

                    var order = model.OrderBy;
                    var nextitm = imgLst.OrderBy(o => o.OrderBy).ToList().LastOrDefault(o => o.OrderBy < model.OrderBy);
                    if (nextitm == null)
                        return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = model.HotelId });
                    model.OrderBy = nextitm.OrderBy;
                    nextitm.OrderBy = order;
                    model.UpdateFrom(model);
                    nextitm.UpdateFrom(nextitm);
                    db.SubmitChanges();
                }
            }
            return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = model.HotelId }); ;
        }
        [HttpGet]
        public ActionResult MoveDown(int id)
        {
            // PreparingData(0);
            var model = new HotelImage();

            if (id != 0)
            {
                using (var db = this.CreateHotelDataContext())
                {
                    model = db.HotelImages.FirstOrDefault(f => f.Id == id);
                    var imgLst = db.HotelImages.Where(f => f.HotelId == model.HotelId);

                    var order = model.OrderBy;
                    var nextitm = imgLst.OrderBy(o => o.OrderBy).ToList().FirstOrDefault(o => o.OrderBy > model.OrderBy);
                    if (nextitm == null)
                        return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = model.HotelId });

                    model.OrderBy = nextitm.OrderBy;
                    nextitm.OrderBy = order;
                    model.UpdateFrom(model);
                    nextitm.UpdateFrom(nextitm);
                    db.SubmitChanges();
                }
            }
            return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = model.HotelId }); ;
        }
        [HttpPost]
        public ActionResult Edit(HotelImage model)
        {
            try
            {
                var imgHelper = new HotelImageHelper();

                if (Request.Files[0] != null && Request.Files[0].ContentLength > 0 && imgHelper.IsImage(Request.Files[0]))
                {
                    //if (ModelState.IsValid)
                    //{
                    using (var db = this.CreateHotelDataContext())
                    {
                        var item = db.HotelImages.FirstOrDefault(c => c.Id == model.Id);

                        if (item != null)
                        {
                            imgHelper.DeleteImage(item.FileName, item.HotelId);
                            item.UpdateFrom(model, "FileName");
                        }
                        else
                        {
                            item = new HotelImage();
                            item.UpdateFrom(model);
                            var images = db.HotelImages.Where(i => i.HotelId == model.HotelId);
                            if (images.Count() > 0)
                            {
                                item.OrderBy = images.Max(i => i.OrderBy);
                                item.OrderBy++;
                            }
                            else
                            {
                                item.OrderBy = 1;
                            }
                            db.HotelImages.InsertOnSubmit(item);
                        }


                        item.FileName = imgHelper.SaveImage(Request.Files[0], item.HotelId);


                        db.SaveChanges(User.Identity.Name);

                    }
                    return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = model.HotelId });
                    // }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid Image");
                }



            }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.Message);
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id, int hotelId)
        {
            // PreparingData(0);
            var model = new HotelImage();

            if (id != 0)
            {
                using (var db = this.CreateHotelDataContext())
                {
                    model = db.HotelImages.FirstOrDefault(f => f.Id == id && f.HotelId == hotelId);
                    if (model == null)
                        return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = hotelId });

                    new HotelImageHelper().DeleteImage(model.FileName, model.HotelId);

                    db.HotelImages.DeleteOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);

                }
            }
            return this.Redirect2Action(s => s.Index(1, 10, model.HotelId), new { hotelId = hotelId });
        }
        //public ActionResult ImageView(int id, int hotelId)
        //{
        //    var pth = AppDomain.CurrentDomain.BaseDirectory + @"Etc\No_image_available.png";
        //    if (id == 0)
        //        return base.File(pth, "image/png");

        //    using (var db = this.CreateHotelDataContext())
        //    {
        //        var model = db.HotelImages.FirstOrDefault(f => f.Id == id && f.HotelId == hotelId);
        //        if (model == null)
        //            return base.File(pth, "image/png");
        //        return base.File(GlobalSettings.HotelContentFolder + "\\" + model.HotelId + "\\" + model.FileName, "image/" + Path.GetExtension(model.FileName));
        //    }



        //}
        #endregion


        [HttpPost]
        public ActionResult SaveImageOrder(int hotelid, int[] ids)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var images = db.HotelImages.Where(i => i.HotelId == hotelid).ToArray();

                for (int i = 0; i < ids.Length; i++)
                {
                    var item = images.FirstOrDefault(c => c.Id == ids[i]);

                    if (item != null)
                        item.OrderBy = (byte)(i + 1);
                }
                db.SubmitChanges();
                //db.SaveChanges(User.Identity.Name);


                return Content("OK");

            }
        }




    }
}
