﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;
using System.Data.Linq;
using BilenAdam.XITT.Admin.HotelCharter.Models;


namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelPensionController : Controller
    {
        public ActionResult Index(int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.HotelPensions.AsQueryable();

                var model = data.ToPagedList<HotelPension>(pageIndex, pageSize);
                return View(model);
            }
        }

      

      

       

        [HttpPost]
        public ActionResult Create(BilenAdam.Data.HotelData.HotelPension model)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                  
                    
                    db.HotelPensions.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }

                return this.Redirect2Action(s => s.Index(1, 10));
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.Message);
            }

            return this.Redirect2Action(s => s.Index(1, 10));
        }


       

        [HttpPost]
        public ActionResult Edit(BilenAdam.Data.HotelData.HotelPension model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.CreateHotelDataContext())
                {


                    var item = db.HotelPensions.FirstOrDefault(c => c.Id == model.Id);

                    if (item == null)
                        return HttpNotFound();

                    item.UpdateFrom(model,"xRes");
                    
                    db.SaveChanges(User.Identity.Name);

                }
                
            }
            return this.Redirect2Action(s => s.Index(1, 10));
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {

                    db.HotelPensions.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }


            }
            catch (Exception exc)
            {
                ModelState.AddModelError(null, exc.Message);
            }
            return this.Redirect2Action(s => s.Index(1, 10));
        }
    }
}
