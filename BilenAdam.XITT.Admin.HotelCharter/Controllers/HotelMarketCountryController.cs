﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;
using System.Data.Linq;


namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelMarketCountryController : Controller
    {
        public ActionResult Index(int marketId,int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
              ////  DataLoadOptions dlo = new DataLoadOptions();
              //  dlo.LoadWith<Contact>(c=>c.CharterProRataContracts)
                //db.LoadOptions = dlo;
                var data = db.HotelMarketCountries.Where(o => o.MarketId == marketId).AsQueryable();
                
                var model = data.ToPagedList<HotelMarketCountry>(pageIndex, pageSize);
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Create(int marketId)
        {

            var model = new BilenAdam.Data.HotelData.HotelMarketCountry();
            model.MarketId = marketId;
            
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id  )
        {
            int marketId = 0;
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    var itm = db.HotelMarketCountries.FirstOrDefault(o => o.Id == id);
                    marketId = itm.MarketId;
                    db.HotelMarketCountries.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }

               
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(null, exc.Message);
            }
            return RedirectToAction("Index", new { pageIndex = 1, pageSize = 10, marketId = marketId });
        }

        [HttpPost]
        public ActionResult Create(BilenAdam.Data.HotelData.HotelMarketCountry model)
        {
           
            if (ModelState.IsValid)
            {

                try
                {
                    using (var db = this.CreateHotelDataContext())
                    {
                       
                        db.HotelMarketCountries.InsertOnSubmit(model);
                        db.SaveChanges(User.Identity.Name);
                    }

                    return this.Redirect2Action(s => s.Index(1, 10,0), new { pageIndex = 1, pageSize = 10, marketId = model.MarketId });
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.Message);
                }
            }

            return this.Redirect2Action(s => s.Index(1, 10, 0), new { pageIndex = 1, pageSize = 10, marketId = model.MarketId }); 
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                DataLoadOptions dlo = new DataLoadOptions();
                //dlo.LoadWith<HotelMarket>(c => c.HotelMarketCountries);

                var model = db.HotelMarketCountries.FirstOrDefault(c => c.Id == id);

                if (model == null)
                    return HttpNotFound();

                
                
                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(BilenAdam.Data.HotelData.HotelMarketCountry model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.CreateHotelDataContext())
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                   // dlo.LoadWith<HotelMarket>(c => c.HotelMarketCountries);

                    var item = db.HotelMarketCountries.FirstOrDefault(c => c.Id == model.Id);
                    item.CountryCode = model.CountryCode;
                    if (item == null)
                        return HttpNotFound();

                    
                    db.SaveChanges(User.Identity.Name);

                }
            }


            return this.Redirect2Action(s => s.Index(1, 10,0), new { pageIndex = 1, pageSize = 10, marketId = model.MarketId }); 
        }

       
    }
}
