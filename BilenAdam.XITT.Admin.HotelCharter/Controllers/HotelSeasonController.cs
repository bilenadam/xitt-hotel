﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelSeasonController : Controller
    {
        public ActionResult Index(int hotelid)
        {
            var db = this.CreateHotelDataContext();
            this.ViewData.SetDB(db);
            ViewData["Hotel"] = db.Hotels.FirstOrDefault(h => h.Id == hotelid);
            return View(db.HotelSeasons.Where(s => s.HotelId == hotelid).OrderBy(s => s.BeginDate));
            
        }

        [HttpGet]
        public ActionResult Delete(int id, int hotelid)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelSeasons.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }
            }
            catch
            {

            }
            return RedirectToAction("Index", new { hotelid = hotelid });
        }

        [HttpGet]
        public ActionResult Create(int hotelid,int? id = null)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var season = db.HotelSeasons.FirstOrDefault(s => s.Id == id && s.HotelId == hotelid);
                List<HotelFareH> fareHeaders = new List<HotelFareH>();
                if (season == null)
                {
                    season = new HotelSeason { HotelId = hotelid, BeginDate = DateTime.Today.StartOfYear(), EndDate = DateTime.Today.EndofYear() };
                    
                }
                else
                {
                    foreach (var h in season.HotelFareHs.Where(f=>f.IsSPOEnabled == null && f.SPOSalesBeginDate == null && f.SPOSalesEndDate == null))
                    {
                        fareHeaders.Add(h);
                    }
                }

                while(fareHeaders.Count < 15)
                {
                    fareHeaders.Add(new HotelFareH { HotelSeasonId = season.Id, HotelId = season.HotelId, MarketId = season.HotelMarketId, BeginDate = season.BeginDate, EndDate = season.EndDate });
                }
                


                ViewData["MarketList"] = db.HotelMarkets.Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Name }).ToArray();

                return View(fareHeaders);
            }

           
        }

        [HttpPost]
        public ActionResult Create(int itemCount, List<HotelFareH> fareHeaders)
        {
            using (var db = this.CreateHotelDataContext())
            {
                HotelSeason season = null;
                var firstItem = fareHeaders[0];

                if (firstItem.HotelSeasonId == 0)
                {
                    season = new HotelSeason { HotelId = firstItem.HotelId, HotelMarketId = firstItem.MarketId, BeginDate = firstItem.BeginDate, EndDate = firstItem.EndDate, Currency = firstItem.Currency, HandlingFee = 0, TransferFee = 0 };
                    db.HotelSeasons.InsertOnSubmit(season);
                }
                else
                {
                    season = db.HotelSeasons.FirstOrDefault(s => s.Id == firstItem.HotelSeasonId);
                    season.HotelMarketId = firstItem.MarketId;
                    season.BeginDate = firstItem.BeginDate;
                    season.EndDate = firstItem.EndDate;
                    season.Currency = firstItem.Currency;
                }

                for (int i = 0; i < fareHeaders.Count; i++)
                {
                    var item = fareHeaders[i];

                    if(i < itemCount) // add item
                    {
                        HotelFareH ex =  season.HotelFareHs.FirstOrDefault(e => item.Id != 0 &&  e.Id == item.Id);
                        if (ex == null)
                        {
                            season.HotelFareHs.Add(
                                new HotelFareH {
                                     BeginDate = item.BeginDate,
                                     EndDate = item.EndDate,
                                     Currency = firstItem.Currency,
                                     MarketId = firstItem.MarketId,
                                     HotelId = item.HotelId,
                                     SeasonCode = item.SeasonCode
                                });
                        }
                        else
                        {

                            ex.MarketId = firstItem.MarketId;
                            ex.Currency = firstItem.Currency;
                            ex.BeginDate = item.BeginDate;
                            ex.EndDate = item.EndDate;
                            ex.SeasonCode = item.SeasonCode;
                        }
                         
                        season.EndDate = item.EndDate;
                    }
                    else if(item.Id != 0) // remove item
                    {
                        db.HotelFareHs.DeleteByIdOnSubmit(item.Id);
                    }
                }

                db.SaveChanges(User.Identity.Name);
            }

            return RedirectToAction("Index", new { hotelid = fareHeaders[0].HotelId });
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var model = db.HotelSeasons.FirstOrDefault(i => i.Id == Id);
            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(HotelSeason model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelSeasons.FirstOrDefault(i => i.Id == model.Id);
                if(item != null)
                {
                    item.Currency = model.Currency;
                    item.HandlingFee = model.HandlingFee;
                    item.Commission = model.Commission;
                    item.TransferFee = model.TransferFee;
                    db.SaveChanges(User.Identity.Name);
                }
            }
            return RedirectToAction("Index", new { hotelid = model.HotelId });
        }

        [HttpPost]
        public ActionResult NextDay(DateTime today)
        {
            return Content(today.AddDays(1).ToShortDateString());
        }
    }
}
