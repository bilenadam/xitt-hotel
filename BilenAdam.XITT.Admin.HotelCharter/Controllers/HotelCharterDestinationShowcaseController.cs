﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using System.IO;
using BilenAdam.Hotels.Charter.Images;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelCharterDestinationShowcaseController : Controller
    {
        public ActionResult Index()
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var model = db.HotelDestinationShowcases.OrderBy(d => d.ShowOrder).ToArray();
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveShowcaseOrder(int[] ids)
        {
            using (var db = this.CreateHotelDataContext())
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    var item = db.HotelDestinationShowcases.FirstOrDefault(it => it.HotelDestinationId == ids[i]);

                    if (item != null)
                        item.ShowOrder = i + 1;
                }

                db.SaveChanges(User.Identity.Name);
            }

            return Content("OK");
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var model = db.HotelDestinationShowcases.FirstOrDefault(i => i.HotelDestinationId == Id);
            if (model == null)
                model = new HotelDestinationShowcase { HotelDestinationId = Id, ShowOrder = db.HotelShowcases.Count() + 1, IsActive = true, HotelDestination = db.HotelDestinations.FirstOrDefault(i => i.Id == Id) };

            return View(model);
        }

        string getfullPath(string fileName)
        {
            return Path.Combine(new BilenAdam.Config.AppHotelContentSettings().GetFolderPath(), "Destinations", fileName);
        }


        System.Web.HttpPostedFileBase getImageFileFromRequest(System.Web.HttpRequestBase request)
        {
            if (Request.Files != null && Request.Files.Count > 0 && Request.Files.AllKeys.Contains("imageFile") && Request.Files["imageFile"] != null && Request.Files["imageFile"].ContentLength > 0 && new HotelImageHelper().IsImage(Request.Files["imageFile"]))
            {
                return Request.Files["imageFile"];
            }
            else
                return null;
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(HotelDestinationShowcase model)
        {

            List<string> filesToDelete = new List<string>();
            System.Web.HttpPostedFileBase postedFile = null;

            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelDestinationShowcases.FirstOrDefault(d => d.HotelDestinationId == model.HotelDestinationId);
                postedFile = getImageFileFromRequest(Request);

                if (postedFile != null)
                {
                    if (item != null && System.IO.File.Exists(getfullPath(item.ImageFileName)))
                        filesToDelete.Add(getfullPath(item.ImageFileName));

                  
                   

                    model.ImageFileName = Path.ChangeExtension(System.IO.Path.GetRandomFileName(), Path.GetExtension(postedFile.FileName));
                }
                else if ( item ==  null)
                {
                        ModelState.AddModelError(string.Empty, "Invalid Image");
                        model.HotelDestination = db.HotelDestinations.FirstOrDefault(d => d.Id == model.HotelDestinationId);
                        return View(model);
                    
                }

                while (db.HotelDestinationShowcases.Count(c => c.ShowOrder == model.ShowOrder && c.HotelDestinationId != model.HotelDestinationId) > 0)
                {
                    model.ShowOrder++;
                }
                
                var upsertResult = db.HotelDestinationShowcases.Upsert(i => i.HotelDestinationId == model.HotelDestinationId, model, "HotelDestinationId");
                db.SaveChanges(User.Identity.Name);
            }

            if (postedFile != null)
            {
                string filePath = getfullPath(model.ImageFileName);
                postedFile.SaveAs(filePath);
               
            }
            foreach (var fileToDelete in filesToDelete)
            {
                System.IO.File.Delete(fileToDelete);
            }

            return RedirectToAction("Index");



        }

        [HttpGet]
        public ActionResult Delete(int Id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.HotelDestinationShowcases.FirstOrDefault(d => d.HotelDestinationId == Id);
                if (item != null)
                {
                    if (System.IO.File.Exists(getfullPath(item.ImageFileName)))
                        System.IO.File.Delete(getfullPath(item.ImageFileName));

                   

                    db.HotelDestinationShowcases.DeleteOnSubmit(item);

                    db.SaveChanges(User.Identity.Name);
                }

            }


            return RedirectToAction("Index");
        }
        public ActionResult Image(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                return HttpNotFound();
            var filePath = getfullPath(fileName);

            if (System.IO.File.Exists(filePath))
                return File(filePath, System.Web.MimeMapping.GetMimeMapping(filePath));
            else
                return HttpNotFound();
        }
    }
}
