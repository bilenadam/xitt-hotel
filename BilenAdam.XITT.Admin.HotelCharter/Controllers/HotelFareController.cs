﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

using System.Data.Linq;


namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelFareController : Controller
    {
        public ActionResult HotelSearch(string name = null, string countryCode = null, int? cityId = null, int? districtId = null, int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.Hotels.AsQueryable();

                if (!string.IsNullOrEmpty(name))
                    data = data.Where(o => o.Name.Contains(name));

                if (!string.IsNullOrEmpty(countryCode))
                    data = data.Where(o => o.HotelDestination.CountryCode == countryCode);


                if (districtId != null && cityId != null)
                    data = data.Where(o => o.HotelDestination.Id == districtId && o.HotelDestination.ParentId == cityId);
                else if (cityId != null)
                    data = data.Where(o => o.HotelDestionationId == cityId);

                var model = data.ToPagedList<Hotel>(pageIndex, pageSize);
                return View(model);
            }
        }
        public ActionResult Index(int hotelSeasonId)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

           


            var season = db.HotelSeasons.FirstOrDefault(s => s.Id == hotelSeasonId);
            ViewData["HotelSeason"] = season;
            ViewData["PensionLists"] = db.HotelPensions.Select(p => new SelectListItem { Value = p.Id.ToString(), Text = p.Name }).ToArray();

            return View(season);
        }




        [HttpGet]
        public ActionResult FareDetails(int hotelSeasonId, int pensionId)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var season = db.HotelSeasons.FirstOrDefault(s => s.Id == hotelSeasonId);
            ViewData["HotelSeason"] = season;

            Models.HotelFareDetailsModel model = new Models.HotelFareDetailsModel
            {
                HotelData = db.Hotels.FirstOrDefault(h => h.Id == season.HotelId),
                MarketData = db.HotelMarkets.FirstOrDefault(m => m.Id == season.HotelMarketId),
                PensionData = db.HotelPensions.FirstOrDefault(p => p.Id == pensionId),
                Headers = db.HotelFareHs.Where(h => h.HotelSeasonId == season.Id && h.IsSPOEnabled == null).OrderBy(h => h.BeginDate).ToList(),
                RoomGroups = new List<Models.HotelFareRoomGroupModel>()
            };


            foreach (var room in db.HotelRooms.Where(r => r.HotelId == season.HotelId))
            {
                Models.HotelFareRoomGroupModel roomModel = new Models.HotelFareRoomGroupModel
                {
                    RoomData = room,
                    DetailGroups = new List<Models.HotelFareDetailGroupModel>()
                };

                model.RoomGroups.Add(roomModel);
                foreach (var fareDetail in db.HotelFareDs.Where(r => r.RoomId == room.Id && r.PensionId == pensionId && r.HotelFareH.HotelSeasonId == hotelSeasonId && r.HotelFareH.IsSPOEnabled == null))
                {
                    var group = roomModel.DetailGroups.FirstOrDefault(g => g.Adults == fareDetail.Adults && g.Childs == fareDetail.Childs && g.FirstChildMinAge == (fareDetail.FirstChildMinAge ?? 0) && g.FirstChildMaxAge == (fareDetail.FirstChildMaxAge ?? 0) && g.SecondChildMinAge == (fareDetail.SecondChildMinAge ?? 0) && g.SecondChildMaxAge == (fareDetail.SecondChildMaxAge ?? 0) && g.ThirdChildMinAge == (fareDetail.ThirdChildMinAge ?? 0) && g.ThirdChildMaxAge == (fareDetail.ThirdChildMaxAge ?? 0) && g.FourthChildMinAge == (fareDetail.FourthChildMinAge ?? 0) && g.FourthChildMaxAge == (fareDetail.FourthChildMaxAge ?? 0));
                    if (group == null)
                    {
                        group = new Models.HotelFareDetailGroupModel
                        {
                            Adults = fareDetail.Adults,
                            Childs = fareDetail.Childs,
                            Details = model.Headers.Select(h => new Models.HotelFareHeaderDetailModel { FareHeader = h }).ToList(),
                            FirstChildMinAge = fareDetail.FirstChildMinAge ?? 0,
                            FirstChildMaxAge = fareDetail.FirstChildMaxAge ?? 0,
                            SecondChildMinAge = fareDetail.SecondChildMinAge ?? 0,
                            SecondChildMaxAge = fareDetail.SecondChildMaxAge ?? 0,
                            ThirdChildMinAge = fareDetail.ThirdChildMinAge ?? 0,
                            ThirdChildMaxAge = fareDetail.ThirdChildMaxAge ?? 0,
                            FourthChildMinAge = fareDetail.FourthChildMinAge ?? 0,
                            FourthChildMaxAge = fareDetail.FourthChildMaxAge ?? 0
                        };
                        roomModel.DetailGroups.Add(group);
                    }

                    group.Details.FirstOrDefault(d => d.FareHeader.Id == fareDetail.FareHId).FareDetail = fareDetail;
                }


            }

            return View(model);

        }


        [HttpPost]
        public ActionResult UpdateDetail(int fareid, decimal fare)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var detail = db.HotelFareDs.FirstOrDefault(i => i.Id == fareid);
                foreach (var item in db.HotelFareDs.Where(i =>  i.HotelFareH.SeasonCode == detail.HotelFareH.SeasonCode && i.HotelFareH.HotelSeasonId == detail.HotelFareH.HotelSeasonId && i.Adults == detail.Adults && i.Childs == detail.Childs))
                {
                    if (item.RoomId == detail.RoomId && item.FirstChildMaxAge == detail.FirstChildMaxAge && item.FirstChildMinAge == detail.FirstChildMinAge && item.SecondChildMaxAge == detail.SecondChildMaxAge && item.SecondChildMinAge == detail.SecondChildMinAge && item.ThirdChildMaxAge == detail.ThirdChildMaxAge && item.ThirdChildMinAge == detail.ThirdChildMinAge)
                    {
                        item.Fare = fare;
                    }
                }
                db.SaveChanges(User.Identity.Name);
            }

            return Content("OK");
        }

        [HttpPost]
        public ActionResult AddDetail(int seasonid, string seasoncode, int roomid, int pensionid, byte adults, byte childs, decimal fare, string childAges = null)
        {
            using (var db = this.CreateHotelDataContext())
            {
                byte[] childAgesArray = childAges.Split('-').Select(s => byte.Parse(s)).ToArray();
                foreach (var fareHeader in db.HotelFareHs.Where(h => h.HotelSeasonId == seasonid && h.SeasonCode == seasoncode))
                {


                    var hotelFareD = new HotelFareD
                    {
                        FareHId = fareHeader.Id,
                        RoomId = roomid,
                        PensionId = pensionid,
                        Adults = adults,
                        Childs = childs,
                        Fare = fare,
                        FirstChildMinAge = childs >= 1 ? childAgesArray[0] : (byte?)null,
                        FirstChildMaxAge = childs >= 1 ? childAgesArray[1] : (byte?)null,
                        SecondChildMinAge = childs >= 2 ? childAgesArray[2] : (byte?)null,
                        SecondChildMaxAge = childs >= 2 ? childAgesArray[3] : (byte?)null,
                        ThirdChildMinAge = childs >= 3 ? childAgesArray[4] : (byte?)null,
                        ThirdChildMaxAge = childs >= 3 ? childAgesArray[5] : (byte?)null,
                        FourthChildMinAge = childs >= 4 ? childAgesArray[6] : (byte?)null,
                        FourthChildMaxAge = childs >= 4 ? childAgesArray[7] : (byte?)null
                    };

                    db.HotelFareDs.Upsert(i =>
                    i.FareHId == hotelFareD.FareHId
                    && i.RoomId == hotelFareD.RoomId
                    && i.PensionId == hotelFareD.PensionId
                    && i.Adults == hotelFareD.Adults
                    && i.Childs == hotelFareD.Childs
                    && (childs < 1 || i.FirstChildMinAge == hotelFareD.FirstChildMinAge)
                    && (childs < 1 || i.FirstChildMaxAge == hotelFareD.FirstChildMaxAge)
                    && (childs < 2 || i.SecondChildMinAge == hotelFareD.SecondChildMinAge)
                    && (childs < 2 || i.SecondChildMaxAge == hotelFareD.SecondChildMaxAge)
                    && (childs < 3 || i.ThirdChildMinAge == hotelFareD.ThirdChildMinAge)
                    && (childs < 3 || i.ThirdChildMaxAge == hotelFareD.ThirdChildMaxAge)
                    && (childs < 4 || i.FourthChildMinAge == hotelFareD.FourthChildMinAge)
                    && (childs < 4 || i.FourthChildMaxAge == hotelFareD.FourthChildMaxAge)
                    , hotelFareD);
                }
                db.SaveChanges(User.Identity.Name);

                return Content("OK");
            }
        }

        [HttpPost]
        public ActionResult DeleteDetails(int hotelSeasonId, int pensionId, int roomid, byte adults, byte childs, string childAges = null)
        {
            byte[] childAgesArray = childAges == null ? null : childAges.Split('-').Select(s => byte.Parse(s)).ToArray();

            using (var db = this.CreateHotelDataContext())
            {
                var items = db.HotelFareDs.Where(d => d.HotelFareH.HotelSeasonId == hotelSeasonId && d.PensionId == pensionId && d.RoomId == roomid && d.Adults == adults && d.Childs == childs && d.HotelFareH.IsSPOEnabled == null

                  && (childs < 1 || d.FirstChildMinAge == childAgesArray[0])
                    && (childs < 1 || d.FirstChildMaxAge == childAgesArray[1])
                    && (childs < 2 || d.SecondChildMinAge == childAgesArray[2])
                    && (childs < 2 || d.SecondChildMaxAge == childAgesArray[3])
                    && (childs < 3 || d.ThirdChildMinAge == childAgesArray[4])
                    && (childs < 3 || d.ThirdChildMaxAge == childAgesArray[5])
                    && (childs < 4 || d.FourthChildMinAge == childAgesArray[6])
                    && (childs < 4 || d.FourthChildMaxAge == childAgesArray[7])

                ).ToArray();

                db.HotelFareDs.DeleteAllOnSubmit(items);
                db.SaveChanges(User.Identity.Name);
            }

            return RedirectToAction("FareDetails", new { hotelSeasonId = hotelSeasonId, pensionId = pensionId });
        }

    }
}
