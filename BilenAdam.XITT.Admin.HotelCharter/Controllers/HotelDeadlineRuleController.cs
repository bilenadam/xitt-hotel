﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelDeadlineRuleController : Controller
    {
        public ActionResult Index(int hotelId)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var model = db.HotelDeadlineRules.Where(r => r.HotelId == hotelId).OrderByDescending(r => r.BeginDate).ToArray();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(HotelDeadlineRule model)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelDeadlineRules.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }
            }
            catch
            {
            }
            return RedirectToAction("Show","Hotel", new { Id = model.HotelId });
        }

        [HttpGet]
        public ActionResult Delete(int Id, int hotelId)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelDeadlineRules.DeleteByIdOnSubmit(Id);
                    db.SaveChanges(User.Identity.Name);
                }
            }
            catch
            {
            }
            return RedirectToAction("Show", "Hotel", new { Id = hotelId });
        }

        [HttpPost]
        public ActionResult Edit(HotelDeadlineRule model)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelDeadlineRules.Upsert(i=>i.Id == model.Id,model, "HotelId");
                    db.SaveChanges(User.Identity.Name);
                }
            }
            catch
            {
            }
            return RedirectToAction("Show", "Hotel", new { Id = model.HotelId });
        }
    }
}
