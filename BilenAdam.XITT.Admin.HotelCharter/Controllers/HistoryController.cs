﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HistoryController : Controller
    {
        // GET: History
        public ActionResult Index(int rowId = 0, string tableName = null, int changeType = 0, string changedBy = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var tableNameSelectList = db.HotelLogs.Select(i => i.TableName).Distinct().OrderBy(i => i).ToArray().Select(i => new SelectListItem { Value = i, Text = i, Selected = i == tableName }).ToList();
                tableNameSelectList.Insert(0, new SelectListItem { Value = "", Text = "...", Selected = string.IsNullOrWhiteSpace(tableName) });
                ViewData["tableNameSelectList"] = tableNameSelectList;
                var userNameSelectList = db.HotelLogs.Select(i => i.ChangedBy).Distinct().OrderBy(i => i).ToArray().Select(i => new SelectListItem { Value = i, Text = i, Selected = i == changedBy }).ToList();
                userNameSelectList.Insert(0, new SelectListItem { Value = "", Text = "...", Selected = string.IsNullOrWhiteSpace(changedBy) });
                ViewData["userNameSelectList"] = userNameSelectList;
                ViewData["changeTypeSelectList"] = new List<SelectListItem>() { new SelectListItem { Value = "0", Text = "...", Selected = changeType == 0 }, new SelectListItem { Value = "1", Text = "Insert", Selected = changeType == 1 }, new SelectListItem { Value = "2", Text = "Update", Selected = changeType == 2 }, new SelectListItem { Value = "3", Text = "Delete", Selected = changeType == 3 } };

                var data = db.HotelLogs.AsQueryable();

                if(rowId != 0)
                    data = data.Where(i => i.RowId == rowId);

                if (!string.IsNullOrWhiteSpace(tableName))
                    data = data.Where(i => i.TableName == tableName);

                if (changeType != 0)
                    data = data.Where(i => i.ChangeType == changeType);

                if (!string.IsNullOrWhiteSpace(changedBy))
                    data = data.Where(i => i.ChangedBy == changedBy);

                if (startDate != null)
                    data = data.Where(i => i.ChangedAt >= startDate.Value);

                if (endDate != null)
                    data = data.Where(i => i.ChangedAt <= endDate.Value);

                return View(data.OrderByDescending(i => i.ChangedAt).ToPagedList(Request));
            }
        }

        public ActionResult FareLogs(int Id, int changeType = 0, string changedBy = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            using (var db = this.CreateHotelDataContext())
            {
               
                var userNameSelectList = db.vHotelFareDLogs.Select(i => i.ChangedBy).Distinct().OrderBy(i => i).ToArray().Select(i => new SelectListItem { Value = i, Text = i, Selected = i == changedBy }).ToList();
                userNameSelectList.Insert(0, new SelectListItem { Value = "", Text = "...", Selected = string.IsNullOrWhiteSpace(changedBy) });
                ViewData["userNameSelectList"] = userNameSelectList;
                ViewData["changeTypeSelectList"] = new List<SelectListItem>() { new SelectListItem { Value = "0", Text = "...", Selected = changeType == 0 }, new SelectListItem { Value = "1", Text = "Insert", Selected = changeType == 1 }, new SelectListItem { Value = "2", Text = "Update", Selected = changeType == 2 }, new SelectListItem { Value = "3", Text = "Delete", Selected = changeType == 3 } };

                var data = db.vHotelFareDLogs.Where(i=>i.HotelId == Id).AsQueryable();

                 

            
                if (changeType != 0)
                    data = data.Where(i => i.ChangeType == changeType);

                if (!string.IsNullOrWhiteSpace(changedBy))
                    data = data.Where(i => i.ChangedBy == changedBy);

                if (startDate != null)
                    data = data.Where(i => i.ChangedAt >= startDate.Value);

                if (endDate != null)
                    data = data.Where(i => i.ChangedAt <= endDate.Value);

                return View(data.OrderByDescending(i => i.ChangedAt).ToPagedList(Request));
            }
        }

        public ActionResult QuotaLogs(int Id, int changeType = 0, string changedBy = null, DateTime? quotaDate = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            using (var db = this.CreateHotelDataContext())
            {

                var userNameSelectList = db.vHotelQuotaLogs.Select(i => i.ChangedBy).Distinct().OrderBy(i => i).ToArray().Select(i => new SelectListItem { Value = i, Text = i, Selected = i == changedBy }).ToList();
                userNameSelectList.Insert(0, new SelectListItem { Value = "", Text = "...", Selected = string.IsNullOrWhiteSpace(changedBy) });
                ViewData["userNameSelectList"] = userNameSelectList;
                ViewData["changeTypeSelectList"] = new List<SelectListItem>() { new SelectListItem { Value = "0", Text = "...", Selected = changeType == 0 }, new SelectListItem { Value = "1", Text = "Insert", Selected = changeType == 1 }, new SelectListItem { Value = "2", Text = "Update", Selected = changeType == 2 }, new SelectListItem { Value = "3", Text = "Delete", Selected = changeType == 3 } };

                var data = db.vHotelQuotaLogs.Where(i => i.HotelId == Id).AsQueryable();




                if (changeType != 0)
                    data = data.Where(i => i.ChangeType == changeType);

                if (!string.IsNullOrWhiteSpace(changedBy))
                    data = data.Where(i => i.ChangedBy == changedBy);

                if (quotaDate != null)
                    data = data.Where(i => i.QuotaDate == quotaDate.Value);

                if (startDate != null)
                    data = data.Where(i => i.ChangedAt >= startDate.Value);

                if (endDate != null)
                    data = data.Where(i => i.ChangedAt <= endDate.Value);

                return View(data.OrderByDescending(i => i.ChangedAt).ToPagedList(Request));
            }
        }
    }
}