﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

using System.Data.Linq;


namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelMarketController : Controller
    {
        public ActionResult Index(int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
              ////  DataLoadOptions dlo = new DataLoadOptions();
              //  dlo.LoadWith<Contact>(c=>c.CharterProRataContracts)
                //db.LoadOptions = dlo;
                var data = db.HotelMarkets.AsQueryable();
                
                var model = data.ToPagedList<HotelMarket>(pageIndex, pageSize);
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Create()
        {

            var model = new BilenAdam.Data.HotelData.HotelMarket();
            
            
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id  )
        {
            
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    

                    
                    db.HotelMarkets.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }

               
            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
            }
            return RedirectToAction("Index", new {   pageIndex = 1, pageSize = 10 });
        }

        [HttpPost]
        public ActionResult Create(BilenAdam.Data.HotelData.HotelMarket model)
        {
           
            if (ModelState.IsValid)
            {

                try
                {
                    using (var db = this.CreateHotelDataContext())
                    {
                     
                        db.HotelMarkets.InsertOnSubmit(model);
                        db.SaveChanges(User.Identity.Name);
                    }

                    return this.Redirect2Action(s => s.Index(  1, 10), new {   pageIndex = 1, pageSize = 10 });
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.Message);
                }
            }

            return this.Redirect2Action(s => s.Index( 1, 10), new { pageIndex = 1, pageSize = 10 }); 
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                DataLoadOptions dlo = new DataLoadOptions();
                //dlo.LoadWith<HotelMarket>(c => c.HotelMarketCountries);

                var model = db.HotelMarkets.FirstOrDefault(c => c.Id == id);

                if (model == null)
                    return HttpNotFound();

                
                
                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(BilenAdam.Data.HotelData.HotelMarket model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.CreateHotelDataContext())
                {
                    DataLoadOptions dlo = new DataLoadOptions();
                   // dlo.LoadWith<HotelMarket>(c => c.HotelMarketCountries);

                    var item = db.HotelMarkets.FirstOrDefault(c => c.Id == model.Id);
                    item.Name = model.Name;
                    if (item == null)
                        return HttpNotFound();

                    
                    db.SaveChanges(User.Identity.Name);

                }
            }


            return this.Redirect2Action(s => s.Index( 1, 10), new { pageIndex = 1, pageSize = 10 }); 
        }

       
    }
}
