﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;
using System.Data.Linq;


namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelStopSaleController : Controller
    {
       
        public ActionResult Index(int hotelId,int? roomId = null, int? pensionId = null, int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
                DataLoadOptions dlo = new DataLoadOptions();
                dlo.LoadWith<HotelRoomStopSale>(c => c.HotelRoom);
                dlo.LoadWith<HotelRoomStopSale>(c => c.HotelPension);
                db.LoadOptions = dlo;

                BindingList(hotelId,addAllRoomOption:true, addPensionAll:true,selectedRoomId: roomId,selectedPensionId:pensionId);
               
                ViewBag.HotelId = hotelId;
                ViewData["HotelData"] = db.Hotels.FirstOrDefault(h => h.Id == hotelId);
              

                var data = db.HotelRoomStopSales.Where(  o=>o.HotelRoom.HotelId==hotelId).AsQueryable();

                if (roomId != null)
                    data=data.Where(o => o.RoomId == roomId);

                if (pensionId != null)
                    data = data.Where(i => i.PensionId == pensionId);

                var model = data.OrderBy(i=>i.BeginDate).ToPagedList<HotelRoomStopSale>(pageIndex, pageSize);
                return View(model);

            }
            
        }

     
        [HttpGet]
        public ActionResult Delete(int id,  int hotelId)
        {
            
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    

                    
                    db.HotelRoomStopSales.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }

               
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(null, exc.Message);
            }
            return RedirectToAction("Index", new {   pageIndex = 1, pageSize = 10, hotelId = hotelId });
        }

       


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                DataLoadOptions dlo = new DataLoadOptions();
                dlo.LoadWith<HotelRoomStopSale>(c => c.HotelRoom);

                var model = db.HotelRoomStopSales.FirstOrDefault(c => c.Id == id);

                if (model == null)
                    return HttpNotFound();
                ViewBag.HotelId = model.HotelRoom.HotelId;

                BindingList(model.HotelRoom.HotelId,selectedRoomId:model.RoomId, selectedPensionId:model.PensionId, selectedMarketId:model.MarketId);

               

                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(HotelRoomStopSale model)
        {
            if (model.BeginDate <= model.EndDate && model.EndDate >= DateTime.Today && model.BeginDate.AddYears(1) >= model.EndDate)
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelRoomStopSales.Upsert(it=>it.Id == model.Id,model);
                    
                    db.SaveChanges(User.Identity.Name);

                }
            }


            return this.Redirect2Action(s => s.Index(0,null,null,1, 10), new { hotelId = Request["hotelId"], pageIndex = 1, pageSize = 10 }); 
        }
        public void BindingList(int hotelId,bool addAllRoomOption = false ,bool addPensionAll = false,int? selectedRoomId = null, int? selectedPensionId = null, int? selectedMarketId = null)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var roomList= db.HotelRooms.Where(o => o.HotelId == hotelId).OrderBy(i => i.Name).Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.Id.ToString(),
                    Selected = o.Id == (selectedRoomId ?? -1)

                }).ToList();

                if (addAllRoomOption)
                    roomList.Insert(0,new SelectListItem
                    {
                        Value = "",
                        Text = "ALL",
                        Selected = selectedRoomId == null
                    });

                ViewBag.RoomList = roomList;
                
                ViewBag.MarketList = db.HotelMarkets.OrderBy(i => i.Name).Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.Id.ToString(),
                    Selected = o.Id == (selectedMarketId??-1)
                }).ToList();

                var pensionList = db.HotelPensions.Where(p=>p.HotelFareDs.Any(d=>d.HotelFareH.HotelId == hotelId)).OrderBy(i => i.Name).Select(i => new SelectListItem()
                {
                    Text = i.Name,
                    Value = i.Id.ToString(),
                    Selected = i.Id == (selectedPensionId??-1)
                }).ToList();

                if (addPensionAll)
                    pensionList.Insert(0,new SelectListItem() { Text = "ALL", Value = "", Selected = selectedPensionId == null });
                ViewBag.PensionList = pensionList;
            }
        }



        [HttpGet]
        public ActionResult Quick(int hotelId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                Models.QuickStopSaleModel model = new Models.QuickStopSaleModel
                {
                    HotelId = hotelId,
                    ItemCount = 1,
                    Items = new List<Models.QuickStopSaleModel.DateItem>(),
                    Rooms = db.HotelRooms.Where(r=>r.HotelId == hotelId).Select(i=>new Models.QuickStopSaleModel.RoomItem { IsSelected = true, Name = i.Name, RoomId = i.Id }).ToList(),
                    Pensions = db.HotelPensions.Where(p=>p.HotelFareDs.Any(d=>d.HotelFareH.HotelId == hotelId)).OrderBy(p=>p.Name).Select(i=> new Models.QuickStopSaleModel.PensionItem {  IsSelected = false, Name = i.Name, PensionId = i.Id }).ToList(),
                    Criteria = "*+*"
                };

                for (int i = 0; i < 10; i++)
                {
                    model.Items.Add(new Models.QuickStopSaleModel.DateItem { StartDate = DateTime.Today, EndDate = DateTime.Today });
                }

                return View(model);
            }
        }


        [HttpPost]
        public ActionResult Quick(Models.QuickStopSaleModel model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                
                var marketIds = db.HotelMarkets.Select(m => m.Id).ToArray();

                
                var criteriaList = model.GetCriteriaList();
                var specificPensions = model.Pensions.Any(p => !p.IsSelected);
                var pensionIds = model.Pensions.Where(p => p.IsSelected).Select(p => p.PensionId).ToArray();

                for (int i = 0; i < model.ItemCount; i++)
                {
                    var item = model.Items[i];

                    if (item.EndDate < item.StartDate)
                        continue;

                    if (item.EndDate < DateTime.Today)
                        continue;

                    if (item.StartDate.AddYears(1) < item.EndDate)
                        continue;
                    foreach (var room in model.Rooms)
                    {
                        if (room.IsSelected)
                        {
                            foreach (var marketId in marketIds)
                            {
                                foreach (var criteria in criteriaList)
                                {


                                    if (specificPensions)
                                    {
                                        foreach (var pensionId in pensionIds)
                                        {


                                            db.HotelRoomStopSales.InsertOnSubmit(new HotelRoomStopSale
                                            {
                                                BeginDate = item.StartDate,
                                                CreateDate = DateTime.Now,
                                                EndDate = item.EndDate,
                                                isActive = true,
                                                MarketId = marketId,
                                                RoomId = room.RoomId,
                                                Adults = criteria.Item1,
                                                Childs = criteria.Item2,
                                                PensionId = pensionId
                                            });
                                        }
                                    }
                                    else
                                    {


                                        db.HotelRoomStopSales.InsertOnSubmit(new HotelRoomStopSale
                                        {
                                            BeginDate = item.StartDate,
                                            CreateDate = DateTime.Now,
                                            EndDate = item.EndDate,
                                            isActive = true,
                                            MarketId = marketId,
                                            RoomId = room.RoomId,
                                            Adults = criteria.Item1,
                                            Childs = criteria.Item2,
                                            PensionId = null
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
                db.SaveChanges(User.Identity.Name);


            }

            return RedirectToAction("Index", new { pageIndex = 1, pageSize = 10, hotelId = model.HotelId });

        }
    }
}
