﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;
using System.Data.Linq;
using BilenAdam.Web;
using BilenAdam.XITT.Admin.HotelCharter.Models;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelController : Controller
    {
        public ActionResult Show(int Id)
        {
            var db = this.CreateHotelDataContext();

            var model = db.Hotels.FirstOrDefault(h => h.Id == Id);

            if (model == null)
            {
                db.Dispose();
                return HttpNotFound();
            }
            else
            {
                ViewData.SetDB(db);
                return View(model);
            }
        }

        #region Hotel CRUD & List

        public ActionResult Index(int? Id = null, string name = null, string location = null, bool? active = null, string giata = null, string season = null, int sortby = 0)
        {
            if (Id != null)
                return RedirectToAction("Show", new { Id = Id.Value });



            ////  DataLoadOptions dlo = new DataLoadOptions();
            //  dlo.LoadWith<Contact>(c=>c.CharterProRataContracts)
            //db.LoadOptions = dlo;

            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);
            var data = db.Hotels.AsQueryable();


            if (!string.IsNullOrEmpty(name))
                data = data.Where(o => o.Name.Contains(name));

            if (!string.IsNullOrEmpty(location))
            {
                int cityId = 0;
                if (int.TryParse(location, out cityId))
                {
                    data = data.Where(o => o.HotelDestionationId == cityId);
                }
                else
                {
                    data = data.Where(o => o.HotelDestination.CountryCode == location);
                }
            }


            if (active != null)
            {
                data = data.Where(i => i.IsActive == active.Value);
            }

            if (!string.IsNullOrWhiteSpace(giata))
            {
                data = data.Where(i => i.GIATA == giata);
            }

            try
            {
                if (season != null)
                {
                    int year = 2000 + int.Parse(season.Substring(1));
                    if (season.StartsWith("W"))
                    {
                        data = data.Where(i => i.HotelSeasons.Any(s => s.BeginDate > new DateTime(year, 11, 1) && s.BeginDate < new DateTime(year + 1, 4, 30)));
                    }
                    else
                    {
                        data = data.Where(i => i.HotelSeasons.Any(s => s.BeginDate > new DateTime(year, 1, 1) && s.BeginDate < new DateTime(year, 10, 30)));
                    }





                }
            }
            catch
            {

            }


            switch (sortby)
            {
                case 1:
                    data = data.OrderBy(i => i.Stars);
                    break;
                case 2:
                    data = data.OrderBy(i => i.HotelDestination.Name);
                    break;
                case 3:
                    data = data.OrderBy(i => i.HotelSeasons.Max(s => s.BeginDate));
                    break;
                case 4:
                    data = data.OrderBy(i => i.GIATA);
                    break;
                case 0:
                default:
                    data = data.OrderBy(i => i.Name);
                    break;
            }


            var model = data.ToPagedList<Hotel>(Request);



            List<SelectListItem> hotelDestinations = this.HttpContext.Cache["HotelDestinations"] as List<SelectListItem>;

            if (hotelDestinations == null)
            {
                hotelDestinations = new List<SelectListItem>();
                hotelDestinations.Add(new SelectListItem { Value = "", Text = "..." });
                string lastCountry = null;
                foreach (var l in db.HotelDestinations.Where(d => d.Hotels.Any()).OrderBy(d => d.CountryCode).ThenBy(c => c.Name))
                {
                    if (l.CountryCode != lastCountry)
                    {
                        hotelDestinations.Add(new SelectListItem { Value = l.CountryCode, Text = BilenAdam.TravelCode.Countries.CountryDictionary.GetCountry(l.CountryCode).GetLocalizedName(System.Globalization.CultureInfo.CurrentCulture), Selected = location == l.CountryCode });
                        lastCountry = l.CountryCode;
                    }
                    hotelDestinations.Add(new SelectListItem { Value = l.Id.ToString(), Text = "&nbsp;&nbsp;&nbsp;&nbsp;" + l.Name, Selected = location == l.Id.ToString() });

                }

                this.HttpContext.Cache["HotelDestinations"] = hotelDestinations;
            }
            else
            {
                foreach (var item in hotelDestinations)
                {
                    item.Selected = item.Value == location;

                }
            }



            ViewData["destinations"] = hotelDestinations;
            return View(model);

        }

        public ActionResult Delete(int Id, string countryCode = null, int? cityId = null, string name = null, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    //db.HotelCharterReservations.DeleteAllOnSubmit(r => r.HotelId == Id);
                    db.HotelPromotionCombinations.DeleteAllOnSubmit(c => c.HotelPromotion.HotelId == Id || c.HotelPromotion1.HotelId == Id);
                    db.HotelPromotions.DeleteAllOnSubmit(p => p.HotelId == Id);
                    db.Hotels.DeleteByIdOnSubmit(Id);
                    db.SaveChanges(User.Identity.Name);
                }
            }
            catch
            {

            }
            return RedirectToAction("Index", new { countryCode = countryCode, cityId = cityId, name = name, pageIndex = pageIndex, pageSize = pageSize });
        }

        [HttpGet]
        public ActionResult Create()
        {

            var model = new Hotel();


            using (var db = this.CreateHotelDataContext())
            {
                var lst = db.HotelCategories.ToList();
                ViewBag.ListItem = lst.Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.Id.ToString()
                }).ToList();

            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(Hotel model)
        {
            try
            {
                var mdl = new Hotel();
                using (var db = this.CreateHotelDataContext())
                {

                    PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);
                    PropertyResourceEditorModelUtils.FillResources(model, m => m.Description, Request);
                    PropertyResourceEditorModelUtils.FillResources(model, m => m.RoomDescription, Request);
                    PropertyResourceEditorModelUtils.FillResources(model, m => m.LocationDescription, Request);
                    db.Hotels.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }

                this.HttpContext.Cache.Remove("HotelDestinations");

                return this.Redirect2Action(s => s.Show(model.Id), new { Id = model.Id });

            }
            catch (Exception exc)
            {
                ModelState.AddModelError("", exc.Message);
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                DataLoadOptions dlo = new DataLoadOptions();

                dlo.LoadWith<Hotel>(h => h.HotelDestination);
                db.LoadOptions = dlo;

                var model = db.Hotels.FirstOrDefault(c => c.Id == id);

                if (model == null)
                    return HttpNotFound();

                var lst = db.HotelCategories.ToList();
                ViewBag.ListItem = lst.Select(o => new SelectListItem()
                {
                    Text = o.Name,
                    Value = o.Id.ToString()
                }).ToList();

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(Hotel model)
        {

            using (var db = this.CreateHotelDataContext())
            {

                var item = db.Hotels.FirstOrDefault(c => c.Id == model.Id);

                if (item == null)
                    return HttpNotFound();

                PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);
                PropertyResourceEditorModelUtils.FillResources(model, m => m.Description, Request);
                PropertyResourceEditorModelUtils.FillResources(model, m => m.RoomDescription, Request);
                PropertyResourceEditorModelUtils.FillResources(model, m => m.LocationDescription, Request);

                item.UpdateFrom(model);
                db.SaveChanges(User.Identity.Name);

            }



            return this.Redirect2Action(s => s.Show(model.Id), new { Id = model.Id });

        }

        #endregion

        public ActionResult Location()
        {



            return View();

        }

        #region Hotel Amenity 

        [HttpGet]
        public ActionResult AmenityPair(int hotelId)
        {
            using (var db = this.CreateHotelDataContext())
            {

                DataLoadOptions dlo = new DataLoadOptions();
                dlo.LoadWith<HotelRoomAmenityPair>(c => c.HotelAmenity);

                var amenites = db.HotelAmenities.Where(o => o.isProperty).ToList();

                var roomAmenity =
                    db.HotelAmenityPairs.Where(o => o.HotelId == hotelId).ToList().Select(o => new HotelAmenityPair()
                    {

                        HotelId = o.HotelId,
                        Id = o.Id,
                        IsPaid = o.IsPaid,
                        HotelAmenity = o.HotelAmenity

                    }).ToList();

                foreach (var hotelAmenity in amenites)
                {
                    if (!roomAmenity.Any(o => o.AmenityId == hotelAmenity.Id))
                    {
                        var hotelRoomAmenityPair = new HotelAmenityPair()
                        {
                            Id = 0,
                            IsPaid = 0,
                            HotelId = hotelId,
                            HotelAmenity = hotelAmenity

                        };
                        roomAmenity.Add(hotelRoomAmenityPair);
                    }
                }
                ViewBag.ListItem = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Value = "0",
                        Text = "Yok"
                    },
                    new SelectListItem()
                    {
                        Value = "1",
                        Text = "Ücretsiz"
                    },
                    new SelectListItem()
                    {
                        Value = "2",
                        Text = "Ücretli"
                    }
                };

                return View(roomAmenity.OrderBy(o => o.HotelAmenity.Name).ToList());
            }



        }

        [HttpPost]
        public ActionResult AmenityPair(List<HotelAmenityPair> model, int hId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                foreach (var htlAmnty in model)
                {
                    if (htlAmnty.IsPaid == 0 && htlAmnty.Id != 0)
                    {
                        db.HotelAmenityPairs.DeleteByIdOnSubmit(htlAmnty.Id);
                        db.SaveChanges(User.Identity.Name);
                    }
                    if (htlAmnty.IsPaid != 0 && htlAmnty.Id != 0)
                    {
                        var itm = db.HotelAmenityPairs.FirstOrDefault(o => o.Id == htlAmnty.Id);
                        itm.IsPaid = htlAmnty.IsPaid;
                        db.SaveChanges(User.Identity.Name);
                    }
                    if (htlAmnty.IsPaid != 0 && htlAmnty.Id == 0)
                    {

                        db.HotelAmenityPairs.InsertOnSubmit(htlAmnty);
                        db.SaveChanges(User.Identity.Name);
                    }
                }
            }


            return this.Redirect2Action(s => s.Show(1), new { Id = hId });
        }

        #endregion

        public ActionResult ToggleActivation(int id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var item = db.Hotels.FirstOrDefault(i => i.Id == id);
                if (item != null)
                {
                    item.IsActive = !item.IsActive;
                    db.SaveChanges(User.Identity.Name);
                    if (item.IsActive)
                        return Content("<i class=\"fa fa-2x fa-check text-success\"></i>");
                }
            }

            return Content("<i class=\"fa fa-2x fa-minus-circle text-danger\"></i>");
        }

        public ActionResult acName(string term, int limit = 50)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.Hotels.Where(h => h.Name.StartsWith(term));

                return Json(data.Select(r => r.Name).ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

    }
}
