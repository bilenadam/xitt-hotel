﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;
using System.Data.Linq;
using BilenAdam.XITT.Admin.HotelCharter.Models;


namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelRoomController : Controller
    {
        public ActionResult Index(int hotelId=0,int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
             
                var data = db.HotelRooms.Where(o=>o.HotelId==hotelId).AsQueryable();
                ViewBag.HotelId = hotelId;
                var model = data.ToPagedList<HotelRoom>(pageIndex, pageSize);
                


                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Create(int hotelId)
        {

            using (var db = this.CreateHotelDataContext())
            {
                ViewData["RoomTypes"] = db.HotelRoomTypes.OrderBy(r=>r.RoomType).ToArray().Select(r => new SelectListItem { Value = r.Id.ToString(), Text = r.GetPropertyResouce(t => t.RoomType) }).ToArray();
            }

            var model = new BilenAdam.Data.HotelData.HotelRoom();
            model.HotelId = hotelId;
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int id , int hotelId)
        {
            
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    
                    db.HotelRooms.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }

               
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(null, exc.Message);
            }
            return RedirectToAction("Show","Hotel", new { Id = hotelId });
        }

        [HttpPost]
        public ActionResult Create(BilenAdam.Data.HotelData.HotelRoom model)
        {
           
            if (ModelState.IsValid)
            {

                try
                {
                    using (var db = this.CreateHotelDataContext())
                    {
                     
                        
                        PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);
                        PropertyResourceEditorModelUtils.FillResources(model, m => m.Description, Request);

                        db.HotelRooms.InsertOnSubmit(model);
                        db.SaveChanges(User.Identity.Name);
                    }

                    return this.Redirect2Action(s => s.Index(model.HotelId, 1, 10), new { hotelId = model.HotelId, pageIndex = 1, pageSize = 10 });
                }
                catch (Exception exc)
                {

                    ModelState.AddModelError(string.Empty, exc.Message);
                }
            }

            return RedirectToAction("Show", "Hotel", new { Id = model.HotelId });
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            using (var db = this.CreateHotelDataContext())
            {
              
                var model = db.HotelRooms.FirstOrDefault(c => c.Id == id);

                if (model == null)
                    return HttpNotFound();

                
                    ViewData["RoomTypes"] = db.HotelRoomTypes.OrderBy(r => r.RoomType).ToArray().Select(r => new SelectListItem { Value = r.Id.ToString(), Text = r.GetPropertyResouce(t => t.RoomType),Selected =  r.Id == model.HotelRoomTypeId }).ToArray();
                


                ViewBag.HotelId = model.HotelId;
            
                return View(model);
            }
            
        }

        [HttpPost]
        public ActionResult Edit(BilenAdam.Data.HotelData.HotelRoom model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.CreateHotelDataContext())
                {
                   

                    var item = db.HotelRooms.FirstOrDefault(c => c.Id == model.Id);

                    if (item == null)
                        return HttpNotFound();


                    PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);
                    PropertyResourceEditorModelUtils.FillResources(model, m => m.Description, Request);

                    item.UpdateFrom(model);

                    db.SaveChanges(User.Identity.Name);

                  
                }
            }


            return RedirectToAction("Show", "Hotel", new { Id = model.HotelId });
        }

        [HttpGet]
        public ActionResult AmenityPair(int roomId,int hotelId)
        {
            using (var db = this.CreateHotelDataContext())
            {

                DataLoadOptions dlo = new DataLoadOptions();
                dlo.LoadWith<HotelRoomAmenityPair>(c => c.HotelAmenity);

                var amenites = db.HotelAmenities.Where(o => !o.isProperty).ToList();

                var roomAmenity= db.HotelRoomAmenityPairs.Where(o => o.RoomId == roomId).ToList().Select(o=> new HotelRoomAmenityPair()
                {
                    
                    RoomId = o.RoomId,
                    Id = o.Id,
                    IsPaid = o.IsPaid,
                    HotelAmenity = o.HotelAmenity
                    
                }).ToList();

                foreach (var hotelAmenity in amenites)
                {
                    if (!roomAmenity.Any(o => o.AmenityId == hotelAmenity.Id))
                    {
                        var hotelRoomAmenityPair = new HotelRoomAmenityPair()
                        {
                            
                            Id = 0,
                            IsPaid = false,
                            RoomId = roomId,
                            HotelAmenity = hotelAmenity
                            
                        };
                        roomAmenity.Add(hotelRoomAmenityPair);
                    }
                }
                ViewBag.ListItem = new List<SelectListItem>()
                {
                    new SelectListItem()
                    {
                        Value = "0",
                        Text = "Yok"
                    },
                        new SelectListItem()
                    {
                        Value = "1",
                        Text = "Ücretsiz"
                    },
                        new SelectListItem()
                    {
                        Value = "2",
                        Text = "Ücretli"
                    }
                };

                return View(roomAmenity.OrderBy(o=>o.HotelAmenity.Name).ToList());
            }


            
        }

        [HttpPost]
        public ActionResult AmenityPair(List<BilenAdam.Data.HotelData.HotelRoomAmenityPair> model)
        {
            using (var db = this.CreateHotelDataContext())
            {
                foreach (var htlAmnty in model)
                {
                    if (htlAmnty.IsPaid == false && htlAmnty.Id != 0)
                    {
                        db.HotelRoomAmenityPairs.DeleteByIdOnSubmit(htlAmnty.Id);
                        db.SaveChanges(User.Identity.Name);
                    }
                    if (htlAmnty.IsPaid  && htlAmnty.Id != 0)
                    {
                        var itm = db.HotelRoomAmenityPairs.FirstOrDefault(o => o.Id == htlAmnty.Id);
                        itm.IsPaid = htlAmnty.IsPaid;
                        db.HotelRoomAmenityPairs.Upsert(i=>i.Id == itm.Id,itm);
                        db.SaveChanges(User.Identity.Name);
                    }
                    if (htlAmnty.IsPaid && htlAmnty.Id == 0)
                    {
                        
                        db.HotelRoomAmenityPairs.InsertOnSubmit(htlAmnty);
                        db.SaveChanges(User.Identity.Name);
                    }
                }
            }
            return RedirectToAction("Show", "Hotel", new { Id = Request.Form["hotelId"] });
           
        }
    }
}
