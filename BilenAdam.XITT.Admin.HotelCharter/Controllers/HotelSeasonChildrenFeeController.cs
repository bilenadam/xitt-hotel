﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelSeasonChildrenFeeController : Controller
    {
        public ActionResult Index(int hotelSeasonId, int hotelId)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);
            var model = db.HotelSeasonChildrenFees.Where(s => s.HotelSeasonId == hotelSeasonId).ToArray();
            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(int hotelSeasonId, int hotelId, int maxAge)
        {
            using (var db = this.CreateHotelDataContext())
            {
                db.HotelSeasonChildrenFees.DeleteFirstOnSubmit(i => i.HotelSeasonId == hotelSeasonId && i.MaxAge == maxAge);
                db.SaveChanges(User.Identity.Name);
            }
            return RedirectToAction("Index", new { hotelSeasonId = hotelSeasonId, hotelId = hotelId });
        }
        [HttpPost]
        public ActionResult Save(HotelSeasonChildrenFee model, int hotelId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                db.HotelSeasonChildrenFees.Upsert(m => m.HotelSeasonId == model.HotelSeasonId && m.MaxAge == model.MaxAge, model, "HotelSeasonId", "MaxAge");
                db.SaveChanges(User.Identity.Name);
            }
            return RedirectToAction("Index", new { hotelSeasonId = model.HotelSeasonId, hotelId = hotelId });
        }
    }
}
