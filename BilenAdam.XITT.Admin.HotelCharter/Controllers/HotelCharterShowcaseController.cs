﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelCharterShowcaseController : Controller
    {
        public ActionResult Index()
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var model = db.HotelShowcases.OrderBy(s => s.ShowOrder).ToArray();
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveShowcaseOrder(int[] ids)
        {
            using (var db = this.CreateHotelDataContext())
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    var item = db.HotelShowcases.FirstOrDefault(it => it.Id == ids[i]);

                    if (item != null)
                        item.ShowOrder = i + 1;
                }

                db.SaveChanges(User.Identity.Name);
            }

            return Content("OK");
        }

        [HttpGet]
        public ActionResult Edit(int hotelid)
        {
            var db = this.CreateHotelDataContext();
            ViewData.SetDB(db);

            var model = db.HotelShowcases.FirstOrDefault(i => i.HotelId == hotelid);
            if (model == null)
                model = new HotelShowcase { HotelId = hotelid, ShowOrder = db.HotelShowcases.Count() + 1 };

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(HotelShowcase model, bool redirectToHotel = false)
        {
            using (var db = this.CreateHotelDataContext())
            {
                while(db.HotelShowcases.Count(c=>c.ShowOrder == model.ShowOrder && c.Id != model.Id) > 0)
                {
                    model.ShowOrder++;
                }


                db.HotelShowcases.Upsert(i => i.HotelId == model.HotelId, model);
                db.SaveChanges(User.Identity.Name);
                
            }

            if (redirectToHotel)
                return RedirectToAction("Show", "Hotel", new { Id = model.HotelId });
            else
                return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int hotelid, bool redirectToHotel = false)
        {
            using (var db = this.CreateHotelDataContext())
            {
                db.HotelShowcases.DeleteFirstOnSubmit(d => d.HotelId == hotelid);
                db.SaveChanges(User.Identity.Name);
            }

            if (redirectToHotel)
                return RedirectToAction("Show", "Hotel", new { Id = hotelid });
            else
                return RedirectToAction("Index");
        }
    }
}
