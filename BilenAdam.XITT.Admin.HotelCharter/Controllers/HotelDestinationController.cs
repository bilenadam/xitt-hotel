﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

using System.Data.Linq;
using BilenAdam.XITT.Admin.HotelCharter.Models;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelDestinationController : Controller
    {
    
        public ActionResult GetDestionations(string countryCode)
        {
            using (var db = this.CreateHotelDataContext())
            {
                return Json(db.HotelDestinations.Where(s => s.CountryCode == countryCode).OrderBy(d=>d.Name).Select(d => new { id = d.Id, state = d.State??"" , name = d.Name }).ToArray(), JsonRequestBehavior.AllowGet);
            }
        }

       

        public ActionResult Index(string name = null,string country = null, string iataCode = null, int pageIndex = 1, int pageSize = 10)
        {
            using (var db = this.CreateHotelDataContext())
            {
                DataLoadOptions dlo = new DataLoadOptions();
                dlo.LoadWith<HotelDestination>(d => d.HotelDestinationShowcase);
                db.LoadOptions = dlo;

                var data = db.HotelDestinations.AsQueryable();

                if(!string.IsNullOrWhiteSpace(name))
                {
                    data = data.Where(d => d.Name.Contains(name));
                }
                if (!string.IsNullOrWhiteSpace(country))
                {
                    data = data.Where(d => d.CountryCode == country.Trim().ToUpperInvariant());
                }

                if (!string.IsNullOrWhiteSpace(iataCode))
                {
                    data = data.Where(d => d.IataCityCode == iataCode.Trim().ToUpperInvariant());
                }

                var model = data.ToPagedList<HotelDestination>(pageIndex, pageSize);
                return View(model);
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new HotelDestination());
        }

        [HttpPost]
        public ActionResult Create(HotelDestination model)
        {
            if (string.IsNullOrWhiteSpace(model.State))
                model.State = null;
            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = this.CreateHotelDataContext())
                    {
                        PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);
                        db.HotelDestinations.InsertOnSubmit(model);
                        db.SaveChanges(User.Identity.Name);
                    }

                    return RedirectToAction("Index", new { iataCode = model.IataCityCode });
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.Message);
                }
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var model = db.HotelDestinations.GetById(Id);
                if (model == null)
                    return HttpNotFound();
                else
                    return View(model);
            }
        }

        [HttpGet]
        public ActionResult Delete(int Id, string name = null, string country = null, string iataCode = null, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelDestinations.DeleteByIdOnSubmit(Id);
                    db.SaveChanges(User.Identity.Name);   
                }
            }
            catch
            {
                
            }

            return RedirectToAction("Index",new {name = name, country = country, iataCode = iataCode, pageIndex = pageIndex, pageSize = pageSize });
        }

        [HttpPost]
        public ActionResult Edit(HotelDestination model)
        {
            if (string.IsNullOrWhiteSpace(model.State))
                model.State = null;

            if (ModelState.IsValid)
            {
                try
                {
                    using (var db = this.CreateHotelDataContext())
                    {
                        PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);

                        var item = db.HotelDestinations.GetById(model.Id);
                        if (item == null)
                            throw new Exception("record does not exists");
                        else
                            item.UpdateFrom(model);
                        db.SaveChanges(User.Identity.Name);
                        return RedirectToAction("Index", new { iataCode = item.IataCityCode });
                    }


                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.Message);
                }
            }

            return View(model);
        }


        [HttpPost]
        public ActionResult GetCities(string countryCode)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.HotelDestinations.Where(d => d.CountryCode == countryCode).Select(d => new { Id = d.Id, Name = d.Name }).ToArray();
                return Json(data);
            }
            
        }

        [HttpPost]
        public ActionResult GetDistrincts(int cityId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.HotelDestinations.Where(d =>  d.ParentId == cityId).Select(d => new { Id = d.Id, Name = d.Name }).ToArray();
                return Json(data);
            }
        }
    }
}
