﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password, bool rememberMe = false, string returnUrl = null)
        {
            using (var db = BilenAdam.Data.HotelData.HotelDataContextFactory.Create())
            {
                var user = db.ManagementUsers.FirstOrDefault(u => u.Username == username && u.Password == password && u.IsActive);

                if (user != null)
                {

                    string[] roles = string.IsNullOrWhiteSpace(user.Roles) ? null : user.Roles.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()).ToArray();
                    BilenAdam.Web.Membership.FormAuthenticationExtensions.SetAdminUser(username, roles, false);

                    //db.LoginLogs.InsertOnSubmit(new BilenAdam.Data.TravelData.LoginLog { ApplicationName = "admin.xitt.de", IpAddress = Request.ServerVariables["REMOTE_ADDR"], IsSuccess = true, Username = username, LoginDate = DateTime.UtcNow });
                    //db.SubmitChanges();

                    if (string.IsNullOrWhiteSpace(returnUrl))
                        return RedirectToAction("Index", "Home");
                    else
                        return Redirect(returnUrl);
                }
                else
                {
                    //db.LoginLogs.InsertOnSubmit(new BilenAdam.Data.TravelData.LoginLog { ApplicationName = "admin.xitt.de", IpAddress = Request.ServerVariables["REMOTE_ADDR"], IsSuccess = false, Username = username, LoginDate = DateTime.UtcNow });
                    //db.SubmitChanges();
                }
            }
            ModelState.AddModelError("", "Not authorized");
            return View();
        }

        public ActionResult LogOff(string returnUrl = null)
        {
            BilenAdam.Web.Membership.FormAuthenticationExtensions.Logoff();

            if (!string.IsNullOrWhiteSpace(returnUrl))
                return Redirect(returnUrl);
            else
                return RedirectToAction("Login");
        }
    }
}