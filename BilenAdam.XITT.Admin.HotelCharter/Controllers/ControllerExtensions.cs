﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public static class ControllerExtensions
    {
      



        public static HotelDataContext CreateHotelDataContext(this Controller controller, DataLoadOptions loadOptions = null)
        {
            return HotelDataContextFactory.Create(loadOptions);
        }
    }
}
