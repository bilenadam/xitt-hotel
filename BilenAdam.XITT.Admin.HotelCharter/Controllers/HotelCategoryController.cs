﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

using System.Data.Linq;
using BilenAdam.XITT.Admin.HotelCharter.Models;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelCategoryController : Controller
    {
        public ActionResult Index(int pageIndex = 1, int pageSize = 10)
        {
           
         
            using (var db = this.CreateHotelDataContext())
            {
                ////  DataLoadOptions dlo = new DataLoadOptions();
                //  dlo.LoadWith<Contact>(c=>c.CharterProRataContracts)
                //db.LoadOptions = dlo;
                var data = db.HotelCategories.AsQueryable();

                var model = data.ToPagedList<HotelCategory>(pageIndex, pageSize);
                return View(model);
            }
        }

    

        [HttpPost]
        public ActionResult Create(BilenAdam.Data.HotelData.HotelCategory model)
        {
            
            if (ModelState.IsValid)
            {

                try
                {
                    using (var db = this.CreateHotelDataContext())
                    {
                       

                        PropertyResourceEditorModelUtils.FillResources(model, m => m.Name, Request);
                        db.HotelCategories.InsertOnSubmit(model);
                        db.SaveChanges(User.Identity.Name);
                    }

                    return this.Redirect2Action(s => s.Index(1, 10));
                }
                catch (Exception exc)
                {
                    ModelState.AddModelError(string.Empty, exc.Message);
                }
            }
            return this.Redirect2Action(s => s.Index(1, 10));
        }

       


     

        [HttpPost]
        public ActionResult Edit(BilenAdam.Data.HotelData.HotelCategory model)
        {
            if(ModelState.IsValid)
            {
                using (var db = this.CreateHotelDataContext())
                {
                  

                    var item = db.HotelCategories.FirstOrDefault(c => c.Id == model.Id);

                    if (item == null)
                        return HttpNotFound();

                    item.UpdateFrom(model,"xRes");
                    PropertyResourceEditorModelUtils.FillResources(item, m => m.Name, Request);
                    db.SaveChanges(User.Identity.Name);
                   

                }
            }

            return this.Redirect2Action(s => s.Index(1, 10));
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {
                    db.HotelCategories.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                    
                }


            }
            catch (Exception exc)
            {
                ModelState.AddModelError(null, exc.Message);
            }
            return this.Redirect2Action(s => s.Index(1, 10));
        }

       
    }
}
