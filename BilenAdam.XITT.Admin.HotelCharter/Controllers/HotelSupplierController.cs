﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using System.Web.Mvc;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelSupplierController : Controller
    {
        [HttpGet]
        public ActionResult Edit(int hotelId)
        {
            using (var db = this.CreateHotelDataContext())
            {
                int[] contactTypes = new int[] { (int)BilenAdam.Crm.ContactType.Hotel, (int)BilenAdam.Crm.ContactType.Incoming, (int)BilenAdam.Crm.ContactType.TourOperator, (int)BilenAdam.Crm.ContactType.HandlingPartner };

                

                var model = db.HotelSuppliers.FirstOrDefault(s => s.HotelId == hotelId);
                if (model == null)
                    model = new HotelSupplier { HotelId = hotelId };

                using (var tdb = HotelDataContextFactory.Create())
                {
                    
                    ViewBag.ContactList = tdb.Contacts.Where(c => contactTypes.Contains(c.Type)).Select(c => new SelectListItem { Value = c.Id.ToString(), Text = c.Name, Selected = c.Id == model.SupplierContactId }).ToArray();
                }
               

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(HotelSupplier model)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {

                    if (string.IsNullOrWhiteSpace(model.IncomingEmailAddress))
                    {

                        using (var tdb = HotelDataContextFactory.Create())
                        {
                            var incProfile = tdb.IncommingProfiles.FirstOrDefault(p => p.ContactId == model.SupplierContactId);
                            if (incProfile != null)
                            {
                                model.IncomingEmailAddress = incProfile.IncommingEmail;
                                model.SaleListEmailAddress = incProfile.SaleListEmail;
                                model.TransferListEmailAddress = incProfile.TransferListEmail;
                            }
                            else
                            {
                                model.IncomingEmailAddress = tdb.Contacts.FirstOrDefault(c => c.Id == model.SupplierContactId).GetCompanyProfile().Email;
                            }
                        }

                      
                    }

                    db.HotelSuppliers.Upsert(i => i.HotelId == model.HotelId, model);
                    db.SaveChanges(User.Identity.Name);

                    return RedirectToAction("Index", "Hotel", new { Id = model.HotelId});
                }
            }
            catch
            {
                return RedirectToAction("Edit", new { hotelId = model.HotelId });
            }
        }
    }
}
