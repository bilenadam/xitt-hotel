﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BilenAdam.Data.HotelData;
using BilenAdam.Utilities.Paging;

namespace BilenAdam.XITT.Admin.HotelCharter.Controllers
{
    public class HotelRoomTypeController : Controller
    {

        public ActionResult Index()
        {
            using (var db = this.CreateHotelDataContext())
            {
                var data = db.HotelRoomTypes.AsQueryable();

                if (!string.IsNullOrWhiteSpace(Request["name"]))
                    data = data.Where(i => i.RoomType.Contains(Request["name"]));

                var model = data.OrderBy(i=>i.RoomType).ToPagedList<HotelRoomType>(Request);
                return View(model);
            }
        }







        [HttpPost]
        public ActionResult Create(BilenAdam.Data.HotelData.HotelRoomType model)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {

                    db.HotelRoomTypes.InsertOnSubmit(model);
                    db.SaveChanges(User.Identity.Name);
                }

                return this.Redirect2Action(s => s.Index());
            }
            catch (Exception exc)
            {
                ModelState.AddModelError(string.Empty, exc.Message);
            }

            return this.Redirect2Action(s => s.Index());
        }




        [HttpPost]
        public ActionResult Edit(BilenAdam.Data.HotelData.HotelRoomType model)
        {
            if (ModelState.IsValid)
            {
                using (var db = this.CreateHotelDataContext())
                {


                    var item = db.HotelRoomTypes.FirstOrDefault(c => c.Id == model.Id);

                    if (item == null)
                        return HttpNotFound();

                    item.UpdateFrom(model, "xRes");

                    db.SaveChanges(User.Identity.Name);

                }

            }
            return this.Redirect2Action(s => s.Index());
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = this.CreateHotelDataContext())
                {

                    db.HotelRoomTypes.DeleteByIdOnSubmit(id);
                    db.SaveChanges(User.Identity.Name);
                }


            }
            catch (Exception exc)
            {
                ModelState.AddModelError(null, exc.Message);
            }
            return this.Redirect2Action(s => s.Index());
        }
    }
}
