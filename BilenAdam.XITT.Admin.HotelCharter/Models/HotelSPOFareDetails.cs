﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.Models
{
    public class HotelSPOFareDetails
    {
        

        public BilenAdam.Data.HotelData.HotelPension PensionData { get; set; }
        

        public BilenAdam.Data.HotelData.HotelFareH Header { get; set; }

        public List<HotelSPOFareRoomGroup> RoomGroups { get; set; }
    }

    public class HotelSPOFareRoomGroup
    {
        public BilenAdam.Data.HotelData.HotelRoom RoomData { get; set; }

        public List<BilenAdam.Data.HotelData.HotelFareD> Details { get; set; }
    }
}
