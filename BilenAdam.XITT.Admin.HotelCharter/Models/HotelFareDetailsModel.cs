﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.Models
{
    public class HotelFareDetailsModel
    {
        public BilenAdam.Data.HotelData.Hotel HotelData { get; set; }

        public BilenAdam.Data.HotelData.HotelMarket MarketData { get; set; }

        public BilenAdam.Data.HotelData.HotelPension PensionData { get; set; }

        public List<BilenAdam.Data.HotelData.HotelFareH> Headers { get; set; }

        public List<HotelFareRoomGroupModel> RoomGroups { get; set; }
    }

    public class HotelFareRoomGroupModel
    {
        public BilenAdam.Data.HotelData.HotelRoom RoomData { get; set; }

        public List<HotelFareDetailGroupModel> DetailGroups { get; set; }
    }

    public class HotelFareDetailGroupModel
    {
        public byte Adults { get; set; }

        public byte Childs { get; set; }

        public List<HotelFareHeaderDetailModel> Details { get; set; }

        public byte FirstChildMinAge { get; set; }
        public byte FirstChildMaxAge { get; set; }
        public byte SecondChildMinAge { get; set; }
        public byte SecondChildMaxAge { get; set; }

        public byte ThirdChildMinAge { get; set; }

        public byte ThirdChildMaxAge { get; set; }

        public byte FourthChildMinAge { get; set; }
        public byte FourthChildMaxAge { get; set; }

        public string GetChildAges()
        {
            return string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}", FirstChildMinAge, FirstChildMaxAge, SecondChildMinAge, SecondChildMaxAge, ThirdChildMinAge, ThirdChildMaxAge, FourthChildMinAge, FourthChildMaxAge);
        }
    }

    public class HotelFareHeaderDetailModel
    {
        public BilenAdam.Data.HotelData.HotelFareH FareHeader { get; set; }
        public BilenAdam.Data.HotelData.HotelFareD FareDetail { get; set; }
    }


}
