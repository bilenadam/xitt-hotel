﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.XITT.Admin.HotelCharter.Models
{
    public class QuickStopSaleModel
    {
        public int HotelId { get; set; }

        public int ItemCount { get; set; }
        public List<DateItem> Items { get; set; }

        public List<RoomItem> Rooms { get; set; }

        public List<PensionItem> Pensions { get; set; }
        public class DateItem
        {
            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }
        }

        public class RoomItem
        {
            public int RoomId { get; set; }

            public string Name { get; set; }

            public bool IsSelected { get; set; }

        }

        public string Criteria
        {
            get;set;
        }

        public class PensionItem
        {
            public int PensionId { get; set; }

            public string Name { get; set; }

            public bool IsSelected { get; set; }

        }

       
        public List<Tuple<byte?, byte?>> GetCriteriaList()
        {
            List<Tuple<byte?, byte?>> list = new List<Tuple<byte?, byte?>>();

            if (string.IsNullOrWhiteSpace(Criteria))
            {
                list.Add(new Tuple<byte?, byte?>(null, null));
            }
            else
            {
                foreach (var item in Criteria.RemoveWhitespaces().Split(','))
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        try
                        {
                            var parts = item.Split('+');
                            list.Add(new Tuple<byte?, byte?>(TryParseUtil.TryParseByte(parts[0]), TryParseUtil.TryParseByte(parts[1])));
                        }
                        catch
                        {

                        }
                    }
                }
            }

            return list;
        }
    }
}
