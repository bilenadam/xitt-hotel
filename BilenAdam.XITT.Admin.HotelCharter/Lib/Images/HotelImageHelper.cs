﻿using ImageProcessor;
using ImageProcessor.Imaging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Images
{
    public class HotelImageHelper
    {
        HotelContentFolder hcf = new HotelContentFolder();
        string getGuidName(System.Web.HttpPostedFileBase postedFile)
        {
            return Path.ChangeExtension(Guid.NewGuid().ToString("N").ToLowerInvariant(), Path.GetExtension(postedFile.FileName));
        }

       
        public void DeleteImage(string fileName, int hotelId)
        {
            // delete orginal file
            var filePath = hcf.GetOrginalPath(hotelId, fileName);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            // delete thumbnail
            filePath = hcf.GetThumbnailPath(hotelId, fileName);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);

            // delete showcase
            filePath = hcf.GetShowcasePath(hotelId, fileName);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);

        }

        /// <summary>
        ///  creates a random name for image, save it to hotel content folder, create a thumbnail 
        /// </summary>
        /// <param name="postedFile"></param>
        /// <param name="hotelId"></param>
        /// <param name="thumbnailSize"></param>
        /// <returns></returns>
        public string SaveImage(System.Web.HttpPostedFileBase postedFile, int hotelId)
        {
            
            var fileName = getGuidName(postedFile);
           
            
            // save orginal
            var op = hcf.EnsureOrginalPath(hotelId,fileName);

            postedFile.SaveAs(op);
          

           var tp = hcf.EnsureThumbnailPath(hotelId,fileName);

           createThumbnail(op, tp, 192);

            var sc = hcf.EnsureShowcasePath(hotelId, fileName);

            createThumbnail(op, sc, 512);

            return fileName;
        }

        void createThumbnail(string filePath, string thumnailPath,int maxSize)
        {


            
            Size size = new Size(maxSize, maxSize);
            using (var inStream = File.OpenRead(filePath))
            {
                using (var outStream = File.Create(thumnailPath))
                {
                    // Initialize the ImageFactory using the overload to preserve EXIF metadata.
                    using (ImageFactory imageFactory = new ImageFactory(preserveExifData: true))
                    {
                        // Load, resize, set the format and quality and save an image.
                        imageFactory.Load(inStream)
                                    .Resize(new ResizeLayer(size, ResizeMode.Max))
                                    .Save(outStream);
                    }

                    outStream.Flush();
                }
            }



            
        }

        public bool IsImage(System.Web.HttpPostedFileBase postedFile)
        {
            int imageMinimumBytes = 512;
            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (postedFile.ContentType.ToLower() != "image/jpg" &&
                        postedFile.ContentType.ToLower() != "image/jpeg" &&
                        postedFile.ContentType.ToLower() != "image/pjpeg" &&
                        postedFile.ContentType.ToLower() != "image/gif" &&
                        postedFile.ContentType.ToLower() != "image/x-png" &&
                        postedFile.ContentType.ToLower() != "image/png")
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            if (Path.GetExtension(postedFile.FileName).ToLower() != ".jpg"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".png"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".gif"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".jpeg")
            {
                return false;
            }

            //-------------------------------------------
            //  Attempt to read the file and check the first bytes
            //-------------------------------------------
            try
            {
                if (!postedFile.InputStream.CanRead)
                {
                    return false;
                }

                if (postedFile.ContentLength < imageMinimumBytes)
                {
                    return false;
                }

                byte[] buffer = new byte[512];
                postedFile.InputStream.Read(buffer, 0, 512);
                string content = System.Text.Encoding.UTF8.GetString(buffer);
                if (Regex.IsMatch(content, @"<script|<html|<head|<title|<body|<pre|<table|<a\s+href|<img|<plaintext|<cross\-domain\-policy",
                    RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Multiline))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            //-------------------------------------------
            //  Try to instantiate new Bitmap, if .NET will throw exception
            //  we can assume that it's not a valid image
            //-------------------------------------------
            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                }
            }
            catch (Exception)
            {
                return false;
            }
        

            return true;
        }

    }
}
