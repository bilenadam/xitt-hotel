﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public static class HotelImageExtensions
{
    public static string GetUrl(this BilenAdam.Data.HotelData.HotelImage item)
    {
        if (item != null)
            return string.Format("https://cdn.xitt.de/hotel/{0}/images/{1}", item.HotelId, item.FileName);
        else
            return "http://cdn.xitt.de/images/no_image_available_2000.png";
    }

    public static string GetThumbnailUrl(this BilenAdam.Data.HotelData.Hotel item)
    {
        if (item != null && item.HotelImages.Count > 0)
            return item.HotelImages.OrderBy(i => i.OrderBy).First().GetUrl();
        else
            return "http://cdn.xitt.de/images/no_image_available_2000.png";
    }
}
