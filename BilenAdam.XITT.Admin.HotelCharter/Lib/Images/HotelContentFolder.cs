﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Images
{
    public class HotelContentFolder
    {
        const string imagesFolder = "images";
        const string thumbnailFolder = "thumbnails";
        const string showcaseFolder = "showcase";
        static string contentFolder;

        static HotelContentFolder()
        {
            contentFolder = new BilenAdam.Config.AppHotelContentSettings().GetFolderPath();
            ensureDirectory(contentFolder);
        }

        private static void ensureDirectory(string path)
        {
            if (string.IsNullOrEmpty(path))
                return;

            if (Directory.Exists(path))
                return;

            var parentDir = System.IO.Path.GetDirectoryName(path);
            if (!string.IsNullOrEmpty(parentDir))
            {
                ensureDirectory(parentDir);
            }
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }

        public string GetHotelFolder(int hotelId)
        {
            return   System.IO.Path.Combine(contentFolder,  hotelId.ToString(), imagesFolder);
        }

        public string EnsureHotelFolder(int hotelId)
        {
            var hf = GetHotelFolder(hotelId);
            ensureDirectory(hf);
            return hf;
        }

        public string GetHotelThumbnailsFolder(int hotelId)
        {
            return System.IO.Path.Combine(GetHotelFolder(hotelId), thumbnailFolder);
        }

        public string EnsureHotelThumbnailsFolder(int hotelId)
        {
            var f = GetHotelThumbnailsFolder(hotelId);
            ensureDirectory(f);
            return f;
        }

        public string GetHotelShowcaseFolder(int hotelId)
        {
            return System.IO.Path.Combine(GetHotelFolder(hotelId), showcaseFolder);
        }

        public string EnsureHotelShowcaseFolder(int hotelId)
        {
            var f = GetHotelShowcaseFolder(hotelId);
            ensureDirectory(f);
            return f;
        }

        public string GetOrginalPath(int hotelId, string filename)
        {
            return System.IO.Path.Combine(GetHotelFolder(hotelId), filename);
        }
        public string EnsureOrginalPath(int hotelId, string filename)
        {
            var p = EnsureHotelFolder(hotelId);

            return System.IO.Path.Combine(p, filename);
        } 

        public string GetThumbnailPath(int hotelId, string filename)
        {
            return System.IO.Path.Combine(GetHotelThumbnailsFolder(hotelId), filename);
        }

        public string GetShowcasePath(int hotelId, string filename)
        {
            return System.IO.Path.Combine(GetHotelShowcaseFolder(hotelId), filename);
        }
        public string EnsureThumbnailPath(int hotelId, string filename)
        {
            var p = EnsureHotelThumbnailsFolder(hotelId);
            return System.IO.Path.Combine(p, filename);
        }

        public string EnsureShowcasePath(int hotelId, string filename)
        {
            var p = EnsureHotelShowcaseFolder(hotelId);
            return System.IO.Path.Combine(p, filename);
        }


    }
}
