﻿using BilenAdam.Web.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace System.Web.Mvc
{
    public static class ControllerNavigationExtensions
    {
        public static RedirectResult Redirect2Action<T>(this Controller controller, Expression<Func<T, ActionResult>> expression) where T : Controller
        {
            var c = new ControllerHelper<T>();
            return new RedirectResult(controller.Url.Action(c.GetActionName(expression), c.GetControllerName()));
        }

        public static RedirectResult Redirect2Action<T>(this Controller controller, Expression<Func<T, ActionResult>> expression, object rootValues) where T : Controller
        {
            var c = new ControllerHelper<T>();
            return new RedirectResult(controller.Url.Action(c.GetActionName(expression), c.GetControllerName(), rootValues));
        }

        public static RedirectResult Redirect2Action<T>(this T controller, Expression<Func<T, ActionResult>> expression) where T : Controller
        {
            var c = new ControllerHelper<T>();
            return new RedirectResult(controller.Url.Action(c.GetActionName(expression), c.GetControllerName()));
        }

        public static RedirectResult Redirect2Action<T>(this T controller, Expression<Func<T, ActionResult>> expression, object rootValues) where T : Controller
        {
            var c = new ControllerHelper<T>();
            return new RedirectResult(controller.Url.Action(c.GetActionName(expression), c.GetControllerName(),rootValues));
        }

        public static RedirectResult Redirect2Action<T>(this T controller, Expression<Func<T, ActionResult>> expression, object rootValues, string protocol) where T : Controller
        {
            var c = new ControllerHelper<T>();
            return new RedirectResult(controller.Url.Action(c.GetActionName(expression), c.GetControllerName(), rootValues,protocol));
        }
    }
}
