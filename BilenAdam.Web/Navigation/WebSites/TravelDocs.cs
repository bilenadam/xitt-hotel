﻿using BilenAdam.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Web.Navigation.WebSites
{
    public class TravelDocs
    {
        public const string Host = "https://docs.xitt.de";

        public  Uri CreateUrl(string path, QueryStringBuilder parameters = null)
        {
            var builder = new UriBuilder(Host);
            builder.Path = path;
            if (parameters != null)
                builder.Query = parameters.ToString();

            return builder.Uri;
        }
        public Uri FlightOnlyApprovalUrl(int tripPlanId)
        {
            return CreateUrl("/TravelMail/SendNurFlugBestatung", new QueryStringBuilder("tripPlanId", tripPlanId.ToString()));

        }
        public  Uri CreateReservationApprovalUrl(int tripPlanId)
        {
            return CreateUrl("/TravelMail/SendReservationApproval", new QueryStringBuilder("tripPlanId", tripPlanId.ToString()));

        }
        public  Uri CreateAdminNotificationUrl(int tripPlanId)
        {
            return CreateUrl("/TravelMail/SendAdminNotification", new QueryStringBuilder("tripPlanId", tripPlanId.ToString()));
        }

        public  Uri CreateTripPlanUrl(int tripPlanId, string surname)
        {
            return CreateUrl("/TravelDocs/TripPlan", new QueryStringBuilder("Id", tripPlanId.ToString()).Add("surname", surname));

        }
        public  Uri CreateBillUrl(int tripPlanId, string surname)
        {
            return CreateUrl("/TravelDocs/Bill", new QueryStringBuilder("Id", tripPlanId.ToString()).Add("surname", surname));

        }
        public Uri CreateRefundBillUrl(int tripPlanId, string surname)
        {
            return CreateUrl("/TravelDocs/BillRefund", new QueryStringBuilder("Id", tripPlanId.ToString()).Add("surname", surname));

        }
        public  Uri CreateAirTicketUrl(int Id, string surname)
        {
            return CreateUrl("/TravelDocs/AirTicket", new QueryStringBuilder("Id", Id.ToString()).Add("surname", surname));

        }
        public  Uri CreateHotelCharterVoucherUrl(int Id, string surname)
        {
            return CreateUrl("/TravelDocs/HotelCharterVoucher/", new QueryStringBuilder("Id", Id.ToString()).Add("surname", surname));

        }
        public Uri CreateHotelCharterVoucherUrl(string pnr, string surname)
        {
            return CreateUrl("/TravelDocs/HotelCharterVoucher", new QueryStringBuilder("pnr", pnr).Add("surname", surname));

        }
        public  Uri CreateHotelCharterRezUrl(string pnr, string surname)
        {
            return CreateUrl("/TravelDocs/HotelCharterRez", new QueryStringBuilder("pnr", pnr).Add("surname", surname));

        }

        public  Uri CreateNewsletterActivationUrl(int Id)
        {
            return CreateUrl("/Newsletter/SendActivationMail/" + Id.ToString());

        }

        public  Uri CreateNewsletterConfirmationUrl(int Id)
        {
            return CreateUrl("/Newsletter/SendConfirmationMail/" + Id.ToString());

        }
        public Uri CreateSendChangeNotificationUrl(int remarkId)
        {
            return CreateUrl("/TravelMail/SendChangeNotification", new QueryStringBuilder("remarkId", remarkId.ToString()));
        }

        public  Uri CreateSendTravelDcoumentsUrl(int tripPlanId)
        {
            return CreateUrl("/TravelMail/SendTravelDcouments", new QueryStringBuilder("tripPlanId", tripPlanId.ToString()));
        }

        public Uri CreateSendReservationBillUrl(int tripPlanId)
        {
            return CreateUrl("/TravelMail/SendReservationBill", new QueryStringBuilder("tripPlanId", tripPlanId.ToString()));
        }
        public Uri CreateSendReservationApprovalUrl(int tripPlanId)
        {
            return CreateUrl("/TravelMail/SendReservationApproval", new QueryStringBuilder("tripPlanId", tripPlanId.ToString()));
        }

        public  Uri CreateSendPaymentReceivedMailUrl(int paymentPlanId)
        {
            return CreateUrl("/PaymentMail/SendPaymentReceivedMail/" + paymentPlanId.ToString());
        }


        public static string DoGet(Func<TravelDocs,Uri> urlFactory)
        {
            try
            {
               return new BilenAdam.Net.WebClient().DoGet(urlFactory(new TravelDocs()).ToString());
            }
            catch(Exception exc)
            {
                return exc.ToString();
            }
        } 

    }
}
