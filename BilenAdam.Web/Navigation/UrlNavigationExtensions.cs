﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Web.Navigation;

namespace System.Web.Mvc 
{
    public static class UrlHelperExtensions
    {
        public static UrlHelperNavigation<T> Nav<T>(this System.Web.Mvc.UrlHelper helper) where T : Controller
        {
            return new UrlHelperNavigation<T> { Helper = helper };
        }
    }
}
