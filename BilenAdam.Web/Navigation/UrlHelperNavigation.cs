﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Navigation
{
    public class UrlHelperNavigation<T> where T : Controller
    {
        public UrlHelper Helper
        {
            get;
            set;
        }

        public string Action(Expression<Func<T, ActionResult>> expression)
        {
            var c = new ControllerHelper<T>();
            return Helper.Action(c.GetActionName(expression), c.GetControllerName());
        }

        public string Action(Expression<Func<T, ActionResult>> expression, object rootValues)
        {
            var c = new ControllerHelper<T>();
            return Helper.Action(c.GetActionName(expression), c.GetControllerName(), rootValues);
        }

        public string Action(Expression<Func<T, ActionResult>> expression, object rootValues, string protocol)
        {
            var c = new ControllerHelper<T>();
            return Helper.Action(c.GetActionName(expression), c.GetControllerName(), rootValues, protocol);
        }
    }
}
