﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Navigation
{
    public class ControllerHelper<T> where T : Controller
    {
        public string GetControllerName()
        {
            return typeof(T).Name.ReplaceLastOccurrence("Controller", "");
        }



        public string GetActionName(Expression<Func<T, ActionResult>> expression)
        {
            MethodCallExpression callExpression =
                expression.Body as MethodCallExpression;

            if (callExpression == null)
            {
                throw new Exception("expression must be a MethodCallExpression");
            }

            return callExpression.Method.Name;
        }
    }
}
