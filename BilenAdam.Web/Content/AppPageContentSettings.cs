﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Web.Content
{
    public class AppPageContentSettings : BilenAdam.Config.ApplicationContentSettings
    {
        public const string AppPagesFolderKey = "app.content.pagesFolder";
        public string GetFolderPath()
        {
            return getOrAddCached(AppPagesFolderKey, k => Path.Combine(GetAppContentFolder(), "Pages"));
        }
    }
}
