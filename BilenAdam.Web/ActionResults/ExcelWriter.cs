﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace BilenAdam.Web.ActionResults
{
    public class ExcelWriter
    {
        public ExcelWriter()
        {
            sw = new StringWriter();
            tw = new HtmlTextWriter(sw);

        }
        StringWriter sw;
        HtmlTextWriter tw;

        public void StartTable()
        {
            tw.RenderBeginTag(HtmlTextWriterTag.Table);
        }

        public void StartThead()
        {
            tw.RenderBeginTag(HtmlTextWriterTag.Thead);
        }
        public void StartTbody()
        {
            tw.RenderBeginTag(HtmlTextWriterTag.Tbody);
        }

        public void EndTag()
        {
            tw.RenderEndTag();
        }


        public void StartTr()
        {
            tw.RenderBeginTag(HtmlTextWriterTag.Tr);
        }

    

        public void AddCell( HtmlTextWriterTag tag ,string headerText, int colSpan = 1, int rowSpan = 1)
        {
            if (colSpan > 1)
            {
                tw.AddAttribute("colspan", colSpan.ToString());
            }
            if (rowSpan > 1)
            {
                tw.AddAttribute("rowspan", rowSpan.ToString());
            }

            tw.RenderBeginTag( tag );
            tw.Write(System.Net.WebUtility.HtmlEncode(headerText));
            tw.RenderEndTag();
        
        }
        public void AddTh(string headerText, int colSpan = 1, int rowSpan = 1)
        {
            AddCell(HtmlTextWriterTag.Th, headerText, colSpan, rowSpan);
        }

        public void AddTd(string headerText, int colSpan = 1, int rowSpan = 1)
        {
            AddCell(HtmlTextWriterTag.Td, headerText, colSpan, rowSpan);
        }

       

        public override string ToString()
        {
            return sw.ToString();
        }
    }
}
