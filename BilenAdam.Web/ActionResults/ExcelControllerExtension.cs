﻿
using BilenAdam.Web.ActionResults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace System.Web.Mvc
{
    public static class ExcelControllerExtensions
    {

        public static ActionResult CSV<T>(this Controller controller, IEnumerable<T> data, IEnumerable<ExcelResultColumn<T>> columns, string fileName,bool renderHeaderRow = true, char seperator = ',')
        {
            return new CSVResult<T>(data, columns, fileName, renderHeaderRow,seperator);
        }

        public static ActionResult Excel<T>(this Controller controller,IEnumerable<T> data, IEnumerable<ExcelResultColumn<T>> columns, string fileName , TableStyle tableStyle = null, TableItemStyle headerStyle = null, TableItemStyle itemStyle = null, TableItemStyle footerStyle = null)
        {
            return new ExcelResult<T>(data, columns, fileName,  tableStyle, headerStyle, itemStyle);
        }



        public static ActionResult Excel<T>(this Controller controller, IEnumerable<T> data, string fileName, IFormatProvider formatProvider, params ExcelResultColumn<T>[] customColumns)
        {
            List<ExcelResultColumn<T>> columns = new List<ExcelResultColumn<T>>();


            foreach (PropertyInfo pi in typeof(T).GetProperties())
            {
                if (pi.PropertyType == typeof(string) || pi.PropertyType.IsValueType || Nullable.GetUnderlyingType(pi.PropertyType) != null)
                {
                    columns.Add(new ExcelResultColumn<T>(pi.Name,formatProvider));
                }
               
                //if (pi.PropertyType == typeof(string)  || !typeof(System.Collections.IEnumerable).IsAssignableFrom(pi.PropertyType))
                //{
                //    columns.Add(new ExcelResultColumn<T>(pi.Name));
                //}
            }
            if(customColumns != null)
                columns.AddRange(customColumns);
            return new ExcelResult<T>(data, columns, fileName);
        }

    }
}
