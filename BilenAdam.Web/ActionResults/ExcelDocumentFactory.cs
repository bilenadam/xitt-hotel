﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BilenAdam.Web.ActionResults
{
    public class ExcelDocumentFactory
    {

        public string CreateDocument<T>(IEnumerable<ExcelResultColumn<T>> columns, IEnumerable<T> data,TableStyle tableStyle = null,TableItemStyle headerStyle = null,TableItemStyle itemStyle = null, TableItemStyle footerStyle = null)
        {
            // provide defaults
            if (tableStyle == null)
            {
                tableStyle = new TableStyle();
                tableStyle.BackColor = Color.Transparent;
                tableStyle.BorderColor = Color.Gray;

            }
            if (headerStyle == null)
            {
                headerStyle = new TableItemStyle();
                headerStyle.Font.Bold = true;
                headerStyle.BorderWidth = Unit.Pixel(1);
            }

            if (itemStyle == null)
            {
                itemStyle = new TableItemStyle();
                itemStyle.BorderWidth = Unit.Pixel(1);


            }
            if (footerStyle == null)
            {
                footerStyle = headerStyle;
            }

            // Create HtmlTextWriter
            using(StringWriter sw = new StringWriter())
            using (HtmlTextWriter tw = new HtmlTextWriter(sw))
            {

                // Build HTML Table from Items
                if (tableStyle != null)
                    tableStyle.AddAttributesToRender(tw);
                tw.RenderBeginTag(HtmlTextWriterTag.Table);




                // Create Header Row
                tw.RenderBeginTag(HtmlTextWriterTag.Thead);
                foreach (ExcelResultColumn<T> header in columns)
                {
                    if (headerStyle != null)
                        headerStyle.AddAttributesToRender(tw);
                    tw.RenderBeginTag(HtmlTextWriterTag.Th);
                    tw.Write(header.Header);
                    tw.RenderEndTag();
                }
                tw.RenderEndTag();



                // Create Data Rows
                tw.RenderBeginTag(HtmlTextWriterTag.Tbody);
                if (data != null)
                {
                    foreach (T row in data)
                    {
                        tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                        foreach (ExcelResultColumn<T> header in columns)
                        {

                            string strValue = header.GetValue(row);


                            strValue = ReplaceSpecialCharacters(strValue);
                            if (itemStyle != null)
                                itemStyle.AddAttributesToRender(tw);
                            tw.RenderBeginTag(HtmlTextWriterTag.Td);
                            tw.Write(System.Net.WebUtility.HtmlEncode(strValue));
                            tw.RenderEndTag();
                        }
                        tw.RenderEndTag();
                    }


                }

                if (columns.Any(c => c.GetTotalValue != null))
                {

                    tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                    foreach (ExcelResultColumn<T> header in columns)
                    {
                        if (headerStyle != null)
                            headerStyle.AddAttributesToRender(tw);
                        tw.RenderBeginTag(HtmlTextWriterTag.Td);
                        tw.Write(header.Header);
                        tw.RenderEndTag();
                    }
                    tw.RenderEndTag();


                    tw.RenderBeginTag(HtmlTextWriterTag.Tr);
                    foreach (ExcelResultColumn<T> header in columns)
                    {

                        string strValue = "";
                        if (header.GetTotalValue != null)
                            strValue = header.GetTotalValue(data);

                        strValue = ReplaceSpecialCharacters(strValue);
                        if (footerStyle != null)
                            footerStyle.AddAttributesToRender(tw);
                        tw.RenderBeginTag(HtmlTextWriterTag.Td);
                        tw.Write(System.Net.WebUtility.HtmlEncode(strValue));
                        tw.RenderEndTag();
                    }
                    tw.RenderEndTag();

                }
                tw.RenderEndTag(); // tbody

                tw.RenderEndTag(); // table

                return sw.ToString();
            }
        }

        private static string ReplaceSpecialCharacters(string value)
        {
            value = value.Replace("’", "'");
            value = value.Replace("“", "\"");
            value = value.Replace("”", "\"");
            value = value.Replace("–", "-");
            value = value.Replace("…", "...");
            return value;
        }

    }
}
