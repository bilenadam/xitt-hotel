﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc
{
    public class ExcelResultColumn<T>
    {
        public ExcelResultColumn()
        {
        }
        public ExcelResultColumn(string propertyName,IFormatProvider formatProvider)
        {
            Header = GetHeader(propertyName);
            GetValue = CreateGetValue(propertyName,formatProvider);
            GetTotalValue = null;

        }
        public ExcelResultColumn(string header, Func<T, string> getValue, Func<IEnumerable<T>, string> getTotalValue)
        {
            Header = header;
            GetValue = getValue;
            GetTotalValue = getTotalValue;
        }
        public string Header { get; set; }
        public Func<T, string> GetValue { get; set; }
        public Func<IEnumerable<T>, string> GetTotalValue { get; set; }

        public static Func<T, string> CreateGetValue(string propertyName, IFormatProvider formatProvider)
        {
            return new Func<T, string>(delegate(T item)
                {
                    string strValue = "";
                    object val = typeof(T).GetProperty(propertyName).GetValue(item, null);
                    if (val != null)
                    {
                        if (val is DateTime)
                        {
                            DateTime dVal = (DateTime)val;
                            if (dVal == dVal.Date)
                                strValue = (dVal).ToString("d", formatProvider);
                            else
                                strValue = (dVal).ToString(formatProvider);
                        }
                        else if (val is DateTime?)
                        {
                            DateTime dVal = ((DateTime?)val).Value;
                            if (dVal == dVal.Date)
                                strValue = (dVal).ToString("d",formatProvider);
                            else
                                strValue = (dVal).ToString(formatProvider);
                        }
                        else if (Nullable.GetUnderlyingType(item.GetType()) != null)
                        {
                            object v = item.GetType().GetProperty("Value").GetValue(item, null);
                            if (v != null)
                            {
                                if (v is IFormattable)
                                {
                                    strValue = ((IFormattable)v).ToString(null, formatProvider);
                                }
                                else
                                {
                                    strValue = v.ToString();
                                }
                            }
                        }
                        else
                        {
                            if (val is IFormattable)
                            {
                                strValue = ((IFormattable)val).ToString(null, formatProvider);
                            }
                            else
                            {
                                strValue = val.ToString();
                            }
                        }
                    }
                    return strValue;
                });
        }

        public static string GetHeader(string propertyName)
        {
            string header = propertyName;
            var property = typeof(T).GetProperty(propertyName);
            DisplayAttribute dAttr = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));

            if (dAttr != null)
            {
                if (dAttr.ResourceType != null)
                {
                    header = new ResourceManager(dAttr.ResourceType).GetString(dAttr.Name);
                }
                else
                {
                    header = dAttr.Name;
                }
            }

            return header;

        }
    }
}
