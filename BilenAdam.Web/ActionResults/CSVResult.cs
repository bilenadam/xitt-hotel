﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.Web.ActionResults
{
    public class CSVResult<T> : ActionResult
    {
        private string _fileName;
        private IEnumerable<T> _data;
        private ExcelResultColumn<T>[] _columns;
        bool _renderHeaderRow;
        char _seperator;
        IDisposable db;

        public CSVResult(IEnumerable<T> data, IEnumerable<ExcelResultColumn<T>> columns, string fileName, bool renderHeaderRow = true, char seperator = ',', IDisposable dataContext = null)
        {

            _data = data;
            _columns = columns.ToArray();
            _fileName = fileName;
            _renderHeaderRow = renderHeaderRow;
            _seperator = seperator;
            db = dataContext;
        }
        public override void ExecuteResult(ControllerContext context)
        {
            try
            {
                StringBuilder csv = new StringBuilder();

                if (_renderHeaderRow)
                {
                    for (int c = 0; c < _columns.Length; c++)
                    {
                        if (c > 0)
                            csv.Append(_seperator);
                        string columnTitleCleaned = CleanCSVString(_columns[c].Header);
                        csv.Append(columnTitleCleaned);
                    }
                    csv.Append(Environment.NewLine);
                }

                if (_data != null)
                {
                    foreach (T row in _data)
                    {
                        for (int c = 0; c < _columns.Length; c++)
                        {
                            if (c > 0)
                                csv.Append(_seperator);
                            string strVal = CleanCSVString(_columns[c].GetValue(row));
                            csv.Append(strVal);
                        }
                        csv.Append(Environment.NewLine);


                    }


                }

                if (_columns.Any(c => c.GetTotalValue != null))
                {
                    for (int c = 0; c < _columns.Length; c++)
                    {
                        if (c > 0)
                            csv.Append(_seperator);
                        if (_columns[c].GetTotalValue != null)
                        {
                            string strVal = CleanCSVString(_columns[c].GetTotalValue(_data));
                            csv.Append(strVal);
                        }
                    }
                    csv.Append(Environment.NewLine);
                }

                Encoding encoding = Encoding.UTF8;
                HttpResponseBase response = context.HttpContext.Response;
                response.ContentType = "text/csv";
                response.ContentEncoding = Encoding.Unicode;
                response.Charset = encoding.EncodingName;
                response.AppendHeader("Content-Disposition", "attachment;filename=" + _fileName);
                response.BinaryWrite(encoding.GetBytes(csv.ToString()));
            }
            finally
            {
                if (db != null)
                    db.Dispose();
            }
        }

       


        protected string CleanCSVString(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "";
            
            string output = "\"" + input.Replace("\"", "\"\"").Replace("\r\n", " ").Replace("\r", " ").Replace("\n", "") + "\"";
            return output;
        }
    }
}
