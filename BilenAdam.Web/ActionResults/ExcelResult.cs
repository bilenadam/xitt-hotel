﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace System.Web.Mvc
{
    public class ExcelResult : ActionResult
    {
        public ExcelResult(string fileName, string content)
        {
            _fileName = fileName;
            _content = content;
        }
        string _fileName;
        string _content;
        public override void ExecuteResult(ControllerContext context)
        {
            WriteFile(context, _fileName, "application/ms-excel", _content);
        }

        private void WriteFile(ControllerContext context, string fileName, string contentType, string content)
        {


            context.HttpContext.Response.Clear();
            context.HttpContext.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            context.HttpContext.Response.ContentEncoding = System.Text.Encoding.Unicode;
            context.HttpContext.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            context.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.HttpContext.Response.ContentType = contentType;
            context.HttpContext.Response.Write(content);
            context.HttpContext.Response.End();
        }
    }
    public class ExcelResult<T> : ActionResult
    {
        private string _fileName;
        private IEnumerable<T>  _data;
        private IEnumerable<ExcelResultColumn<T>> _columns;
        

        private TableStyle _tableStyle;
        private TableItemStyle _headerStyle;
        private TableItemStyle _itemStyle;
        private TableItemStyle _footerStyle;
        public string FileName
        {
            get { return _fileName; }
        }





        public ExcelResult(IEnumerable<T> data, IEnumerable<ExcelResultColumn<T>> columns, string fileName,   TableStyle tableStyle = null, TableItemStyle headerStyle = null, TableItemStyle itemStyle = null, TableItemStyle footerStyle = null)
        {
            _fileName = fileName;
            _data = data;
            _columns = columns;
            _tableStyle = tableStyle;
            _headerStyle = headerStyle;
            _itemStyle = itemStyle;
            _footerStyle = footerStyle;
            // provide defaults
            if (_tableStyle == null)
            {
                _tableStyle = new TableStyle();
                _tableStyle.BackColor = Color.Transparent;
                _tableStyle.BorderColor = Color.Gray;

            }
            if (_headerStyle == null)
            {
                _headerStyle = new TableItemStyle();
                _headerStyle.Font.Bold = true;
                _headerStyle.BorderWidth = Unit.Pixel(1);
            }

            if (_itemStyle == null)
            {
                _itemStyle = new TableItemStyle();
                _itemStyle.BorderWidth = Unit.Pixel(1);


            }
            if (_footerStyle == null)
            {
                _footerStyle = _headerStyle;
            }
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var content = new BilenAdam.Web.ActionResults.ExcelDocumentFactory().CreateDocument<T>(_columns, _data, _tableStyle, _headerStyle, _itemStyle, _footerStyle);
            WriteFile(context,_fileName, "application/ms-excel", content);
        }


        private static string ReplaceSpecialCharacters(string value)
        {
            value = value.Replace("’", "'");
            value = value.Replace("“", "\"");
            value = value.Replace("”", "\"");
            value = value.Replace("–", "-");
            value = value.Replace("…", "...");
            return value;
        }

        private  void WriteFile(ControllerContext context,string fileName, string contentType, string content)
        {


            context.HttpContext.Response.Clear();
            context.HttpContext.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
            context.HttpContext.Response.ContentEncoding = System.Text.Encoding.Unicode;
            context.HttpContext.Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            context.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.HttpContext.Response.ContentType = contentType;
            context.HttpContext.Response.Write(content);
            context.HttpContext.Response.End();
        }
    }

  
}
