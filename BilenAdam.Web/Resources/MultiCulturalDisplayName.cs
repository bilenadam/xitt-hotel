﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace System.ComponentModel
{
    public class MultiCulturalDisplayName : DisplayNameAttribute
    {
        private DisplayAttribute display;

        public MultiCulturalDisplayName(Type resourceType, string resourceName)
        {
            this.display = new DisplayAttribute { ResourceType = resourceType, Name = resourceName };
        }

        public override string DisplayName
        {
            get { return display.GetName(); }
        }
    }
}