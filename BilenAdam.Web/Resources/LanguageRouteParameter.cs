﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Threading;
using System.Globalization;

namespace BilenAdam.Web.Resources
{
    public class LanguageRouteParameter
    {
        public const string LANGUAGE_ROUTE_PARAMETER = "language";


        public static string ResolveCulture()
        {
            HttpContextBase currentContext = new HttpContextWrapper(HttpContext.Current);
            RouteData routeData = RouteTable.Routes.GetRouteData(currentContext);


            if (routeData == null)
                return null;

            var language = (string)routeData.Values[LANGUAGE_ROUTE_PARAMETER];
         

            language = string.IsNullOrWhiteSpace(language) ? new BilenAdam.Web.GlobalSettings().GetNaturalLanguage()  : language;

            return language;

        }
        public static void SetCulture()
        {
            try
            {

                var language = ResolveCulture();
                if (language == null)
                    return;

                Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo(language);
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(language);

                
            }
            catch
            {
            }

        }

        public static string ValidLanguagesPattern = "^" + CultureInfo.GetCultures(CultureTypes.NeutralCultures).Where(c=>c.Name.Length > 1).Select(c => c.Name).Distinct().Aggregate((s1, s2) => s1 + "|" + s2) + "$";
    }

    public class LanguageParameterRouteConstraint : IRouteConstraint
    {
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(values[parameterName].ToString(), LanguageRouteParameter.ValidLanguagesPattern); 
        }
    }
}
