﻿using BilenAdam.Web.Resources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


    public static class PropertyResourceEditorModelUtils
    {
        public static PropertyResourceEditorModel Create<T>(T container, Expression<Func<T, string>> propertyLambda, string propertyLabel, bool useTextArea = false) where T : BilenAdam.Data.Resources.IXResourceContainer
        {

            return new PropertyResourceEditorModel()
            {
                Model = container,
                PropertyName = ((MemberExpression)propertyLambda.Body).Member.Name,
                PropertyValue = container == null ? string.Empty : propertyLambda.Compile().Invoke(container),
                PropertyLabel = propertyLabel,
                UseTextArea = useTextArea

            };
        }

        public static void FillResources<T>(T container, Expression<Func<T, string>> propertyLambda, System.Web.HttpRequestBase request) where T : BilenAdam.Data.Resources.IXResourceContainer
        {
            var key = ((MemberExpression)propertyLambda.Body).Member.Name;

            foreach (var l in new BilenAdam.Web.GlobalSettings().GetSupportedLanguages())
            {
                var rv = request[GetInputName(key, l)].Trim();
                container.SetPropertyResource(propertyLambda, l, rv);

            }

        }

        public static string GetInputName(string property, string language)
        {
            return string.Format("propRex_{0}_{1}", property.ToLowerInvariant(), language.ToLowerInvariant());
        }
    }


