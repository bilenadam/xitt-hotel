﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.Resources;

namespace BilenAdam.Web.Resources.Models
{
    public class PropertyResourceEditorModel
    {
        public bool UseTextArea { get; set; }
        public PropertyResourceEditorModel()
        {
            this.LanguageSet = new GlobalSettings().GetSupportedLanguages();
        }
        public BilenAdam.Data.Resources.IXResourceContainer Model { get; set; }

        public string PropertyValue { get; set; }
        public string PropertyName { get; set; }

        public string PropertyLabel { get; set; }

        public string[] LanguageSet { get; set; }

        public static string GetInputName(string property, string language)
        {
            return PropertyResourceEditorModelUtils.GetInputName(property, language);
        }

        public static PropertyResourceEditorModel Create<T>(T container, Expression<Func<T, string>> propertyLambda, string propertyLabel, bool useTextArea = false) where T : IXResourceContainer
        {

            return PropertyResourceEditorModelUtils.Create(container, propertyLambda, propertyLabel, useTextArea);
        }

        public static void FillResources<T>(T container, Expression<Func<T, string>> propertyLambda, System.Web.HttpRequestBase request) where T : IXResourceContainer
        {
            PropertyResourceEditorModelUtils.FillResources(container, propertyLambda, request);

        }

    }
}
