﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public static class FormatExtensions
    {
        public static string ToMoneyString(this decimal? money)
        {
            if (money == null)
                return null;

            return money.Value.ToString(BilenAdam.Web.Resources.Formats.Money);

        }

        public static string ToMoneyString(this decimal money)
        {

            return money.ToString(BilenAdam.Web.Resources.Formats.Money);

        }
    }

