﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Compilation;
using System.Globalization;
using System.Web.Routing;
using System.Collections.Specialized;
using System.Text;
using System.Web.Mvc.Html;
using System.Linq.Expressions;

namespace System.Web
{
    public static class ResourceHelpers
    {




        public static string CultureAction(this HtmlHelper helper, string  culture)
        {
            return helper.CultureAction(new System.Globalization.CultureInfo(culture));

        }

        public static string CultureAction(this HtmlHelper helper, System.Globalization.CultureInfo newCulture)
        {
            var viewContext = helper.ViewContext;
            var action = viewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();
            RouteValueDictionary combinedRouteValues = new RouteValueDictionary(viewContext.RouteData.Values);

            NameValueCollection queryString = viewContext.RequestContext.HttpContext.Request.QueryString;

            foreach (string key in queryString.AllKeys.Where(key => key != null))
            {
                DateTime dateVal = DateTime.Now;
                if (DateTime.TryParse(queryString[key], out dateVal))
                {
                    combinedRouteValues[key] = dateVal.ToString("d",newCulture);
                }
                else if (queryString[key] == "true,false")
                {
                    combinedRouteValues[key] = "true";  // TODO : for checkbox shit and staff, if possible find a elegant solution
                }
                else
                {
                    combinedRouteValues[key] = queryString[key];
                }
            }

            combinedRouteValues[BilenAdam.Web.Resources.LanguageRouteParameter.LANGUAGE_ROUTE_PARAMETER] = newCulture.Name;


            return new UrlHelper(viewContext.RequestContext).Action(action, combinedRouteValues);

        }



      
        static SelectList getCultureSelectList(string[] cultures = null)
        {
            var clist = cultures ?? new BilenAdam.Web.GlobalSettings().GetSupportedLanguages();

            var list = (from c in System.Globalization.CultureInfo.GetCultures(CultureTypes.NeutralCultures).Where(c => clist.Contains(c.TwoLetterISOLanguageName))
                        select new
                        {
                            Code = c.TwoLetterISOLanguageName,
                            Text = string.Format("{1} - ({2})", c.TwoLetterISOLanguageName, c.EnglishName, c.NativeName)
                        }).OrderBy(i => i.Text).ToList();
            list.Insert(0, new { Code = "", Text = "" });

            return  new SelectList(list, "Code", "Text", null);
        }

        public static MvcHtmlString CultureSelect(this HtmlHelper htmlHelper, string name, string selectedvalue, object htmlAttributes = null, string[] cultures = null)
        {

            return htmlHelper.Select(name, getCultureSelectList(cultures), htmlAttributes);

        }

        public static MvcHtmlString CultureSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlAttributes = null, string[] cultures = null)
        {
            
            return htmlHelper.SelectFor(expression, getCultureSelectList(cultures), htmlAttributes);
        }

       

        public static MvcHtmlString DisplayCulture(this HtmlHelper helper, string culture)
        {
            var cc = new CultureInfo(culture);
            var html = new StringBuilder();
            html.Append(cc.EnglishName);
            html.Append(" (");
            html.Append(cc.NativeName);
            html.Append(" )");
            return new MvcHtmlString(html.ToString());
        }


      

 

    }

}
