﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using BilenAdam.Utilities.IoC;

namespace BilenAdam.Web.IoC
{
    public class SimpleIoCContainer : IDependencyResolver, IContainer
    {
        public BilenAdam.Utilities.IoC.IContainer _container;
        IDependencyResolver _orginalResolver;

        public SimpleIoCContainer(IDependencyResolver orginal, BilenAdam.Utilities.IoC.IContainer container = null)
        {
            _orginalResolver = orginal;
            _container = container ?? new BilenAdam.Utilities.IoC.SimpleContainer();
        }
        public  T Resolve<T>()
        {

            return (T)GetService(typeof(T));
        }

        public void Register<T>(Func<IDependencyResolver,T> getService)
        {
            var type = typeof(T);
            _container.Register<T>(i=>getService(this));

        }
        
        

       
        
        public object GetService(Type serviceType)
        {
            var ows = _container.GetService(serviceType);
            if (ows == null)
                return _orginalResolver.GetService(serviceType);
            else
                return ows;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            
            try
            {
                var ows = _container.GetServices(serviceType);
                var org = _orginalResolver.GetServices(serviceType);

                if (ows != null && org != null)
                {
                    return ows.Union(org).ToArray();
                }
                else if (ows != null)
                {
                    return ows.ToArray();
                }
                else if (org != null)
                {
                    return org.ToArray();
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public void Register(Type serviceType, Func<IContainer, object> serviceFactory)
        {
            _container.Register(serviceType, serviceFactory);
        }
    }
}
