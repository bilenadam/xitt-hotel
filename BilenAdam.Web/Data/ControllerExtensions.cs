﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static  class ControllerExtensions
    {
       public static ActionResult DisposeDBSandReturnHttpNotFound(this Controller controller)
        {
            controller.ViewData.DisposeDBS();
            return new HttpNotFoundResult();
        }
       
    }
}
