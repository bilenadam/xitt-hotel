﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

public static class ViewDataExtensions
{
    public static string getKey<T>()
    {
        return typeof(T).FullName;
    }
    public static void SetDB<T>(this ViewDataDictionary viewData, T db) where T : System.Data.Linq.DataContext
    {
        var key = getKey<T>();

        viewData[key] = db;
    }

    public static T GetDB<T>(this ViewDataDictionary viewData) where T : System.Data.Linq.DataContext
    {
        var key = getKey<T>();

        if (viewData.ContainsKey(key))
            return viewData[key] as T;
        else
            return null;
    }

    public static void DisposeDB<T>(this ViewDataDictionary viewData) where T : System.Data.Linq.DataContext
    {
        var key = getKey<T>();
        if (viewData.ContainsKey(key))
        {
            var db = viewData[key] as T;
            db.Dispose();
            viewData.Remove(key);
        }
    }

    public static void DisposeDBS(this ViewDataDictionary viewData)
    {
        foreach (var key in viewData.Keys.ToArray())
        {
            var db = viewData[key] as System.Data.Linq.DataContext;
            if (db != null)
            {
                db.Dispose();
                viewData.Remove(key);
            }
        }
    }
}

