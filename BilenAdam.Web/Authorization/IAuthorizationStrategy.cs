﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Authorization
{
    public interface IAuthorizationStrategy
    {
        bool Authorize(AuthorizationContext filterContext);
    }

    public interface IAuthorizationStrategyFactory
    {
        IAuthorizationStrategy Create(AuthorizationContext filterContext);
    }
}
