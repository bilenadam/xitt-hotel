﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Authorization
{
    /// <summary>
    /// http://mvcauthorization.codeplex.com/
    /// </summary>
    public class AuthorizationFilterAttribute : IAuthorizationFilter
    {
        public AuthorizationFilterAttribute(IAuthorizationStrategyFactory strategyFactory)
        {
            StrategyFactory = strategyFactory; 
        }
        public IAuthorizationStrategyFactory StrategyFactory { get; set; }
        public void OnAuthorization(AuthorizationContext filterContext)
        {

            if (StrategyFactory != null)
            {
                var strategy = StrategyFactory.Create(filterContext);
                if(strategy != null && !strategy.Authorize(filterContext))
                    filterContext.Result = new HttpUnauthorizedResult();
            }

        }
    }
}
