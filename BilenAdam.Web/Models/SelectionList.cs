﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc
{
    public class SelectionList<T> : List<SelectionItem<T>>
    {
        public SelectionList():base()
        {

        }

        public SelectionList(IEnumerable<SelectionItem<T>> coll): base(coll)
        {

        }

        
    }

    public static class SelectionListExtensions
    {
        public static SelectionList<T> ToSelectionList<T>(this IEnumerable<T> source)
        {
            if (source == null)
                return null;

            return new SelectionList<T>(source.Select(s => new SelectionItem<T> { Item = s }));
        }
    }
}
