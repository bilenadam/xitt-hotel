﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace  System.Web.Mvc
{
    public class SelectionItem<T> 
    {
        public bool IsSelected { get; set; }

        public T Item { get; set; }
    }

   
}
