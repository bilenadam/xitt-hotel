﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Configuration;

namespace BilenAdam.Web
{

    public class GlobalSettings : ApplicationSettings
    {

        public const string SupportedLanguagesKey = "app.supportedLanguages";
        public const string SupportedCurrenciesKey = "app.supportedCurrencies";


        static string[] supportedLanguages = new ApplicationSettings().Get(SupportedLanguagesKey, "de;tr;en").Split(';');
        public string[] GetSupportedLanguages()
        {
            return supportedLanguages;
        }
        static string[] supportedCurrencies = new ApplicationSettings().Get(SupportedCurrenciesKey, "EUR;TRY;USD").Split(';');
        public string[] GetSupportedCurrencies()
        {
            return supportedCurrencies;
        }

    }
}
