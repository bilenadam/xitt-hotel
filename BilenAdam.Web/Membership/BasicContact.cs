﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Crm;

namespace BilenAdam.Web.Membership
{
    public class BasicContact : Crm.IContact
    {
        public ContactType ContactType
        {
            get;set;
        }

        public int Id
        {
            get; set;
        }

        public string Name
        {
            get; set;
        }
    }
}
