﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

namespace BilenAdam.Web.Membership
{
    public class FormAuthenticationExtensions
    {
       


        public static void SetUser(string username, string userData ,bool rememberMe)
        {
            DateTime expires = rememberMe ? DateTime.Today.AddDays(7) : DateTime.Today.AddDays(1); 


            // Create the cookie that contains the forms authentication ticket
            HttpCookie authCookie = FormsAuthentication.GetAuthCookie(username, rememberMe);

            // Get the FormsAuthenticationTicket out of the encrypted cookie
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

            // Create a new FormsAuthenticationTicket that includes our custom User Data
            FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(1, ticket.Name, DateTime.Now, expires, rememberMe, userData ?? username, FormsAuthentication.FormsCookiePath);

            // Update the authCookie's Value to use the encrypted version of newTicket
            authCookie.Value = FormsAuthentication.Encrypt(newTicket);
            authCookie.Domain = FormsAuthentication.CookieDomain;
            authCookie.Expires = expires;
            HttpContext.Current.Response.Cookies.Add(authCookie);

        }


        public static string SerializeUserData(BilenAdam.Crm.IContact contact, string[] roles)
        {
            StringBuilder sb = new StringBuilder();
            if (contact != null)
            {
                sb.Append(contact.Id);
                sb.Append(";");
                sb.Append((int)contact.ContactType);
                sb.Append(";");
                sb.Append(contact.Name.Replace(";", ""));
            }

            if (roles.HasAny())
            {
                foreach (var role in roles)
                {
                    sb.Append(";");
                    sb.Append(role);
                }
            }

            return sb.ToString().Trim(';');

        }

        public static void SetAdminUser(string username, string[] roles, bool rememberMe)
        {
            SetAdminPrincipal(username, roles);
            string userData = SerializeUserData(null,roles);
            SetUser(username, userData, rememberMe);
            

        }

      

        public static void SetContactUser(BilenAdam.Crm.IContact contact,string username, string[] roles, bool rememberMe)
        {
            SetContactPrincipal(contact, username, roles);
            string userData = SerializeUserData(contact, roles);
            SetUser(username, userData, rememberMe);
        

        }

       

      

        public static void SetContactPrincipal(BilenAdam.Crm.IContact contact, string username, string[] roles)
        {
            HttpContext.Current.User = new BilenAdam.Security.ContactPrincipal(new GenericIdentity(username, "Forms"), contact, roles);
        }

        public static void SetContactPrincipal()
        {
            if (HttpContext.Current != null)
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if(authCookie != null)
                {
                    // Get the FormsAuthenticationTicket out of the encrypted cookie
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    string[] parts = ticket.UserData.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    SetContactPrincipal(
                        new BasicContact
                        {
                            Id = int.Parse(parts[0]),
                            ContactType = (BilenAdam.Crm.ContactType)int.Parse(parts[1]),
                            Name = parts[2]
                        },
                        ticket.Name,
                        parts.Length > 3 ? parts.Skip(2).ToArray() : null
                    );

                }
            }
        }


        public static void SetAdminPrincipal(string username, string[] roles)
        {
            HttpContext.Current.User = new BilenAdam.Security.AdminPrincipal(new GenericIdentity(username, "Forms"), roles);
        }

        public static void SetAdminPrincipal()
        {
            if (HttpContext.Current != null)
            {
                HttpCookie authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    // Get the FormsAuthenticationTicket out of the encrypted cookie
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    string[] parts = ticket.UserData.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                    SetAdminPrincipal(
                        ticket.Name,
                        parts
                    );

                }
            }
        }


        public static void Logoff()
        {
            FormsAuthentication.SignOut();

            HttpContext.Current.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);


            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session.Abandon();

            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie1);
        }
    }
}
