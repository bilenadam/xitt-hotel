﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Membership
{
    public static class MembershipHelper
    {
        public static BilenAdam.Membership.IMembership GetMembership()
        {

            return DependencyResolver.Current.GetService<BilenAdam.Membership.IMembership>();
        }

        public static int? GetUserId()
        {

            return null;
        }

        public static int? GetContactId()
        {

            return GetMembership()?.GetContactId();
        }

        
    }
}
