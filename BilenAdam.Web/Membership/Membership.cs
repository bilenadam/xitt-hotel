﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Crm;

namespace BilenAdam.Web.Membership
{
    public class Membership : BilenAdam.Membership.IMembership
    {
        int? contactId = null;
        ContactType contactType;
        string username = null;
        public Membership()
        {
            if (System.Web.HttpContext.Current != null)
            {
                var user = System.Web.HttpContext.Current.User;
                if (user.IsContactPrincipal())
                {
                    var cp = user.AsContactPrincipal();
                    contactId = cp.Contact.Id;
                    contactType = cp.Contact.ContactType;
                    username = user.Identity.Name;
                }
                else
                { 
                    username = user.Identity.Name;
                    contactType = ContactType.Person;
                }
               

            }
        }


        public int? GetContactId()
        {
            return contactId;
        }

        public ContactType GetContactType()
        {
            return contactType;
        }

        public virtual int? GetReferrerId()
        {
            return null;
        }

        public string GetUsername()
        {
            return username;
        }
    }
}
