﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
namespace BilenAdam.Web.Worklfow
{
    public interface IWizardStorageDictionary
    {
        void CleanUp();
        void CleanUp(DateTime olderThen);

        void Dump(StringBuilder stringBuilder);
    }

    public class WizardStorageDictionary<T> : IWizardStorageDictionary
    {
        Dictionary<string, WizardStorageItem<T>> _itemDictionary = new Dictionary<string, WizardStorageItem<T>>();
        private readonly object _lock = new object();

        private DateTime nextCheck = DateTime.Now;
        private TimeSpan checkInterval;
        private TimeSpan maxAge;

        public WizardStorageDictionary(TimeSpan checkInterval, TimeSpan maxAge)
        {
            this.checkInterval = checkInterval;
            this.maxAge = maxAge;
        }

        public WizardStorageDictionary():this(new TimeSpan(0,20,0), new TimeSpan(6,0,0))
        {

        }

        public T Get(string wizardCode)
        {
            if (string.IsNullOrWhiteSpace(wizardCode))
                return default(T);
            lock (_lock)
            {
                if (_itemDictionary.TryGetValue(wizardCode, out var item))
                {
                    item.UpdateAccessed();
                    return item.Item;
                }
                else
                    return default(T);
            }
        }

        public bool AddWizard(string wizardCode, T item)
        {
            lock (_lock)
            {
                scheduleCleanUp();

                if (_itemDictionary.ContainsKey(wizardCode))
                    return false;


                _itemDictionary.Add(wizardCode, new WizardStorageItem<T>(item, wizardCode));

                return true;
            }
        }


        

        public void RemoveWizard(string wizardCode)
        {

            lock (_lock)
            {
                if (_itemDictionary.ContainsKey(wizardCode))
                    _itemDictionary.Remove(wizardCode);
            }
        }

        public void CleanUp(DateTime olderThen)
        {
            lock (_lock)
            {
                var keys = _itemDictionary.Keys.ToArray();

                foreach (var key in keys)
                {
                    if (_itemDictionary[key].LastAccessed < olderThen)
                        _itemDictionary.Remove(key);
                }
            }
        }

        public void Dump(StringBuilder stringBuilder)
        {
            stringBuilder.AppendLine(typeof(T).ToString());
            lock (_lock)
            {
                var keys = _itemDictionary.Keys.ToArray();

                foreach (var key in keys)
                {
                    stringBuilder.Append(key);
                    stringBuilder.Append(";");
                    var item = _itemDictionary[key];
                    stringBuilder.Append(item.Created);
                    stringBuilder.Append(";");
                    stringBuilder.Append(item.LastAccessed);
                    stringBuilder.Append(";");
                }
            }
        }


        void scheduleCleanUp()
        {
            if (nextCheck > DateTime.Now)
                return;

            nextCheck = DateTime.Now.Add(checkInterval);
            System.Threading.ThreadPool.QueueUserWorkItem(o => ((IWizardStorageDictionary)o).CleanUp(), this);
        }

        public void CleanUp()
        {
            CleanUp(DateTime.Now.Subtract(maxAge));
        }
    }
}
