﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Web.Worklfow.Logs
{
    public class ActivityLog : BilenAdam.Log.ILog
    {
        public BilenAdam.Log.IObjectLogSerializer ObjectSerializer { get; set; }

        public BilenAdam.Log.ActivityLogs.IActivityLogService LogService { get; set; }
        public ActivityHeader Header { get; set; }

        StringBuilder logBuilder = new StringBuilder();

        public void LogException(Exception exc)
        {
            logBuilder.AppendLine(exc.ToString());
        }

        public void LogFile(string filepath)
        {
            logBuilder.AppendLine(string.Format("{0} : Log File",DateTime.Now.ToString("u")));
            logBuilder.AppendLine(filepath);
        }

        public void LogMessage(string message)
        {
            logBuilder.AppendLine(string.Format("{0} : {1}", DateTime.Now.ToString("u"), message));
        }

        public void LogObject(object obj)
        {
            if (obj == null)
                return;

            try
            {
                if (ObjectSerializer != null)
                    logBuilder.AppendLine(ObjectSerializer.Serialize(obj));
                else
                    logBuilder.AppendLine(obj.ToString());
            }
            catch
            {
                logBuilder.AppendLine("failed to searialize log object");
            }
        }


        public void PushToContext()
        {
            BilenAdam.Log.LogContext.AddLog(this);
        }

        public void RemoveFromContext()
        {
            BilenAdam.Log.LogContext.RemoveLog(this);
        }

        public void SaveActivityLog()
        {
            try
            {
                if (LogService != null)
                {
                    LogService.Log(new BilenAdam.Log.ActivityLogs.ActivityReport
                    {
                        ActivityCode = Header.ActivityCode,
                        ActivityTime = DateTime.Now,
                        ActivityType = Header.ActivityType,
                        ApplicationName = Header.ApplicationName,
                        IpAddress = Header.IpAddress,
                        UserName = Header.UserName,
                        LogReport = logBuilder.ToString()
                    });
                }
            }
            catch
            {

            }
        }

        public override string ToString()
        {
            return logBuilder.ToString();
        }
    }
}
