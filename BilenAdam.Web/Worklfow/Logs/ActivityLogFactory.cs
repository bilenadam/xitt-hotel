﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Worklfow.Logs
{
    public class ActivityLogFactory
    {
        public static ActivityLog Create(Controller controller, string activityType, string wizardCode = null)
        {
            return new ActivityLog
            {
                Header = ActivityHeaderFactory.Create(controller, activityType, wizardCode),
                ObjectSerializer = DependencyResolver.Current.GetService<BilenAdam.Log.IObjectLogSerializer>(),
                LogService = DependencyResolver.Current.GetService<BilenAdam.Log.ActivityLogs.IActivityLogService>()
            };
        }
    }
}
