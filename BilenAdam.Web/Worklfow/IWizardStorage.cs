﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Web.Worklfow
{
    public interface IWizardStorage<T> 
    {
        T GetWizard(string wizardCode);
        bool AddWizard(string wizardCode, T item);
        void RemoveWizard(string wizardCode);

    }
}
