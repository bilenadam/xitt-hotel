﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Worklfow
{
    public interface IEndEventRouter
    {
        string EndWithFail(Controller controller, string errorMessage);
    }
}
