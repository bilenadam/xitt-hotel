﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BilenAdam.Web.Worklfow.ApplicationStorage
{
    public static class WizardApplicationStorageMaintenance
    {
        public static void CleanUp(System.Web.Mvc.Controller controller, DateTime olderThen)
        {
            CleanUp(controller.ControllerContext.HttpContext.Application,olderThen);
        }
        public static void CleanUp(HttpApplicationStateBase appState, DateTime olderThen)
        {
            var keys =  appState.AllKeys;
            foreach (var key in keys)
            {
                var item = appState[key] as IWizardStorageDictionary;
                if (item != null)
                    item.CleanUp(olderThen);
            }
        }

        public static void Dump(HttpApplicationStateBase appState, StringBuilder stringBuilder)
        {
            var keys = appState.AllKeys;
            foreach (var key in keys)
            {
                var item = appState[key] as IWizardStorageDictionary;
                if (item != null)
                    item.Dump(stringBuilder);
            }
        }

        public static void Dump(System.Web.Mvc.Controller controller, StringBuilder stringBuilder)
        {
            Dump(controller.ControllerContext.HttpContext.Application, stringBuilder);
        }
    }
}
