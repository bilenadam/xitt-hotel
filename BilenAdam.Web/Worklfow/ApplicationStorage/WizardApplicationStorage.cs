﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.Web.Worklfow.ApplicationStorage
{
    public class WizardApplicationStorage<T> : BaseWizardStorage<T>
    {
        static string AppStateKey = "ApplicationStateDictionary_" + typeof(T).FullName;
   


        public WizardApplicationStorage(HttpApplicationStateBase appState) : base(GetOrAddStorageDictionary(appState))
        {
        }

        public WizardApplicationStorage(Controller controller) : this(controller.ControllerContext.HttpContext.Application)
        {

        }

       



        static object _lock = new object();
        public static WizardStorageDictionary<T> GetOrAddStorageDictionary(HttpApplicationStateBase appState)
        {
            lock (_lock)
            {
                WizardStorageDictionary<T> itemDictionary = appState[AppStateKey] as WizardStorageDictionary<T>;

                if(itemDictionary == null)
                {
                    itemDictionary = new WizardStorageDictionary<T>();
                    appState[AppStateKey] = itemDictionary;
                }

                return itemDictionary;
            }
        }
    }
}
