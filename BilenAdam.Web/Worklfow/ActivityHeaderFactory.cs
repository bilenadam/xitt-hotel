﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BilenAdam.Web.Worklfow
{
    public class ActivityHeaderFactory
    {
        public static ActivityHeader Create(Controller controller, string activityType, string wizardCode = null)
        {
            var header = new ActivityHeader
            {
                 ActivityType = activityType,
                 ActivityCode = wizardCode ?? controller.GenerateWizardCode(),
                 ApplicationName = string.Format("{0}{1}", controller.Request.Url.Authority, controller.Request.ApplicationPath.TrimEnd('/')),
                 IpAddress = controller.Request.ServerVariables["REMOTE_ADDR"],
                 UserName = (controller.User != null && controller.User.Identity != null) ? controller.User.Identity.Name : null

            };

            return header;
        }
    }
}
