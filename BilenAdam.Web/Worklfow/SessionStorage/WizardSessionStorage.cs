﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BilenAdam.Web.Worklfow.SessionStorage
{
    public class WizardSessionStorage<T> : BaseWizardStorage<T>
    {
        static string SessionKey = "SessionDictionary_" + typeof(T).FullName;
        static object _lock = new object();
        public static WizardStorageDictionary<T> GetOrAddStorageDictionary(HttpSessionStateBase sessionState)
        {
            lock (_lock)
            {
                WizardStorageDictionary<T> itemDictionary = sessionState[SessionKey] as WizardStorageDictionary<T>;

                if (itemDictionary == null)
                {
                    itemDictionary = new WizardStorageDictionary<T>();
                    sessionState[SessionKey] = itemDictionary;
                }

                return itemDictionary;
            }
        }

        public static WizardStorageDictionary<T> GetOrAddStorageDictionary(System.Web.SessionState.HttpSessionState  sessionState)
        {
            lock (_lock)
            {
                WizardStorageDictionary<T> itemDictionary = sessionState[SessionKey] as WizardStorageDictionary<T>;

                if (itemDictionary == null)
                {
                    itemDictionary = new WizardStorageDictionary<T>();
                    sessionState[SessionKey] = itemDictionary;
                }

                return itemDictionary;
            }
        }

        public WizardSessionStorage(HttpSessionStateBase session) : base(GetOrAddStorageDictionary(session))
        {
           

        }


        public WizardSessionStorage(Controller controller) : this(controller.Session)
        {

        }

        public WizardSessionStorage(System.Web.SessionState.HttpSessionState session) : base(GetOrAddStorageDictionary(session))
        {

        }


    }
}
