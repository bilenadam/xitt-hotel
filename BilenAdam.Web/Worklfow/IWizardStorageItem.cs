﻿using System;

namespace BilenAdam.Web.Worklfow
{
    public interface IWizardStorageItem
    {
        DateTime Created { get; }
        DateTime LastAccessed { get; }
        string WizardCode { get; }

        Type GetItemType();
        object GetItem();
    }

    public interface IWizardStorageItem<T> : IWizardStorageItem 
    {
        T Item { get; }
    }
}