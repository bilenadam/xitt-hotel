﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace System.Web.Mvc
{
    public static class WizardCodeExtensions
    {
        public static string GenerateWizardCode()
        {
            return Guid.NewGuid().ToString("N");
           // return System.IO.Path.GetRandomFileName().Substring(0, 8).ToUpperInvariant();
        }

        public static string GenerateWizardCode(this Controller controller)
        {
            return GenerateWizardCode();
        }
    }
}
