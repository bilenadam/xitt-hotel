﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Web.Worklfow
{
    public class WizardStorageItem<T> : IWizardStorageItem<T>
    {
        public WizardStorageItem(T item, string wizardCode)
        {
            this.WizardCode = wizardCode;
            this.Item = item;
            Created = DateTime.Now;
            LastAccessed = DateTime.Now;
        }
        public string WizardCode { get; protected set; }
        public T Item { get; protected set; }

        public DateTime Created { get; protected set; }

        public DateTime LastAccessed { get; protected set; }

        public object GetItem()
        {
            return Item;
        }

        public Type GetItemType()
        {
            return typeof(T);
        }

        public void UpdateAccessed()
        {
            LastAccessed = DateTime.Now;
        }
    }
}
