﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Web.Worklfow
{
    public  class BaseWizardStorage<T> : IWizardStorage<T> 
    {
        protected  WizardStorageDictionary<T> ItemDictionary { get; }

        public BaseWizardStorage(WizardStorageDictionary<T> dictionary)
        {
            ItemDictionary = dictionary;
        }
      

        public virtual T GetWizard(string wizardCode)
        {
            return ItemDictionary.Get(wizardCode);
        }

        public virtual bool AddWizard(string wizardCode, T item)
        {
            return ItemDictionary.AddWizard(wizardCode, item);
        }

        public virtual void RemoveWizard(string wizardCode)
        {
            ItemDictionary.RemoveWizard(wizardCode);
        }

       
    }
}
