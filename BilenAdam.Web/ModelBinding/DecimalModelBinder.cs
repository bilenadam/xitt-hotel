﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BilenAdam.Web.ModelBinding
{
    public class DecimalModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (value != null)
            {
                decimal d;

                if (decimal.TryParse(value.AttemptedValue, out d))
                {
                    return d;
                }
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
