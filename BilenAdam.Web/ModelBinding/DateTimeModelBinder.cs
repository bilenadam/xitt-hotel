﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BilenAdam.Web.ModelBinding
{
    public class DateTimeModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (value != null)
            {
                DateTime date;

                if (DateTime.TryParse(value.AttemptedValue, out date))
                {
                    return date;
                }
            }


            return base.BindModel(controllerContext, bindingContext);
        }

       
    }
}
