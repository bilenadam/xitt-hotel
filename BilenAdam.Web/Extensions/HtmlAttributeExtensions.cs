﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{
    public static class HtmlAttributeExtensions
    {
        public static RouteValueDictionary ExtendHtmlAttr(object hattr, object extattr)
        {
            if (hattr != null && extattr != null)
            {
                var cvalues = hattr is RouteValueDictionary ? hattr as RouteValueDictionary :   new RouteValueDictionary(hattr);
                var evalues = extattr is RouteValueDictionary ? extattr as RouteValueDictionary :  new RouteValueDictionary(extattr);
                foreach (var key in evalues.Keys)
                {
                    if (cvalues.ContainsKey(key) && cvalues[key] is string && evalues[key] is string)
                    {
                        cvalues[key] = (string)cvalues[key] + " " + (string)evalues[key];
                    }
                    else
                    {
                        cvalues[key] = evalues[key];
                    }
                }

                return cvalues;
            }
            else
            {
                return new RouteValueDictionary(hattr ?? extattr);
            }
        }

        public static RouteValueDictionary ExtendHtmlAttr(this HtmlHelper htmlHelper, object hattr, object extattr)
        {
            return ExtendHtmlAttr(hattr, extattr);
        }

     
    }
}
