﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.TravelCode;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace System.Web.Mvc.Html
{
    public static class TravelCodeExtensions
    {
        public static string DisplayCountry(this HtmlHelper htmlhelper, string code)
        {
            if (string.IsNullOrWhiteSpace(code))
                return null;

            var country = BilenAdam.TravelCode.Countries.CountryDictionary.GetCountry(code);
            if (country != null)
            {
                return country.GetLocalizedName(System.Globalization.CultureInfo.CurrentCulture);
            }

            return code;
        }

        public static MvcHtmlString CountrySelect(this HtmlHelper htmlHelper, string name, string selectedvalue = null, object htmlattributes = null)
        {
            var countries = BilenAdam.TravelCode.Countries.CountryDictionary.GetAll().Select(l => new SelectListItem { Value = l.Code, Text = l.GetLocalizedName(System.Globalization.CultureInfo.CurrentCulture) }).OrderBy(t => t.Text).ToList();
            countries.Insert(0, new SelectListItem { Text = "...", Value = "" });

            SelectList selectList = new SelectList(countries, "Value", "Text", selectedvalue);
            return htmlHelper.Select(name, selectList, htmlattributes);


        }

        public static MvcHtmlString CountrySelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlattributes = null, bool addEmptySelection = true)
        {
            var countries = BilenAdam.TravelCode.Countries.CountryDictionary.GetAll().Select(l => new SelectListItem { Value = l.Code, Text = l.GetLocalizedName(System.Globalization.CultureInfo.CurrentCulture) }).OrderBy(t => t.Text).ToList();

            if (addEmptySelection)
                countries.Insert(0, new SelectListItem { Text = "...", Value = "" });

            return htmlHelper.SelectFor(expression, countries, htmlattributes);


        }

        public static MvcHtmlString CountryNameSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlattributes = null, bool addEmptySelection = true)
        {
            var countries = BilenAdam.TravelCode.Countries.CountryDictionary.GetAll().Select(l => new SelectListItem { Value = l.Name, Text = l.GetLocalizedName(System.Globalization.CultureInfo.CurrentCulture) }).OrderBy(t => t.Text).ToList();
            if (addEmptySelection)
                countries.Insert(0, new SelectListItem { Text = "...", Value = "" });


            return htmlHelper.SelectFor(expression, countries, htmlattributes);


        }



        public static MvcHtmlString CountryListBoxFor<TModel, TList>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TList>> expression, object htmlattributes = null) where TList : IEnumerable<string>
        {
            var countries = BilenAdam.TravelCode.Countries.CountryDictionary.GetAll();

            SelectList selectList = new SelectList(countries, "Code", "Name");


            if (htmlHelper.ViewData.Model != null)
            {
                var selectedList = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                if (selectedList != null)
                {
                    foreach (var cc in selectedList)
                    {
                        var item = selectList.FirstOrDefault(c => c.Value == cc);
                        if (item != null)
                        {
                            item.Selected = true;
                        }
                    }
                }


            }

            return htmlHelper.ListBoxFor(expression, selectList, htmlattributes);


        }

    }

}