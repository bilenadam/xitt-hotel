﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace System.Web.Mvc.Html
{
    public static class EnumSelectExtensions
    {
       

       

        public static IEnumerable<SelectListItem> GetEnumSelectList<TEnum>(Func<TEnum, string> valueFunc, Func<TEnum, string> textFunc, string selectedValue, bool addEmptyOption)
              where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            List<SelectListItem> items = new List<SelectListItem>();
            if (addEmptyOption)
            {
                items.Add(new SelectListItem { Text = " ", Value = " ", Selected = string.IsNullOrWhiteSpace(selectedValue) });
            }
            foreach (TEnum e in Enum.GetValues(typeof(TEnum)))
            {
                var t = textFunc(e);
                var v = valueFunc(e);
                items.Add(new SelectListItem { Text = t, Value = v, Selected = selectedValue == v });
            }

            return items;

        }

        public static IEnumerable<SelectListItem> GetEnumSelectList<TEnum>(string selectedValue)
            where TEnum: struct, IComparable, IFormattable, IConvertible
        {
            return GetEnumSelectList<TEnum>(e => e.ToString(), e=>e.ToString(), selectedValue,true);
        }
        public static IEnumerable<SelectListItem> GetEnumSelectList<TEnum>(TEnum selectedValue)
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {

            return GetEnumSelectList<TEnum>(e => e.ToString(), e => e.ToString(), selectedValue.ToString(),false);
        }
        public static IEnumerable<SelectListItem> getEnumSelectList<TEnum>(int? selectedValue)
            where TEnum: struct, IComparable, IFormattable, IConvertible
        {

            return GetEnumSelectList<TEnum>(e => Convert.ToInt32(e).ToString(), e => e.ToString(), selectedValue == null ? null : selectedValue.Value.ToString(),true);
        }
        static IEnumerable<SelectListItem> getEnumSelectList<TEnum>(int selectedValue)
           where TEnum : struct, IComparable, IFormattable, IConvertible
        {

            return GetEnumSelectList<TEnum>(e => Convert.ToInt32(e).ToString(), e => e.ToString(),  selectedValue.ToString(),false);
        }
        public static MvcHtmlString EnumSelect<TEnum>(this HtmlHelper htmlHelper, string name, string selectedValue, object htmlattributes = null) 
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {
         

            
            return htmlHelper.Select(name, GetEnumSelectList<TEnum>(selectedValue), htmlattributes);
        }

        public static MvcHtmlString EnumSelect<TEnum>(this HtmlHelper htmlHelper, string name, int? selectedValue, object htmlattributes = null)
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {



            return htmlHelper.Select(name, getEnumSelectList<TEnum>(selectedValue), htmlattributes);
        }

        public static MvcHtmlString EnumSelect<TEnum>(this HtmlHelper htmlHelper, string name, TEnum selectedValue, object htmlattributes = null)
          where TEnum : struct, IComparable, IFormattable, IConvertible
        {



            return htmlHelper.Select(name, GetEnumSelectList<TEnum>(selectedValue), htmlattributes);
        }


        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlattributes = null) 
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            var name = htmlHelper.GetFullHtmlName(expression);
            string selectedValue = null;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }

            return htmlHelper.Select(name, GetEnumSelectList<TEnum>(selectedValue), htmlattributes);
        }

        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int>> expression, object htmlattributes = null)
          where TEnum : struct, IComparable, IFormattable, IConvertible
        {

            var name = htmlHelper.GetFullHtmlName(expression);
            int selectedValue = 0;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }

            return htmlHelper.Select(name, getEnumSelectList<TEnum>(selectedValue), htmlattributes);


        }

        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int?>> expression, object htmlattributes = null)
            where TEnum : struct, IComparable, IFormattable, IConvertible
        {

            var name = htmlHelper.GetFullHtmlName(expression);
            int? selectedValue = null;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }

            return htmlHelper.Select(name, getEnumSelectList<TEnum>(selectedValue), htmlattributes);

         
        }

        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, byte?>> expression, object htmlattributes = null)
           where TEnum : struct, IComparable, IFormattable, IConvertible
        {

            var name = htmlHelper.GetFullHtmlName(expression);
            byte? selectedValue = null;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }

            return htmlHelper.Select(name, getEnumSelectList<TEnum>(selectedValue), htmlattributes);


        }

        public static MvcHtmlString EnumSelectFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlattributes = null)
             where TEnum : struct, IComparable, IFormattable, IConvertible
        {
            var name = htmlHelper.GetFullHtmlName(expression);
            TEnum selectedValue = default(TEnum);
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }

            return htmlHelper.Select(name, GetEnumSelectList<TEnum>(selectedValue), htmlattributes);

        }

        public static MvcHtmlString ContactTypeSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, BilenAdam.Crm.ContactType>> expression, object htmlattributes = null)
        {
            return htmlHelper.EnumSelectFor<TModel, BilenAdam.Crm.ContactType>(expression, htmlattributes);
        }
        public static MvcHtmlString ContactTypeSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlattributes = null)
        {
            return htmlHelper.EnumSelectFor<TModel, BilenAdam.Crm.ContactType>(expression, htmlattributes);
        }

        public static MvcHtmlString ContactTypeSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int?>> expression, object htmlattributes = null)
        {
            return htmlHelper.EnumSelectFor<TModel, BilenAdam.Crm.ContactType>(expression, htmlattributes);
        }
        public static MvcHtmlString ContactTypeSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int>> expression, object htmlattributes = null)
        {
            return htmlHelper.EnumSelectFor<TModel, BilenAdam.Crm.ContactType>(expression, htmlattributes);
        }

        public static MvcHtmlString ContactTypeSelect(this HtmlHelper htmlHelper, string name, int? selectedValue, object htmlattributes = null)
        {
            return htmlHelper.EnumSelect<BilenAdam.Crm.ContactType>(name, selectedValue, htmlattributes);
        }
        public static MvcHtmlString ContactTypeSelect(this HtmlHelper htmlHelper, string name, string selectedValue, object htmlattributes = null)
        {
            return htmlHelper.EnumSelect<BilenAdam.Crm.ContactType>(name, selectedValue, htmlattributes);
        }

        public static MvcHtmlString ContactTypeSelect(this HtmlHelper htmlHelper, string name, BilenAdam.Crm.ContactType selectedValue, object htmlattributes = null)
        {
            return htmlHelper.EnumSelect<BilenAdam.Crm.ContactType>(name, selectedValue, htmlattributes);
        }

        public static string DisplayEnum<EnumType>(this HtmlHelper htmlHelper, object value)
        {
            if (value == null)
                return string.Empty;
            else
                return Enum.GetName(typeof(EnumType), value);
        }
       
    }
}
