﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.ComponentModel;

namespace System.Web.Mvc.Html
{
    public static class ViewContextExtensions
    {
        /// <summary>
        /// Builds a RouteValueDictionary that combines the request route values, the querystring parameters,
        /// and the passed newRouteValues. Values from newRouteValues override request route values and querystring
        /// parameters having the same key.
        /// </summary>
        public static RouteValueDictionary GetCombinedRouteValues(this ViewContext viewContext, object newRouteValues)
        {
            RouteValueDictionary combinedRouteValues = new RouteValueDictionary(viewContext.RouteData.Values);

            NameValueCollection queryString = viewContext.RequestContext.HttpContext.Request.QueryString;

            foreach (string key in queryString.AllKeys.Where(key => key != null))
                combinedRouteValues[key] = queryString[key] == "true,false" ? "true" : queryString[key]; // TODO : for checkbox shit and staff, if possible find a elegant solution

            if (newRouteValues != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(newRouteValues))
                    combinedRouteValues[descriptor.Name] = descriptor.GetValue(newRouteValues);
            }

            return combinedRouteValues;
        }

        public static string CurrentPageUrl(this ViewContext viewContext, object newRouteValues)
        {

            return new UrlHelper(viewContext.RequestContext).Action(viewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString(), viewContext.GetCombinedRouteValues(newRouteValues));
        }

        public static string GetRouteParameter(this ViewContext viewContext, string parameter)
        {
            return viewContext.HttpContext.Request.QueryString[parameter];

        }

        public static T GetRouteParameter<T>(this ViewContext viewContext, string parameter, Func<string,T> parser, T defaultValue)
        {
            try
            {
                 if(viewContext.HttpContext.Request.QueryString.AllKeys.Contains(parameter))
                        return parser(viewContext.HttpContext.Request.QueryString[parameter]);
                 

            }
            catch
            {
                
            }

            return defaultValue;

        }


        public static DateTime? GetRouteDateParameter(this ViewContext viewContext, string parameter)
        {
            string p = viewContext.HttpContext.Request.QueryString[parameter];

            try
            {
                return DateTime.Parse(p);
            }
            catch
            {
                return null;
            }
        }


    }
}

