﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{
    public static class SelectHelper
    {
        public static MvcHtmlString PercentSelect(this HtmlHelper htmlHelper, string name, decimal? selected, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "...", Selected = selected == null });
            }
            for (int i = 0; i <= 100; i++)
            {

                selectList.Add(new SelectListItem { Value = ((decimal)i / 100M).ToString(), Text = i.ToString(), Selected = (selected.HasValue && ((int)(selected.Value * 100M)) == i) });
            }

            return htmlHelper.Select(name, selectList, htmlattributes);

        }

        public static MvcHtmlString NumberSelect(this HtmlHelper htmlHelper, string name, int start, int count, int? selected, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "...", Selected = selected == null });
            }
            for (int i = 0; i < count; i++)
            {
                var c = start + i;
                selectList.Add(new SelectListItem { Value = c.ToString(), Text = c.ToString(), Selected = (selected.HasValue && selected.Value == c) });
            }

            return htmlHelper.Select(name, selectList, htmlattributes);

        }

       
        public static MvcHtmlString Select(this HtmlHelper htmlHelper, string name, IEnumerable<SelectListItem> items, object htmlattributes = null, bool multiselect = false)
        {
            TagBuilder selectTag = new TagBuilder("select");
            selectTag.MergeAttribute("name", name);
            if (htmlattributes != null)
            {
                selectTag.MergeAttributes(htmlattributes as RouteValueDictionary ?? new RouteValueDictionary(htmlattributes));
            }

            if (multiselect)
                selectTag.MergeAttribute("multiple", "multiple");

            foreach (var item in items)
            {
                TagBuilder optionTag = new TagBuilder("option");
                // optionTag.SetInnerText(item.Text);
                optionTag.InnerHtml = item.Text;
                if (item.Value != null)
                {
                    optionTag.MergeAttribute("value", item.Value);
                }
                else
                {
                    optionTag.MergeAttribute("value", item.Text);
                }
                if (item.Selected)
                {
                    optionTag.MergeAttribute("selected", "selected");
                }

                selectTag.InnerHtml += optionTag.ToString(TagRenderMode.Normal);
            }

            return new MvcHtmlString(selectTag.ToString(TagRenderMode.Normal));
        }


        public static MvcHtmlString NumberSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int>> expression, int start, int count, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "..." });
            }
            for (int i = 0; i < count; i++)
            {
                var c = start + i;
                selectList.Add(new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            }

            return htmlHelper.SelectFor<TModel, int>(expression, selectList, htmlattributes);

        }
        public static MvcHtmlString NumberSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, byte>> expression, byte start, byte count, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "..." });
            }
            for (byte i = 0; i < count; i++)
            {
                var c = start + i;
                selectList.Add(new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            }

            return htmlHelper.SelectFor<TModel, byte>(expression, selectList, htmlattributes);

        }
        public static MvcHtmlString ByteSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, byte>> expression, byte start, byte count, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "..." });
            }
            for (byte i = 0; i < count; i++)
            {
                var c = start + i;
                selectList.Add(new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            }

            return htmlHelper.SelectFor<TModel, byte>(expression, selectList, htmlattributes);

        }

        public static MvcHtmlString PercentSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, decimal>> expression, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            decimal selectedValue = 0M;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }


            for (decimal i = 0; i <= 100; i++)
            {
                decimal rate = i / 100M;
                selectList.Add(new SelectListItem { Value = rate.ToString(), Text = i.ToString(), Selected = rate == selectedValue });
            }



            return htmlHelper.SelectFor<TModel, decimal>(expression, selectList, htmlattributes);

        }

        public static MvcHtmlString NumberSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, int?>> expression, int start, int count, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "..." });
            }
            for (int i = 0; i < count; i++)
            {
                var c = start + i;
                selectList.Add(new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            }

            return htmlHelper.SelectFor<TModel, int?>(expression, selectList, htmlattributes);

        }

        public static MvcHtmlString NumberSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, byte?>> expression, int start, int count, bool addEmpty, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();
            if (addEmpty)
            {
                selectList.Add(new SelectListItem { Value = "", Text = "..." });
            }
            for (int i = 0; i < count; i++)
            {
                var c = start + i;
                selectList.Add(new SelectListItem { Value = c.ToString(), Text = c.ToString() });
            }

            return htmlHelper.SelectFor<TModel, byte?>(expression, selectList, htmlattributes);

        }

        public static MvcHtmlString PercentSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, decimal?>> expression, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>();

            decimal? selectedValue = null;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }


            for (decimal i = 0; i < 100; i++)
            {
                decimal rate = i / 100M;
                selectList.Add(new SelectListItem { Value = rate.ToString(), Text = i.ToString(), Selected = (selectedValue != null && rate == selectedValue.Value) });
            }



            return htmlHelper.SelectFor<TModel, decimal?>(expression, selectList, htmlattributes);

        }

        public static MvcHtmlString ActiveSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>() { new SelectListItem { Value = bool.TrueString, Text = BilenAdam.Web.Resources.Common.ActiveResources.active }, new SelectListItem { Value = bool.FalseString, Text = BilenAdam.Web.Resources.Common.ActiveResources.passive } };

           
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    bool selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    if(selectedValue)
                    {
                        selectList[0].Selected = true;
                    }
                    else
                    {
                        selectList[1].Selected = true;
                    }
                }
                catch
                {
                }
            }


           



            return htmlHelper.SelectFor<TModel, bool>(expression, selectList, htmlattributes);

        }

        public static MvcHtmlString DisplayActive(this HtmlHelper htmlHelper, bool isActive)
        {
            if(isActive)
            {
                return MvcHtmlString.Create(BilenAdam.Web.Resources.Common.ActiveResources.active);
            }
            else
            {
                return MvcHtmlString.Create(BilenAdam.Web.Resources.Common.ActiveResources.passive);
            }
        }

        public static MvcHtmlString SelectFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> items, object htmlattributes = null)
        {
            var name = htmlHelper.GetFullHtmlName(expression);
            TProperty selectedValue = default(TProperty);
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch
                {
                }
            }

            List<SelectListItem> newItems = new List<SelectListItem>(items.Count());



            foreach (var item in items)
            {
                SelectListItem newItem = new SelectListItem
                {
                    Value = item.Value ?? item.Text,
                    Text = item.Text,
                    Selected = item.Selected
                };
                if (!Object.Equals(selectedValue, null))
                {
                    if (newItem.Value == selectedValue.ToString())
                    {
                        newItem.Selected = true;
                    }
                }
                newItems.Add(newItem);

            }


            return htmlHelper.Select(name, newItems, htmlattributes);
        }


        public static MvcHtmlString GenderSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, object htmlattributes = null)
        {
            List<SelectListItem> selectList = new List<SelectListItem>() { new SelectListItem { Value = bool.TrueString, Text = BilenAdam.Resources.Travel.Passenger.Mr }, new SelectListItem { Value = bool.FalseString, Text = BilenAdam.Resources.Travel.Passenger.Ms } };


            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    bool selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    if (selectedValue)
                    {
                        selectList[0].Selected = true;
                    }
                    else
                    {
                        selectList[1].Selected = true;
                    }
                }
                catch
                {
                }
            }






            return htmlHelper.SelectFor<TModel, bool>(expression, selectList, htmlattributes);

        }

       

    }
}
