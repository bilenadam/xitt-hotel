﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace System.Web.Mvc.Html
{
    public static class MoneyExtensions
    {
     
        public static MvcHtmlString DisplayMoney(this HtmlHelper htmlHelper, decimal? dec, string currency)
        {
            if (dec.HasValue)
                return htmlHelper.DisplayMoney(dec, currency);
            else
                return MvcHtmlString.Empty;
        }
        public static MvcHtmlString DisplayMoney(this HtmlHelper htmlHelper, decimal dec, string currency)
        {
            return MvcHtmlString.Create(dec.ToString(BilenAdam.Web.Resources.Formats.Money) + " " + currency);
        }

       

        
        public static MvcHtmlString DisplayMoneyWithLogo(this HtmlHelper htmlHelper, decimal money)
        {


            return MvcHtmlString.Create(money.ToString(BilenAdam.Web.Resources.Formats.Money) + "€");
        }

     

        public static MvcHtmlString DisplayMoneyWithLogo(this HtmlHelper htmlHelper, decimal? money)
        {

            if (money == null)
            {
                return MvcHtmlString.Empty;
            }
            else
            {
                return htmlHelper.DisplayMoneyWithLogo(money.Value);
            }
        }

        public static MvcHtmlString MoneyBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, decimal>> expression, object htmlattributes = null)
        {
            string value = "";
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    decimal dec = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    value = dec.ToString(BilenAdam.Web.Resources.Formats.Money);
                }
                catch (NullReferenceException)
                {
                }
            }

                return htmlHelper.TextBoxFor(expression, htmlHelper.ExtendHtmlAttr(htmlattributes, new { @class = "money", @Value = value }));
          
        }

        public static MvcHtmlString MoneyBox(this HtmlHelper htmlHelper, string name, object htmlattributes = null)
        {
            
            return htmlHelper.TextBox(name, "", htmlHelper.ExtendHtmlAttr(htmlattributes, new { @class = "money" }));
        }

        public static MvcHtmlString MoneyBox(this HtmlHelper htmlHelper, string name,decimal? dec, object htmlattributes = null)
        {
            var value = dec.ToString4Nullable(d => d.ToString(BilenAdam.Web.Resources.Formats.Money));
            return htmlHelper.TextBox(name, value, htmlHelper.ExtendHtmlAttr(htmlattributes, new { @class = "money" }));
        }


        public static MvcHtmlString MoneyBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, decimal?>> expression, object htmlattributes = null)
        {

            string value = "";
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    decimal? dec = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    value = dec.ToString4Nullable(d => d.ToString(BilenAdam.Web.Resources.Formats.Money));
                }
                catch (NullReferenceException)
                {
                }
            }

            return htmlHelper.TextBoxFor(expression, HtmlAttributeExtensions.ExtendHtmlAttr(htmlattributes, new { @class = "money", @Value = value }));
        }

        public static MvcHtmlString CurrencySelect<TModel>(this HtmlHelper<TModel> htmlHelper, string name, string selected, object htmlattributes = null, bool allowNull = false)
        {
            
            var selectedList = new BilenAdam.Web.GlobalSettings().GetSupportedCurrencies().Select(c => new SelectListItem { Value = c, Text = c, Selected = StringComparer.CurrentCultureIgnoreCase.Compare(c, selected ?? "") == 0 }).ToList();
            
            if (allowNull)
            {
                selectedList.Insert(0, new SelectListItem { Text = string.Empty, Value = string.Empty });
            }
            return htmlHelper.Select(name, selectedList, htmlattributes);
        }

        public static MvcHtmlString CurrencySelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, string>> expression, object htmlattributes = null, bool allowNull = false)
        {
            var selectedList = new BilenAdam.Web.GlobalSettings().GetSupportedCurrencies().Select(c => new SelectListItem { Value = c, Text = c }).ToList();

            if (allowNull)
            {
                selectedList.Insert(0, new SelectListItem { Text = string.Empty, Value = string.Empty });
            }
            return htmlHelper.SelectFor<TModel,string>(expression, selectedList, htmlattributes);
        }
       
       
    }
}
