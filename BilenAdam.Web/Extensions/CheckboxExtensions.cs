﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using BilenAdam.Utilities.Extensions;
using System.Web.Routing;
using System.Web;

namespace System.Web.Mvc.Html
{
    public static class CheckboxExtensions
    {
        public static MvcHtmlString BooleanDisplay(this HtmlHelper htmlHelper, bool? value)
        {
            return MvcHtmlString.Create(value == null ? "" : (value.Value ? "&#10003;" : "&#10006;"));
        }

        public static MvcHtmlString CheckBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool?>> expression, object htmlattributes = null )
        {
            var name = htmlHelper.GetFullHtmlName(expression);
            bool? value = null;
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    value = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                }
                catch (NullReferenceException)
                {
                }
            }


            return htmlHelper.CheckBox(name, value ?? false, htmlattributes);
            
        }
    }
}
