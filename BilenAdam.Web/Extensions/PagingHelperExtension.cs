﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using BilenAdam.Utilities.Paging;
using System.Collections.Specialized;

namespace BilenAdam
{
    public static class PagingExtention
    {
        public static PagedList<T> ToPagedList<T>(this IQueryable<T> items, System.Web.HttpRequestBase request,int pSize = 20)
        {
            int pageIndex = 1;
            if (!int.TryParse(request["pageIndex"], out pageIndex))
                pageIndex = 1;

            int pageSize = pSize;
            if (!int.TryParse(request["pageSize"], out pageSize))
                pageSize = pSize;

            return items.ToPagedList(pageIndex, pageSize);
        }
    }
}
namespace System.Web.Mvc.Html
{
    public static class PagingHelperExtension
    {
       
        public static MvcHtmlString Pager<T>(this HtmlHelper htmlHelper, PagedList<T> pagedList, object htmlAttributes = null)
        {
            if (pagedList == null)
                return new MvcHtmlString("");

            var viewContext = htmlHelper.ViewContext;
            var action = viewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString();
            UrlHelper Url = new UrlHelper(htmlHelper.ViewContext.RequestContext);

            RouteValueDictionary combinedRouteValues = new RouteValueDictionary(viewContext.RouteData.Values);
            NameValueCollection queryString = viewContext.RequestContext.HttpContext.Request.QueryString;
            foreach (string key in queryString.AllKeys.Where(key => key != null))
            {
                if (queryString[key] == "true,false")
                {
                    combinedRouteValues[key] = "true";  // TODO : for checkbox shit and staff, if possible find a elegant solution
                }
                else
                {
                    combinedRouteValues[key] = queryString[key];
                }
            }
         
            if (pagedList.SelectedPageNo < 1) pagedList.SelectedPageNo = 1;

            if ((pagedList.SelectedPageNo > pagedList.PageCount ) && pagedList.PageCount!=0) pagedList.SelectedPageNo = pagedList.PageCount;

            TagBuilder pagingDiv = new TagBuilder("div");
            pagingDiv.MergeAttributes(new RouteValueDictionary(htmlAttributes ?? new { @class = "pagination pagination-centered" }));

            TagBuilder ulTag = new TagBuilder("ul");
            ulTag.AddCssClass("pagination");
            StringBuilder sb = new StringBuilder();

            if (pagedList.SelectedPageNo == 1)
                sb.Append(string.Format("<li class=\"disabled\"><a  href=\"{0}\">{1}</a></li>", "#",BilenAdam.Resources.App.Pager.First));
            else
                sb.Append(string.Format("<li><a class=\"pagerLink\" href=\"{0}\">{1}</a></li>", CreatePagingUrl(Url, 1, action, combinedRouteValues), BilenAdam.Resources.App.Pager.First));


            if (pagedList.HasPreviousPage)
                sb.Append(string.Format("<li><a class=\"pagerLink\" href=\"{0}\">{1}</a></li>", CreatePagingUrl(Url, pagedList.SelectedPageNo - 1, action, combinedRouteValues), BilenAdam.Resources.App.Pager.Prev));
            else
                sb.Append(string.Format("<li class=\"disabled\"><a href=\"{0}\">{1}</a></li>", "#", BilenAdam.Resources.App.Pager.Prev));// disabled diye pagerlink koymadım


            for (int i = pagedList.StartPage; i <= pagedList.EndPage; i++)
            {
                if (i == pagedList.SelectedPageNo)
                    sb.AppendLine(string.Format("<li class=\"active\"><a class=\"pagerLink\" href=\"{0}\">{1}</a></li>", CreatePagingUrl(Url, i, action, combinedRouteValues), i));
                else
                    sb.AppendLine(string.Format("<li><a class=\"pagerLink\" href=\"{0}\">{1}</a></li>", CreatePagingUrl(Url, i, action, combinedRouteValues), i));
            }
            if (pagedList.HasNextPage)
                sb.AppendLine(string.Format("<li><a class=\"pagerLink\" href=\"{0}\">{1}</a></li>", CreatePagingUrl(Url, pagedList.SelectedPageNo + 1, action, combinedRouteValues), BilenAdam.Resources.App.Pager.Next));
            else
                sb.AppendLine(string.Format("<li class=\"disabled\"><a href=\"{0}\">{1}</a></li>", "#", BilenAdam.Resources.App.Pager.Next));// disabled diye pagerlink koymadım

            if ((pagedList.SelectedPageNo == pagedList.PageCount)|| pagedList.PageCount ==0)
                sb.Append(string.Format("<li class=\"disabled\"><a  href=\"{0}\">{1}</a></li>", "#", BilenAdam.Resources.App.Pager.Last));
            else
                sb.Append(string.Format("<li><a class=\"pagerLink\" href=\"{0}\">{1}</a></li>", CreatePagingUrl(Url, pagedList.PageCount, action, combinedRouteValues), BilenAdam.Resources.App.Pager.Last));


            string totalText = string.Format("{0} Result(s) found.", pagedList.TotalCount);

            switch (System.Threading.Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)
            {
                case "tr":
                    totalText = string.Format("{0} Sonuc bulundu.", pagedList.TotalCount);
                    break;
                case "de":
                    if (pagedList.TotalCount == 1)
                    {
                        totalText = string.Format("ein Ergebnis gefunden.", pagedList.TotalCount);
                    }
                    else
                    {
                        totalText = string.Format("{0} Ergebnisse gefunden.", pagedList.TotalCount);
                    }
                    break;
                default:
                    break;
            }


           
            sb.Append(string.Format("<li class=\"pagerTotal\">{0}</li>", totalText));

            ulTag.InnerHtml = sb.ToString();
            pagingDiv.InnerHtml = ulTag.ToString();
            return new MvcHtmlString(pagingDiv.ToString());
        }

        private static string CreatePagingUrl(UrlHelper urlHelper, int pageIndex, string action, RouteValueDictionary routeValues)
        {
            routeValues["pageindex"] = pageIndex;
            return urlHelper.Action(action, routeValues);
        }

    }
}