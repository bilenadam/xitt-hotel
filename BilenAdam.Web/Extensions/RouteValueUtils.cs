﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{

    public static  class RouteValueUtils
    {
        public static RouteValueDictionary CombineViewContextRouteValues(ViewContext viewContext, object newValues)
        {

            RouteValueDictionary combinedRouteValues = new RouteValueDictionary(viewContext.RouteData.Values);
            NameValueCollection queryString = viewContext.RequestContext.HttpContext.Request.QueryString;
            foreach (string key in queryString.AllKeys.Where(key => key != null))
            {
                if (queryString[key] == "true,false")
                {
                    combinedRouteValues[key] = "true";  
                }
                else
                {
                    combinedRouteValues[key] = queryString[key];
                }
            }

            RouteValueDictionary newRouteValues = new RouteValueDictionary(newValues);
            foreach (var key in newRouteValues.Keys)
            {
                combinedRouteValues[key] = newRouteValues[key];
            }
            return combinedRouteValues;
        }


        public static string ChangeCurrentUrl(this HtmlHelper htmlHelper, object newValues)
        {
            return new UrlHelper(htmlHelper.ViewContext.RequestContext).Action(htmlHelper.ViewContext.Controller.ValueProvider.GetValue("action").RawValue.ToString(), CombineViewContextRouteValues(htmlHelper.ViewContext, newValues));
        }

        
    }
}
