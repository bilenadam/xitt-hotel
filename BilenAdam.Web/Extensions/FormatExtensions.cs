﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Script.Serialization;
using BilenAdam.Web;
using System.Web.Routing;

namespace System.Web.Mvc.Html
{
public static class FormatExtensions
{
    public static string ToString4Nullable<T>(this Nullable<T> n, Func<T, string> format, string nullstring = "") where T : struct
    {
        if (n == null)
        {
            return nullstring;
        }
        else
        {
            return format(n.Value);
        }
    }


   


    public static string ToJavaScriptDate(this DateTime date)
    {
        return date.ToString("MM/dd/yyyy");
    }


    public static string GetFullHtmlName<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        return htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldName(ExpressionHelper.GetExpressionText(expression));
    }

    public static string GetFullHtmlId<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        return htmlHelper.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression));
    }

  

 

  
   

   
  

  
   


    
}
}
