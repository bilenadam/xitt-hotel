﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Mvc.Html;
using BilenAdam.Utilities.Extensions;
using System.Web.Routing;
using System.Web;

namespace System.Web.Mvc.Html
{
    public static class DateTimeHtmlExtensions
    {
        public static MvcHtmlString DateTimeDisplay(this HtmlHelper htmlHelper, DateTime? dt, string format = null)
        {
            if (dt == null)
                return MvcHtmlString.Empty;
            else
            {
                var d = dt.Value;

                if (format != null)
                    return MvcHtmlString.Create(d.ToString(format));
                else
                    return MvcHtmlString.Create(d.ToString(BilenAdam.Web.Resources.Formats.Date) + " " + d.ToString(BilenAdam.Web.Resources.Formats.Time));
            }
        }

        public static MvcHtmlString UTCDateTimeDisplay(this HtmlHelper htmlHelper, DateTime? dt, string format = null)
        {
            if (dt == null)
                return MvcHtmlString.Empty;
            else
            {
                var d = dt.Value.ToLocalTime();

                if (format != null)
                    return MvcHtmlString.Create(d.ToString(format));
                else
                    return MvcHtmlString.Create(d.ToString(BilenAdam.Web.Resources.Formats.Date) + " " + d.ToString(BilenAdam.Web.Resources.Formats.Time));
            }
        }


        public static MvcHtmlString DateTimeDisplayFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, string format = null )
        {
            if (htmlHelper.ViewData.Model == null)
            {
                return htmlHelper.DateTimeDisplay(null, format);
            }
            else
            {
                DateTime? value = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                return htmlHelper.DateTimeDisplay(value, format);
            }

           
        }

        public static MvcHtmlString TimeRangeDisplay(this HtmlHelper htmlHelper, TimeSpan start, TimeSpan end)
        {
            return MvcHtmlString.Create(String.Format("{0}-{1}", start.ToString(@"hh\:mm"), end.ToString(@"hh\:mm")));
        }

      


        public static MvcHtmlString DateDisplay(this HtmlHelper htmlHelper, DateTime? dt, string format = null)
        {
            return MvcHtmlString.Create(dt.ToString4Nullable(d => d.ToString(format ?? BilenAdam.Web.Resources.Formats.Date)));
        }

        public static MvcHtmlString DateRangeDisplay(this HtmlHelper htmlHelper, DateTime? start, DateTime? end)
        {
            if (start != null && end != null)
            {
                var s = start.Value;
                var e = end.Value;

                if (end.Value.Year != start.Value.Year)
                    return MvcHtmlString.Create(s.ToString("dd.MM.yyyy") + "-" + e.ToString("dd.MM.yyyy"));
                else if(end.Value.Month != start.Value.Month)
                    return MvcHtmlString.Create(s.ToString("dd.MM") + "-" + e.ToString("dd.MM.yyyy"));
                else 
                    return MvcHtmlString.Create(s.ToString("dd") + "-" + e.ToString("dd.MM.yyyy"));
            }
            else if (start != null && end == null)
            {
                return MvcHtmlString.Create(start.Value.ToString("d") + " -");
            }

            else if (start == null && end != null)
            {
                return MvcHtmlString.Create("- " + end.Value.ToString("d"));
            }
            else
                return MvcHtmlString.Empty;

        }

        public static MvcHtmlString DateDisplayFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, string format = null)
        {
            if (htmlHelper.ViewData.Model == null)
            {
                return htmlHelper.DateDisplay(null, format);
            }
            else
            {
                DateTime? value = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                return htmlHelper.DateDisplay(value, format);
            }
        }

        public static string TimeDisplay(this HtmlHelper htmlHelper, DateTime? dt, string format = null)
        {
            return dt.ToString4Nullable(d => d.ToString(format ?? BilenAdam.Web.Resources.Formats.Time));
        }
        public static MvcHtmlString DurationDisplay(this HtmlHelper htmlHelper, TimeSpan? dt)
        {
            return MvcHtmlString.Create(dt.ToString4Nullable(d => d.ToString("g")));
        }

        public static MvcHtmlString DurationDisplay(this HtmlHelper htmlHelper, DateTime? dt, string format = null)
        {
            return MvcHtmlString.Create(dt.ToString4Nullable(d => d.Subtract(new DateTime(1900, 1, 1)).ToString("g")));
        }

        public static MvcHtmlString DurationDisplayFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, string format = null)
        {
            if (htmlHelper.ViewData.Model == null)
            {
                return htmlHelper.DurationDisplay(null, format);
            }
            else
            {
                DateTime? value = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                return htmlHelper.DurationDisplay(value, format);
            }

        }

        public static MvcHtmlString TimeDisplay(this HtmlHelper htmlHelper, TimeSpan? dt, string format = null)
        {
            return MvcHtmlString.Create(dt.ToString4Nullable(d => d.ToString(format ?? BilenAdam.Web.Resources.Formats.TimeSpanTime)));
        }

        public static RouteValueDictionary ExtendHtmlAttr(object hattr, object extattr)
        {
            return HtmlAttributeExtensions.ExtendHtmlAttr(hattr, extattr);
        }

        
        public static MvcHtmlString DateBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime>> expression, string formatstring = null, object htmlattributes = null, bool supportMobile = false)
        {


            var name = htmlHelper.GetFullHtmlName(expression);
          //  htmlHelper.RegisterJquery();
          //  JavascriptExtensions.RegisterHelperScript(htmlHelper, "$(\".date\").datepicker();");
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    var value = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    string valueText = "";
                    if(value != default(DateTime))
                    {
                        valueText = value.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.Date);
                    }
                   

                        return htmlHelper.TextBox(name, valueText, ExtendHtmlAttr(htmlattributes, new { @class = "date" }));
                    
                    
                }
                catch (NullReferenceException)
                {
                }
            }

          
                return htmlHelper.TextBox(name, "", ExtendHtmlAttr(htmlattributes, new { @class = "date" }));
            
           
        }
        public static MvcHtmlString DisplayJscirptDate(this HtmlHelper htmlHelper, DateTime? date)
        {
            if (date != null)
            {
                return new MvcHtmlString(date.Value.ToString("yyyy-MM-dd"));
            }
            else
            {
                return MvcHtmlString.Empty;
            }
        }
        public static MvcHtmlString DateBox(this HtmlHelper htmlHelper, string name, DateTime? date, string formatstring = null, object htmlattributes = null)
        {
           
            var value = date.ToString4Nullable(d => d.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.Date));
            
            return htmlHelper.TextBox(name, value, ExtendHtmlAttr(htmlattributes, new { @class = "date" }));
        }
        public static MvcHtmlString DateBox(this HtmlHelper htmlHelper, string name, DateTime date, string formatstring = null, object htmlattributes = null)
        {
          

            var value = date.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.Date);

            return htmlHelper.TextBox(name, value, ExtendHtmlAttr(htmlattributes, new { @class = "date" }));
        }

       
        public static MvcHtmlString DateBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, string formatstring = null, object htmlattributes = null, bool supportMobile = false)
        {
            //var cattr = ExtendHtmlAttr(htmlattributes, new { @class = "date" });

            var name = htmlHelper.GetFullHtmlName(expression);


            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    DateTime? dt = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    var value = dt.ToString4Nullable(d => d.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.Date));

                  
                        return htmlHelper.TextBox(name, value, ExtendHtmlAttr(htmlattributes, new { @class = "date" }));
                    
                   
                }
                catch (NullReferenceException)
                {
                }
            }
           
                return htmlHelper.TextBox(name, "", ExtendHtmlAttr(htmlattributes, new { @class = "date" }));
            
          
        }

        public static MvcHtmlString DateTimeBox<TModel>(this HtmlHelper<TModel> htmlHelper, string name,DateTime? val, string formatstring = null, object htmlattributes = null)
        {
            var cattr = ExtendHtmlAttr(htmlattributes, new { @class = "date-time" });

            

            var value = val.ToString4Nullable(d => d.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.General_ShortTime));
            return htmlHelper.TextBox(name, value , cattr);
        }

        public static MvcHtmlString DateTimeBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime?>> expression, string formatstring = null, object htmlattributes = null)
        {
            var cattr = ExtendHtmlAttr(htmlattributes, new { @class = "date-time" });

            var name = htmlHelper.GetFullHtmlName(expression);
            

            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    DateTime? dt = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    var value = dt.ToString4Nullable(d => d.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.General_ShortTime));

                    return htmlHelper.TextBox(name, value, cattr);
                }
                catch (NullReferenceException)
                {
                }
            }

            return htmlHelper.TextBox(name, "", cattr);
        }
        public static MvcHtmlString DateTimeBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, DateTime>> expression, string formatstring = null, object htmlattributes = null)
        {
            var cattr = ExtendHtmlAttr(htmlattributes, new { @class = "date-time" });

            var name = htmlHelper.GetFullHtmlName(expression);

            

            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    DateTime dt = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    if (dt != default(DateTime))
                    {
                        var value = dt.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.General_ShortTime);

                        return htmlHelper.TextBox(name, value, cattr);
                    }
                    else
                    {
                        return htmlHelper.TextBox(name, "", cattr);
                    }
                }
                catch (NullReferenceException)
                {
                    return htmlHelper.TextBox(name, "", cattr);
                }
            }

            return htmlHelper.TextBox(name, "", cattr);
        }
        public static MvcHtmlString TimeBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TimeSpan>> expression, string formatstring = null, object htmlattributes = null)
        {

            var name = htmlHelper.GetFullHtmlName(expression);
            


            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    TimeSpan dt = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    var value = dt.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.TimeSpanTime);

                    return htmlHelper.TextBox(name, value, ExtendHtmlAttr(htmlattributes, new { @class = "time" }));
                }
                catch
                {
                    return htmlHelper.TextBox(name, "", ExtendHtmlAttr(htmlattributes, new { @class = "time" }));
                }
            }
            else
            {
                return htmlHelper.TextBox(name, "", ExtendHtmlAttr(htmlattributes, new { @class = "time" }));
            }
        }

        public static MvcHtmlString TimeBoxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TimeSpan?>> expression, string formatstring = null, object htmlattributes = null)
        {
            var name = htmlHelper.GetFullHtmlName(expression);
            
            if (htmlHelper.ViewData.Model != null)
            {
                try
                {
                    TimeSpan? dt = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                    var value = dt.ToString4Nullable(d => d.ToString(formatstring ?? BilenAdam.Web.Resources.Formats.TimeSpanTime));
                    return htmlHelper.TextBox(name, value, ExtendHtmlAttr(htmlattributes, new { @class = "time" }));
                }
                catch (NullReferenceException)
                {
                }
            }
            return htmlHelper.TextBox(name, "", ExtendHtmlAttr(htmlattributes, new { @class = "time" }));
        }

        public static MvcHtmlString DisplayDayName(this HtmlHelper helper, DateTime? date)
        {
            return date == null ? MvcHtmlString.Empty : new MvcHtmlString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(date.Value.DayOfWeek));
        }
        public static MvcHtmlString DisplayMonthName(this HtmlHelper helper, int month)
        {
            return new MvcHtmlString(System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(month));
        }
        
        public static MvcHtmlString DisplayMonthNameFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, int>> expression)
        {
            if (helper.ViewData.Model != null)
            {
                int val = expression.Compile().Invoke(helper.ViewData.Model);
                return helper.DisplayMonthName(val);
            }
            else
            {
                return new MvcHtmlString("");
            }

        }

        public static MvcHtmlString DropDownMonthsList(this HtmlHelper helper, string name, int selectedMonth, object htmlAttributes = null)
        {
            SelectList selectList = new SelectList(
                Enumerable.Range(1,12).Select(i=> new { val = i , text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(i) }), 
                "val","text",selectedMonth);

            return helper.Select(name, selectList,htmlAttributes);
                
        }

        public static MvcHtmlString DropDownMonthsListFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, int>> expression, object htmlAttributes = null)
        {
            int selectedValue = 0;
            if (helper.ViewData.Model != null)
            {
                selectedValue = expression.Compile().Invoke(helper.ViewData.Model);
            }

            SelectList selectList = new SelectList(
                Enumerable.Range(1, 12).Select(i => new { val = i, text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(i) }),
                "val", "text", selectedValue);
            
            return helper.SelectFor(expression, selectList, htmlAttributes);

        }

        public static MvcHtmlString ListBoxMonths(this HtmlHelper helper, string name, IEnumerable<int> selectedMonths, object htmlAttributes = null)
        {
            return helper.ListBox(name, Enumerable.Range(1, 12).Select(i => new SelectListItem { Selected = selectedMonths.Contains(i), Value = i.ToString(), Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(i) }), htmlAttributes);
        }

        public static MvcHtmlString ListBoxMonthsFor<TModel, TList>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TList>> expression, object htmlAttributes = null) where TList : IEnumerable<int>
        {
            IEnumerable<int> selectedMonths = new int[] { };
            if (helper.ViewData.Model != null)
            {
                selectedMonths = expression.Compile().Invoke(helper.ViewData.Model);
            }
            return helper.ListBoxFor(expression, 
                Enumerable.Range(1, 12).Select(i => new SelectListItem { Selected = selectedMonths.Contains(i), Value = i.ToString(), Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(i) }), 
                htmlAttributes);
        }

        public static MvcHtmlString ListBoxDayNameArray<TList>(this HtmlHelper htmlHelper, string name, TList selectedValues, object htmlAttributes) where TList : IEnumerable<DayOfWeek>
        {

            var days = Enum.GetValues(typeof(System.DayOfWeek));


            IEnumerable<SelectListItem> items = days.Cast<System.DayOfWeek>().Select(d => new SelectListItem
            {
                Value = d.ToString(),
                Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(d),
                Selected = selectedValues != null && selectedValues.Contains(d)
            });
            return htmlHelper.ListBox(name, items, htmlAttributes);

        }

        public static MvcHtmlString ListBoxForDayNameArray<TModel,TList>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TList>> expression, object htmlAttributes) where TList : IEnumerable<DayOfWeek>
        {


            var days = Enum.GetValues(typeof(System.DayOfWeek));

            if (htmlHelper.ViewData.Model != null)
            {
                IEnumerable<SelectListItem> items = days.Cast<System.DayOfWeek>().Select(d => new SelectListItem { Value = d.ToString(), Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(d) });
                return htmlHelper.ListBoxFor(expression, items, htmlAttributes);
            }
            else
            {
                var selectedValues = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                IEnumerable<SelectListItem> items = days.Cast<System.DayOfWeek>().Select(d => new SelectListItem
                {
                    Value = d.ToString(),
                    Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(d),
                    Selected = selectedValues != null && selectedValues.Contains(d)
                });
                return htmlHelper.ListBoxFor(expression, items, htmlAttributes);
            }
        }

        public static MvcHtmlString DayNameSelectFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, System.DayOfWeek>> expression, object htmlAttributes)
        {
            var days = Enum.GetValues(typeof(System.DayOfWeek));

            if (htmlHelper.ViewData.Model != null)
            {
                IEnumerable<SelectListItem> items = days.Cast<System.DayOfWeek>().Select(d => new SelectListItem { Value = d.ToString(), Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(d) });
                return htmlHelper.SelectFor(expression, items, htmlAttributes);
            }
            else
            {
                var selectedValue = expression.Compile().Invoke(htmlHelper.ViewData.Model);
                IEnumerable<SelectListItem> items = days.Cast<System.DayOfWeek>().Select(d => new SelectListItem { Value = d.ToString(), Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(d), Selected = (d == selectedValue) });
                return htmlHelper.SelectFor(expression, items, htmlAttributes);
            }
        }

        public static MvcHtmlString DayNameSelect(this HtmlHelper htmlHelper, string name, System.DayOfWeek selectedValue, object htmlAttributes)
        {
            var days = Enum.GetValues(typeof(System.DayOfWeek));
            IEnumerable<SelectListItem> items = days.Cast<System.DayOfWeek>().Select(d => new SelectListItem { Value = d.ToString(), Text = System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(d), Selected = (d == selectedValue) });
            return htmlHelper.Select(name, items, htmlAttributes);
        }
    }
}
