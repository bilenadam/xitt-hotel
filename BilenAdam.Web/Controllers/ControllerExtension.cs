﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Membership;
using BilenAdam.Web.Membership;

namespace System.Web.Mvc
{
    public static class ControllerExtension
    {
        public static IMembership GetMembership(this Controller controller)
        {
            return DependencyResolver.Current.GetService<IMembership>() ?? MembershipHelper.GetMembership();
        }

       
        public static int? GetContactId(this Controller controller)
        {
            return controller.GetMembership()?.GetContactId();
        }


        public static ActionResult RedirectToHome(this Controller controller)
        {
            return new RedirectResult(controller.Url.Action("Index", "Home"));
        }


       
       public static string GetClientIP(this Controller controller)
        {
            if (!string.IsNullOrWhiteSpace(controller.Request.Headers["X_FORWARDED_FOR"]))
            {
                return controller.Request.Headers["X_FORWARDED_FOR"].Split(',').FirstOrDefault();
            }

            if (!string.IsNullOrWhiteSpace(controller.Request.Headers["REMOTE_ADDR"]))
            {
                return controller.Request.Headers["REMOTE_ADDR"];
            }

            return controller.Request.UserHostAddress;
        }
        
       
    }
}
