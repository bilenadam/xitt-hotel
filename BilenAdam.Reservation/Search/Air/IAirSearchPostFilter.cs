﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IAirSearchPostFilter
    {
        IAirSearchProduct Apply(IAirSearchProduct p);

        IOneWaySearchProduct Apply(IOneWaySearchProduct p);

        IDirectOneWaySearchProduct Apply(IDirectOneWaySearchProduct p);
    }
}
