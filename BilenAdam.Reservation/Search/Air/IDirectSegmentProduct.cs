﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Search.Air;
using BilenAdam.TravelCode.Air;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IDirectSegmentProduct : ISegmentProduct, IFlightProduct
    {
    }
}

