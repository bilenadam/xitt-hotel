﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IAirFareProduct : IFareProduct
    {
        IPassengerFareProducts GetPassengerFareProducts();

        IEnumerable<ISegmentProduct> GetSegmentProducts();

        int SegmentCount { get; }
    }
}
