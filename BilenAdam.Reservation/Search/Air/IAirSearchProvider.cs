﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IAirSearchProvider
    {
        void BeginSearch(IAirSearch search);
        void Search(IAirSearch search);
    }
}
