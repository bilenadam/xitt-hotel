﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.Reservation.Elements;


namespace BilenAdam.Reservation.Search.Air
{
    public interface IPassengerFareProduct : IPassengerFare
    {
        PassengerType RequestedType { get; }
        int PaxCount { get; }

        decimal AllPassengersTotal { get; }        

    }


}
