﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Air;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IPassengerFareProducts : IFareProduct
    {
        IPassengerFareProduct Get(PassengerType type);
        IEnumerable<IPassengerFareProduct> GetFareBreakdown();

        int Seated { get; }
    }
}
