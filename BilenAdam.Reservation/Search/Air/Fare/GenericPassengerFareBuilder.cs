﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air.Fare;

namespace BilenAdam.Reservation.Search.Air
{
    public class GenericPassengerFareBuilder : PassengerFareBuilder
    {
        public GenericPassengerFareBuilder(IPassengerFareBuilder b,IFareAmount amt) : base(b, amt.Amount)
        {

            FareAmount = amt;
        }

      

        public override IFareAmount FareAmount
        {
            get;
        }

      
    }
}
