﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Air.Fare;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IPassengerFare
    {
        PassengerType PassengerType { get; }
    
        IEnumerable<IFareAmount> GetFareAmounts();

        decimal PerPassengerTotal { get; }


    }
}


