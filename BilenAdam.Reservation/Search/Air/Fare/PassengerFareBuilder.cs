﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Air.Fare;

namespace BilenAdam.Reservation.Search.Air
{
    public abstract class PassengerFareBuilder : IPassengerFareBuilder
    {
        public PassengerFareBuilder(IPassengerFareBuilder b, decimal amount)
        {
            Base = b;
            PassengerType = b.PassengerType;
            PerPassengerTotal = b.PerPassengerTotal + amount;
        }

        public PassengerFareBuilder(PassengerType type, decimal total)
        {
            Base = null;
            PerPassengerTotal = total;
            PassengerType = type;
        }

        public  IPassengerFareBuilder Base { get; }

        public  PassengerType PassengerType { get; }
        public decimal PerPassengerTotal { get; }
        


        public abstract IFareAmount FareAmount { get; }

        

        public IEnumerable<IFareAmount> GetFareAmounts()
        {
            if(Base != null)
            {
                foreach (var fa in Base.GetFareAmounts())
                {
                    yield return fa;
                }
            }
            yield return FareAmount;
        }

  

     

        
        
    }
}
