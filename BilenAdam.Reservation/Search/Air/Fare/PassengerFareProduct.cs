﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Air.Fare;

namespace BilenAdam.Reservation.Search.Air
{
    public class PassengerFareProduct : IPassengerFareProduct
    {
        protected PassengerFareAmount fareBuilder;

        public virtual PassengerType RequestedType { get; }

        public int PaxCount { get; }

        public PassengerType PassengerType => fareBuilder.PassengerType;

        public PassengerFareProduct(PassengerType requestedType, PassengerType passengerType, decimal fare,  int paxCount, string fareCode = null)
        {
            fareBuilder = new PassengerFareAmount(passengerType, fare, fareCode??KnownFareCodes.Fare, FareAmountType.Fare);
            RequestedType = requestedType;
            PaxCount = paxCount;
            AllPassengersTotal = fareBuilder.PerPassengerTotal  * paxCount;
        }

        

        public PassengerFareProduct(PassengerType passengerType, decimal fare,  int paxCount, string fareCode = null) : this(passengerType,passengerType,fare,paxCount, fareCode) { }

        public PassengerFareProduct(IPassengerFareProduct copy)
        {
           
            RequestedType = copy.RequestedType;
            PaxCount = copy.PaxCount;
            if (copy is PassengerFareProduct)
            {
                this.fareBuilder = ((PassengerFareProduct)copy).fareBuilder;
            }
            else
            {
                foreach (var fa in copy.GetFareAmounts())
                {
                    if (fareBuilder == null)
                        fareBuilder = new PassengerFareAmount(copy.PassengerType, fa.Amount, fa.Code, fa.Type);
                    else
                        fareBuilder = new PassengerFareAmount(fareBuilder, fa.Amount, fa.Code, fa.Type);

                }
            }
            AllPassengersTotal = copy.AllPassengersTotal;
        }





        public  decimal AllPassengersTotal { get; private set; }
        public virtual decimal PerPassengerTotal => fareBuilder.PerPassengerTotal;

        public virtual IEnumerable<IFareAmount> GetFareAmounts() => fareBuilder.GetFareAmounts();
        
       
        private PassengerFareProduct Add(string code, decimal amount, FareAmountType amtType)
        {
            fareBuilder = new PassengerFareAmount(fareBuilder, amount, code, amtType);
            AllPassengersTotal += (amount * PaxCount);
            return this;
        }
        public PassengerFareProduct Add(IFareAmount f) => Add(f.Code, f.Amount, f.Type);
        public PassengerFareProduct AddFare(decimal amount, string code = null) => Add(code??KnownFareCodes.Fare, amount, FareAmountType.Fare);
        public PassengerFareProduct AddTax( decimal amount, string code = null) => Add(code??KnownFareCodes.Tax, amount, FareAmountType.Tax);

        public PassengerFareProduct AddServiceFee( decimal amount, string code = null) => Add(code??KnownFareCodes.SF, amount, FareAmountType.SF);

        public PassengerFareProduct AddAgenyServiceFee(decimal amount) => AddServiceFee(amount, KnownFareCodes.AgencyServiceFee);

        public PassengerFareProduct AddMarkup(decimal amount, string code = null) => AddServiceFee(amount, code??KnownFareCodes.Markup);
        public static PassengerFareProduct Add(IPassengerFareProduct p, IPassengerFareProduct p2)
        {
            PassengerFareProduct np = new PassengerFareProduct(p);
            foreach (var fa in p2.GetFareAmounts())
            {
                np.Add(fa);
            }

            return np;
        }


    }
}
