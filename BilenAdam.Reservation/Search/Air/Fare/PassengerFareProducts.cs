﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public class PassengerFareProducts : IPassengerFareProducts
    {

        protected readonly Dictionary<PassengerType, IPassengerFareProduct> breakdown = new Dictionary<PassengerType, IPassengerFareProduct>();


        public PassengerFareProducts()
        {

        }
        public PassengerFareProducts(IPassengerFareProducts passengerFareProducts)
        {
            foreach (var fare in passengerFareProducts.GetFareBreakdown())
            {
                Add(fare);
            }
        }

        public virtual int Seated { get; private set; } = 0;



        public virtual decimal TotalFare { get; private set; } = 0M;


        public virtual void Add(IPassengerFareProduct product)
        {
            breakdown.Add(product.RequestedType, product);

            if (product.PassengerType != PassengerType.Infant)
                Seated += product.PaxCount;


            TotalFare += product.AllPassengersTotal;

        }






        public static PassengerFareProducts Add(IPassengerFareProducts p1, IPassengerFareProducts p2)
        {
            PassengerFareProducts newProduct = new PassengerFareProducts();
            foreach (var pt in p1.GetFareBreakdown())
            {
                newProduct.Add(PassengerFareProduct.Add(pt, p2.Get(pt.PassengerType)));
            }

            return newProduct;
        }



        public static PassengerFareProducts Create(IEnumerable<IPassengerFareProduct> fares)
        {
            var newProd = new PassengerFareProducts();
            foreach (var fare in fares)
            {
                newProd.Add(fare);
            }
            return newProd;
        }

        public virtual IPassengerFareProduct Get(PassengerType type)
        {
            if (breakdown.ContainsKey(type))
                return breakdown[type];
            else
                return null;
        }

        public virtual IEnumerable<IPassengerFareProduct> GetFareBreakdown()
        {
            return breakdown.Values;
        }
    }


}
