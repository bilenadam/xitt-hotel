﻿using BilenAdam.Reservation.Air.Fare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IPassengerFareBuilder : IPassengerFare
    {
        IPassengerFareBuilder Base { get; }
        IFareAmount FareAmount { get; }
        
    }
}
