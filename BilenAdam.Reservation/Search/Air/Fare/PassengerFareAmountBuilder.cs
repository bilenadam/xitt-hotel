﻿using BilenAdam.Reservation.Air.Fare;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    
    public  class PassengerFareAmount : PassengerFareBuilder, IFareAmount
    {

        public PassengerFareAmount(Reservation.Air.PassengerType passengerType, decimal amount, string code, FareAmountType amountType)
            : base(passengerType, amount)
        {
            this.Amount = amount;
            this.Code = code;
            this.Type = amountType;
        }

        public PassengerFareAmount(IPassengerFareBuilder b, decimal amt , string code, FareAmountType amountType) 
            : base(b, amt)
        {
            this.Amount = amt;
            this.Code = code;
            this.Type = amountType;
        }

        public decimal Amount { get; }

        public string Code { get; }
        public FareAmountType Type { get; }

        public override IFareAmount FareAmount => this;

        
    }
}
