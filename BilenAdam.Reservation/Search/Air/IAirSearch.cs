﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.Reservation.Search.Air;

namespace BilenAdam.Reservation.Search.Air
{
    /// <summary>
    /// air search object with query
    /// </summary>
    public interface IAirSearch
    {
        Query.IAirQuery Query { get; }
        

        void SignalRunning();
        void SignalFinished();

        void AddProduct(IAirSearchProduct p);

        void AddProduct(IOneWaySearchProduct p);

        void AddProduct(IDirectOneWaySearchProduct p);
        

    }
}
