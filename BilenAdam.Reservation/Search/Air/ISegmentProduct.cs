﻿using BilenAdam.TravelCode.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Air;
using BilenAdam.Reservation.Air;

namespace BilenAdam.Reservation.Search.Air
{
    public interface ISegmentProduct
    {
        int SegmentIndex { get; }

        IEnumerable<IFlightProduct> GetFlightProducts();

        int Stops { get; }
        DateTime Departure { get; }
        DateTime Arrival { get; }

        string Carrier { get; }

        string Origin { get; }

        string Destination { get; }
    }
}





