﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public class FlightDate : IFlightDate
    {
        public DateTime Date
        {
            get;
            set;
        }

       

        public TimeSpan? Earliest
        {
            get;
            set;
        }

        public TimeSpan? Latest
        {
            get;
            set;
        }


        public bool IsFlexDate
        {
            get;
            set;
        }
    }
}
