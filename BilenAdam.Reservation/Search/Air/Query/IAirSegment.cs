﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.TravelCode.Air;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public interface IAirSegment
    {
        int SegmentIndex { get; }
        string Origin { get; }
        string Destination { get; }
        IFlightDate FlightDate { get; }

        bool? OriginIsCity { get; }

        bool? DestinationIsCity { get; }
    }
}
