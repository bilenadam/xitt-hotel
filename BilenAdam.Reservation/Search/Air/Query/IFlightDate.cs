﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public interface IFlightDate
    {
        DateTime Date { get; }
        bool IsFlexDate { get; }
        TimeSpan? Earliest { get; }
        TimeSpan? Latest { get; }
    }
}
