﻿using BilenAdam.TravelCode.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public class AirSegment : IAirSegment
    {
        public int SegmentIndex
        {
            get;
            set;
        }

        public string Origin
        {
            get;
            set;
        }

        public string Destination
        {
            get;
            set;
        }

        public FlightDate FlightDate
        {
            get;
            set;
        }

        public bool? OriginIsCity { get; set; }

        public bool? DestinationIsCity { get; set; }

        IFlightDate IAirSegment.FlightDate
        {
            get { return this.FlightDate; }
        }
    }
}
