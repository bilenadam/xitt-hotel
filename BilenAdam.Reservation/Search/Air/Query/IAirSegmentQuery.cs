﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public interface IAirSegmentQuery
    {
        ISegmentProduct GetSegments(); 
        IReadOnlyDictionary<PassengerType, int> GetPaxPlan();

        bool IgnoreBookingClasses { get; }

        CurrencyCode Currency { get; }

        SaleType SaleType { get; }
    }
}
