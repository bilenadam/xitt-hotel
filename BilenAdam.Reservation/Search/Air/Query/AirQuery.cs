﻿using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Search.Air.Query.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public class AirQuery : IAirQuery
    {



        public List<AirSegment> Segments = new List<AirSegment>();
        public IEnumerable<IAirSegment> GetSegments()
        {
            return Segments;
        }
        public Dictionary<PassengerType, int> PaxPlan = new Dictionary<PassengerType, int>();
        public IReadOnlyDictionary<PassengerType, int> GetPaxPlan()
        {
            return PaxPlan;
        }

        public List<IAirFilter> Filters = new List<IAirFilter>();

        public CurrencyCode Currency { get; set; }

        public IEnumerable<IAirFilter> GetFilters()
        {
            return Filters;
        }

       
    }
}
