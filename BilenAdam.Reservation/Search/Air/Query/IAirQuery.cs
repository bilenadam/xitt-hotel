﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Air.Query
{
    public interface IAirQuery
    {
        IEnumerable<IAirSegment> GetSegments();
        IReadOnlyDictionary<PassengerType, int> GetPaxPlan();
        IEnumerable<Filters.IAirFilter> GetFilters();

        CurrencyCode Currency { get; }
      
    }
}
