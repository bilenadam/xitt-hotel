﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Air.Query.Filters
{
    public interface ICabinTypeFilter : IAirFilter
    {
        CabinType CabinType { get; }
    }
}
