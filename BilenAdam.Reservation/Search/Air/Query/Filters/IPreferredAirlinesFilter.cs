﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Air.Query.Filters
{
    public interface IPreferredAirlinesFilter : IAirFilter
    {
        IEnumerable<string> Airlines { get; }
    }
}
