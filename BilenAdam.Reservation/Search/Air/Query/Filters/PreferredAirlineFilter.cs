﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Air.Query.Filters
{
    public class PreferredAirlineFilter : IPreferredAirlinesFilter
    {
        List<string> _airlines = new List<string>();
        public List<string> Airlines {
            get
            {
                return _airlines;
            }
            set
            {
                _airlines = value;
            }
        }
        

      



        IEnumerable<string> IPreferredAirlinesFilter.Airlines
        {
            get { return Airlines; }
        }
    }
}
