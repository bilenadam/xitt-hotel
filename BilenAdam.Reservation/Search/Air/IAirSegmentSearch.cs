﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IAirSegmentSearch
    {
        

        Query.IAirSegmentQuery SegmentQuery { get; }

        void SignalRunning();
        void SignalFinished();

        void AddProduct(IOneWaySearchProduct p);
        
    }
}
