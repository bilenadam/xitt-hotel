﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air.PriceSearch
{
    public interface IAirPriceSearchProvider
    {
        IAirItineraryProduct Price(ISegmentProduct segmentProduct, IReadOnlyDictionary<PassengerType, int> paxPlan, string currency, bool bestFare);
    }
}
