﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IAirSegmentSearchProvider
    {
        void BeginSearch(IAirSegmentSearch search);
        void Search(IAirSegmentSearch search);
    }
}
