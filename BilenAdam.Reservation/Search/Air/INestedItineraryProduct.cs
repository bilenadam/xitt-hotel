﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface INestedItineraryProduct : IAirItineraryProduct
    {
        IAirItineraryProduct GetInnerProduct();
        IAirItineraryProduct Nest(IAirItineraryProduct product);
    }
}
