﻿using BilenAdam.Reservation.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Air
{
    public interface IFlightProduct : IFlight
    {
        string BookingClass { get; }
        CabinType BookingCabin { get; }

        int? AvailableSeats { get; }
    }
}
