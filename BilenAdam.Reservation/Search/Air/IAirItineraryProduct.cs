﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.Reservation.Air;

namespace BilenAdam.Reservation.Search.Air
{
    /// <summary>
    /// a fare product of air segments for passenger/traveller list
    /// <remarks>
    /// at first designed to create a reservation but actually it is the final product of air search and selection
    /// </remarks>
    /// </summary>
    public interface IAirItineraryProduct : IAirFareProduct
    {
        IAirItineraryProduct Combine(IAirItineraryProduct product);
    }
}
