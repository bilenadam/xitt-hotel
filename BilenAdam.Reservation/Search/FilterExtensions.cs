﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Hotels.Query;
using BilenAdam.Reservation.Search.Hotels.Query.Filters;
using BilenAdam.Reservation.Search.Air.Query;
using BilenAdam.Reservation.Search.Air.Query.Filters;

namespace BilenAdam
{
    public static class FilterExtensions
    {
        public static T GetAirFilterValue<F,T>(this IEnumerable<IAirFilter> filters, Func<F,T> getValue , T defaultValue) where F : IAirFilter
        {
            if(filters != null)
            {
                var filter = filters.FirstOrDefault(f => f is F);

                if (filter != null)
                    return getValue((F)filter);
            }

            return defaultValue;
        }

        public static T GetHotelFilterValue<F, T>(this IEnumerable<IHotelFilter> filters, Func<F, T> getValue, T defaultValue) where F : IHotelFilter
        {
            if (filters != null)
            {
                var filter = filters.FirstOrDefault(f => f is F);

                if (filter != null)
                    return getValue((F)filter);
            }

            return defaultValue;
        }

    

       
      


        public static IEnumerable<string> GetPrefferedAirlinesFilter(this IEnumerable<IAirFilter> filters, IEnumerable<string> defaultValue)
        {
            return filters.GetAirFilterValue<IPreferredAirlinesFilter, IEnumerable<string>>(f => f.Airlines, defaultValue);
        }

        public static IEnumerable<string> GetPrefferedAirlines(this IAirQuery query, IEnumerable<string> defaultValue = null)
        {
            return query.GetFilters().GetPrefferedAirlinesFilter(defaultValue);
        }

        public static string GetClientNationalityFilter(this IEnumerable<IHotelFilter> filters, string defaultValue)
        {
            try
            {
                if (filters != null)
                {
                    var filter = filters.FirstOrDefault(f => f is IClientNationalityFilter) as IClientNationalityFilter;

                    if (filter != null)
                        return filter.ClientNationality;
                    
                }
            }
            catch
            {

            }

            return defaultValue;
        }

        public static string GetClientNationality(this IHotelQuery query, string defaultValue = null)
        {
            return query.GetHotelFilters().GetClientNationalityFilter(defaultValue);
        }
    }
}
