﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BilenAdam.Reservation.Search.Hotels.Query;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IHotelSearch
    {
        IHotelQuery Query { get; }
        void AddProduct(IPropertyProduct p);
        IEnumerable<IPropertyProduct> GetResults();

        void SignalRunning();
        void SignalFinished();


     
    }
}
