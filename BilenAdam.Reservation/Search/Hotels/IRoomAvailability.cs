﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IRoomAvailability
    {
        bool IsAvailable { get; }
        IDictionary<DateTime, bool> IsAvailablePerNight { get; }
    }
}
