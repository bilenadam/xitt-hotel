﻿using BilenAdam.Reservation.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IRoomProduct
    {
        Query.IRoomPlan RoomReference { get; }
        int BoardType { get; }
        int RoomType { get; }



        decimal GetTotalFare();
        decimal GetAverageRate();

        IEnumerable<IRoomDateProduct> GetDates();
        IEnumerable<IAmenity> Amenities { get; }

        


    }
}
