﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IHotelSearchProvider
    {
        void BeginSearch(IHotelSearch search);
        //System.Threading.ManualResetEventSlim SearchCompleted { get; }
    }
}
