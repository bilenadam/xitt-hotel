﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IHotelSegmentSearch
    {
        Query.IHotelSegmentQuery SegmentQuery { get; }

        void SignalRunning();
        void SignalFinished();

        void AddProduct(IPropertyProduct propertyProduct);
    }
}
