﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IHotelSearchProviderFactory
    {
        IEnumerable<IHotelSearchProvider> Create(IHotelSearch search);

        IHotelSingleAvailabilityProvider Create(IPropertyProduct product);
    }
}
