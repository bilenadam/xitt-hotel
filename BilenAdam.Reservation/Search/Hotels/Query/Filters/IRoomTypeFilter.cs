﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels.Query.Filters
{
    public interface IRoomTypeFilter : IHotelFilter
    {
        IEnumerable<string> GetRoomTypes();
    }
}
