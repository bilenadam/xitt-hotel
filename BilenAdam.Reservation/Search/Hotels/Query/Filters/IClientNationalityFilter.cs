﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels.Query.Filters
{
    public interface IClientNationalityFilter : IHotelFilter
    {
        string ClientNationality { get; }
    }
}
