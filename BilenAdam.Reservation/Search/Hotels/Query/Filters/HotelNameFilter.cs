﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels.Query.Filters
{
    public class HotelNameFilter : IHotelNameFilter
    {
        public string HotelName
        {
            get;
            set;
        }

        public string ChainName
        {
            get;
            set;
        }

        public string HotelId
        {
            get;
            set;
        }
    }
}
