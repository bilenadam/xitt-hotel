﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels.Query.Filters
{
    public interface IHotelNameFilter : IHotelFilter
    {
        string HotelId { get;  }
        string HotelName { get; }
        string ChainName { get; }
    }
}
