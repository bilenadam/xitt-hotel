﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels.Query
{
    public class HotelQuery : IHotelQuery
    {

        public string Destination
        {
            get;
            set;
        }

        public DateTime CheckIn
        {
            get;
            set;
        }

        public DateTime CheckOut
        {
            get;
            set;
        }

        public bool IncludeHotelTransfer
        {
            get;set;
        }

        public bool IsPackageSearch
        {
            get;set;
        }

        public CurrencyCode Currency { get; }

        public List<IRoomPlan> Rooms = new List<IRoomPlan>();
        public IEnumerable<IRoomPlan> GetRooms()
        {
            return Rooms;
        }

        public List<Filters.IHotelFilter> Filters = new List<Filters.IHotelFilter>();
        public IEnumerable<Filters.IHotelFilter> GetHotelFilters()
        {
            return Filters;
        }
    }
}
