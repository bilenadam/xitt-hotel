﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels.Query
{
    public interface IHotelQuery
    {
        string Destination { get; }
        DateTime CheckIn { get; }
        DateTime CheckOut { get; }

        IEnumerable<IRoomPlan> GetRooms();
        IEnumerable<Filters.IHotelFilter> GetHotelFilters();

        bool IncludeHotelTransfer { get;}

        bool IsPackageSearch { get; }

        CurrencyCode Currency { get; }
    }
}
