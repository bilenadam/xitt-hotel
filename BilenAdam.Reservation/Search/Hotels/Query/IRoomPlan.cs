﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels.Query
{
    public interface IRoomPlan
    {
        int RoomRef { get;  }
        int AdultCount { get; }
        int ChildCount { get; }
        IEnumerable<int> ChildAges { get; }
    }
}
