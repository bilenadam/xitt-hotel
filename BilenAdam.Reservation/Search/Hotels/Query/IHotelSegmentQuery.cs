﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels.Query
{
    public interface IHotelSegmentQuery
    {
        int HotelId { get; }
        DateTime CheckIn { get; }
        DateTime CheckOut { get; }
        IEnumerable<Query.IRoomProductPlan> RoomPlan { get; }

        bool IncludeHotelTransfer { get; }
        bool IsPackageSearch { get; }
    }
}
