﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels.Query
{
    public class RoomPlan : IRoomPlan
    {
        public int AdultCount
        {
            get;
            set;
        }

        public int ChildCount
        {
           get
           {
               return ChildAgeList.Count();
           }
        }

        public List<int> ChildAgeList = new List<int>();
        public IEnumerable<int> ChildAges
        {
            get { return ChildAgeList; }
        }

        public int RoomRef
        {
            get;
            set;
        }
    }
}
