﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IHotelSegmentSearchProvider
    {
        IPropertyProduct Search(Query.IHotelSegmentQuery query);
        
    }
}
