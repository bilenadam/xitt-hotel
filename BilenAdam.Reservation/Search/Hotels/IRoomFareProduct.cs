﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IRoomFareProduct
    {
        decimal AvgRate { get; }
        decimal TotalRoomRate { get; }
        Dictionary<DateTime, decimal> RatesPerNight { get; }
    }
}
