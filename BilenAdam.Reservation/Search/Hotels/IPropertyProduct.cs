﻿using BilenAdam.Reservation.Elements;
using BilenAdam.Reservation.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BilenAdam.Reservation.Search.Hotels
{
    public interface IPropertyProduct : IFareProduct
    {

        string ProductId { get; }

        AvailabilityStatus AvailabilityStatus { get; }
        DateTime CheckIn { get; }
        DateTime CheckOut { get; }
        int Nights { get;  }

        string SpecialDeal { get; }
        
        IEnumerable<IRoomProduct> Rooms { get; }
        
        int HotelId { get; }

        bool IsTransferIncluded();
    }
}
