﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels.Promotions
{
    public interface IDiscount
    {
        BilenAdam.Reservation.AmountType GetDiscountType();

        decimal Apply(decimal amount);
        decimal GetDiscountRate();
    }
}
