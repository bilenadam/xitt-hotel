﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Search.Hotels.Promotions
{
    public interface IHotelPromotion
    {
        IDiscount GetDiscount();

        BilenAdam.Reservation.Hotels.Promotions.PromotionType GetPromotionType();
    }
}
