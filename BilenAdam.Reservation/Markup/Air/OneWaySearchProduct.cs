﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Search.Air;

namespace BilenAdam.Reservation.Markup.Air
{
    public class OneWaySearchProduct<T> : AirSearchProduct<T> , IOneWaySearchProduct where T : IOneWaySearchProduct
    {

        public OneWaySearchProduct(T product, IMarkUp markup) : base(product, markup)
        {
        }



        public int SegmentIndex => OriginalProduct.SegmentIndex;

        public int Stops =>  OriginalProduct.Stops;

        public DateTime Departure => OriginalProduct.Departure;

        public DateTime Arrival => OriginalProduct.Arrival;

        public string Carrier => OriginalProduct.Carrier;

        public string Origin =>   OriginalProduct.Origin;

        public string Destination => OriginalProduct.Destination;


     

        public IEnumerable<IFlightProduct> GetFlightProducts() => OriginalProduct.GetFlightProducts();
    }
}
