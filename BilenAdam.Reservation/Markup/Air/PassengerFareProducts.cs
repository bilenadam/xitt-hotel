﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Air;

namespace BilenAdam.Reservation.Markup.Air
{
    public class PassengerFareProducts : Search.Air.PassengerFareProducts
    {
        public IMarkUp Markup { get; }
        public PassengerFareProducts(IMarkUp markup)
        {
            this.Markup = markup;
        }

        public override void Add(IPassengerFareProduct product)
        {
            base.Add(Markup.Apply(product));
        }

        public PassengerFareProducts(IMarkUp markup, IPassengerFareProducts product)
        {
            this.Markup = markup;
            foreach (var p in product.GetFareBreakdown())
            {
                Add(p);
            }
        }

    }
}
