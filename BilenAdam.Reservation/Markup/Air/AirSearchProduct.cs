﻿using BilenAdam.Reservation.Search.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Markup.Air
{
    public class AirSearchProduct<T> : IAirSearchProduct where T : IAirSearchProduct
    {
        public T OriginalProduct { get; }


        public PassengerFareProducts FareProducts { get; }

        public int SegmentCount => OriginalProduct.SegmentCount;

        public decimal TotalFare => FareProducts.TotalFare;

        public AirSearchProduct(T product, IMarkUp markup)
        {
            OriginalProduct = product;
            FareProducts = new PassengerFareProducts(markup, product.GetPassengerFareProducts());
        }

        public IPassengerFareProducts GetPassengerFareProducts() => FareProducts;

        public IEnumerable<ISegmentProduct> GetSegmentProducts() => OriginalProduct.GetSegmentProducts();
    }
}