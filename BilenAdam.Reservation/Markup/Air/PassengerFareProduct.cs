﻿using BilenAdam.Reservation.Search.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Air.Fare;

namespace BilenAdam.Reservation.Markup.Air
{
    public class PassengerFareProduct : Search.Air.PassengerFareProduct
    {


        public PassengerFareProduct(IPassengerFareProduct orginalProduct, string code, decimal amount) : base(orginalProduct)
        {
            AddMarkup(amount, code);
        }

        

        public IFareAmount MarkupFareAmount => base.fareBuilder.FareAmount;
       

        
    }
}
