﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Air;
using BilenAdam.Reservation.Air.Fare;
using BilenAdam.Reservation.Air;

namespace BilenAdam.Reservation.Markup.Air
{
    public class ServiceFeeMarkup : IMarkUp
    {
        readonly string code;
        readonly decimal amount;

        public ServiceFeeMarkup(string code, decimal amount)
        {
            this.code = code;
            this.amount = amount;
        }


        public IPassengerFareProduct Apply(IPassengerFareProduct passengerFareProduct)
        {
            if (passengerFareProduct.PassengerType == Reservation.Air.PassengerType.Infant)
                return passengerFareProduct;
            else
                return new PassengerFareProduct(passengerFareProduct, code, amount);
        }

        public IFareAmount GetFareAmount(PassengerType passengerType)
        {
            if (passengerType == Reservation.Air.PassengerType.Infant)
            {
                return null;
            }
            else
            {
                return new FareAmount(FareAmountType.SF, code, amount);
            }
        }
    }
}
