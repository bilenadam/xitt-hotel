﻿using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Search.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Markup.Air
{
    public class DirectOneWaySearchProduct<T> : OneWaySearchProduct<T>, IDirectOneWaySearchProduct where T : IDirectOneWaySearchProduct
    {
        public DirectOneWaySearchProduct(T product, IMarkUp markup) : base(product, markup)
        {

        }
        public string BookingClass => OriginalProduct.BookingClass;

        public CabinType BookingCabin => OriginalProduct.BookingCabin;

        public int? AvailableSeats => OriginalProduct.AvailableSeats;

        public short FlightNo => OriginalProduct.FlightNo;

        public string Via => OriginalProduct.Via;
    }
}
