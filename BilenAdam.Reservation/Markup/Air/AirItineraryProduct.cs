﻿using BilenAdam.Reservation.Search.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Air;

namespace BilenAdam.Reservation.Markup.Air
{
    public class AirItineraryProduct : INestedItineraryProduct
    {
        public IAirItineraryProduct InnerProduct { get; }
        public PassengerFareProducts FareProducts { get; }

        public int SegmentCount => InnerProduct.SegmentCount;

        public decimal TotalFare => FareProducts.TotalFare;

        public AirItineraryProduct(IAirItineraryProduct product, IMarkUp markup)
        {
            this.InnerProduct = product;
            this.FareProducts = new PassengerFareProducts(markup, product.GetPassengerFareProducts());
        }









      

        public static AirItineraryProduct Create(IAirItineraryProduct product, IMarkUp markup) => product != null ? new AirItineraryProduct(product, markup) : null;

        public IAirItineraryProduct Combine(IAirItineraryProduct product)
        {
            if (product == null)
                return null;

            if(product is INestedItineraryProduct)
            {
                var p = product as INestedItineraryProduct;
                return Nest(p.Nest(this.InnerProduct));
            }
            else
                return Create(InnerProduct.Combine(product), this.FareProducts.Markup);
        }

        public IEnumerable<ISegmentProduct> GetSegmentProducts() => InnerProduct.GetSegmentProducts();

        public IPassengerFareProducts GetPassengerFareProducts() => FareProducts;

        public IAirItineraryProduct GetInnerProduct() => InnerProduct;

        public IAirItineraryProduct Nest(IAirItineraryProduct product) => Create(InnerProduct.Combine(product), FareProducts.Markup);
    }
}
