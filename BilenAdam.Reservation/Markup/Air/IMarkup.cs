﻿using BilenAdam.Reservation.Air;
using BilenAdam.Reservation.Air.Fare;
using BilenAdam.Reservation.Search.Air;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Reservation.Markup.Air
{
    public interface IMarkUp
    {
        IPassengerFareProduct Apply(IPassengerFareProduct passengerFareProduct);

        IFareAmount GetFareAmount(PassengerType passengerType);
        
    }
}
