﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public class HotelPromotionHelper
    {
        IEnumerable<HotelPromotion> promotions;
        public HotelPromotionHelper(IEnumerable<HotelPromotion> promotions)
        {
            this.promotions = promotions;
        }


        public HotelPromotion GetDayPromotion(int roomTypeId, DateTime checkIn, int stay)
        {
            return GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType.Day, roomTypeId, checkIn, checkIn,stay);
          
        }
        public HotelPromotion[] FilterPromotions(int roomTypeId, DateTime date, DateTime checkIn, int stay)
        {
            List<HotelPromotion> applyList = new List<HotelPromotion>();

            var earlyBookingPromotion = GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType.EarlyBooking, roomTypeId, date, checkIn, stay);

            List<int> IdList = new List<int>();


            if (earlyBookingPromotion != null)
            {
                applyList.Add(earlyBookingPromotion);
                IdList.Add(earlyBookingPromotion.Id);
            }

            var turboEarlyBookingPromotion = GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType.TurboEarlyBooking, roomTypeId, date, checkIn, stay);
            if (turboEarlyBookingPromotion != null)
            {
                if (CheckCombination(turboEarlyBookingPromotion, IdList))
                {
                    applyList.Add(turboEarlyBookingPromotion);
                    IdList.Add(turboEarlyBookingPromotion.Id);
                }
            }


            var rollingEarlyBookingPromotion = GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType.RollingEarlyBooking, roomTypeId, date, checkIn, stay);
            if(rollingEarlyBookingPromotion != null)
            {
                if(CheckCombination(rollingEarlyBookingPromotion,IdList))
                {
                    applyList.Add(rollingEarlyBookingPromotion);
                    IdList.Add(rollingEarlyBookingPromotion.Id);
                }
            }

            var longStayPromotion = GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType.LongStay, roomTypeId, date, checkIn, stay);
            if (longStayPromotion != null)
            {
                if (CheckCombination(longStayPromotion, IdList))
                {
                    applyList.Add(longStayPromotion);
                    IdList.Add(longStayPromotion.Id);
                }
            }


         

            // ignore day promotions at day level

            //var dayPromotion = GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType.Day, roomTypeId, date, checkIn);
            //if(dayPromotion != null && CheckCombination(dayPromotion,IdList))
            //{
            //    applyList.Add(dayPromotion);
            //    IdList.Add(dayPromotion.Id);
            //}

            return applyList.ToArray();

        }

        public HotelPromotion GetPromotion(BilenAdam.Reservation.Hotels.Promotions.PromotionType promotionType, int roomTypeId, DateTime date, DateTime checkIn, int stay)
        {
            var filtered = promotions.Where(p => p.HotelPromotionType == (int)promotionType
            && p.BookingPeriodBegin <= DateTime.Today
            && p.BookingPeriodEnd >= DateTime.Today
            && ((p.ApplicationType == (int)BilenAdam.Reservation.Hotels.Promotions.ApplicationType.Stay && p.AccommodationPeriodBegin <= date && p.AccommodationPeriodEnd >= date)
                || (p.ApplicationType == (int)BilenAdam.Reservation.Hotels.Promotions.ApplicationType.Arrival && p.AccommodationPeriodBegin <= checkIn && p.AccommodationPeriodEnd >= checkIn))
            && (p.RoomTypeSpecific == false || p.HotelPromotionRoomTypes.Any(t => t.HotelRoomId == roomTypeId))
            && (p.MinStay == null || p.MinStay <= stay)

            );
            
            if(promotionType == Reservation.Hotels.Promotions.PromotionType.TurboEarlyBooking)
            {
                filtered = filtered.Where(p =>
                p.TurboBookingApplicationType == (int)Reservation.Hotels.Promotions.TEBPersonApplicationType.PersonDay ||
                (p.TurboBookingApplicationType == (int)Reservation.Hotels.Promotions.TEBPersonApplicationType.PersonWeek && (date.Subtract(checkIn).TotalDays % 7 == 0)) ||
                (p.TurboBookingApplicationType == (int)Reservation.Hotels.Promotions.TEBPersonApplicationType.Person && date == checkIn)
                );
            }
            
            return filtered.OrderByDescending(p => p.DiscountRate).FirstOrDefault();
        }

        public bool CheckCombination(HotelPromotion promotion, List<int> idlist)
        {
            if (idlist.Count == 0)
                return true;

            return idlist.All(i => promotion.CanCombine(i));
        }

    }
}
