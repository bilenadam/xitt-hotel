#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Linq.Expressions;


namespace BilenAdam.Data.HotelData
{	
	public partial class HotelCharterGuest 
	{
	
		
        
     

		 partial void Updating(HotelCharterGuest source,params string[] excludeFields);

         /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool UpdateFrom(HotelCharterGuest source,params string[] excludeFields)
         {
			Updating(source, excludeFields);

			bool isUpdated = false;

            if(excludeFields == null || !excludeFields.Contains("HotelCharterRoomId"))
			{
				if(this.HotelCharterRoomId != source.HotelCharterRoomId)
				{
					this.HotelCharterRoomId = source.HotelCharterRoomId;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("No"))
			{
				if(this.No != source.No)
				{
					this.No = source.No;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("IsAdult"))
			{
				if(this.IsAdult != source.IsAdult)
				{
					this.IsAdult = source.IsAdult;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("FirstName"))
			{
				if(this.FirstName != source.FirstName)
				{
					this.FirstName = source.FirstName;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("LastName"))
			{
				if(this.LastName != source.LastName)
				{
					this.LastName = source.LastName;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("Dob"))
			{
				if(this.Dob != source.Dob)
				{
					this.Dob = source.Dob;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("IsMale"))
			{
				if(this.IsMale != source.IsMale)
				{
					this.IsMale = source.IsMale;
					isUpdated = true;
				}
		    }

			return isUpdated;
         }

		  /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool CheckUpdate(HotelCharterGuest source,params string[] excludeFields)
         {
			
			bool checkUpdate = false;

            if(excludeFields == null || !excludeFields.Contains("HotelCharterRoomId"))
			{
				if(this.HotelCharterRoomId != source.HotelCharterRoomId)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("No"))
			{
				if(this.No != source.No)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("IsAdult"))
			{
				if(this.IsAdult != source.IsAdult)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("FirstName"))
			{
				if(this.FirstName != source.FirstName)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("LastName"))
			{
				if(this.LastName != source.LastName)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("Dob"))
			{
				if(this.Dob != source.Dob)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("IsMale"))
			{
				if(this.IsMale != source.IsMale)
				{
					checkUpdate = true;
				}
		    }

			return checkUpdate;
         }

		
    }

	public static class HotelCharterGuestUpsertExtension
    {
       

		public static UpsertResult<HotelCharterGuest> Upsert(this Table<HotelCharterGuest> table, Expression<Func<HotelCharterGuest, bool>> predicate, HotelCharterGuest source, params string[] excludeFields)
        {
            UpsertResult<HotelCharterGuest> result = new UpsertResult<HotelCharterGuest>();
            var e = table.FirstOrDefault(predicate);
            if(e == null)
            {
               
                table.InsertOnSubmit(source);
                result.Status = UpsertStatus.Inserted;
                result.Result = source;
            }
            else
            {
                if(e.UpdateFrom(source,excludeFields))
                {
                    result.Status = UpsertStatus.Updated;
                }
                else
                {
                    result.Status = UpsertStatus.NoChange;
                }
				 result.Result = e;
            }

           
            return result;
        }
    }

}// End 
