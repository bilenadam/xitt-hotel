#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Linq.Expressions;


namespace BilenAdam.Data.HotelData
{	
	public partial class HotelLog 
	{
	
		
        
     

		 partial void Updating(HotelLog source,params string[] excludeFields);

         /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool UpdateFrom(HotelLog source,params string[] excludeFields)
         {
			Updating(source, excludeFields);

			bool isUpdated = false;

            if(excludeFields == null || !excludeFields.Contains("TableName"))
			{
				if(this.TableName != source.TableName)
				{
					this.TableName = source.TableName;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("RowId"))
			{
				if(this.RowId != source.RowId)
				{
					this.RowId = source.RowId;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("ChangeType"))
			{
				if(this.ChangeType != source.ChangeType)
				{
					this.ChangeType = source.ChangeType;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("ChangedBy"))
			{
				if(this.ChangedBy != source.ChangedBy)
				{
					this.ChangedBy = source.ChangedBy;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("ChangedAt"))
			{
				if(this.ChangedAt != source.ChangedAt)
				{
					this.ChangedAt = source.ChangedAt;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("RowData"))
			{
				if(this.RowData != source.RowData)
				{
					this.RowData = source.RowData;
					isUpdated = true;
				}
		    }

			return isUpdated;
         }

		  /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool CheckUpdate(HotelLog source,params string[] excludeFields)
         {
			
			bool checkUpdate = false;

            if(excludeFields == null || !excludeFields.Contains("TableName"))
			{
				if(this.TableName != source.TableName)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("RowId"))
			{
				if(this.RowId != source.RowId)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("ChangeType"))
			{
				if(this.ChangeType != source.ChangeType)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("ChangedBy"))
			{
				if(this.ChangedBy != source.ChangedBy)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("ChangedAt"))
			{
				if(this.ChangedAt != source.ChangedAt)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("RowData"))
			{
				if(this.RowData != source.RowData)
				{
					checkUpdate = true;
				}
		    }

			return checkUpdate;
         }

		
    }

	public static class HotelLogUpsertExtension
    {
       

		public static UpsertResult<HotelLog> Upsert(this Table<HotelLog> table, Expression<Func<HotelLog, bool>> predicate, HotelLog source, params string[] excludeFields)
        {
            UpsertResult<HotelLog> result = new UpsertResult<HotelLog>();
            var e = table.FirstOrDefault(predicate);
            if(e == null)
            {
               
                table.InsertOnSubmit(source);
                result.Status = UpsertStatus.Inserted;
                result.Result = source;
            }
            else
            {
                if(e.UpdateFrom(source,excludeFields))
                {
                    result.Status = UpsertStatus.Updated;
                }
                else
                {
                    result.Status = UpsertStatus.NoChange;
                }
				 result.Result = e;
            }

           
            return result;
        }
    }

}// End 
