#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Linq.Expressions;


namespace BilenAdam.Data.HotelData
{	
	public partial class HotelSeasonChildrenFee 
	{
	
		
        
     

		 partial void Updating(HotelSeasonChildrenFee source,params string[] excludeFields);

         /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool UpdateFrom(HotelSeasonChildrenFee source,params string[] excludeFields)
         {
			Updating(source, excludeFields);

			bool isUpdated = false;

            if(excludeFields == null || !excludeFields.Contains("HotelSeasonId"))
			{
				if(this.HotelSeasonId != source.HotelSeasonId)
				{
					this.HotelSeasonId = source.HotelSeasonId;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("MaxAge"))
			{
				if(this.MaxAge != source.MaxAge)
				{
					this.MaxAge = source.MaxAge;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("HandlingFee"))
			{
				if(this.HandlingFee != source.HandlingFee)
				{
					this.HandlingFee = source.HandlingFee;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("TransferFee"))
			{
				if(this.TransferFee != source.TransferFee)
				{
					this.TransferFee = source.TransferFee;
					isUpdated = true;
				}
		    }

			return isUpdated;
         }

		  /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool CheckUpdate(HotelSeasonChildrenFee source,params string[] excludeFields)
         {
			
			bool checkUpdate = false;

            if(excludeFields == null || !excludeFields.Contains("HotelSeasonId"))
			{
				if(this.HotelSeasonId != source.HotelSeasonId)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("MaxAge"))
			{
				if(this.MaxAge != source.MaxAge)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("HandlingFee"))
			{
				if(this.HandlingFee != source.HandlingFee)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("TransferFee"))
			{
				if(this.TransferFee != source.TransferFee)
				{
					checkUpdate = true;
				}
		    }

			return checkUpdate;
         }

		
    }

	public static class HotelSeasonChildrenFeeUpsertExtension
    {
       

		public static UpsertResult<HotelSeasonChildrenFee> Upsert(this Table<HotelSeasonChildrenFee> table, Expression<Func<HotelSeasonChildrenFee, bool>> predicate, HotelSeasonChildrenFee source, params string[] excludeFields)
        {
            UpsertResult<HotelSeasonChildrenFee> result = new UpsertResult<HotelSeasonChildrenFee>();
            var e = table.FirstOrDefault(predicate);
            if(e == null)
            {
               
                table.InsertOnSubmit(source);
                result.Status = UpsertStatus.Inserted;
                result.Result = source;
            }
            else
            {
                if(e.UpdateFrom(source,excludeFields))
                {
                    result.Status = UpsertStatus.Updated;
                }
                else
                {
                    result.Status = UpsertStatus.NoChange;
                }
				 result.Result = e;
            }

           
            return result;
        }
    }

}// End 
