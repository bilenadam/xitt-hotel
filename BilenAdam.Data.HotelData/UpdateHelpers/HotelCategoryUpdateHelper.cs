#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Linq.Expressions;


namespace BilenAdam.Data.HotelData
{	
	public partial class HotelCategory 
	{
	
		
        
     

		 partial void Updating(HotelCategory source,params string[] excludeFields);

         /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool UpdateFrom(HotelCategory source,params string[] excludeFields)
         {
			Updating(source, excludeFields);

			bool isUpdated = false;

            if(excludeFields == null || !excludeFields.Contains("Name"))
			{
				if(this.Name != source.Name)
				{
					this.Name = source.Name;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("xRes"))
			{
				if(this.xRes != source.xRes)
				{
					this.xRes = source.xRes;
					isUpdated = true;
				}
		    }

			return isUpdated;
         }

		  /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool CheckUpdate(HotelCategory source,params string[] excludeFields)
         {
			
			bool checkUpdate = false;

            if(excludeFields == null || !excludeFields.Contains("Name"))
			{
				if(this.Name != source.Name)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("xRes"))
			{
				if(this.xRes != source.xRes)
				{
					checkUpdate = true;
				}
		    }

			return checkUpdate;
         }

		
    }

	public static class HotelCategoryUpsertExtension
    {
       

		public static UpsertResult<HotelCategory> Upsert(this Table<HotelCategory> table, Expression<Func<HotelCategory, bool>> predicate, HotelCategory source, params string[] excludeFields)
        {
            UpsertResult<HotelCategory> result = new UpsertResult<HotelCategory>();
            var e = table.FirstOrDefault(predicate);
            if(e == null)
            {
               
                table.InsertOnSubmit(source);
                result.Status = UpsertStatus.Inserted;
                result.Result = source;
            }
            else
            {
                if(e.UpdateFrom(source,excludeFields))
                {
                    result.Status = UpsertStatus.Updated;
                }
                else
                {
                    result.Status = UpsertStatus.NoChange;
                }
				 result.Result = e;
            }

           
            return result;
        }
    }

}// End 
