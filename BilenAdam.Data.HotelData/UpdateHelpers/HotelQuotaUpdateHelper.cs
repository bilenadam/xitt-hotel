#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Linq.Expressions;


namespace BilenAdam.Data.HotelData
{	
	public partial class HotelQuota 
	{
	
		
        
     

		 partial void Updating(HotelQuota source,params string[] excludeFields);

         /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool UpdateFrom(HotelQuota source,params string[] excludeFields)
         {
			Updating(source, excludeFields);

			bool isUpdated = false;

            if(excludeFields == null || !excludeFields.Contains("RoomId"))
			{
				if(this.RoomId != source.RoomId)
				{
					this.RoomId = source.RoomId;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("MarketId"))
			{
				if(this.MarketId != source.MarketId)
				{
					this.MarketId = source.MarketId;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("QuotaDate"))
			{
				if(this.QuotaDate != source.QuotaDate)
				{
					this.QuotaDate = source.QuotaDate;
					isUpdated = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("Amount"))
			{
				if(this.Amount != source.Amount)
				{
					this.Amount = source.Amount;
					isUpdated = true;
				}
		    }

			return isUpdated;
         }

		  /// <summary>
         /// do not updates primary key fields and db generated fields
         /// </summary>
         /// <param name="source"></param>
         public bool CheckUpdate(HotelQuota source,params string[] excludeFields)
         {
			
			bool checkUpdate = false;

            if(excludeFields == null || !excludeFields.Contains("RoomId"))
			{
				if(this.RoomId != source.RoomId)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("MarketId"))
			{
				if(this.MarketId != source.MarketId)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("QuotaDate"))
			{
				if(this.QuotaDate != source.QuotaDate)
				{
					checkUpdate = true;
				}
		    }
            if(excludeFields == null || !excludeFields.Contains("Amount"))
			{
				if(this.Amount != source.Amount)
				{
					checkUpdate = true;
				}
		    }

			return checkUpdate;
         }

		
    }

	public static class HotelQuotaUpsertExtension
    {
       

		public static UpsertResult<HotelQuota> Upsert(this Table<HotelQuota> table, Expression<Func<HotelQuota, bool>> predicate, HotelQuota source, params string[] excludeFields)
        {
            UpsertResult<HotelQuota> result = new UpsertResult<HotelQuota>();
            var e = table.FirstOrDefault(predicate);
            if(e == null)
            {
               
                table.InsertOnSubmit(source);
                result.Status = UpsertStatus.Inserted;
                result.Result = source;
            }
            else
            {
                if(e.UpdateFrom(source,excludeFields))
                {
                    result.Status = UpsertStatus.Updated;
                }
                else
                {
                    result.Status = UpsertStatus.NoChange;
                }
				 result.Result = e;
            }

           
            return result;
        }
    }

}// End 
