﻿using BilenAdam.Data.Tracking;
using BilenAdam.Log;
using BilenAdam.Log.FileSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace BilenAdam.Data.HotelData
{
    public class HotelCharterLogUtil
    {
        static string logFolder = "HotelCharterDB_Logs";
        public string Save(EntityChange change, string user)
        {
            var logPath = new LogPathUtil(".xml").AddFolder(logFolder).AddFolder(change.TableName).AddFolder(change.Id.ToString()).AppendTodayName().AppendNowName(format: "HHmm").AppendName(change.ChangeType.ToString()).AppendName(user).Create();
         

            new FileLog().SaveXml(logPath, change.Data.ToString());
            return logPath;
        }

        public string Save(HotelLog log)
        {
            var changeType = ((ChangeType)log.ChangeType);
            var logPath = new LogPathUtil(".xml").AddFolder(logFolder).AddFolder(log.TableName).AddFolder(log.Id.ToString()).AppendDateTimeName(log.ChangedAt, "yyMMdd").AppendDateTimeName(log.ChangedAt, "HHmm").AppendName(changeType.ToString()).AppendName(log.ChangedBy).Create();
         

            new FileLog().SaveXml(logPath, log.RowData.ToString());
            return logPath;
        }

        public string GetFolder(string tableName, int rowid)
        {
            return System.IO.Path.Combine(new AppLogSettings().GetAppLogFolder(), logFolder, tableName, rowid.ToString());
        }


    }
}
