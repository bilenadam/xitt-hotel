﻿using BilenAdam.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData.Cache
{
    public class HotelDestinationCache
    {
        static TimeCache<int, HotelDestination> cache;
        static HotelDestinationCache()
        {
            HotelDestination[] data = null;
            using (var db = HotelDataContextFactory.Create())
            {
                data = db.HotelDestinations.Where(d => d.Hotels.Any()).ToArray();
            }

            if (data != null && data.Length > 0)
            {
                cache = new TimeCache<int, HotelDestination>(data.Select(i => new KeyValuePair<int, HotelDestination>(i.Id, i)), DateTime.Today.AddDays(1));
            }
            else
            {
                cache = new TimeCache<int, HotelDestination>();
            }
        }

        private HotelDestination getData(int id)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return db.HotelDestinations.FirstOrDefault(i => i.Id == id);
            }
        }

        public HotelDestination Get(int id)
        {
            return cache.Get(id, i => getData(i), DateTime.Today.AddDays(1));
        }
    }
}
