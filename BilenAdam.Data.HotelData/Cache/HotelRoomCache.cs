﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Cache;

namespace BilenAdam.Data.HotelData.Cache
{
    public class HotelRoomCache
    {
        static TimeCache<int, HotelRoom> cache;

        static HotelRoomCache()
        {

            HotelRoom[] data = null;
            using (var db = HotelDataContextFactory.Create())
            {
                db.LoadOptions = createLoadOptions();
                data = db.HotelRooms.Where(h => h.Hotel.IsActive).ToArray();
            }
            if (data != null && data.Length > 0)
            {
                cache = new TimeCache<int, HotelRoom>(data.Select(c => new KeyValuePair<int, HotelRoom>(c.Id, c)), DateTime.Today.AddDays(1));
            }
            else
            {
                cache = new TimeCache<int, HotelRoom>();
            }

        }


        public HotelRoom Get(int id)
        {
            return cache.Get(id, i => getData(i), DateTime.Today.AddDays(1));
        }

        private HotelRoom getData(int id)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                db.LoadOptions = createLoadOptions();
                return db.HotelRooms.FirstOrDefault(i => i.Id == id);
            }
        }
        private static DataLoadOptions createLoadOptions()
        {
            DataLoadOptions dataLoadOptions = new DataLoadOptions();
            dataLoadOptions.LoadWith<HotelRoom>(h => h.HotelRoomAmenityPairs);
            dataLoadOptions.LoadWith<HotelRoomAmenityPair>(h => h.HotelAmenity);
            return dataLoadOptions;
        }
    }
}
