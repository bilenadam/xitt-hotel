﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Cache;

namespace BilenAdam.Data.HotelData.Cache
{
    public  class HotelCache
    {
        static TimeCache<int, Hotel> cache;

        static HotelCache()
        {

            Hotel[] data = null;
            using (var db = HotelDataContextFactory.Create())
            {
                db.LoadOptions = createLoadOptions();
                data = db.Hotels.Where(h => h.IsActive).ToArray();
            }
            if (data != null && data.Length > 0)
            {
                cache = new TimeCache<int, Hotel>(data.Select(c => new KeyValuePair<int, Hotel>(c.Id, c)), DateTime.Now.AddHours(1));
            }
            else
            {
                cache = new TimeCache<int, Hotel>();
            }

        }

        public static void CreateCache()
        {

        }


        public Hotel Get(int id)
        {
            return cache.Get(id, i => getData(i), DateTime.Now.AddHours(1));
        }

        private Hotel getData(int id)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                db.LoadOptions = createLoadOptions();
                return db.Hotels.FirstOrDefault(i => i.Id == id);
            }
        }
        private static DataLoadOptions createLoadOptions()
        {
            DataLoadOptions dataLoadOptions = new DataLoadOptions();
            dataLoadOptions.LoadWith<Hotel>(h => h.HotelDestination);
            dataLoadOptions.LoadWith<Hotel>(h => h.HotelImages);
            dataLoadOptions.LoadWith<Hotel>(h => h.HotelSeasons);
            dataLoadOptions.LoadWith<Hotel>(h => h.HotelAmenityPairs);
            dataLoadOptions.LoadWith<HotelAmenityPair>(h => h.HotelAmenity);
            return dataLoadOptions;
        }


        public Hotel Get(string pnr)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                var rezId = PnrHelper.ParseHotelCharterPnr(pnr);
                var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == rezId);
                if (rez == null)
                    return null;

                return Get(rez.HotelId);
            }
        }
    }
}
