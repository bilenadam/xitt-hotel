﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Cache;

namespace BilenAdam.Data.HotelData.Cache
{
    public class HotelPensionCache
    {
        static TimeCache<int, HotelPension> cache;

        static HotelPensionCache()
        {

            HotelPension[] data = null;
            using (var db = HotelDataContextFactory.Create())
            {
                data = db.HotelPensions.ToArray();
            }
            if (data != null && data.Length > 0)
            {
                cache = new TimeCache<int, HotelPension>(data.Select(c => new KeyValuePair<int, HotelPension>(c.Id, c)), DateTime.Today.AddDays(1));
            }
            else
            {
                cache = new TimeCache<int, HotelPension>();
            }

        }


        public HotelPension Get(int id)
        {
            return cache.Get(id, i => getData(i), DateTime.Today.AddDays(1));
        }

        private HotelPension getData(int id)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return db.HotelPensions.FirstOrDefault(i => i.Id == id);
            }
        }

        public IEnumerable<HotelPension> GetAll()
        {
            return cache.GetAll();
        }
      
    }
}
