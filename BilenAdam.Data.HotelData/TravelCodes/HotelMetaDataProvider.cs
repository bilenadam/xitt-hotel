﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.TravelCode.Hotel;

namespace BilenAdam.Data.HotelData.TravelCodes
{
    public class HotelMetaDataProvider : IHotelMetaDataProvider
    {
        public IHotelMetaData GetHotel(int Id) => new Cache.HotelCache().Get(Id);

        public IHotelDestination GetHotelDestination(int Id) => new Cache.HotelDestinationCache().Get(Id);
    }
}
