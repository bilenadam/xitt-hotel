﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Cache;

namespace BilenAdam.Data.HotelData.TravelCodes
{
    public  class DestIataCodeUtil
    {
        static TimeCache<int, string> cache = new TimeCache<int, string>();

        public  string GetIataCode(int id)
        {
            return cache.Get(id, i => getCode(i), DateTime.Today.AddDays(1));
        }

        
        private  string getCode(int i)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return db.GetIataCode(i);
            }
        }
    }
}
