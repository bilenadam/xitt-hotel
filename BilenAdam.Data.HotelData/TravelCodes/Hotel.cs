﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.TravelCode.Hotel;

namespace BilenAdam.Data.HotelData
{
    public partial class Hotel : IHotelAddress, ILatLong, IHotelContactInfo, IHotelMetaData
    {
        public int HotelDestinationId
        {
            get
            {
                return HotelDestionationId;
            }
        }

        public IHotelContactInfo HotelContactInfo => this;

        public ILatLong LatLong => this;


        IHotelAddress IHotelMetaData.Address => this;
    }
}
