﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public class HotelDataContextFactory
    {
        static string connectionString = new ApplicationSettings().GetConnectionString("HotelDataConnectionString");

        public static HotelDataContext Create()
        {
            if (!string.IsNullOrWhiteSpace(connectionString))
                return new HotelDataContext(connectionString);
            else
                return null;
        }

        public static HotelDataContext Create(DataLoadOptions loadOptions)
        {
            var db = Create();
            if (loadOptions != null)
                db.LoadOptions = loadOptions;
            return db;
        }
    }
}
