#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using System.Linq.Expressions;
using BilenAdam.Data.Tracking;


namespace BilenAdam.Data.HotelData
{	
	public  partial class HotelPromotion : ITrackable
	{
	
         public EntityChange GetChange()
         {

			EntityChange change = new EntityChange(){  TableName = "HotelPromotion",Id = this.Id,  Data = new XElement("HotelPromotion") };
			 
						   change.Data.Add(new XElement("Id", this.Id));
						   change.Data.Add(new XElement("HotelId", this.HotelId));
						   change.Data.Add(new XElement("MarketId", this.MarketId));
						   change.Data.Add(new XElement("HotelPromotionType", this.HotelPromotionType));
						   change.Data.Add(new XElement("ApplicationType", this.ApplicationType));
						   change.Data.Add(new XElement("BookingPeriodBegin", this.BookingPeriodBegin));
						   change.Data.Add(new XElement("BookingPeriodEnd", this.BookingPeriodEnd));
						   change.Data.Add(new XElement("AccommodationPeriodBegin", this.AccommodationPeriodBegin));
						   change.Data.Add(new XElement("AccommodationPeriodEnd", this.AccommodationPeriodEnd));
						   change.Data.Add(new XElement("DiscountType", this.DiscountType));
						   change.Data.Add(new XElement("DiscountRate", this.DiscountRate));
						   change.Data.Add(new XElement("RoomTypeSpecific", this.RoomTypeSpecific));
						   change.Data.Add(new XElement("PaymentDate", this.PaymentDate));
						   change.Data.Add(new XElement("PaymentRate", this.PaymentRate));
						   change.Data.Add(new XElement("MinStay", this.MinStay));
						   change.Data.Add(new XElement("MaxStay", this.MaxStay));
						   change.Data.Add(new XElement("MinAge", this.MinAge));
						   change.Data.Add(new XElement("HotelSeasonId", this.HotelSeasonId));
						   change.Data.Add(new XElement("ChildDiscount", this.ChildDiscount));
						   change.Data.Add(new XElement("TurboBookingApplicationType", this.TurboBookingApplicationType));
						   change.Data.Add(new XElement("DayPromotionRules", this.DayPromotionRules));
			
			return change;
         }

		

		
    }

	

}// End 
