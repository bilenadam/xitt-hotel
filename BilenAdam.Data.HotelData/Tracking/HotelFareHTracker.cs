#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.Xml.Linq;
using System.Linq.Expressions;
using BilenAdam.Data.Tracking;


namespace BilenAdam.Data.HotelData
{	
	public  partial class HotelFareH : ITrackable
	{
	
         public EntityChange GetChange()
         {

			EntityChange change = new EntityChange(){  TableName = "HotelFareH",Id = this.Id,  Data = new XElement("HotelFareH") };
			 
						   change.Data.Add(new XElement("Id", this.Id));
						   change.Data.Add(new XElement("BeginDate", this.BeginDate));
						   change.Data.Add(new XElement("EndDate", this.EndDate));
						   change.Data.Add(new XElement("MarketId", this.MarketId));
						   change.Data.Add(new XElement("HotelId", this.HotelId));
						   change.Data.Add(new XElement("Currency", this.Currency));
						   change.Data.Add(new XElement("HotelSeasonId", this.HotelSeasonId));
						   change.Data.Add(new XElement("SeasonCode", this.SeasonCode));
						   change.Data.Add(new XElement("IsSPOEnabled", this.IsSPOEnabled));
						   change.Data.Add(new XElement("SPOSalesBeginDate", this.SPOSalesBeginDate));
						   change.Data.Add(new XElement("SPOSalesEndDate", this.SPOSalesEndDate));
						   change.Data.Add(new XElement("IsSPOArrival", this.IsSPOArrival));
			
			return change;
         }

		

		
    }

	

}// End 
