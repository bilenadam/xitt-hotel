﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelCharterRoomServiceFee
    {
        public BilenAdam.Reservation.Hotels.ServiceFeeTypes GetServiceFeeType()
        {
            return (Reservation.Hotels.ServiceFeeTypes)Enum.Parse(typeof(BilenAdam.Reservation.Hotels.ServiceFeeTypes), Code);
        }

        public decimal ApplyTo(decimal total)
        {
            if (AmountType == 1) // fixed
            {
                total += Amount;
            }
            else
            {
                
                if (Amount >= 0 & Amount < 1)
                    total = total / (1 - Amount);
                else
                    total += (total * Amount);
            }

            return total;
        }

       
    }
}
