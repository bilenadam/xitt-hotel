﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelDestinationShowcase : BilenAdam.Data.Resources.IXResourceContainer
    {
        public string GetLocalizedSummaryText()
        {
            return this.GetPropertyResouce(s => s.SummaryText);
        }

        public string GetLocalizedFullText()
        {
            return this.GetPropertyResouce(s => s.FullText);
        }
    }
}
