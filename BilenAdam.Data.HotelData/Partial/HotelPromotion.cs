﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelPromotion
    {
        public bool CanCombine(int promotionId)
        {
            if (this.HotelPromotionCombinations.Any(c => c.HotelPromotionId2 == promotionId))
                return true;

            if (this.HotelPromotionCombinations1.Any(c => c.HotelPromotionId1 == promotionId))
                return true;
            return false;
        }

        public BilenAdam.Reservation.Hotels.Promotions.PromotionType GetPromotionType()
        {
            return (BilenAdam.Reservation.Hotels.Promotions.PromotionType)this.HotelPromotionType;
        }
    }
}
