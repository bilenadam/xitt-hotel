﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Utilities.Base32;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelCharterReservation
    {
        string pnr;
        public string GetPNR()
        {
            if (pnr == null)
                pnr = PnrHelper.ToHotelCharterPnr(this.Id);

            return pnr;
        }

        public IEnumerable<HotelCharterRoom> GetRooms()
        {
            var orderedRoomList = this.HotelCharterRooms.OrderBy(r => r.RoomRef).ThenByDescending(r => r.Id).ToArray();
            int lastRoomRef = -1;
            foreach (var room in orderedRoomList)
            {
                if (room.RoomRef != lastRoomRef)
                {
                    lastRoomRef = room.RoomRef;
                    yield return room;
                  
                }
            }
        }

        public decimal GetTotalRate()
        {
            decimal total = 0M;
            foreach (var room in GetRooms())
            {
                total += room.GetTotalRate();
            }
            return total;

        }

        public decimal GetTotal()
        {
            return GetTotalRate();
        }

       

        public DateTime GetCheckIn()
        {
            var room = this.HotelCharterRooms.Where(r => r.CancelTimeUTC == null).FirstOrDefault();
            if (room != null)
                return room.CheckIn;
            else
                return this.HotelCharterRooms.OrderByDescending(r=>r.CancelTimeUTC).First().CheckIn;
        }

        public DateTime GetCheckOut()
        {
            var room = this.HotelCharterRooms.Where(r => r.CancelTimeUTC == null).FirstOrDefault();
            if (room != null)
                return room.CheckOut;
            else
                return this.HotelCharterRooms.OrderByDescending(r => r.CancelTimeUTC).First().CheckOut;
        }

        public BilenAdam.Reservation.ReservationStatus GetStatus()
        {
            if (this.HotelCharterRooms.Any(r => r.CancelTimeUTC == null))
            {
                if (this.ConfirmedTimeUTC == null)
                    return BilenAdam.Reservation.ReservationStatus.OnRequest;
                else
                    return BilenAdam.Reservation.ReservationStatus.Confirmed;


            }
            else
            {
                return BilenAdam.Reservation.ReservationStatus.Canceled;
            }
        }

        public bool IsTransferIncluded()
        {
            return HotelCharterRooms.Any(r => r.HasTransferFee());
        }

        public decimal GetTotalTransferFee()
        {
            decimal total = 0M;
            foreach (var room in GetRooms())
            {
                total += room.GetTransferFee();
            }
            return total;
        }

        public decimal GetTotalHandlingFee()
        {
            decimal total = 0M;
            foreach (var room in GetRooms())
            {
                total += room.GetHandlingFee();
            }
            return total;
        }

     

    }
}
