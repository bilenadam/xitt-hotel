﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Utilities.Serialization;
using System.Data.Linq;
using BilenAdam.Crm.Profile;
using BilenAdam.Crm;

namespace BilenAdam.Data.HotelData
{
    public partial class Contact : IContact
    {
        public ContactType ContactType
        {
            get
            {
                return (Crm.ContactType)this.Type;
            }
        }

        public CompanyProfile GetCompanyProfile()
        {
            return GetProfile<CompanyProfile>();
        }

        public PersonProfile GetPersonProfile()
        {
            return GetProfile<PersonProfile>();
        }

        public T GetProfile<T>() where T : ContactProfile
        {

            T profile = null;

            try
            {
                if (this.Profile != null)
                    profile = this.Profile.DeserializeFromXml<T>();
            }
            catch
            {

            }

            if (profile == null)
                profile = Activator.CreateInstance<T>();

            profile.Id = this.Id;
            profile.IsActive = this.IsActive;
            profile.City = this.City;
            profile.Country = this.Country;

            if (profile is CompanyProfile)
            {
                var companyProfile = profile as CompanyProfile;
                companyProfile.CompanyType = this.ContactType;
                companyProfile.CompanyName = this.Name;
            }

            return profile;




        }

        public void SetProfile<T>(T profile) where T : ContactProfile
        {
            this.Type = (int)profile.ContactType;
            this.Name = profile.Name;
            this.City = profile.City;
            this.Country = profile.Country;
            this.Profile = profile.SerializeToXml<T>();
        }








    }

}
