﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelRoomStopSale
    {
        public bool IsMatch(int adultCount, int childCount)
        {
            return (Adults == null || Adults.Value == adultCount) && (Childs == null || Childs.Value == childCount);
        }
    }
}
