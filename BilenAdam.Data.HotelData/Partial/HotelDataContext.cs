﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.Tracking;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelDataContext
    {
     

        public void SaveChanges(string user)
        {
            var cs = this.GetChangeSet();
            this.SubmitChanges();
            var clist = new ChangeSetTracker().Track(cs);
            try
            {
                foreach (var c in clist)
                {
                   // var filePath = new HotelCharterLogUtil().Save(c, user);
                    this.HotelLogs.InsertOnSubmit(new HotelLog { ChangedAt = DateTime.UtcNow, ChangeType = (int)c.ChangeType, RowId = c.Id, TableName = c.TableName, ChangedBy = user,  RowData = c.Data });

                }
                this.SubmitChanges();
            }
            catch
            {

            }
        }
    }
}
