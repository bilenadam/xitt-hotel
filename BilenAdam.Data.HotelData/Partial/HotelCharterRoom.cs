﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelCharterRoom
    {
       
        public decimal GetTotalRate()
        {
            decimal total = this.HotelCharterRoomDateDetails.Sum(d => d.GetRate());

            foreach (var f in this.HotelCharterRoomServiceFees.OrderBy(s => s.ApplyOrder))
            {
                total = f.ApplyTo(total);
            }



            return total;
        }

      

      

     


        public bool HasTransferFee()
        {
            return HotelCharterRoomServiceFees.Any(s => s.Code == BilenAdam.Reservation.Hotels.ServiceFeeTypes.TransferFee.ToString());
        }

        public decimal GetTransferFee()
        {
            var sf = HotelCharterRoomServiceFees.FirstOrDefault(s => s.Code == BilenAdam.Reservation.Hotels.ServiceFeeTypes.TransferFee.ToString());
            if (sf != null)
                return sf.Amount;
            else
                return 0M;
            
        }

        public decimal GetHandlingFee()
        {
            var sf = HotelCharterRoomServiceFees.FirstOrDefault(s => s.Code == BilenAdam.Reservation.Hotels.ServiceFeeTypes.HandlingFee.ToString());
            if (sf != null)
                return sf.Amount;
            else
                return 0M;

        }

        public void FullRefund()
        {
            this.CancelTimeUTC = DateTime.UtcNow;
            this.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
            {
                Amount = -1 * this.GetTotalRate(),
                AmountType = (int)BilenAdam.Reservation.AmountType.Fix,
                ApplyOrder = this.HotelCharterRoomServiceFees.Max(s => s.ApplyOrder) + 1,
                Code = BilenAdam.Reservation.Hotels.ServiceFeeTypes.Refund.ToString()
            });
        }

        public PriceBreakDown GetPriceBreakdown()
        {
            PriceBreakDown priceBreakDown = new PriceBreakDown();
            var total = this.HotelCharterRoomDateDetails.Sum(d => d.GetRate());
            priceBreakDown.DayRates = total;
            priceBreakDown.TotalPrice = total;

            foreach (var f in this.HotelCharterRoomServiceFees.OrderBy(s => s.ApplyOrder))
            {
                total = f.ApplyTo(total);
                var difference = total - priceBreakDown.TotalPrice;
                switch (f.GetServiceFeeType())
                {
                    case Reservation.Hotels.ServiceFeeTypes.TransferFee:
                        priceBreakDown.TransferFee = difference;
                        break;
                    case Reservation.Hotels.ServiceFeeTypes.HandlingFee:
                        priceBreakDown.HandlingsFee = difference;
                        break;
                    case Reservation.Hotels.ServiceFeeTypes.Comission:
                        priceBreakDown.Commission = difference;
                        break;
                    case Reservation.Hotels.ServiceFeeTypes.Refund:
                        priceBreakDown.Refund += difference;
                        break;
                    case Reservation.Hotels.ServiceFeeTypes.Markup:
                        priceBreakDown.Markup += difference;
                        break;
                    default:
                        break;
                }

                priceBreakDown.TotalPrice = total;
            }
            return priceBreakDown;
        }


        public class PriceBreakDown
        {
            public decimal DayRates { get; set; } = 0M;

         
            public decimal? HandlingsFee { get; set; } 
            public decimal? TransferFee { get; set; } 

          

            public decimal Commission { get; set; } = 0m;

            public decimal Markup { get; set; } = 0M;
          
            public decimal Refund { get; set; } = 0M;

            public decimal TotalPrice { get; set; } = 0M;
        }
    }
}
