﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class vHotelFareDLog
    {
        public BilenAdam.Data.Tracking.ChangeType GetChangeType()
        {
            return (Tracking.ChangeType)this.ChangeType;
        }

        public string GetSeasonCode()
        {
            return string.Format("{0}{1}", SeasonBeginDate.Month < 11 ? "S" : "W", this.SeasonBeginDate.ToString("yy"));
            
        }

        public string GetPaxes()
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("{0} ADT ", Adults.Value);
            int chd = Childs ?? 0;
            if(chd >= 1)
            {
                sb.AppendFormat("({0}-{1})", FirstChildMinAge.Value, FirstChildMaxAge.Value);
            }
            if (chd >= 2)
            {
                sb.AppendFormat("({0}-{1})", SecondChildMaxAge.Value, SecondChildMaxAge.Value);
            }

            if (chd >=3)
            {
                sb.AppendFormat("({0}-{1})", ThirdChildMinAge.Value, ThirdChildMaxAge.Value);
            }

         
            return sb.ToString();
        }
    }
}
