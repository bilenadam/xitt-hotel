﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Data.HotelData
{
    public partial class HotelCharterGuest
    {
        public string GetTitle()
        {
            var lang = System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToLowerInvariant();

            switch (lang)
            {
                case "tr": return IsMale ? "Bay" : "Bayan";
                case "de": return IsMale ? "Herr" : "Frau";
                default:  return IsMale ? "MR" : "MS";
            }
        }

        public int CalculateAge(DateTime date)
        {
            return Utilities.AgeCalculator.CalculateAge(Dob, date);
        }
    }
}
