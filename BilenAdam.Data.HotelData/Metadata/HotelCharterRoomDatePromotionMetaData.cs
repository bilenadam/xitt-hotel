#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using BilenAdam.Data.HotelData.Resources;


namespace BilenAdam.Data.HotelData
{	
	[MetadataType(typeof(HotelCharterRoomDatePromotion.HotelCharterRoomDatePromotionMetaData))]
	public partial class HotelCharterRoomDatePromotion 
	{
	
        #pragma warning disable 0649
        class HotelCharterRoomDatePromotionMetaData 
        {
	
                
        //Int NOT NULL IDENTITY

        [Required(ErrorMessageResourceName = "id_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "id_display", ResourceType = typeof(EntityResources))]
        public int Id;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelcharterroomdatedetailid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelcharterroomdatedetailid_display", ResourceType = typeof(EntityResources))]
        public int HotelCharterRoomDateDetailId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelpromotionid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelpromotionid_display", ResourceType = typeof(EntityResources))]
        public int HotelPromotionId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "amounttype_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "amounttype_display", ResourceType = typeof(EntityResources))]
        public int AmountType;
        
                
        //Money NOT NULL

        [Required(ErrorMessageResourceName = "amount_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "amount_display", ResourceType = typeof(EntityResources))]
        public decimal Amount;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "applyorder_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "applyorder_display", ResourceType = typeof(EntityResources))]
        public int ApplyOrder;
        
        		}
        #pragma warning restore 0649
    }
}// End 
