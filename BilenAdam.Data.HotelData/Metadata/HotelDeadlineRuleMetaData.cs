#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using BilenAdam.Data.HotelData.Resources;


namespace BilenAdam.Data.HotelData
{	
	[MetadataType(typeof(HotelDeadlineRule.HotelDeadlineRuleMetaData))]
	public partial class HotelDeadlineRule 
	{
	
        #pragma warning disable 0649
        class HotelDeadlineRuleMetaData 
        {
	
                
        //Int NOT NULL IDENTITY

        [Required(ErrorMessageResourceName = "id_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "id_display", ResourceType = typeof(EntityResources))]
        public int Id;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelid_display", ResourceType = typeof(EntityResources))]
        public int HotelId;
        
                
        //Date NOT NULL

        [Required(ErrorMessageResourceName = "begindate_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "begindate_display", ResourceType = typeof(EntityResources))]
        public DateTime BeginDate;
        
                
        //Date NOT NULL

        [Required(ErrorMessageResourceName = "enddate_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "enddate_display", ResourceType = typeof(EntityResources))]
        public DateTime EndDate;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "deadline_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "deadline_display", ResourceType = typeof(EntityResources))]
        public int Deadline;
        
        		}
        #pragma warning restore 0649
    }
}// End 
