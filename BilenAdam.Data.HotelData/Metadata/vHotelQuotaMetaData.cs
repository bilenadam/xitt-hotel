#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using BilenAdam.Data.HotelData.Resources;


namespace BilenAdam.Data.HotelData
{	
	[MetadataType(typeof(vHotelQuota.vHotelQuotaMetaData))]
	public partial class vHotelQuota 
	{
	
        #pragma warning disable 0649
        class vHotelQuotaMetaData 
        {
	
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "id_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "id_display", ResourceType = typeof(EntityResources))]
        public int Id;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "roomid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "roomid_display", ResourceType = typeof(EntityResources))]
        public int RoomId;
        
                
        //NVarChar(50) NOT NULL

        [Required(ErrorMessageResourceName = "roomname_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(50)]
        [Display(Name = "roomname_display", ResourceType = typeof(EntityResources))]
        public string RoomName;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelid_display", ResourceType = typeof(EntityResources))]
        public int HotelId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "marketid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "marketid_display", ResourceType = typeof(EntityResources))]
        public int MarketId;
        
                
        //NVarChar(50) NOT NULL

        [Required(ErrorMessageResourceName = "marketname_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(50)]
        [Display(Name = "marketname_display", ResourceType = typeof(EntityResources))]
        public string MarketName;
        
                
        //Date NOT NULL

        [Required(ErrorMessageResourceName = "quotadate_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "quotadate_display", ResourceType = typeof(EntityResources))]
        public DateTime QuotaDate;
        
                
        //TinyInt NOT NULL

        [Required(ErrorMessageResourceName = "amount_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "amount_display", ResourceType = typeof(EntityResources))]
        public byte Amount;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "isstopsale_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "isstopsale_display", ResourceType = typeof(EntityResources))]
        public int IsStopSale;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "sold_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "sold_display", ResourceType = typeof(EntityResources))]
        public int Sold;
        
                
        //Int

                [Display(Name = "remain_display", ResourceType = typeof(EntityResources))]
        public int? Remain;
        
        		}
        #pragma warning restore 0649
    }
}// End 
