#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using BilenAdam.Data.HotelData.Resources;


namespace BilenAdam.Data.HotelData
{	
	[MetadataType(typeof(vHotelFareDLog.vHotelFareDLogMetaData))]
	public partial class vHotelFareDLog 
	{
	
        #pragma warning disable 0649
        class vHotelFareDLogMetaData 
        {
	
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "rowid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "rowid_display", ResourceType = typeof(EntityResources))]
        public int RowId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "changetype_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "changetype_display", ResourceType = typeof(EntityResources))]
        public int ChangeType;
        
                
        //VarChar(50) NOT NULL

        [Required(ErrorMessageResourceName = "changedby_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(50)]
        [Display(Name = "changedby_display", ResourceType = typeof(EntityResources))]
        public string ChangedBy;
        
                
        //DateTime2 NOT NULL

        [Required(ErrorMessageResourceName = "changedat_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "changedat_display", ResourceType = typeof(EntityResources))]
        public DateTime ChangedAt;
        
                
        //Int

                [Display(Name = "roomid_display", ResourceType = typeof(EntityResources))]
        public int? RoomId;
        
                
        //NVarChar(50) NOT NULL

        [Required(ErrorMessageResourceName = "roomname_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(50)]
        [Display(Name = "roomname_display", ResourceType = typeof(EntityResources))]
        public string RoomName;
        
                
        //Int

                [Display(Name = "pensionid_display", ResourceType = typeof(EntityResources))]
        public int? PensionId;
        
                
        //VarChar(50) NOT NULL

        [Required(ErrorMessageResourceName = "pensionname_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(50)]
        [Display(Name = "pensionname_display", ResourceType = typeof(EntityResources))]
        public string PensionName;
        
                
        //Int

                [Display(Name = "farehid_display", ResourceType = typeof(EntityResources))]
        public int? FareHId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelseasonid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelseasonid_display", ResourceType = typeof(EntityResources))]
        public int HotelSeasonId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelid_display", ResourceType = typeof(EntityResources))]
        public int HotelId;
        
                
        //Date NOT NULL

        [Required(ErrorMessageResourceName = "seasonbegindate_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "seasonbegindate_display", ResourceType = typeof(EntityResources))]
        public DateTime SeasonBeginDate;
        
                
        //Date NOT NULL

        [Required(ErrorMessageResourceName = "seasonenddate_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "seasonenddate_display", ResourceType = typeof(EntityResources))]
        public DateTime SeasonEndDate;
        
                
        //NVarChar(3)

                [MaxLength(3)]
        [Display(Name = "seasoncode_display", ResourceType = typeof(EntityResources))]
        public string SeasonCode;
        
                
        //Money

                [Display(Name = "fare_display", ResourceType = typeof(EntityResources))]
        public decimal? Fare;
        
                
        //TinyInt

                [Display(Name = "adults_display", ResourceType = typeof(EntityResources))]
        public byte? Adults;
        
                
        //TinyInt

                [Display(Name = "childs_display", ResourceType = typeof(EntityResources))]
        public byte? Childs;
        
                
        //TinyInt

                [Display(Name = "firstchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? FirstChildMinAge;
        
                
        //TinyInt

                [Display(Name = "firstchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? FirstChildMaxAge;
        
                
        //TinyInt

                [Display(Name = "secondchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? SecondChildMinAge;
        
                
        //TinyInt

                [Display(Name = "secondchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? SecondChildMaxAge;
        
                
        //TinyInt

                [Display(Name = "thirdchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? ThirdChildMinAge;
        
                
        //TinyInt

                [Display(Name = "thirdchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? ThirdChildMaxAge;
        
                
        //TinyInt

                [Display(Name = "fourthchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? FourthChildMinAge;
        
                
        //TinyInt

                [Display(Name = "fourthchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? FourthChildMaxAge;
        
        		}
        #pragma warning restore 0649
    }
}// End 
