#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using BilenAdam.Data.HotelData.Resources;


namespace BilenAdam.Data.HotelData
{	
	[MetadataType(typeof(HotelFareD.HotelFareDMetaData))]
	public partial class HotelFareD 
	{
	
        #pragma warning disable 0649
        class HotelFareDMetaData 
        {
	
                
        //Int NOT NULL IDENTITY

        [Required(ErrorMessageResourceName = "id_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "id_display", ResourceType = typeof(EntityResources))]
        public int Id;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "farehid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "farehid_display", ResourceType = typeof(EntityResources))]
        public int FareHId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "roomid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "roomid_display", ResourceType = typeof(EntityResources))]
        public int RoomId;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "pensionid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "pensionid_display", ResourceType = typeof(EntityResources))]
        public int PensionId;
        
                
        //TinyInt NOT NULL

        [Required(ErrorMessageResourceName = "minnights_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "minnights_display", ResourceType = typeof(EntityResources))]
        public byte MinNights;
        
                
        //Money NOT NULL

        [Required(ErrorMessageResourceName = "fare_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "fare_display", ResourceType = typeof(EntityResources))]
        public decimal Fare;
        
                
        //TinyInt NOT NULL

        [Required(ErrorMessageResourceName = "adults_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "adults_display", ResourceType = typeof(EntityResources))]
        public byte Adults;
        
                
        //TinyInt NOT NULL

        [Required(ErrorMessageResourceName = "childs_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "childs_display", ResourceType = typeof(EntityResources))]
        public byte Childs;
        
                
        //TinyInt

                [Display(Name = "firstchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? FirstChildMinAge;
        
                
        //TinyInt

                [Display(Name = "firstchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? FirstChildMaxAge;
        
                
        //TinyInt

                [Display(Name = "secondchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? SecondChildMinAge;
        
                
        //TinyInt

                [Display(Name = "secondchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? SecondChildMaxAge;
        
                
        //TinyInt

                [Display(Name = "thirdchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? ThirdChildMinAge;
        
                
        //TinyInt

                [Display(Name = "thirdchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? ThirdChildMaxAge;
        
                
        //TinyInt

                [Display(Name = "fourthchildminage_display", ResourceType = typeof(EntityResources))]
        public byte? FourthChildMinAge;
        
                
        //TinyInt

                [Display(Name = "fourthchildmaxage_display", ResourceType = typeof(EntityResources))]
        public byte? FourthChildMaxAge;
        
        		}
        #pragma warning restore 0649
    }
}// End 
