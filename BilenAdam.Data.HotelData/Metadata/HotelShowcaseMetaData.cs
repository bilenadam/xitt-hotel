#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by LINQ to SQL simple dto generator for T4 C#
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Data.Linq;
using System.ComponentModel.DataAnnotations;
using BilenAdam.Data.HotelData.Resources;


namespace BilenAdam.Data.HotelData
{	
	[MetadataType(typeof(HotelShowcase.HotelShowcaseMetaData))]
	public partial class HotelShowcase 
	{
	
        #pragma warning disable 0649
        class HotelShowcaseMetaData 
        {
	
                
        //Int NOT NULL IDENTITY

        [Required(ErrorMessageResourceName = "id_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "id_display", ResourceType = typeof(EntityResources))]
        public int Id;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "hotelid_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "hotelid_display", ResourceType = typeof(EntityResources))]
        public int HotelId;
        
                
        //Bit NOT NULL

        [Required(ErrorMessageResourceName = "isactive_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "isactive_display", ResourceType = typeof(EntityResources))]
        public bool IsActive;
        
                
        //Int NOT NULL

        [Required(ErrorMessageResourceName = "showorder_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "showorder_display", ResourceType = typeof(EntityResources))]
        public int ShowOrder;
        
                
        //VarChar(150) NOT NULL

        [Required(ErrorMessageResourceName = "pension_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(150)]
        [Display(Name = "pension_display", ResourceType = typeof(EntityResources))]
        public string Pension;
        
                
        //Money NOT NULL

        [Required(ErrorMessageResourceName = "farebase_required", ErrorMessageResourceType = typeof(EntityResources))]
        [Display(Name = "farebase_display", ResourceType = typeof(EntityResources))]
        public decimal FareBase;
        
                
        //Char(3) NOT NULL

        [Required(ErrorMessageResourceName = "farecurrency_required", ErrorMessageResourceType = typeof(EntityResources))]
        [MaxLength(3)]
        [Display(Name = "farecurrency_display", ResourceType = typeof(EntityResources))]
        public string FareCurrency;
        
        		}
        #pragma warning restore 0649
    }
}// End 
