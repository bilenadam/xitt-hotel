﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation;
using BilenAdam.Reservation.Hotels;
using BilenAdam.Reservation.Search.Hotels;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class PropertyProduct : BilenAdam.Reservation.Search.Hotels.IPropertyProduct
    {
        public int HotelId { get; set; }
        public int MarketId { get; set; }
        public bool IncludeTransfer { get; set; }
        public bool IsPackageSearch { get; set; }

      


        public AvailabilityStatus AvailabilityStatus
        {
            get
            {
                return BilenAdam.Reservation.AvailabilityStatus.InstantConfirmation;
            }
        }

        public DateTime CheckIn
        {
            get; set;
        }

        public DateTime CheckOut
        {
            get; set;
        }

     
        public int Nights
        {
            get
            {
                return (int)CheckOut.Subtract(CheckIn).TotalDays;
            }
        }

        readonly string productid = System.IO.Path.GetRandomFileName().Replace(".", "");
        public string ProductId
        {
            get { return productid; }
        }

        public List<RoomProduct> RoomProducts = new List<RoomProduct>();
        public IEnumerable<IRoomProduct> Rooms
        {
            get
            {
                return RoomProducts;
            }
        }

        public string SpecialDeal
        {
            get
            {
                return "";
            }
        }

        public decimal TotalFare { get; internal set; }

      

        public bool IsTransferIncluded()
        {
            return IncludeTransfer;
        }
    }
}
