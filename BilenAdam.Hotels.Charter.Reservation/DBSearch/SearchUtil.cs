﻿using BilenAdam.Data.HotelData;
using BilenAdam.Reservation.Search.Hotels.Query;
using BilenAdam.Reservation.Search.Hotels.Query.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class SearchUtil
    {

        Tuple<string,string,string> getFilterValues(IEnumerable<IHotelFilter> filters)
        {
            string hotelName = null;
            string stars = null;
            string boards = null;
            if (filters.HasAny())
            {

                hotelName = filters.GetHotelFilterValue<IHotelNameFilter, string>(f => f.HotelName, "");

                if (string.IsNullOrWhiteSpace(hotelName))
                    hotelName = null;

                var starSelection = filters.GetHotelFilterValue<IStarSelectionFilter, IEnumerable<int>>(f => f.GetStarSelection(), null);
                if (starSelection.HasAny())
                    stars = starSelection.Select(s => s.ToString()).AggregateByComma();

                var boardSelection = filters.GetHotelFilterValue<IBoardTypeFilter, IEnumerable<int>>(f => f.GetBoardTypes(), null);
                if (boardSelection.HasAny())
                    boards = boardSelection.Select(b => b.ToString()).AggregateByComma();

            }

            return new Tuple<string, string, string>(hotelName, stars, boards);


        }

        public PropertyProduct SegmentSearch(int hotelId, int marketId, DateTime checkIn, DateTime checkOut, IEnumerable<IRoomProductPlan> roomPlan, bool includeHotelTransfer, bool isPackageSearch)
        {
            int nights = (int)checkOut.Subtract(checkIn).TotalDays;
            int roomCount = 0;

            PropertyProduct product = new PropertyProduct()
            {
                CheckIn = checkIn,
                CheckOut = checkOut,
                HotelId = hotelId,
                IncludeTransfer = includeHotelTransfer,
                MarketId = marketId,
                TotalFare = 0M,
                IsPackageSearch = isPackageSearch
        };

            foreach (var rp in roomPlan)
            {
                roomCount++;
                var rc = new RoomRefChildAgesUtil(rp);

                using (var db = HotelDataContextFactory.Create())
                {
                    DateTime start = DateTime.Now;

                    var item = db.GetHotelSingleAvailability(hotelId, rp.RoomType.ToString(), rp.BoardType.ToString(), checkIn, checkOut, rc.Adults, rc.Childs, rc.FirstChildAge, rc.SecondChildAge, rc.ThirdChildAge, includeHotelTransfer, DateTime.Now).FirstOrDefault();
                    System.Diagnostics.Debug.WriteLine("Hotel core search time {0}", DateTime.Now.Subtract(start));




                    product.RoomProducts.Add(new RoomProduct()
                    {
                        BoardType = item.PensionId,
                        BoardTypeName = item.BoardType,
                        HotelId = item.HotelId,
                        Nights = nights,
                        Price = item.Price,
                        RoomReference = rp,
                        RoomType = item.RoomId,
                        RoomTypeName = item.RoomType
                    });

                    product.TotalFare += item.Price;


                }
            }

            return product;
        }

     

        public IEnumerable<PropertyProduct> MultiAvailabilitySearch(string destination, int marketId, DateTime checkIn, DateTime checkOut, IEnumerable<IRoomPlan> roomPlan, bool includeHotelTransfer, IEnumerable<IHotelFilter> filters, DateTime rezDate)
        {
            int? destinationId = TryParseUtil.TryParseInt(destination);
            string countryCode = destinationId == null && destination.Length == 2 ? destination : null;
            string iataCode = destinationId == null && destination.Length == 3 ? destination : null;

            var fv =  getFilterValues(filters);
            string hotelName = fv.Item1; string stars = fv.Item2; string boards = fv.Item3; 

            int nights = (int)checkOut.Subtract(checkIn).TotalDays;

            Dictionary<int, PropertyProduct> propertyProducts = new Dictionary<int, PropertyProduct>();
            using (var db = HotelDataContextFactory.Create())
            {

                bool firstRoom = true;

                foreach (var rp in roomPlan)
                {
                    var rc = new RoomRefChildAgesUtil(rp);

                    DateTime start = DateTime.Now;

                    var data = db.GetHotelMultiAvailablity(countryCode, iataCode, destinationId, hotelName, stars, null, boards, checkIn, checkOut, rc.Adults, rc.Childs, rc.FirstChildAge, rc.SecondChildAge, rc.ThirdChildAge, includeHotelTransfer, rezDate).ToArray();


                    System.Diagnostics.Debug.WriteLine("Hotel core search time {0}", DateTime.Now.Subtract(start));

                    if (firstRoom)
                    {
                        firstRoom = false;

                        int[] hotelIds = data.Select(i => i.HotelId).ToArray();

                        foreach (var hotelId in hotelIds)
                        {
                            propertyProducts.Add(hotelId, new PropertyProduct
                            {
                                CheckIn = checkIn,
                                CheckOut = checkOut,
                                HotelId = hotelId,
                                IncludeTransfer = includeHotelTransfer,
                                MarketId = marketId,
                                TotalFare = 0M
                            });
                        }
                    }


                    foreach (var item in data)
                    {
                        if (propertyProducts.ContainsKey(item.HotelId))
                        {
                            var propertyProduct = propertyProducts[item.HotelId];

                            RoomProduct roomProduct = new RoomProduct()
                            {
                                BoardType = item.PensionId,
                                BoardTypeName = item.BoardType,
                                HotelId = item.HotelId,
                                Nights = nights,
                                Price = item.Price,
                                RoomReference = rp,
                                RoomType = item.RoomId,
                                RoomTypeName = item.RoomType
                            };

                            propertyProduct.TotalFare += item.Price;

                            propertyProduct.RoomProducts.Add(roomProduct);
                        }
                    }




                }



            }



            return propertyProducts.Values.Where(p => p.RoomProducts.Count == roomPlan.Count());


        }

        public IEnumerable<PropertyProduct> SingleAvailabilitySearch(int hotelId, int marketId, DateTime checkIn, DateTime checkOut, IEnumerable<IRoomPlan> roomPlan, bool includeHotelTransfer, IEnumerable<IHotelFilter> filters, DateTime rezDate)
        {
            var fv = getFilterValues(filters);
            string boards = fv.Item3;

            int nights = (int)checkOut.Subtract(checkIn).TotalDays;
            int roomCount = 0;

            List<List<RoomProduct>> roomProducts = new List<List<RoomProduct>>();

            foreach (var rp in roomPlan)
            {
                roomCount++;
                var rc = new RoomRefChildAgesUtil(rp);

                using (var db = HotelDataContextFactory.Create())
                {
                    DateTime start = DateTime.Now;

                    var data = db.GetHotelSingleAvailability(hotelId, null, boards, checkIn, checkOut, rc.Adults, rc.Childs, rc.FirstChildAge, rc.SecondChildAge, rc.ThirdChildAge, includeHotelTransfer,rezDate).ToArray();
                    System.Diagnostics.Debug.WriteLine("Hotel core search time {0}", DateTime.Now.Subtract(start));

                    List<RoomProduct> rpList = new List<RoomProduct>();

                    foreach (var item in data)
                    {

                        rpList.Add(new RoomProduct()
                        {
                            BoardType = item.PensionId,
                            BoardTypeName = item.RoomType,
                            HotelId = item.HotelId,
                            Nights = nights,
                            Price = item.Price,
                            RoomReference = rp,
                            RoomType = item.RoomId,
                            RoomTypeName = item.RoomType
                        });

                    }

                    roomProducts.Add(rpList);


                }
            }

            List<PropertyProduct> results = new List<PropertyProduct>();
            if (roomCount == 1)
            {

                foreach (var rp in roomProducts[0])
                {
                    PropertyProduct product = new PropertyProduct()
                    {
                        CheckIn = checkIn,
                        CheckOut = checkOut,
                        HotelId = hotelId,
                        IncludeTransfer = includeHotelTransfer,
                        MarketId = marketId,
                        TotalFare = rp.Price
                    };

                    product.RoomProducts.Add(rp);

                    results.Add(product);
                }
            }
            else if (roomCount == 2)
            {
                foreach (var rp in roomProducts[0])
                    foreach (var rp2 in roomProducts[1])
                    {
                        PropertyProduct product = new PropertyProduct()
                        {
                            CheckIn = checkIn,
                            CheckOut = checkOut,
                            HotelId = hotelId,
                            IncludeTransfer = includeHotelTransfer,
                            MarketId = marketId,
                            TotalFare = rp.Price + rp2.Price
                        };

                        product.RoomProducts.Add(rp);
                        product.RoomProducts.Add(rp2);

                        results.Add(product);
                    }
            }
            else if (roomCount == 3)
            {

                foreach (var rp in roomProducts[0])
                    foreach (var rp2 in roomProducts[1])
                        foreach (var rp3 in roomProducts[2])
                        {
                            PropertyProduct product = new PropertyProduct()
                            {
                                CheckIn = checkIn,
                                CheckOut = checkOut,
                                HotelId = hotelId,
                                IncludeTransfer = includeHotelTransfer,
                                MarketId = marketId,
                                TotalFare = rp.Price + rp2.Price + rp3.Price
                            };

                            product.RoomProducts.Add(rp);
                            product.RoomProducts.Add(rp2);
                            product.RoomProducts.Add(rp3);

                            results.Add(product);
                        }
            }

            return results;
        }
    }
}
