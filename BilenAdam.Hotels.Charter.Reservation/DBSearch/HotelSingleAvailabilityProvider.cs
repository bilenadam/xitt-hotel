﻿using BilenAdam.Data.HotelData;
using BilenAdam.Reservation.Search.Hotels.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class HotelSingleAvailabilityProvider : BilenAdam.Reservation.Search.Hotels.IHotelSingleAvailabilityProvider
    {

        public int DefaultMarketId { get; set;}

        public IEnumerable<BilenAdam.Reservation.Search.Hotels.IPropertyProduct> Search(BilenAdam.Reservation.Search.Hotels.IPropertyProduct product)
        {
            var hotelProduct = product as PropertyProduct;

            return SearchHotel(hotelProduct);
        }

        public PropertyProduct[] SearchHotel(PropertyProduct hotelProduct)
        {
            Log.LogContext.LogMessage("Hotel Charter Single Availability Search Started");
            try
            {

                return new HotelMultiAvailabilityProvider() { DefaultMarketId = this.DefaultMarketId}.Search( hotelProduct.HotelId , hotelProduct.MarketId, hotelProduct.CheckIn, hotelProduct.CheckOut, hotelProduct.RoomProducts.Select(r => r.RoomReference).ToArray(), hotelProduct.IncludeTransfer , hotelProduct.IsPackageSearch,null,DateTime.Now).ToArray();
            }
            catch (Exception exc)
            {
                Log.LogContext.LogMessage("Hotel Charter Single Availability Search Error");
                Log.LogContext.LogExecption(exc);
                throw exc;
            }
            finally
            {
                Log.LogContext.LogMessage("Hotel Charter Single Availability Search Finished");
            }
        }

      

    }
}
