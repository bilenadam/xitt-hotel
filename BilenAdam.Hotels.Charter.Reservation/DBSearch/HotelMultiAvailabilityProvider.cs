﻿using BilenAdam.Data.HotelData;
using BilenAdam.Reservation.Search.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Search.Hotels.Query.Filters;
using System.Data.Linq;
using BilenAdam.Reservation.Search.Hotels.Query;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class HotelMultiAvailabilityProvider : IHotelSearchProvider
    {
       
        public int DefaultMarketId { get; set; }

        IEnumerable<Log.ILog> logs;
      

        public void BeginSearch(IHotelSearch search)
        {
          
            search.SignalRunning();
            logs = Log.LogContext.GetLogs();
            System.Threading.ThreadPool.QueueUserWorkItem(o => doSearch(search));
        }

        private void doSearch(IHotelSearch search)
        {
#if DEBUG
            var start = DateTime.Now;
#endif
            try
            {
                Log.LogContext.AddLogs(logs);
                Log.LogContext.LogMessage("Hotel Charter Multi Availability Search Started");





                var results = Search(search.Query,DateTime.Now); 


                if (results != null)
                {
                    foreach (var r in results)
                    {
                        search.AddProduct(r);
                    }
                }



            }
            catch (Exception exc)
            {
                Log.LogContext.LogMessage("Hotel Charter Multi Availability Search Error");
                Log.LogContext.LogExecption(exc);
            }
            finally
            {
                search.SignalFinished();
                Log.LogContext.LogMessage("Hotel Charter Multi Availability Search Finished");
                Log.LogContext.RemoveLogs(logs);
#if DEBUG
                System.Diagnostics.Debug.WriteLine("Hotel search time {0}", DateTime.Now.Subtract(start));
#endif
        
                
            }
        }

        public IEnumerable<PropertyProduct> Search(IHotelQuery query, DateTime rezDate)
        {
            var filters = query.GetHotelFilters();

            if (filters.HasAny() && TryParseUtil.TryParseInt(filters.GetHotelFilterValue<IHotelNameFilter, string>(f => f.HotelId, "")) != null)
            {
                int hotelId = TryParseUtil.TryParseInt(filters.GetHotelFilterValue<IHotelNameFilter, string>(f => f.HotelId, "")).Value;
                return Search(hotelId, DefaultMarketId, query.CheckIn.Date, query.CheckOut.Date, query.GetRooms(), query.IncludeHotelTransfer, query.IsPackageSearch, filters,rezDate);
            }
            else
            {
                return Search(query.Destination, DefaultMarketId, query.CheckIn.Date, query.CheckOut.Date, query.GetRooms(), query.IncludeHotelTransfer, query.IsPackageSearch, filters,rezDate);
            }
        }

        public IEnumerable<PropertyProduct> Search(int hotelId, int marketId, DateTime checkIn, DateTime checkOut, IEnumerable<IRoomPlan> roomPlan, bool includeHotelTransfer, bool isPackageSearch, IEnumerable<IHotelFilter> filters, DateTime rezDate)
        {
            




            var results = new SearchUtil().SingleAvailabilitySearch(hotelId, marketId, checkIn, checkOut, roomPlan, includeHotelTransfer, filters,rezDate);

            foreach (var result in results)
            {
                result.IsPackageSearch = isPackageSearch;
            }
            return results;
            
        }

        public IEnumerable<PropertyProduct> Search(string destination, int marketId, DateTime checkIn, DateTime checkOut, IEnumerable<IRoomPlan> roomPlan, bool includeHotelTransfer, bool isPackageSearch, IEnumerable<IHotelFilter> filters, DateTime rezDate)
        {

            var results = new SearchUtil().MultiAvailabilitySearch(destination,marketId, checkIn, checkOut, roomPlan, includeHotelTransfer, filters,rezDate);

         
            foreach (var result in results)
            {
                result.IsPackageSearch = isPackageSearch;
            }
            return results;
        }

    }
}
