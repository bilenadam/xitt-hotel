﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Reservation.Hotels;
using BilenAdam.Reservation.Search.Hotels;
using BilenAdam.Reservation.Search.Hotels.Query;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class RoomProduct : BilenAdam.Reservation.Search.Hotels.IRoomProduct
    {
        public IEnumerable<BilenAdam.Reservation.Hotels.IAmenity> Amenities
        {
            get
            {
                return null;
            }
        }

        public int HotelId
        {
            get;
            set;
        }
      
        public string BoardTypeName
        {
            get;set;
        }
        public int BoardType
        {
            get;set;
        }

        public IRoomPlan RoomReference
        {
            get;set;
        }

      
        public int RoomType
        {
            get;set;
        }

        public decimal Price
        {
            get;set;
        }
        public string RoomTypeName
        {
            get;set;
        }

        public int Nights
        {
            get;
            set;
        }
        
        public decimal GetAverageRate()
        {
            return Price / Nights;
        }

        public IEnumerable<IRoomDateProduct> GetDates()
        {
            throw new NotImplementedException();
        }

        public decimal GetTotalFare()
        {
            return Price;
        }
    }
}
