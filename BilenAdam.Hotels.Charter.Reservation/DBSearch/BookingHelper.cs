﻿using BilenAdam.Data.HotelData;
using BilenAdam.Reservation.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class BookingHelper
    {
        public HotelCharterReservation CreateBooking(PropertyProduct product, IEnumerable<IRoom> rooms, IEnumerable<object> elements, string nationality)
        {
            PropertyProduct propertyProduct = product as PropertyProduct;


          
         
           

            using (var hotelDB = HotelDataContextFactory.Create())
            {
                var hotel = hotelDB.Hotels.First(h => h.Id == product.HotelId);

                BilenAdam.Log.LogContext.LogMessage($"Starting to book {hotel.Name}, {product.CheckIn} - {product.CheckOut}");
              


                var charterRezEntity = new HotelCharterReservation()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    HotelId = propertyProduct.HotelId,
                    MarketId = propertyProduct.MarketId,
                    Currency = CurrencyString.EUR
                };


                foreach (var room in rooms)
                {
                    var roomProduct = propertyProduct.RoomProducts.FirstOrDefault(r => r.RoomReference.RoomRef == room.RoomRef);

                    BilenAdam.Log.LogContext.LogMessage($"adding room {roomProduct.RoomTypeName} - {roomProduct.BoardTypeName}");


                    // charter room
                    var charterRoomEntity = new HotelCharterRoom()
                    {
                        CreateTimeUTC = DateTime.UtcNow,
                        CheckIn = propertyProduct.CheckIn,
                        CheckOut = propertyProduct.CheckOut,
                        HotelCharterReservation = charterRezEntity,
                        RoomRef = room.RoomRef,
                        HotelPensionId = roomProduct.BoardType,
                        HotelRoomId = roomProduct.RoomType
                  

                    };
                   

                   // tpRoomEntity.SetTotalPrice(roomProduct.GetTotalFare());

                    int gIndex = 1;

                    byte adults = 0;  byte childs = 0;
                    byte? child1 = null, child2 = null, child3 = null;

                    
                    foreach (var guest in room.GetGuests())
                    {
                        var traveller = guest;

                        // charter record guest
                        var charterGuestEntity = new HotelCharterGuest()
                        {
                            HotelCharterRoom = charterRoomEntity,
                            IsAdult = (guest.GuestType != RoomGuestType.Child),
                            No = gIndex++,
                            Dob = traveller.Dob.Value,
                            FirstName = traveller.FirstNSecondName(),
                            IsMale = traveller.isMale,
                            LastName = traveller.LastName
                        };

                     

                        if(guest.GuestType == RoomGuestType.Child)
                        {
                            byte age = (byte)Utilities.AgeCalculator.CalculateAge(traveller.Dob.Value, product.CheckIn);
                            childs++;
                            if (child1 == null)
                                child1 = age;
                            else if (child2 == null)
                                child2 = age;
                            else if (child3 == null)
                                child3 = age;

                        }
                        else
                        {
                            adults++;
                        }

                    }


                    BilenAdam.Log.LogContext.LogMessage($" adt {adults}  chd {childs} ages {child1} {child2} {child3}");
                    var fees = hotelDB.GetRoomFees(roomProduct.HotelId, product.CheckIn, adults, childs, child1, child2, child3, product.IncludeTransfer).FirstOrDefault();
                    BilenAdam.Log.LogContext.LogMessage($" Fee : {fees.Fees}  commission : {fees.Commission}");
                    // add handling or transfer fees
                    charterRoomEntity.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                    {
                        AmountType = (int)BilenAdam.Reservation.AmountType.Fix,
                        Amount = fees.Fees ?? 0,
                        ApplyOrder = 1,
                        Code = product.IncludeTransfer ? ServiceFeeTypes.TransferFee.ToString() : ServiceFeeTypes.HandlingFee.ToString()
                    });
                    // add commission
                    charterRoomEntity.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                    {
                        AmountType = (int)BilenAdam.Reservation.AmountType.Percent,
                        Amount = fees.Commission,
                        ApplyOrder = 2,
                        Code = ServiceFeeTypes.Comission.ToString()
                    });





                    var dateProducts = hotelDB.GetDateProducts(roomProduct.HotelId, roomProduct.RoomType, roomProduct.BoardType, product.CheckIn, product.CheckOut, adults, childs, child1, child2, child3, product.IncludeTransfer, DateTime.Now).ToArray();

                    if(dateProducts.Length != (int)product.CheckOut.Subtract(product.CheckIn).TotalDays)
                    {
                        BilenAdam.Log.LogContext.LogMessage($"Date products expected {(int)product.CheckOut.Subtract(product.CheckIn).TotalDays} returned {dateProducts.Length}");
                        throw new Exception("hotel is not available");
                    }
                        

                    foreach (var roomDate in dateProducts)
                    {




                        BilenAdam.Log.LogContext.LogMessage($"add date product header id {roomDate.FareHId} details Id {roomDate.FareDId} spo enabled ? {roomDate.IsSPOEnabled} ");
                        var roomDetail = new HotelCharterRoomDateDetail()
                        {
                            Date = roomDate.QuotaDate,
                            HotelFareDId = roomDate.FareDId,
                            Price = roomDate.Fare,
                            HotelCharterRoom = charterRoomEntity
                        };

                        if (roomDate.IsSPOEnabled ?? false)
                            continue;

                        
                        int applyOrder = 1;
                        if(roomDate.EBId != null)
                        {
                            var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.EBId);
                            BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate}");

                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }
                        if(roomDate.TEBId != null)
                        {
                            var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.TEBId);
                            BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate} * {adults}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate * adults,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.REBId != null)
                        {
                            var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.REBId);
                            BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.LSId != null)
                        {
                            var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.LSId);
                            BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.IsDP)
                        {
                            BilenAdam.Log.LogContext.LogMessage($"day promotion {roomDate.DPId}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = (int)BilenAdam.Reservation.AmountType.Percent,
                                Amount = 1,
                                HotelPromotionId = roomDate.DPId.Value
                            });

                            applyOrder++;
                        }


                    }

                    //   var prodPrice = roomProduct.GetTotalFare();
                    //   var rezPrice = charterRoomEntity.GetTotalRate();
                    BilenAdam.Log.LogContext.LogMessage($"total rate {charterRoomEntity.GetTotalRate()}");
                }

                hotelDB.HotelCharterReservations.InsertOnSubmit(charterRezEntity);
                hotelDB.SubmitChanges();

                BilenAdam.Log.LogContext.LogMessage($"pnr  {charterRezEntity.GetPNR()}");
                return charterRezEntity;
            }

          
        }
    }
}
