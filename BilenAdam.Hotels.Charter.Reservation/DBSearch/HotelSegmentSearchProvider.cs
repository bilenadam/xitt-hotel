﻿using BilenAdam.Reservation.Search.Hotels;
using BilenAdam.Reservation.Search.Hotels.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.DBSearch
{
    public class HotelSegmentSearchProvider : IHotelSegmentSearchProvider
    {
        public IPropertyProduct Search(IHotelSegmentQuery query)
        {
            return Search(query.HotelId, query.CheckIn, query.CheckOut, query.RoomPlan, query.IncludeHotelTransfer, query.IsPackageSearch);
        }

        public int MarketId { get; set; }
        public PropertyProduct Search(int hotelId, DateTime checkIn, DateTime checkOut, IEnumerable<IRoomProductPlan> roomPlan, bool includeHotelTransfer, bool isPackageSearch)
        {
            try
            {
                return new SearchUtil().SegmentSearch(hotelId, MarketId, checkIn, checkOut, roomPlan, includeHotelTransfer, isPackageSearch);
            }
            catch
            {
                return null;
            }
        }
    }
}
