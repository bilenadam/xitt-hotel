﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.TravelCode;
using BilenAdam.TravelCode.Hotel;
using BilenAdam.Data.HotelData;

namespace BilenAdam.Hotels.Charter.Reservation.Locations
{
    public class HotelDestinationRepository 
    {
       
        public HotelDestinationRepository()
        {
           
        }
        public IEnumerable<ICountry> GetCountries()
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return db.HotelDestinations.Where(d => d.Hotels.Any()).Select(c => c.CountryCode).Distinct().ToArray().Select(c => BilenAdam.TravelCode.Countries.CountryDictionary.GetCountry(c)).ToArray();
            }
        }

        public IHotelDestination GetDestination(string code)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                int id = 0;

                if (int.TryParse(code, out id))
                {

                    var dest = db.HotelDestinations.FirstOrDefault(d => d.Id == id);
                    if (dest == null)
                        return null;
                    else
                        return new HotelDestination(dest);
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<IHotelDestination> GetDestionations(string countryCode)
        {
            using (var db = HotelDataContextFactory.Create())
            {
                return db.HotelDestinations.Where(d => d.CountryCode == countryCode && (d.Hotels.Any() || d.HotelDestinations.Any(d2=>d2.Hotels.Any()))).ToArray().Select(d => new HotelDestination(d)).ToArray();
            }
        }

        public string GetIataCode(string code)
        {
            try
            {
                using (var db = HotelDataContextFactory.Create())
                {
                    int id = int.Parse(code);


                    var hr = db.GetHotelDestionationHierarchy(id);

                    foreach (var r in hr)
                    {
                        if (!string.IsNullOrWhiteSpace(r.IataCityCode))
                            return r.IataCityCode;
                    }
                    return null;
                }
            }
            catch
            {
                return null;
            }
        }

        public IHotelDestination GetByIataCode(string iataCityCode)
        {
            using (var db = HotelDataContextFactory.Create())
            {
               

                var dest = db.HotelDestinations.FirstOrDefault(d =>d.IataCityCode == iataCityCode);
                if (dest == null)
                    return null;
                else
                    return new HotelDestination(dest);
            }
        }
    }
}
