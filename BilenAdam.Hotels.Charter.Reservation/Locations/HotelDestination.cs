﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Locations
{
    public class HotelDestination : BilenAdam.TravelCode.Hotel.IHotelDestination
    {
        public HotelDestination()
        {

        }

        public HotelDestination(BilenAdam.Data.HotelData.HotelDestination data)
        {
            this.Id = data.Id;
            this.Name = data.Name;
            this.State = data.State;
            this.Country = BilenAdam.TravelCode.Countries.CountryDictionary.GetCountry(data.CountryCode);
            this.IataCityCode = data.IataCityCode;
            this.ParentId = data.ParentId;

        }
        public int Id { get; set; }


        public string Name
        {
            get;
            set;
        }

        public string State
        {
            get;
            set;
        }

        public string CountryCode
        {
            get
            {
                return Country.Code;
            }
            
        }


        public TravelCode.ICountry Country
        {
            get;
            set;
        }

        public TravelCode.ICountry GetCountry()
        {
            return Country;
        }

        public string GetLocalName(CultureInfo culture)
        {
            return Name;
        }

        public string IataCityCode { get; set; }

        public string Code
        {
            get
            {
                return Id.ToString();
            }
        }

       

        public int? ParentId { get; set; }
    }
}
