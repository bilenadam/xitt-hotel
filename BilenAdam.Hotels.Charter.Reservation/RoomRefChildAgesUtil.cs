﻿using BilenAdam.Reservation.Search.Hotels.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation
{
    public class RoomRefChildAgesUtil
    {
        public byte Adults { get; }
        public byte Childs { get; }

        public byte? FirstChildAge { get; }
        public byte? SecondChildAge { get; }

        public byte? ThirdChildAge { get; }

        public RoomRefChildAgesUtil(IRoomPlan rp)
        {
            Adults = (byte)rp.AdultCount;
            Childs = (byte)rp.ChildCount;

            if(rp.ChildCount > 0)
            {
                int index = 0;
                foreach (var age in rp.ChildAges.OrderByDescending(a=>a))
                {
                    if (index == 0)
                        FirstChildAge = (byte)age;
                    else if (index == 1)
                        SecondChildAge = (byte)age;
                    else if (index == 2)
                        ThirdChildAge = (byte)age;


                    index++;
                }
            }
        }
    }
}
