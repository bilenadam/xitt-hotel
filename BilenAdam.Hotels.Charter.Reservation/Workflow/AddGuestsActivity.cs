﻿using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using BilenAdam.Reservation.Hotels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class AddGuestsActivity
    {
        public int AddGuests(int hotelCharterRoomId, PropertyProduct product, IEnumerable<IRoomGuest> guests)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {


                HotelCharterRoom room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == hotelCharterRoomId);
                var roomProduct = product.RoomProducts.FirstOrDefault();



                var roomEntity = new HotelCharterRoom()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    CheckIn = product.CheckIn,
                    CheckOut = product.CheckOut,
                    HotelCharterReservation = room.HotelCharterReservation,
                    RoomRef = room.RoomRef,
                    HotelPensionId = roomProduct.BoardType,
                    HotelRoomId = roomProduct.RoomType

                };




                int gIndex = 1;
                foreach (var guest in room.HotelCharterGuests.Where(g => g.IsAdult))
                {
                    var guestentity = new HotelCharterGuest()
                    {
                        HotelCharterRoom = roomEntity,
                        IsAdult = guest.IsAdult,
                        No = gIndex++,
                        FirstName = guest.FirstName,
                        LastName = guest.LastName,
                        Dob = guest.Dob,
                        IsMale = guest.IsMale
                    };

                }

                foreach (var guest in guests.Where(g => g.GuestType != RoomGuestType.Child))
                {
                    var travelEntity = guest;

                    var guestentity = new HotelCharterGuest()
                    {
                        HotelCharterRoom = roomEntity,
                        IsAdult = true,
                        No = gIndex++,
                        FirstName = travelEntity.FirstNSecondName(),
                        LastName = travelEntity.LastName,
                        Dob = travelEntity.Dob.Value,
                        IsMale = travelEntity.isMale
                    };

                   
                }

                var children = room.HotelCharterGuests.Where(g => !g.IsAdult).ToList();
                foreach (var guest in guests.Where(g => g.GuestType == RoomGuestType.Child))
                {
                    var traveller = guest;
                    children.Add(new HotelCharterGuest
                    {
                        FirstName = traveller.FirstNSecondName(),
                        LastName = traveller.LastName,
                        Dob = traveller.Dob.Value,
                        IsMale = traveller.isMale,
                        IsAdult = false
                    });

                   
                }

                children.Sort(new Comparison<HotelCharterGuest>((t1, t2) => t1.Dob.CompareTo(t2.Dob)));


                foreach (var child in children)
                {
                    var guestentity = new HotelCharterGuest()
                    {
                        HotelCharterRoom = roomEntity,
                        IsAdult = false,
                        No = gIndex++,
                        FirstName = child.FirstName,
                        LastName = child.LastName,
                        Dob = child.Dob,
                        IsMale = child.IsMale
                    };

                    
                  
                }


              

                new RoomApplyProduct().Apply(roomEntity, roomProduct,room.HasTransferFee(), product.CheckIn,product.CheckOut);
                room.FullRefund();


           

                hdb.SubmitChanges();



                return room.HotelCharterRezervationId;
            }

            
        }
    }
}
