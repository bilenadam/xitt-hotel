﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class MergeRoomsActivity
    {
        public void MergeRooms(int HotelCharterReservationId, PropertyProduct product, int[] selectedRoomIds)
        {
           
            
            using (var hdb = HotelDataContextFactory.Create())
            {
                HotelCharterReservation hotelCharterReservation = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == HotelCharterReservationId);

                var roomProduct = product.RoomProducts.FirstOrDefault();

                var roomEntity = new HotelCharterRoom()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    CheckIn = product.CheckIn,
                    CheckOut = product.CheckOut,
                    HotelCharterReservation = hotelCharterReservation,
                    RoomRef = 100,
                    HotelPensionId = roomProduct.BoardType,
                    HotelRoomId = roomProduct.RoomType

                };

              

                int gIndex = 1;
                foreach (var room in hotelCharterReservation.HotelCharterRooms.Where(r => selectedRoomIds.Contains(r.Id)))
                {
                    if (room.RoomRef < roomEntity.RoomRef)
                        roomEntity.RoomRef = room.RoomRef;

                    foreach (var guest in room.HotelCharterGuests)
                    {
                        var guestentity = new HotelCharterGuest()
                        {
                            HotelCharterRoom = roomEntity,
                            No = gIndex++

                        };

                        guestentity.UpdateFrom(guest, "HotelCharterRoomId", "No");
                            
                    }

                    room.FullRefund();
                }
                new RoomApplyProduct().Apply(roomEntity, roomProduct,hotelCharterReservation.IsTransferIncluded(),product.CheckIn,product.CheckOut);
                hdb.SubmitChanges();
            }


        }
    }
}
