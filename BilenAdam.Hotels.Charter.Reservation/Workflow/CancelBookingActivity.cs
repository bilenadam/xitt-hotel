﻿using BilenAdam.Data.HotelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class CancelBookingActivity
    {

        
        public void CancelBooking(int HotelCharterReservationId, string username, decimal refundRatio)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                var reservation = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == HotelCharterReservationId);


                foreach (var room in reservation.HotelCharterRooms.Where(r=>r.CancelTimeUTC == null))
                {
                    room.CancelTimeUTC = DateTime.UtcNow;

                    int order = 1;
                    if (room.HotelCharterRoomServiceFees.Count > 0)
                        order = room.HotelCharterRoomServiceFees.Max(r => r.ApplyOrder) + 1;

                    room.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                    {
                        Amount = -1 * refundRatio,
                        AmountType = (int)BilenAdam.Reservation.AmountType.Percent,
                        ApplyOrder = order,
                        Code = BilenAdam.Reservation.Hotels.ServiceFeeTypes.Refund.ToString()
                    });

                    
                }







                hdb.SubmitChanges();
            }


        }

       

        public void CancelRoom(int roomId, string username, decimal refundRatio)
        {
            int hotelCharterReservationId = 0;

            using (var hdb = HotelDataContextFactory.Create())
            {
                var room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == roomId);
                hotelCharterReservationId = room.HotelCharterRezervationId;
                room.CancelTimeUTC = DateTime.UtcNow;
                int order = 1;
                if (room.HotelCharterRoomServiceFees.Count > 0)
                    order = room.HotelCharterRoomServiceFees.Max(r => r.ApplyOrder) + 1;

                room.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                {
                    Amount = -1 * refundRatio,
                    AmountType = (int)BilenAdam.Reservation.AmountType.Percent,
                    ApplyOrder = order,
                    Code = BilenAdam.Reservation.Hotels.ServiceFeeTypes.Refund.ToString()
                });

                hdb.SubmitChanges();
            }



        }
    }
}
