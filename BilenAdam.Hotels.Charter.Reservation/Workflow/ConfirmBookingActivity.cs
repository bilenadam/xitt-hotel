﻿using BilenAdam.Data.HotelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class ConfirmBookingActivity
    {
        public void ConfirmBooking(int HotelCharterReservationId, string username)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                var reservation = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == HotelCharterReservationId && r.ConfirmedTimeUTC == null && r.HotelCharterRooms.Any(r2 => r2.CancelTimeUTC == null));
                if(reservation != null)
                {
                    reservation.ConfirmedTimeUTC = DateTime.UtcNow;
                }
                   

                hdb.SubmitChanges();
            }


        }
    }
}
