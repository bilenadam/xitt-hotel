﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class SetRoomServiceFeeActivity
    {
        public int SetServiceFee(int id, decimal feeAmount)
        {
            int roomId = 0;
            using (var db = HotelDataContextFactory.Create())
            {
                var sf = db.HotelCharterRoomServiceFees.First(i => i.Id == id);
                if (sf.GetServiceFeeType() == BilenAdam.Reservation.Hotels.ServiceFeeTypes.Refund && sf.AmountType == (int)BilenAdam.Reservation.AmountType.Percent)
                    sf.Amount = feeAmount * -1;
                else
                    sf.Amount = feeAmount;

                roomId = sf.HotelCharterRoomId;
                db.SubmitChanges();
            }
            using (var hdb = HotelDataContextFactory.Create())
            {
                var room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == roomId);
                return room.HotelCharterRezervationId;
            }

        }
    }
}
