﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using BilenAdam.Reservation.Hotels;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class AddRoomActivity
    {
        public void AddGuests(int hotelCharterReservationId, PropertyProduct product, IEnumerable<IRoomGuest> guests)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                HotelCharterReservation hotelCharterReservation = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == hotelCharterReservationId);


                var roomProduct = product.RoomProducts.FirstOrDefault();

                var hotelCharterRoom = new HotelCharterRoom()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    CheckIn = product.CheckIn,
                    CheckOut = product.CheckOut,
                    HotelCharterReservation = hotelCharterReservation,
                    RoomRef = hotelCharterReservation.HotelCharterRooms.Max(r => r.RoomRef) + 1,
                    HotelPensionId = roomProduct.BoardType,
                    HotelRoomId = roomProduct.RoomType

                };

            



                int gIndex = 1;

                foreach (var guest in guests.Where(g => g.GuestType != RoomGuestType.Child))
                {

                    var traveller = guest;
                    var guestentity = new HotelCharterGuest()
                    {
                        HotelCharterRoom = hotelCharterRoom,
                        IsAdult = true,
                        No = gIndex++,
                        FirstName = traveller.FirstNSecondName(),
                        LastName = traveller.LastName,
                        Dob = traveller.Dob.Value,
                        IsMale = traveller.isMale
                    };
                }



                foreach (var child in guests.Where(g => g.GuestType == RoomGuestType.Child))
                {
                    var traveller = child;
                    var guestentity = new HotelCharterGuest()
                    {
                        HotelCharterRoom = hotelCharterRoom,
                        IsAdult = false,
                        No = gIndex++,
                        FirstName = traveller.FirstNSecondName(),
                        LastName = traveller.LastName,
                        Dob = traveller.Dob.Value,
                        IsMale = traveller.isMale
                    };
                }
                new RoomApplyProduct().Apply(hotelCharterRoom, roomProduct, hotelCharterReservation.IsTransferIncluded(),product.CheckIn,product.CheckOut);
                hdb.SubmitChanges();
            }

        }
    }
}
