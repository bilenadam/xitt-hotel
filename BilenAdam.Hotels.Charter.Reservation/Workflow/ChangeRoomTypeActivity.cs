﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class ChangeRoomTypeActivity
    {
        public void ChangeRoomType(int HotelCharterRoomId, PropertyProduct product)
        {
            int rezId;int roomRef;
            using (var hdb = HotelDataContextFactory.Create())
            {
                HotelCharterRoom room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == HotelCharterRoomId);
                rezId = room.HotelCharterRezervationId;
                roomRef = room.RoomRef;
                var roomProduct = product.RoomProducts.FirstOrDefault();

                var roomEntity = new HotelCharterRoom()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    CheckIn = product.CheckIn,
                    CheckOut = product.CheckOut,
                    HotelCharterReservation = room.HotelCharterReservation,
                    RoomRef = room.RoomRef,
                    HotelPensionId = roomProduct.BoardType,
                    HotelRoomId = roomProduct.RoomType

                };

                int gIndex = 1;
                foreach (var guest in room.HotelCharterGuests)
                {

                    var guestentity = new HotelCharterGuest()
                    {
                        HotelCharterRoom = roomEntity,
                        No = gIndex++
                    };

                    guestentity.UpdateFrom(guest, "HotelCharterRoomId", "No", "TravellerId");



                }


                new RoomApplyProduct().Apply(roomEntity, roomProduct, room.HasTransferFee(), product.CheckIn, product.CheckOut);





                room.FullRefund();
                hdb.SubmitChanges();

            }

        
        }
    }
}
