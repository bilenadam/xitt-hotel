﻿using System;
using System.Linq;
using BilenAdam.Data.HotelData;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class AddTransferActivity
    {
        public void AddTransfer(int rezId)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                
                var rooms = hdb.HotelCharterRooms.Where(r => r.HotelCharterRezervationId == rezId).ToArray();
                foreach (var room in rooms)
                {
                    if (room.HasTransferFee())
                        continue;

                    var sf = room.HotelCharterRoomServiceFees.FirstOrDefault(s => s.Code == BilenAdam.Reservation.Hotels.ServiceFeeTypes.HandlingFee.ToString());

                    if(sf == null)
                    {
                        int order = 0;
                        if (room.HotelCharterRoomServiceFees.Count > 0)
                            order = Math.Max(order, room.HotelCharterRoomServiceFees.Max(m => m.ApplyOrder));

                        order++;

                        sf = new HotelCharterRoomServiceFee
                        {
                            ApplyOrder = order,
                            HotelCharterRoom = room,
                            Amount = 0,
                            AmountType = (int)BilenAdam.Reservation.AmountType.Fix
                        };



                    }
                    sf.Code = BilenAdam.Reservation.Hotels.ServiceFeeTypes.TransferFee.ToString();

                    var season =  room.HotelRoom.Hotel.HotelSeasons.FirstOrDefault(s => s.BeginDate <= room.CheckIn && s.EndDate >= room.CheckIn);

                    sf.Amount = season.TransferFee * room.HotelCharterGuests.Count(g => g.IsAdult);
                    foreach (var chd in room.HotelCharterGuests.Where(g=>!g.IsAdult))
                    {
                        bool isadtfee = true;
                        foreach (var csf in season.HotelSeasonChildrenFees)
                        {

                            if(csf.MaxAge >= chd.CalculateAge(room.CheckIn))
                            {
                                sf.Amount += csf.TransferFee;
                                isadtfee = false;
                                break;
                            }
                        }

                        if (isadtfee)
                        {
                            sf.Amount += season.TransferFee;
                        }
                    }


                    hdb.SubmitChanges();

                  
                }


            }
        }
    }
}
