﻿using BilenAdam.Data.HotelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class ChangeDatesActivity
    {
        public void ChangeDates(int rezId, DateTime checkIn, DateTime checkOut)
        {
            int stay = (int)Math.Ceiling(checkOut.Subtract(checkIn).TotalDays);
            using (var hdb = HotelDataContextFactory.Create())
            {
                var rez = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == rezId);

                if (rez != null)
                {
                    var promotionHelper = new HotelPromotionHelper(rez.Hotel.HotelPromotions.Where(p => p.MarketId == rez.MarketId).ToArray());

                    foreach (var room in rez.HotelCharterRooms.Where(r=>r.CancelTimeUTC == null))
                    {
                        var adultCount = room.HotelCharterGuests.Count(g => g.IsAdult);
                        var childCount = room.HotelCharterGuests.Count(g => !g.IsAdult);
                        int[] childAges = room.HotelCharterGuests.Where(g => !g.IsAdult).OrderBy(g => g.No).Select(g => g.CalculateAge(checkOut)).ToArray();
                        room.CheckIn = checkIn;
                        room.CheckOut = checkOut;
                        for (DateTime i = checkIn; i < checkOut; i = i.AddDays(1))
                        {
                            if (room.HotelCharterRoomDateDetails.Count(d => d.Date == i) == 0)
                            {

                                var fares = hdb.HotelFareDs.Where(f => f.HotelFareH.BeginDate <= i && f.HotelFareH.EndDate >= i && f.PensionId == room.HotelPensionId && room.HotelRoomId == f.RoomId && room.HotelCharterReservation.MarketId == f.HotelFareH.MarketId && f.Adults == adultCount && f.Childs == childCount);

                                if (childCount > 0)
                                {
                                    fares = fares.Where(f => f.FirstChildMaxAge >= childAges[0] && f.FirstChildMinAge <= childAges[0]);
                                }
                                if (childCount > 1)
                                {
                                    fares = fares.Where(f => f.SecondChildMaxAge >= childAges[1] && f.SecondChildMinAge <= childAges[1]);
                                }
                                if (childCount > 2)
                                {
                                    fares = fares.Where(f => f.ThirdChildMaxAge >= childAges[2] && f.ThirdChildMinAge <= childAges[2]);
                                }

                                var fare = fares.FirstOrDefault();
                                if (fare != null)
                                {
                                    var roomDateDetail = new HotelCharterRoomDateDetail { Date = i, Price = fare.Fare, HotelFareD = fare };
                                    room.HotelCharterRoomDateDetails.Add(roomDateDetail);

                                    var promotions = promotionHelper.FilterPromotions(room.HotelRoomId, i, checkIn, stay);

                                    if (promotions != null)
                                    {
                                        int applyOrder = 1;
                                        foreach (var p in promotions)
                                        {
                                            if (p.HotelPromotionType == (int)BilenAdam.Reservation.Hotels.Promotions.PromotionType.TurboEarlyBooking)
                                            {
                                                decimal discount = p.DiscountRate * room.HotelCharterGuests.Count(g => g.IsAdult);

                                                if (p.ChildDiscount != null)
                                                {
                                                    discount += (p.ChildDiscount.Value * room.HotelCharterGuests.Count(g => !g.IsAdult));
                                                }

                                                roomDateDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                                                {
                                                    Amount = p.DiscountRate,
                                                    AmountType = p.DiscountType,
                                                    HotelPromotionId = p.Id,
                                                    ApplyOrder = applyOrder
                                                });

                                            }
                                            else
                                            {
                                                roomDateDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                                                {
                                                    Amount = p.DiscountRate,
                                                    AmountType = p.DiscountType,
                                                    HotelPromotionId = p.Id,
                                                    ApplyOrder = applyOrder
                                                });
                                            }

                                            applyOrder++;
                                        }
                                    }

                                }
                            }
                        }

                        hdb.HotelCharterRoomDateDetails.DeleteAllOnSubmit(d => d.HotelCharterRoomId == room.Id && (d.Date < checkIn || d.Date >= checkOut));

                    }
                }
                hdb.SubmitChanges();
            }
          
        }
    }
}
