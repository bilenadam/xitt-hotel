﻿using BilenAdam.Data.HotelData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class AddMarkupActivity
    {
        public int AddMarkup(int roomId, decimal markup)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                var room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == roomId);
                int order = 0;
                if (room.HotelCharterRoomServiceFees.Count > 0)
                    order = Math.Max(order, room.HotelCharterRoomServiceFees.Max(m => m.ApplyOrder));

                order++;

                if (room.CancelTimeUTC != null) // add as refund
                {
                    var refundFee = room.HotelCharterRoomServiceFees.FirstOrDefault(f => f.Code == BilenAdam.Reservation.Hotels.ServiceFeeTypes.Refund.ToString() && f.AmountType == (int)BilenAdam.Reservation.AmountType.Fix);

                    if (refundFee == null)
                    {
                        room.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee { ApplyOrder = order, Code = BilenAdam.Reservation.Hotels.ServiceFeeTypes.Refund.ToString(), Amount = markup, AmountType = (int)BilenAdam.Reservation.AmountType.Fix });
                    }
                    else
                        refundFee.Amount += markup;
                }
                else
                {
                    var markupFee = room.HotelCharterRoomServiceFees.FirstOrDefault(f => f.Code == BilenAdam.Reservation.Hotels.ServiceFeeTypes.Markup.ToString() && f.AmountType == (int)BilenAdam.Reservation.AmountType.Fix);
                    if (markupFee == null)
                    {
                        room.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee { ApplyOrder = order, Code = BilenAdam.Reservation.Hotels.ServiceFeeTypes.Markup.ToString(), Amount = markup, AmountType = (int)BilenAdam.Reservation.AmountType.Fix });
                    }
                    else
                        markupFee.Amount += markup;
                }






                hdb.SubmitChanges();



                return room.HotelCharterRezervationId;

            }
        }
    }
}
