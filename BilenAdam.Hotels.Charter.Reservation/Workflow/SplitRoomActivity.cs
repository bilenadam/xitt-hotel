﻿using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class SplitRoomActivity
    {
        public void SplitRoom(int HotelCharterRoomId, PropertyProduct product, int[] selectedGuestsId)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                HotelCharterRoom room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == HotelCharterRoomId);

              
                foreach (var roomProduct in product.RoomProducts)
                {
                    var roomEntity = new HotelCharterRoom()
                    {
                        CreateTimeUTC = DateTime.UtcNow,
                        CheckIn = product.CheckIn,
                        CheckOut = product.CheckOut,
                        HotelCharterReservation = room.HotelCharterReservation,
                        RoomRef = room.RoomRef + roomProduct.RoomReference.RoomRef - 1,
                        HotelPensionId = roomProduct.BoardType,
                        HotelRoomId = roomProduct.RoomType

                    };

                 

                    int gIndex = 1;
                    foreach (var guest in room.HotelCharterGuests)
                    {

                        if ((roomProduct.RoomReference.RoomRef == 1 && !selectedGuestsId.Contains(guest.Id)) || (roomProduct.RoomReference.RoomRef == 2 && selectedGuestsId.Contains(guest.Id)))
                        {
                            var guestentity = new HotelCharterGuest()
                            {
                                HotelCharterRoom = roomEntity,
                                No = gIndex++
                            };
                            guestentity.UpdateFrom(guest, "HotelCharterRoomId", "No");

                        }

                    }


                    new RoomApplyProduct().Apply(roomEntity, roomProduct,room.HasTransferFee(),product.CheckIn,product.CheckOut);
                }


                room.FullRefund();
                hdb.SubmitChanges();

            }
        }
    }
}
