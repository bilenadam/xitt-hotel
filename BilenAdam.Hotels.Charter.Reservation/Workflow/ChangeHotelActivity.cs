﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class ChangeHotelActivity
    {
        public int ChangeHotel(int hotelCharterReservationId, PropertyProduct product)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                HotelCharterReservation oldReservation = hdb.HotelCharterReservations.FirstOrDefault(r => r.Id == hotelCharterReservationId);

                HotelCharterReservation newReservation = new HotelCharterReservation
                {
                    ConfirmedTimeUTC = oldReservation.ConfirmedTimeUTC,
                    CreateTimeUTC = oldReservation.CreateTimeUTC,
                    Currency = oldReservation.Currency,
                    HotelId = product.HotelId,
                    MarketId = product.MarketId
                };
             
               


                foreach (var roomProduct in product.RoomProducts)
                {
                    var room = oldReservation.HotelCharterRooms.FirstOrDefault(r => r.CancelTimeUTC == null && r.RoomRef == roomProduct.RoomReference.RoomRef);

                    var roomEntity = new HotelCharterRoom()
                    {
                        CreateTimeUTC = DateTime.UtcNow,
                        CheckIn = product.CheckIn,
                        CheckOut = product.CheckOut,
                        HotelCharterReservation = newReservation,
                        RoomRef = room.RoomRef,
                        HotelPensionId = roomProduct.BoardType,
                        HotelRoomId = roomProduct.RoomType

                    };

                    int gIndex = 1;
                    foreach (var guest in room.HotelCharterGuests)
                    {

                        var guestentity = new HotelCharterGuest()
                        {
                            HotelCharterRoom = roomEntity,
                            No = gIndex++
                        };

                        guestentity.UpdateFrom(guest, "HotelCharterRoomId", "No", "TravellerId");


                    }
                    room.FullRefund();

                    new RoomApplyProduct().Apply(roomEntity, roomProduct,oldReservation.IsTransferIncluded(),product.CheckIn,product.CheckOut);

                }
                hdb.HotelCharterReservations.InsertOnSubmit(newReservation);
                hdb.SubmitChanges();
                return newReservation.Id;

            }
        }
    }
}
