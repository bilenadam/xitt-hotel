﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class RemoveGuestsActivity
    {

        public void RemoveGuests(int HotelCharterRoomId, PropertyProduct product, int[] selectedGuestsId)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                HotelCharterRoom hotelCharterRoom = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == HotelCharterRoomId);
              

                var roomProduct = product.RoomProducts.FirstOrDefault();

                var roomEntity = new HotelCharterRoom()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    CheckIn = product.CheckIn,
                    CheckOut = product.CheckOut,
                    HotelCharterReservation = hotelCharterRoom.HotelCharterReservation,
                    RoomRef = hotelCharterRoom.RoomRef,
                    HotelPensionId = roomProduct.BoardType,
                    HotelRoomId = roomProduct.RoomType

                };

                int gIndex = 1;
                foreach (var guest in hotelCharterRoom.HotelCharterGuests)
                {

                    if (!selectedGuestsId.Contains(guest.Id))
                    {
                        var guestentity = new HotelCharterGuest()
                        {
                            HotelCharterRoom = roomEntity,
                            No = gIndex++
                        };
                        guestentity.UpdateFrom(guest, "HotelCharterRoomId", "No");
                    }
                   

                }



                new RoomApplyProduct().Apply(roomEntity, roomProduct, roomEntity.HasTransferFee(),product.CheckIn,product.CheckOut);


                hotelCharterRoom.FullRefund();

                hdb.SubmitChanges();

            }
        }
    }
}
