﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;
using BilenAdam.Hotels.Charter.Reservation.DBSearch;
using BilenAdam.Reservation.Hotels;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class RoomApplyProduct
    {
        public void Apply(HotelCharterRoom room, RoomProduct product, bool includeTransfer, DateTime checkIn , DateTime checkOut)
        {
            using (var hotelDB = HotelDataContextFactory.Create())
            {
                var rc = new RoomRefChildAgesUtil(product.RoomReference);

            
                var fees = hotelDB.GetRoomFees(product.HotelId, checkIn, rc.Adults,rc.Childs, rc.FirstChildAge, rc.SecondChildAge, rc.ThirdChildAge, includeTransfer).FirstOrDefault();

                // add handling or transfer fees
                room.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                {
                    AmountType = (int)BilenAdam.Reservation.AmountType.Fix,
                    Amount = fees.Fees ?? 0,
                    ApplyOrder = 1,
                    Code = includeTransfer ? ServiceFeeTypes.TransferFee.ToString() : ServiceFeeTypes.HandlingFee.ToString()
                });
                // add commission
                room.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                {
                    AmountType = (int)BilenAdam.Reservation.AmountType.Percent,
                    Amount = fees.Commission,
                    ApplyOrder = 2,
                    Code = ServiceFeeTypes.Comission.ToString()
                });


                var dateProducts = hotelDB.GetDateProducts(product.HotelId,product.RoomType,product.BoardType,checkIn,checkOut, rc.Adults, rc.Childs, rc.FirstChildAge, rc.SecondChildAge, rc.ThirdChildAge, includeTransfer, DateTime.Now).ToArray();

                foreach (var roomDate in dateProducts)
                {
                    var roomDetail = new HotelCharterRoomDateDetail()
                    {
                        Date = roomDate.QuotaDate,
                        HotelFareDId = roomDate.FareDId,
                        Price = roomDate.Fare,
                        HotelCharterRoom = room
                    };

                    if (roomDate.IsSPOEnabled ?? false)
                        continue;


                    int applyOrder = 1;
                    if (roomDate.EBId != null)
                    {
                        var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.EBId);
                        roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                        {
                            ApplyOrder = applyOrder,
                            AmountType = pr.DiscountType,
                            Amount = pr.DiscountRate,
                            HotelPromotionId = pr.Id
                        });

                        applyOrder++;
                    }
                    if (roomDate.TEBId != null)
                    {
                        var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.TEBId);
                        roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                        {
                            ApplyOrder = applyOrder,
                            AmountType = pr.DiscountType,
                            Amount = pr.DiscountRate * (int)rc.Adults,
                            HotelPromotionId = pr.Id
                        });

                        applyOrder++;
                    }

                    if (roomDate.REBId != null)
                    {
                        var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.REBId);
                        roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                        {
                            ApplyOrder = applyOrder,
                            AmountType = pr.DiscountType,
                            Amount = pr.DiscountRate,
                            HotelPromotionId = pr.Id
                        });

                        applyOrder++;
                    }

                    if (roomDate.LSId != null)
                    {
                        var pr = hotelDB.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.LSId);
                        roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                        {
                            ApplyOrder = applyOrder,
                            AmountType = pr.DiscountType,
                            Amount = pr.DiscountRate,
                            HotelPromotionId = pr.Id
                        });

                        applyOrder++;
                    }

                    if (roomDate.IsDP)
                    {

                        roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                        {
                            ApplyOrder = applyOrder,
                            AmountType = (int)BilenAdam.Reservation.AmountType.Percent,
                            Amount = 1,
                            HotelPromotionId = roomDate.DPId.Value
                        });

                        applyOrder++;
                    }


                }
            }

        }
    }
}
