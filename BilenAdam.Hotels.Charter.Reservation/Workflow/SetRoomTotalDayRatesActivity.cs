﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilenAdam.Data.HotelData;

namespace BilenAdam.Hotels.Charter.Reservation.Workflow
{
    public class SetRoomTotalDayRatesActivity
    {
        public int SetTotalDayRates(int roomId, decimal totalDayRates)
        {
            using (var hdb = HotelDataContextFactory.Create())
            {
                var roomDates = hdb.HotelCharterRoomDateDetails.Where(r => r.HotelCharterRoomId == roomId).ToArray();
                var dayPrice = totalDayRates / roomDates.Length;

                foreach (var roomDate in roomDates)
                {
                    roomDate.Price = dayPrice;
                    if(roomDate.HotelCharterRoomDatePromotions.Count > 0)
                        hdb.HotelCharterRoomDatePromotions.DeleteAllOnSubmit(roomDate.HotelCharterRoomDatePromotions.ToArray());
                }


                hdb.SubmitChanges();
            }


            using (var hdb = HotelDataContextFactory.Create())
            {

                var room = hdb.HotelCharterRooms.FirstOrDefault(r => r.Id == roomId);
           

                return room.HotelCharterRezervationId;
            }
        }
    }
}
