﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using BilenAdam.XITT.WebApi.Models.Hotel.MetaData;
using BilenAdam.XITT.WebApi.Workflow.Authentication;
using BilenAdam.XITT.WebApi.Workflow.HotelData;

namespace BilenAdam.XITT.WebApi.Controllers
{
    public class HotelController : ApiController
    {
        public async Task<HotelMetaData> Get(string apiKey, int id)
        {
            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(apiKey);
                if (profile == null)
                    return null;

                return await new HotelMetaDataCacheUtil().Get(id);
                
            }
            catch
            {
                return null;
            }
        }

        public async Task<HotelMetaData[]> Get(string apiKey)
        {
            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(apiKey);
                if (profile == null)
                    return null;

                var list = await new HotelMetaDataCacheUtil().Get();
                return list.ToArray();
            }
            catch
            {
                return null;
            }
        }
    }
}