﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using BilenAdam.XITT.WebApi.Workflow.Authentication;
using BilenAdam.XITT.WebApi.Models.Hotel.Search;

namespace BilenAdam.XITT.WebApi.Controllers
{
    public class HotelMultiAvailabilityController : ApiController
    {
        public async Task<AvailabilitySearchResult> Get(string apiKey, string dest, string checkin, string checkout,int adt1,int chd1=0, int adt2=0, int chd2=0,  int adt3=0, int chd3=0, string ages1 = null, string ages2 = null, string ages3 = null)
        {
            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(apiKey);
                if (profile == null)
                {
                    return new AvailabilitySearchResult
                    {
                        Count = 0,
                        Error = "UnAuthorized Access"
                    };
                }

                return await new Workflow.HotelMultiAvailability.Activity().Search(apiKey,
                    dest,
                    StringParseUtils.ParseDate(checkin),
                    StringParseUtils.ParseDate(checkout),
                    adt1,
                    chd1,
                    StringParseUtils.ParseIntArray(ages1),
                    adt2,
                    chd2,
                    StringParseUtils.ParseIntArray(ages2),
                    adt3,
                    chd3,
                    StringParseUtils.ParseIntArray(ages3));


            }
            catch 
            {
                return new AvailabilitySearchResult { Count = 0, Error = "Unknown Error" };
            }
        }
    }
}