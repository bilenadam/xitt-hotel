﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using BilenAdam.XITT.WebApi.Models.Hotel.MetaData;
using BilenAdam.XITT.WebApi.Workflow.Authentication;
using BilenAdam.XITT.WebApi.Workflow.HotelData;


namespace BilenAdam.XITT.WebApi.Controllers
{
    public class BoardTypeController : ApiController
    {
        public async Task<string[]> Get(string apiKey)
        {
            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(apiKey);
                if (profile == null)
                    return null;

                return await new BoardTypeCacheUtil().Get();
            }
            catch
            {
                return null;
            }
        }
    }
}