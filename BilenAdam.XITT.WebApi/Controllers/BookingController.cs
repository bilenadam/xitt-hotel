﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using BilenAdam.XITT.WebApi.Workflow.Authentication;


namespace BilenAdam.XITT.WebApi.Controllers
{
    public class BookingController : ApiController
    {

        [HttpGet]
        public async Task<BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response.Booking> Show(string apikey, string pnr)
        {

            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(apikey);
                if (profile == null)
                {
                    return new BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response.Booking
                    {
                        Error = "UnAuthorized Access"
                    };
                }


                return new Workflow.Booking.RetrieveActivity(profile).Retrieve(pnr);

            }
            catch
            {
                return new BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response.Booking { Error = "Unknown Error" };
            }
        }
        [HttpPost]
        public async Task<BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response.Booking> Book(BilenAdam.XITT.WebApi.Models.Hotel.Booking.Request.Booking bookingRequest)
        {

            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(bookingRequest.ApiKey);
                if (profile == null)
                {
                    return new BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response.Booking
                    {
                        Error = "UnAuthorized Access"
                    };
                }


                return await new Workflow.Booking.BookingActivity(profile).Book(bookingRequest);

            }
            catch
            {
                return new BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response.Booking {  Error = "Unknown Error" };
            }
        }
    }
}