﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Threading.Tasks;
using BilenAdam.XITT.WebApi.Models.Hotel.MetaData;
using BilenAdam.XITT.WebApi.Workflow.Authentication;
using BilenAdam.XITT.WebApi.Workflow.HotelData;
using BilenAdam.EF.Hotel.Charter;
using System.Data.Entity;

namespace BilenAdam.XITT.WebApi.Controllers
{
    public class HotelDestinationController : ApiController
    {
        public async Task<HotelDestinationTree> Get(string apiKey)
        {
            try
            {
                var profile = await new AuthenticationCacheUtil().Authenticate(apiKey);
                if (profile == null)
                    return null;

                var model = HotelDeatinationTreeCache.Get();
                if (model == null)
                {


                    using (var db = new HotelCharterDBEntities())
                    {
                        var data = await db.GetHotelDestionationTree().OrderBy(i=>i.ParentId).ThenBy(i=>i.Id).ToListAsync();
                        model = new Models.Hotel.MetaData.HotelDestinationTree();
                        model.Destinations = data.Select(i => new Models.Hotel.MetaData.HotelDestination
                        {
                            ID = i.Id,
                            CountryCode = i.CountryCode,
                            Name = i.Name,
                            IataCode = i.IataCityCode,
                            ParentID = i.ParentId
                        }).ToArray();
                        HotelDeatinationTreeCache.Set(model);
                       
                    }
                }
                return model;
            }
            catch
            {
                return null;
            }
        }
    }
}