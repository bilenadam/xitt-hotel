﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Search
{
    public class Offer
    {
        public string Price { get; set; }
        public string Currency { get; set; }
        public string DeepLink { get; set; }

        public int HotelId { get; set; }

        public Room[] Rooms { get; set; }
        public int RoomCount { get; set; }

    }
}