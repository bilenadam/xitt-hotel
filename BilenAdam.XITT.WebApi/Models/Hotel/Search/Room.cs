﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Search
{
    public class Room
    {
        public int RoomRef { get; set; }
        public string RoomType { get; set; }
        public int RoomTypeId { get; set; }
        public string BoardType { get; set; }
        public int BoardTypeId { get; set; }

        public string Price { get; set; }

    }
}