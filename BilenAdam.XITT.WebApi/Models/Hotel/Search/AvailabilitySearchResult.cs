﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Search
{
    public class AvailabilitySearchResult
    {
        public int Count { get; set; }
        public string Error { get; set; }
        public Offer[] Offers { get; set; }
    }
}