﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response
{
    public class Guest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsMale { get; set; }
        public string DateOfBirth { get; set; }

        public bool IsAdult { get; set; }
    }
}