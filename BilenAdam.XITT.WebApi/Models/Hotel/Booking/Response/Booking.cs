﻿using BilenAdam.XITT.WebApi.Models.Hotel.Booking.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response
{
    public class Booking
    {
        public string Error { get; set; }
        public string Pnr { get; set; }
        public int HotelId { get; set; }
        public string Price { get; set; }
        public string Currency { get; set; }

        public Room[] Rooms { get; set; }
    }
}