﻿using BilenAdam.EF.Hotel.Charter;
using BilenAdam.XITT.WebApi.Models.Hotel.Booking.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response
{
    public class Room
    {
        public int RoomRef { get; set; }
        public string RoomType { get; set; }
        public string BoardType { get; set; }

        public string CheckIn { get; set; }
        public string CheckOut { get; set; }

        public string Price { get; set; }
        public string Currency { get; set; }

        public bool IsCanceled { get; set; }

        public Guest[] Guests { get; set; }
    }
}