﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Booking.Request
{
    public class Room
    {
        public int RoomRef { get; set; }
        public int RoomTypeId { get; set; }
        public int BoardTypeId { get; set; }
        public Guest[] Guests { get; set; }
    }
}