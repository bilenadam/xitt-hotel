﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.Booking.Request
{
    public class Booking
    {
        public string ApiKey { get; set; }
        public int HotelId { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }

        public Room[] Rooms { get; set; }

        public bool IncludeTransfer { get; set; }
    }
}