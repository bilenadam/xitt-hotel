﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.MetaData
{
    public class HotelDestinationTree
    {
        public HotelDestination[] Destinations { get; set; }
    }
}