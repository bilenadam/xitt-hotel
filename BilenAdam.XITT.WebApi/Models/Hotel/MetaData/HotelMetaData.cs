﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.MetaData
{
    public class HotelMetaData
    {
        public int ID { get; set; }
        public string GIATAID { get; set; }
        public string Name { get; set; }
        public int Stars { get; set; }
        public HotelDestination Destination { get; set; }
        public HotelAddress Address { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Web { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string HotelDescription { get; set; }
        public string RoomDescription { get; set; }
        public string LocationDescription { get; set; }
        public string[] Images { get; set; }
    }
}