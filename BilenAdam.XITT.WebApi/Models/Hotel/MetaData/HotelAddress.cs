﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Models.Hotel.MetaData
{
    public class HotelAddress
    {
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
    }
}