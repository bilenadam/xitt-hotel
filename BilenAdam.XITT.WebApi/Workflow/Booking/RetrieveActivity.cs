﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using XITTData = BilenAdam.EF.xITT.Data;
using Res = BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response;
using BilenAdam.EF.Hotel.Charter;

namespace BilenAdam.XITT.WebApi.Workflow.Booking
{
    public class RetrieveActivity
    {
        public XITTData.WSProfile Profile { get; }
        public RetrieveActivity(XITTData.WSProfile profile)
        {
            Profile = profile;
        }

        public Res.Booking Retrieve(string pnr)
        {
            return Retrieve(int.Parse(pnr));
        }

        public Res.Booking Retrieve(int id)
        {
            using (var db = new HotelCharterDBEntities())
            {
                var rez = db.HotelCharterReservations.FirstOrDefault(r => r.Id == id);


                return new Res.Booking
                {
                    Pnr = rez.Id.ToString(),
                    Currency = rez.Currency,
                    HotelId = rez.HotelId,
                    Price = rez.GetTotalRate().ToApiMoneyString(),
                     Rooms = rez.GetRooms().Select(r=>
                        new Res.Room
                        {
                              RoomRef = r.RoomRef,
                              BoardType = r.HotelPension.Name,
                              RoomType = r.HotelRoom.Name,
                              CheckIn = r.CheckIn.ToApiDateString(),
                              CheckOut = r.CheckOut.ToApiDateString(),
                              Currency = rez.Currency,
                              Price = rez.GetTotalRate().ToApiMoneyString(),
                              IsCanceled = r.CancelTimeUTC != null,
                               Guests = r.HotelCharterGuests.Select(g=> 
                                new Res.Guest
                                {
                                     DateOfBirth = g.Dob.ToApiDateString(),
                                     IsAdult = g.IsAdult,
                                     FirstName = g.FirstName,
                                     LastName = g.LastName,
                                     IsMale = g.IsMale
                                }
                               ).ToArray()
                        }
                     ).ToArray()
                };
            }

        }

    }
}