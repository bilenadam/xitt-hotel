﻿using XITTData = BilenAdam.EF.xITT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Req = BilenAdam.XITT.WebApi.Models.Hotel.Booking.Request;
using Res = BilenAdam.XITT.WebApi.Models.Hotel.Booking.Response;
using BilenAdam.EF.Hotel.Charter;
using System.Data.Entity;
using System.Runtime.InteropServices.WindowsRuntime;

namespace BilenAdam.XITT.WebApi.Workflow.Booking
{
    public class BookingActivity
    {
        public XITTData.WSProfile Profile { get; }
        public BookingActivity(XITTData.WSProfile profile)
        {
            Profile = profile;
        }

        public static int CalculateAge(DateTime dob, DateTime date)
        {
            int age = date.Year - dob.Year;
            if (dob > date.AddYears(-age)) age--;

            return age;
        }
        public async  Task<Res.Booking> Book(Req.Booking bookingRequest)
        {

            // todo : validate request 
            DateTime checkIn = StringParseUtils.ParseDate(bookingRequest.CheckIn);
            DateTime checkOut = StringParseUtils.ParseDate(bookingRequest.CheckOut);
            int nights = (int)checkOut.Subtract(checkIn).TotalDays;

            using (var db = new HotelCharterDBEntities())
            {
                //  check for room availability

                Dictionary<int, int> roomRequests = new Dictionary<int, int>();

                foreach (var room in bookingRequest.Rooms)
                {
                    if (!roomRequests.ContainsKey(room.RoomTypeId))
                        roomRequests.Add(room.RoomTypeId, 0);

                    roomRequests[room.RoomTypeId]++;
                }

                foreach (var roomReq in roomRequests)
                {
                    var av = await db.vHotelQuotas.Where(q => q.RoomId == roomReq.Key && q.QuotaDate >= checkIn && q.QuotaDate < checkOut && q.IsStopSale == 0 && q.Remain >= roomReq.Value).CountAsync();
                    if(av < nights)
                    {
                        return new Res.Booking { Error = "Room Unavailable" };
                    }
                }
                // todo : create booking

                var hotel = db.Hotels.First(h => h.Id == bookingRequest.HotelId);

                //BilenAdam.Log.LogContext.LogMessage($"Starting to book {hotel.Name}, {product.CheckIn} - {product.CheckOut}");



                var charterRezEntity = new HotelCharterReservation()
                {
                    CreateTimeUTC = DateTime.UtcNow,
                    HotelId = bookingRequest.HotelId,
                    MarketId = 1,//bookingRequest.MarketId,
                    Currency = "EUR"//CurrencyString.EUR
                    // todo : add contact id from profile
                };


                foreach (var room in bookingRequest.Rooms)
                {
                    //var roomProduct = propertyProduct.RoomProducts.FirstOrDefault(r => r.RoomReference.RoomRef == room.RoomRef);

                    //BilenAdam.Log.LogContext.LogMessage($"adding room {roomProduct.RoomTypeName} - {roomProduct.BoardTypeName}");


                    // charter room
                    var charterRoomEntity = new HotelCharterRoom()
                    {
                        CreateTimeUTC = DateTime.UtcNow,
                        CheckIn = checkIn, //propertyProduct.CheckIn,
                        CheckOut = checkOut,// propertyProduct.CheckOut,
                        HotelCharterReservation = charterRezEntity,
                        RoomRef = room.RoomRef,
                        HotelPensionId = room.BoardTypeId,
                        HotelRoomId = room.RoomTypeId


                    };

                    int gIndex = 1;

                    byte adults = 0; byte childs = 0;
                    byte? child1 = null, child2 = null, child3 = null;


                    foreach (var guest in room.Guests)
                    {
                        var traveller = guest;

                        var dob = StringParseUtils.ParseDate(guest.DateOfBirth);

                        // charter record guest
                        var charterGuestEntity = new HotelCharterGuest()
                        {
                            HotelCharterRoom = charterRoomEntity,
                            IsAdult = guest.IsAdult,
                            No = gIndex++,
                            Dob = dob,
                            FirstName = traveller.FirstName,
                            IsMale = traveller.IsMale,
                            LastName = traveller.LastName
                        };



                        if (!guest.IsAdult)
                        {
                            byte age = (byte)CalculateAge(dob,checkIn);
                            childs++;
                            if (child1 == null)
                                child1 = age;
                            else if (child2 == null)
                                child2 = age;
                            else if (child3 == null)
                                child3 = age;

                        }
                        else
                        {
                            adults++;
                        }

                    }

                  //  BilenAdam.Log.LogContext.LogMessage($" adt {adults}  chd {childs} ages {child1} {child2} {child3}");
                    var fees = db.GetRoomFees(bookingRequest.HotelId, checkIn, adults, childs, child1, child2, child3, bookingRequest.IncludeTransfer).FirstOrDefault();
                   // BilenAdam.Log.LogContext.LogMessage($" Fee : {fees.Fees}  commission : {fees.Commission}");
                    // add handling or transfer fees
                    charterRoomEntity.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                    {
                        AmountType = 1, //(int)BilenAdam.Reservation.AmountType.Fix,
                        Amount = fees.Fees ?? 0,
                        ApplyOrder = 1,
                        Code = bookingRequest.IncludeTransfer ? "TransferFee" : "HandlingFee", //ServiceFeeTypes.TransferFee.ToString() : ServiceFeeTypes.HandlingFee.ToString()
                    });
                    // add commission
                    charterRoomEntity.HotelCharterRoomServiceFees.Add(new HotelCharterRoomServiceFee
                    {
                        AmountType = 2,//(int)BilenAdam.Reservation.AmountType.Percent,
                        Amount = fees.Commission,
                        ApplyOrder = 2,
                        Code = "Comission" //ServiceFeeTypes.Comission.ToString()
                    });


                    var dateProducts = db.GetDateProducts(bookingRequest.HotelId, room.RoomTypeId, room.BoardTypeId, checkIn, checkOut, adults, childs, child1, child2, child3, bookingRequest.IncludeTransfer, DateTime.Now).ToArray();

                    foreach (var roomDate in dateProducts)
                    {
                        var roomDetail = new HotelCharterRoomDateDetail()
                        {
                            Date = roomDate.QuotaDate,
                            HotelFareDId = roomDate.FareDId,
                            Price = roomDate.Fare,
                            HotelCharterRoom = charterRoomEntity
                        };

                        if (roomDate.IsSPOEnabled ?? false)
                            continue;

                        int applyOrder = 1;
                        if (roomDate.EBId != null)
                        {
                            var pr = db.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.EBId);

                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.TEBId != null)
                        {
                            var pr = db.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.TEBId);
                          //  BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate} * {adults}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate * adults,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.REBId != null)
                        {
                            var pr = db.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.REBId);
                           // BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.LSId != null)
                        {
                            var pr = db.HotelPromotions.FirstOrDefault(p => p.Id == roomDate.LSId);
                           // BilenAdam.Log.LogContext.LogMessage($"{pr.GetPromotionType()} {pr.Id} discount type {pr.DiscountType}  rate {pr.DiscountRate}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = pr.DiscountType,
                                Amount = pr.DiscountRate,
                                HotelPromotionId = pr.Id
                            });

                            applyOrder++;
                        }

                        if (roomDate.IsDP)
                        {
                          //  BilenAdam.Log.LogContext.LogMessage($"day promotion {roomDate.DPId}");
                            roomDetail.HotelCharterRoomDatePromotions.Add(new HotelCharterRoomDatePromotion
                            {
                                ApplyOrder = applyOrder,
                                AmountType = 2,//(int)BilenAdam.Reservation.AmountType.Percent,
                                Amount = 1,
                                HotelPromotionId = roomDate.DPId.Value
                            });

                            applyOrder++;
                        }
                    }

                    }


                db.HotelCharterReservations.Add(charterRezEntity);
                db.SaveChanges();

                return new RetrieveActivity(Profile).Retrieve(charterRezEntity.Id);
            }
        }
    }
}