﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi
{
    public class StringParseUtils
    {
        public static DateTime ParseDate(string text) => DateTime.ParseExact(text, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

        public static DateTime? TryParseDate(string text)
        {
            try
            {
                return ParseDate(text);
            }
            catch
            {
                return null;
            }
        }

        public static int[] ParseIntArray(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return new int[0];
            try
            {
                return text.Split(',').Select(i => int.Parse(i)).ToArray();
            }
            catch
            {
                return new int[0];
            }
        }
    }
}