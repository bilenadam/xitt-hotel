﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using BilenAdam.EF.Hotel.Charter;
using System.Data.Entity;
using BilenAdam.XITT.WebApi.Models.Hotel.Search;
using System.Text;

namespace BilenAdam.XITT.WebApi.Workflow.HotelMultiAvailability
{
    public class Activity
    {
        public async Task<AvailabilitySearchResult> Search(string apiKey, string destination, DateTime checkIn, DateTime checkOut, int adtCount1, int chdCount1,int[] chdAges1, int adtCount2, int chdCount2, int[] chdAges2, int adtCount3, int chdCount3, int[] chdAges3)
        {
            int? destinationId = null;int destid;
            if (int.TryParse(destination, out  destid))
                destinationId = destid;

            string countryCode = destinationId == null && destination.Length == 2 ? destination : null;
            string iataCode = destinationId == null && destination.Length == 3 ? destination : null;



            Dictionary<int, Offer> offers = new Dictionary<int, Offer>();
            var roomCount = adtCount3 > 0 ? 3 : (adtCount2 > 0 ? 2 : 1);
            var deepLinkUtil = new HotelDeepLinkUtil();
            using (var db = new HotelCharterDBEntities())
            {
                for (int roomIndex = 0; roomIndex < roomCount; roomIndex++)
                {
                    bool firstRoom = roomIndex == 0;
                    HotelChildAgeUtil ageUtil = null;
                    if (roomIndex == 0)
                        ageUtil = new HotelChildAgeUtil(chdCount1, chdAges1);
                    else if (roomIndex == 1)
                        ageUtil = new HotelChildAgeUtil(chdCount2, chdAges2);
                    else
                        ageUtil = new HotelChildAgeUtil(chdCount3, chdAges3);

                    byte adults = (byte)adtCount1;
                    byte childs = (byte)chdCount1;
                    if (roomIndex == 1)
                    {
                        adults = (byte)adtCount2;
                        childs = (byte)chdCount2;
                    }
                    else if (roomIndex == 2)
                    {
                        adults = (byte)adtCount3;
                        childs = (byte)chdCount3;
                    }

                    byte? child1 = ageUtil.FirstChildAge;
                    byte? child2 = ageUtil.SecondChildAge;
                    byte? child3 = ageUtil.ThirdChildAge;

                    var data = await db.GetHotelMultiAvailablity(countryCode, iataCode, destinationId, null, null, null, null, checkIn, checkOut, adults, childs, child1, child2, child3, true,DateTime.Now).ToListAsync();
                    foreach (var item in data)
                    {
                        if (firstRoom)
                        {
                            Offer offer = new Offer { Currency = "EUR", HotelId = item.HotelId, Rooms = new Room[roomCount], Price = item.Price.ToApiMoneyString(), RoomCount = 1 };
                            offer.Rooms[roomIndex] = new Room { RoomRef = roomIndex + 1, RoomType = item.RoomType,RoomTypeId = item.RoomId, BoardType = item.BoardType, BoardTypeId = item.PensionId, Price = item.Price.ToApiMoneyString() };
                            offer.DeepLink = deepLinkUtil.Create(apiKey, item.HotelId, checkIn, checkOut, item.RoomId, item.PensionId, adults, childs, child1, child2, child3);
                            offers.Add(item.HotelId, offer);
                        }
                        else
                        {
                            if (offers.ContainsKey(item.HotelId))
                            {
                                var offer = offers[item.HotelId];
                                offer.RoomCount++;
                                offer.Price = (parsePrice(offer.Price) + item.Price).ToApiMoneyString();
                                offer.Rooms[roomIndex] = new Room { RoomRef = roomIndex + 1, RoomType = item.RoomType, RoomTypeId = item.RoomId, BoardType = item.BoardType, BoardTypeId = item.PensionId, Price = item.Price.ToApiMoneyString() };
                                offer.DeepLink = deepLinkUtil.Append(offer.DeepLink, item.RoomId, item.PensionId, adults, childs, child1, child2, child3);
                            }
                        }
                    }

                }
            }

            AvailabilitySearchResult result = new AvailabilitySearchResult
            {
                Offers = offers.Values.Where(o => o.RoomCount == roomCount).OrderBy(o => parsePrice(o.Price)).ToArray()
            };
            result.Count = result.Offers.Length;

            return result;
        }

       

        private decimal parsePrice(string price)
        {
            return decimal.Parse(price, new System.Globalization.CultureInfo("en"));
        }
      
    }
}