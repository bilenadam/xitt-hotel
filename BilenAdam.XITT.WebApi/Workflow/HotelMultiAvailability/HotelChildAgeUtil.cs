﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Workflow.HotelMultiAvailability
{
    public class HotelChildAgeUtil
    {
        public HotelChildAgeUtil(int chdCount, int[] chdAges)
        {
            ChildCount = chdCount;
            
            ChildAges = chdAges;
            if (chdAges.Length > 0)
                ChildAges = chdAges.OrderByDescending(i => i).ToArray();
        }

        
        public int ChildCount { get; }
        public int[] ChildAges { get; }

        public byte? GetAge(int childIndex) => (ChildCount > childIndex ? (byte)ChildAges[childIndex] : (byte?)null);
        public byte? FirstChildAge => GetAge(0);
        public byte? SecondChildAge => GetAge(1);
        public byte? ThirdChildAge => GetAge(2);
    }
}