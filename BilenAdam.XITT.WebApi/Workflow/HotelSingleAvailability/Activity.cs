﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using BilenAdam.EF.Hotel.Charter;
using System.Data.Entity;
using BilenAdam.XITT.WebApi.Models.Hotel.Search;

namespace BilenAdam.XITT.WebApi.Workflow.HotelSingleAvailability
{
    public class Activity
    {
        public async Task<AvailabilitySearchResult> Search(string apiKey, int hotelId, DateTime checkIn, DateTime checkOut, int adtCount1, int chdCount1, int[] chdAges1, int adtCount2, int chdCount2, int[] chdAges2, int adtCount3, int chdCount3, int[] chdAges3)
        {


            var roomCount = adtCount3 > 0 ? 3 : (adtCount2 > 0 ? 2 : 1);
            var deepLinkUtil = new HotelDeepLinkUtil();
            List<Offer> offers = new List<Offer>();

            using (var db = new HotelCharterDBEntities())
            {

                var ageUtil = new HotelMultiAvailability.HotelChildAgeUtil(chdCount1, chdAges1);



                var data1 = await db.GetHotelSingleAvailability(hotelId, null, null, checkIn, checkOut, (byte)adtCount1, (byte)chdCount1, ageUtil.FirstChildAge, ageUtil.SecondChildAge, ageUtil.ThirdChildAge, true,DateTime.Now).ToListAsync();

                if (roomCount == 1)
                {

                    foreach (var item in data1.OrderBy(i => i.Price))
                    {
                        Offer offer = new Offer
                        {
                            Currency = "EUR",
                            HotelId = item.HotelId,
                            Price = item.Price.ToApiMoneyString(),
                            RoomCount = 1,
                            Rooms = new[] { new Room { RoomRef = 1, RoomType = item.RoomType, RoomTypeId = item.RoomId, BoardType = item.BoardType, BoardTypeId = item.PensionId, Price = item.Price.ToApiMoneyString() } },
                            DeepLink = deepLinkUtil.Create(apiKey, item.HotelId, checkIn, checkOut, item.RoomId, item.PensionId, adtCount1, chdCount1, ageUtil.FirstChildAge, ageUtil.SecondChildAge, ageUtil.ThirdChildAge)
                        };
                        offers.Add(offer);

                    }
                }
                else
                {
                    var ageUtil2 = new HotelMultiAvailability.HotelChildAgeUtil(chdCount2, chdAges2);
                    var data2 = await db.GetHotelSingleAvailability(hotelId, null, null, checkIn, checkOut, (byte)adtCount2, (byte)chdCount2, ageUtil2.FirstChildAge, ageUtil2.SecondChildAge, ageUtil2.ThirdChildAge, true,DateTime.Now).ToListAsync();
                    if(roomCount == 2)
                    {
                        foreach (var item1 in data1.OrderBy(i => i.Price))
                        {
                            foreach (var item2 in data2.OrderBy(i => i.Price))
                            {
                                Offer offer = new Offer
                                {
                                    Currency = "EUR",
                                    HotelId = item1.HotelId,
                                    Price = (item1.Price+item2.Price).ToApiMoneyString(),
                                    RoomCount = 2,
                                    Rooms = new[] {
                                        new Room { RoomRef = 1, RoomType = item1.RoomType, RoomTypeId = item1.RoomId ,BoardType = item1.BoardType, BoardTypeId = item1.PensionId ,Price = item1.Price.ToApiMoneyString() },
                                        new Room { RoomRef = 2, RoomType = item2.RoomType, RoomTypeId = item2.RoomId ,BoardType = item2.BoardType, BoardTypeId = item2.PensionId ,Price = item2.Price.ToApiMoneyString() }
                                    },
                                    DeepLink =deepLinkUtil.Append(                                    
                                    deepLinkUtil.Create(apiKey, item1.HotelId, checkIn, checkOut, item1.RoomId, item1.PensionId, adtCount1, chdCount1, ageUtil.FirstChildAge, ageUtil.SecondChildAge, ageUtil.ThirdChildAge),
                                    item2.RoomId,item2.PensionId, adtCount2, chdCount2, ageUtil2.FirstChildAge, ageUtil2.SecondChildAge, ageUtil2.ThirdChildAge)
                                };
                                offers.Add(offer);

                            }
                        }
                    }
                    else
                    {
                        var ageUtil3 = new HotelMultiAvailability.HotelChildAgeUtil(chdCount3, chdAges3);
                        var data3 = await db.GetHotelSingleAvailability(hotelId, null, null, checkIn, checkOut, (byte)adtCount3, (byte)chdCount3, ageUtil3.FirstChildAge, ageUtil3.SecondChildAge, ageUtil3.ThirdChildAge, true,DateTime.Now).ToListAsync();

                        foreach (var item1 in data1)
                        {
                            foreach (var item2 in data2)
                            {
                                foreach (var item3 in data3)
                                {


                                    Offer offer = new Offer
                                    {
                                        Currency = "EUR",
                                        HotelId = item1.HotelId,
                                        Price = (item1.Price + item2.Price + item3.Price).ToApiMoneyString(),
                                        RoomCount = 3,
                                        Rooms = new[] {
                                        new Room { RoomRef = 1, RoomType = item1.RoomType, RoomTypeId = item1.RoomId , BoardType = item1.BoardType, BoardTypeId = item1.PensionId ,Price = item1.Price.ToApiMoneyString() },
                                        new Room { RoomRef = 2, RoomType = item2.RoomType, RoomTypeId = item2.RoomId , BoardType = item2.BoardType, BoardTypeId = item2.PensionId ,Price = item2.Price.ToApiMoneyString() },
                                         new Room { RoomRef = 3, RoomType = item3.RoomType, RoomTypeId = item3.RoomId, BoardType = item3.BoardType, BoardTypeId = item3.PensionId ,Price = item3.Price.ToApiMoneyString() }
                                    },
                                        DeepLink = deepLinkUtil.Append(
                                            deepLinkUtil.Append(
                                            deepLinkUtil.Create(apiKey, item1.HotelId, checkIn, checkOut, item1.RoomId, item1.PensionId, adtCount1, chdCount1, ageUtil.FirstChildAge, ageUtil.SecondChildAge, ageUtil.ThirdChildAge),
                                            item2.RoomId, item2.PensionId, adtCount2, chdCount2, ageUtil2.FirstChildAge, ageUtil2.SecondChildAge, ageUtil2.ThirdChildAge),
                                            item3.RoomId, item3.PensionId, adtCount3, chdCount3, ageUtil3.FirstChildAge, ageUtil3.SecondChildAge, ageUtil3.ThirdChildAge)
                                    };
                                    offers.Add(offer);
                                }

                            }
                        }
                    }

                }


            }

            AvailabilitySearchResult result = new AvailabilitySearchResult
            {
                Offers = offers.ToArray()
            };
            result.Count = result.Offers.Length;

            return result;
        }


    }
}