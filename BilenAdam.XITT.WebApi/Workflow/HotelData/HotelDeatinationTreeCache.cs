﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using BilenAdam.EF.Hotel.Charter;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BilenAdam.XITT.WebApi.Workflow.HotelData
{
    public class HotelDeatinationTreeCache
    {
        static private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();
        static private Models.Hotel.MetaData.HotelDestinationTree model =new Models.Hotel.MetaData.HotelDestinationTree();
        static private DateTime expires = DateTime.MinValue;

        public static Models.Hotel.MetaData.HotelDestinationTree Get()
        {
            cacheLock.EnterReadLock();
            try
            {
                if (expires > DateTime.Now)
                    return model;
                else
                    return null;
            }
            finally
            {
                cacheLock.ExitReadLock();
            }
                
        }

        public static void Set(Models.Hotel.MetaData.HotelDestinationTree m)
        {
            cacheLock.EnterWriteLock();
            try
            {
                model = m;
                expires = DateTime.Now.AddHours(1);
            }
            finally
            {
                cacheLock.ExitWriteLock();
            }
        }

        
    }
}