﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BilenAdam.EF.Hotel.Charter;
using System.Threading.Tasks;
using System.Data.Entity;

namespace BilenAdam.XITT.WebApi.Workflow.HotelData
{
    public class BoardTypeCacheUtil
    {
        public async Task<string[]> Get()
        {
            using (var db = new HotelCharterDBEntities())
            {
                return await db.HotelPensions.OrderBy(p=>p.Name).Select(p => p.Name).Distinct().ToArrayAsync();
            }
        }
    }
}