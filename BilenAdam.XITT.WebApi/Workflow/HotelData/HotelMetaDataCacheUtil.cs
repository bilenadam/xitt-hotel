﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BilenAdam.XITT.WebApi.Models.Hotel.MetaData;
using BilenAdam.XITT.WebApi.Workflow.Caching;
using BilenAdam.EF.Hotel.Charter;
using System.Threading.Tasks;
using System.Data.Entity;
namespace BilenAdam.XITT.WebApi.Workflow.HotelData
{
    public class HotelMetaDataCacheUtil
    {
        static TimeCache<int, HotelMetaData> cache = new TimeCache<int, HotelMetaData>();
        public async Task<HotelMetaData> Get(int id)
        {
            var item = cache.Get(id);
            if (item != null && item.Expires > DateTime.UtcNow)
                return item.Value;

            using (var db = new HotelCharterDBEntities())
            {
                var data = await db.Hotels.Include(h => h.HotelImages).Include(h=>h.HotelDestination).FirstOrDefaultAsync(h => h.Id == id);
                if (data != null)
                {
                    item = new Item<HotelMetaData>()
                    {
                        Expires = DateTime.Now.AddMinutes(30),
                        Value = new HotelMetaData
                        {
                            ID = data.Id,
                            GIATAID = data.GIATA,
                            Name = data.Name,
                            Stars = data.Stars,
                            Destination = new Models.Hotel.MetaData.HotelDestination
                            {
                                ID = data.HotelDestionationId,
                                Name = data.HotelDestination.Name,
                                IataCode = data.HotelDestination.IataCityCode,
                                CountryCode = data.HotelDestination.CountryCode,
                                ParentID = data.HotelDestination.ParentId
                            },
                            Address = new HotelAddress {
                                Street = data.Address,
                                ZipCode = data.ZipCode,
                                City = data.City,
                                CountryCode = data.CountryCode
                            },
                            Latitude = data.Latitude,
                            Longitude = data.Longitude,
                            Web = data.Web,
                            Phone = data.Tel,
                            Email = data.EMail,
                            HotelDescription = data.Description,
                            LocationDescription = data.LocationDescription,
                            RoomDescription = data.RoomDescription,
                            Images = data.HotelImages.OrderBy(i=>i.OrderBy).Select(i => string.Format("https://cdn.xitt.de/hotel/{0}/images/{1}", i.HotelId, i.FileName)).ToArray()
                        }

                    };

                }
                else
                {
                    item = new Item<HotelMetaData>() { Expires = DateTime.Now.AddMinutes(30), Value = null };
                }

                cache.Set(id, item);
            }


            return item.Value;
        }

      

        public async Task<List<HotelMetaData>> Get()
        {
            

            using (var db = new HotelCharterDBEntities())
            {
                List<HotelMetaData> items = new List<HotelMetaData>();
                var list = await db.Hotels.Where(h=>h.IsActive).Include(h => h.HotelImages).Include(h => h.HotelDestination).OrderBy(h=>h.Id).ToListAsync();
                foreach(var data in list)
                {
                    var item = new HotelMetaData
                    {
                        ID = data.Id,
                        GIATAID = data.GIATA,
                        Name = data.Name,
                        Stars = data.Stars,
                        Destination = new Models.Hotel.MetaData.HotelDestination
                        {
                            ID = data.HotelDestionationId,
                            Name = data.HotelDestination.Name,
                            IataCode = data.HotelDestination.IataCityCode,
                            CountryCode = data.HotelDestination.CountryCode,
                            ParentID = data.HotelDestination.ParentId
                        },
                        Address = new HotelAddress
                        {
                            Street = data.Address,
                            ZipCode = data.ZipCode,
                            City = data.City,
                            CountryCode = data.CountryCode
                        },
                        Latitude = data.Latitude,
                        Longitude = data.Longitude,
                        Web = data.Web,
                        Phone = data.Tel,
                        Email = data.EMail,
                        HotelDescription = data.Description,
                        LocationDescription = data.LocationDescription,
                        RoomDescription = data.RoomDescription,
                        Images = data.HotelImages.OrderBy(i => i.OrderBy).Select(i => string.Format("https://cdn.xitt.de/hotel/{0}/images/{1}", i.HotelId, i.FileName)).ToArray()
                    };

                    items.Add(item);
                    cache.Set(item.ID, new Item<HotelMetaData> { Expires = DateTime.Now.AddMinutes(30), Value = item });

                }

                return items;
            }


        }
    }
}