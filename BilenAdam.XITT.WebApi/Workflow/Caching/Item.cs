﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi.Workflow.Caching
{
    public class Item<V>
    {
        public DateTime Expires { get; set; }
        public V Value { get; set; }
    }
}