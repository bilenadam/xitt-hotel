﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace BilenAdam.XITT.WebApi.Workflow.Caching
{
    public class TimeCache<K, V>
    {
        private Dictionary<K, Item<V>> items = new Dictionary<K, Item<V>>();
        private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();

       

     

       

   
        public Item<V> Get(K key)
        {
            cacheLock.EnterReadLock();
            try
            {
                Item<V> item = null;
                if (items.TryGetValue(key, out item))
                    return item;

                return null;
            }
            finally
            {
                cacheLock.ExitReadLock();
            }
        }

        public void Set(K key,Item<V> item)
        {
            cacheLock.EnterWriteLock();
            try
            {
                items.Remove(key);
                items.Add(key, item);
            }
            finally
            {
                cacheLock.ExitWriteLock();
            }

        }
    }
}