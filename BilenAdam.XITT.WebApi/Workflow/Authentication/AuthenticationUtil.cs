﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Threading.Tasks;
using BilenAdam.EF.xITT.Data;

namespace BilenAdam.XITT.WebApi.Workflow.Authentication
{
    public class AuthenticationUtil
    {
        public async Task<WSProfile> Auth(string apiKey)
        {
            using (var db = new xITTDBEntities())
            {
                return await db.WSProfiles.FirstOrDefaultAsync(i => i.ID == apiKey);
            }

        }
    }
}