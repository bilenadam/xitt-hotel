﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using BilenAdam.XITT.WebApi.Workflow.Caching;
using BilenAdam.EF.xITT.Data;

namespace BilenAdam.XITT.WebApi.Workflow.Authentication
{
    public class AuthenticationCacheUtil
    {
        static TimeCache<string, WSProfile> cache = new TimeCache<string, WSProfile>();
        
        public async Task<WSProfile> Authenticate(string apikey)
        {
            var item = cache.Get(apikey);
            if (item != null && item.Expires > DateTime.UtcNow)
                return item.Value;

            var profile = await new AuthenticationUtil().Auth(apikey);
            item = new Item<WSProfile>() { Expires = DateTime.Now.AddMinutes(30), Value = profile };
            cache.Set(apikey, item);
            return item.Value;
        }

    }
}