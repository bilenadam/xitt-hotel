﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace BilenAdam.XITT.WebApi.Workflow
{
    public class HotelDeepLinkUtil
    {
        public string Create(string apiKey, int hotelid, DateTime checkin, DateTime checkout, int roomtype, int boardtype, int adt, int chd, byte? age1, byte? age2, byte? age3)
        {
            //https://xitt.de/referring/Refer?refid=itt&data=hotel-id-290-check-in-2019-05-10-check-out-2019-05-13-room-type-1043-board-2-adt-1-chd-1-age-3-room-type-1043-board-2-adt-1-chd-2-age-5-age-2
            var sb = new StringBuilder()
              .Append("https://xitt.de/referring/Refer?refid=")
              .Append(apiKey)
              .Append("&data=")
              .Append("hotel-id")
              .Append("-").Append(hotelid)
              .Append("-check-in")
              .Append("-").Append(checkin.ToApiDateString())
              .Append("-check-out")
              .Append("-").Append(checkout.ToApiDateString())
              .Append("-room-type")
              .Append("-").Append(roomtype)
              .Append("-board")
              .Append("-").Append(boardtype)
              .Append("-adt")
              .Append("-").Append(adt)
              .Append("-chd")
              .Append("-").Append(chd);



            if (age1 != null)
                sb = sb.Append("-age-").Append(age1.Value);
            if (age2 != null)
                sb = sb.Append("-age-").Append(age2.Value);
            if (age3 != null)
                sb = sb.Append("-age-").Append(age3.Value);

            return sb.ToString();
        }

        public string Append(string deepLink, int roomtype, int boardtype, int adt, int chd, byte? age1, byte? age2, byte? age3)
        {
            var sb = new StringBuilder(deepLink).Append("-room-type")
              .Append("-").Append(roomtype)
              .Append("-board")
              .Append("-").Append(boardtype)
              .Append("-adt")
              .Append("-").Append(adt)
              .Append("-chd")
              .Append("-").Append(chd);


            if (age1 != null)
                sb = sb.Append("-age-").Append(age1.Value);
            if (age2 != null)
                sb = sb.Append("-age-").Append(age2.Value);
            if (age3 != null)
                sb = sb.Append("-age-").Append(age3.Value);

            return sb.ToString();
        }

       
    }
}