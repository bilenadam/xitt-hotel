﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BilenAdam.XITT.WebApi
{
    public static class DataStringHelpers
    {
        public static string ToApiMoneyString(this decimal amt)
        {
            return amt.ToString("F2", new System.Globalization.CultureInfo("en"));
        }
        public static string ToApiDateString(this DateTime date)
        {
            return date.ToString("yyyy-MM-dd");
        }

        public static string ToApiDateTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm");
        }

    }
}