//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BilenAdam.EF.Hotel.Charter
{
    using System;
    using System.Collections.Generic;
    
    public partial class HotelAmenity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HotelAmenity()
        {
            this.HotelAmenityPairs = new HashSet<HotelAmenityPair>();
            this.HotelRoomAmenityPairs = new HashSet<HotelRoomAmenityPair>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool isProperty { get; set; }
        public string xRes { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HotelAmenityPair> HotelAmenityPairs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HotelRoomAmenityPair> HotelRoomAmenityPairs { get; set; }
    }
}
