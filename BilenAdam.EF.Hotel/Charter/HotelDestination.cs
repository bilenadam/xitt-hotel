//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BilenAdam.EF.Hotel.Charter
{
    using System;
    using System.Collections.Generic;
    
    public partial class HotelDestination
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HotelDestination()
        {
            this.HotelDestinations1 = new HashSet<HotelDestination>();
            this.Hotels = new HashSet<Hotel>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public string CountryCode { get; set; }
        public string State { get; set; }
        public string IataCityCode { get; set; }
        public Nullable<int> ParentId { get; set; }
        public string xRes { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HotelDestination> HotelDestinations1 { get; set; }
        public virtual HotelDestination HotelDestination1 { get; set; }
        public virtual HotelDestinationShowcas HotelDestinationShowcas { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Hotel> Hotels { get; set; }
    }
}
