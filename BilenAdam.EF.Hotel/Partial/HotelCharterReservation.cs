﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.EF.Hotel.Charter
{
    public partial class HotelCharterReservation
    {
        public decimal GetTotalRate()
        {
            decimal total = 0M;
            foreach (var room in GetRooms())
            {
                total += room.GetTotalRate();
            }
            return total;

        }

        public IEnumerable<HotelCharterRoom> GetRooms()
        {
            var orderedRoomList = this.HotelCharterRooms.OrderBy(r => r.RoomRef).ThenByDescending(r => r.Id).ToArray();
            int lastRoomRef = -1;
            foreach (var room in orderedRoomList)
            {
                if (room.RoomRef != lastRoomRef)
                {
                    lastRoomRef = room.RoomRef;
                    yield return room;

                }
            }
        }
    }
}
