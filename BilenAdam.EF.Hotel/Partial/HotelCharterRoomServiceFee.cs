﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.EF.Hotel.Charter
{
    public partial class HotelCharterRoomServiceFee
    {
        public decimal ApplyTo(decimal total)
        {
            if (AmountType == 1) // fixed
            {
                total += Amount;
            }
            else
            {

                if (Amount >= 0 & Amount < 1)
                    total = total / (1 - Amount);
                else
                    total += (total * Amount);
            }

            return total;
        }
    }
}
