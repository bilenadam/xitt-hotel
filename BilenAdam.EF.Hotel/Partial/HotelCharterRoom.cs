﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.EF.Hotel.Charter
{
    public partial class HotelCharterRoom
    {
        public decimal GetTotalRate()
        {
            decimal total = this.HotelCharterRoomDateDetails.Sum(d => d.GetRate());

            foreach (var f in this.HotelCharterRoomServiceFees.OrderBy(s => s.ApplyOrder))
            {
                total = f.ApplyTo(total);
            }



            return total;
        }

    }
}
