﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilenAdam.EF.Hotel.Charter
{
    public partial class HotelCharterRoomDateDetail
    {
        public decimal GetRate()
        {
            var rate = this.Price;

            foreach (var p in this.HotelCharterRoomDatePromotions.OrderBy(i => i.ApplyOrder))
            {
                if (p.AmountType == 1)
                    rate -= p.Amount;
                else
                    rate = rate * (1 - p.Amount);
            }

            return rate;
        }
    }
}
